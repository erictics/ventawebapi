﻿using AppWCFService.Entity;
using AppWCFService.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace AppWCFService.Controllers
{
    public class sucursalController
    {
        public List<sucursalEntity> sucursal_obtenLista(long usuarioId)
        {
            List<sucursalEntity> result = new List<sucursalEntity>();
            dbHelper db = new dbHelper();
            SqlDataReader reader = null;

            try
            {
                db.agregarParametro("@usuarioId", SqlDbType.BigInt, usuarioId);
                reader = db.consultaReader("sucursal_obtenLista");
                result = db.MapDataToEntityCollection<sucursalEntity>(reader).ToList();
                db.conexion.Close();
                db.conexion.Dispose();
                if (reader != null)
                {
                    reader.Close();
                }
            }
            catch {
                db.conexion.Close();
                db.conexion.Dispose();
                if (reader != null)
                {
                    reader.Close();
                }

            }
           
            return result;
        }

        public sucursalEntity sucursal_obten(long? id)
        {
            sucursalEntity result = new sucursalEntity();
            dbHelper db = new dbHelper();
            SqlDataReader reader = null;
            try
            {
                db.agregarParametro("@id", SqlDbType.BigInt, id);
                reader = db.consultaReader("sucursal_obten");
                result = db.MapDataToEntityCollection<sucursalEntity>(reader).ToList()[0];
                db.conexion.Close();
                db.conexion.Dispose();
                if (reader != null)
                {
                    reader.Close();
                }
                
                
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
                if (reader != null)
                {
                    reader.Close();
                }
            }
            
            return result;
        }


        public long? sucursal_nuevo(sucursalEntity sucursal)
        {
            List<sucursalEntity> result = new List<sucursalEntity>();
            dbHelper db = new dbHelper();
            long id = 0;
            try
            {
                db.agregarParametro("@nombre", SqlDbType.VarChar, sucursal.nombre);
                db.agregarParametro("@serie", SqlDbType.VarChar, sucursal.serie);
                db.agregarParametro("@calle", SqlDbType.VarChar, sucursal.calle);
                db.agregarParametro("@numero", SqlDbType.Int, sucursal.numero);
                db.agregarParametro("@colonia", SqlDbType.VarChar, sucursal.colonia);
                db.agregarParametro("@cp", SqlDbType.Int, sucursal.cp);
                db.agregarParametro("@ciudad", SqlDbType.VarChar, sucursal.ciudad);
                db.agregarParametro("@estado", SqlDbType.VarChar, sucursal.estado);
                db.agregarParametro("@telefono", SqlDbType.VarChar, sucursal.telefono);
                db.agregarParametro("@email", SqlDbType.VarChar, sucursal.email);
                db.agregarParametro("@id", SqlDbType.BigInt, ParameterDirection.Output);
                db.consultaOutput("sucursal_nuevo");
                id = long.Parse(db.diccionarioOutput["@id"].ToString());
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch {
                db.conexion.Close();
                db.conexion.Dispose();

            }
            
            
            return id;
        }

        public long? sucursal_edita(sucursalEntity sucursal)
        {
            List<sucursalEntity> result = new List<sucursalEntity>();
            dbHelper db = new dbHelper();

            try
            {
                db.agregarParametro("@nombre", SqlDbType.VarChar, sucursal.nombre);
                db.agregarParametro("@serie", SqlDbType.VarChar, sucursal.serie);
                db.agregarParametro("@calle", SqlDbType.VarChar, sucursal.calle);
                db.agregarParametro("@numero", SqlDbType.Int, sucursal.numero);
                db.agregarParametro("@colonia", SqlDbType.VarChar, sucursal.colonia);
                db.agregarParametro("@cp", SqlDbType.Int, sucursal.cp);
                db.agregarParametro("@ciudad", SqlDbType.VarChar, sucursal.ciudad);
                db.agregarParametro("@estado", SqlDbType.VarChar, sucursal.estado);
                db.agregarParametro("@telefono", SqlDbType.VarChar, sucursal.telefono);
                db.agregarParametro("@email", SqlDbType.VarChar, sucursal.email);
                db.agregarParametro("@id", SqlDbType.BigInt, sucursal.id);
                db.consultaSinRetorno("sucursal_edita");

                db.conexion.Close();
                db.conexion.Dispose();

            }
            catch
                {
                db.conexion.Close();
                db.conexion.Dispose();

            }
            
            
            return sucursal.id;
        }

    }
}