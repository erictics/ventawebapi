﻿using AppWCFService.Entity;
using AppWCFService.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace AppWCFService.Controllers
{
    public class departamentoController
    {

        public List<departamentoEntity> departamento_obtenLista()
        {
            List<departamentoEntity> result = new List<departamentoEntity>();
            dbHelper db = new dbHelper();
            SqlDataReader reader = null;
            try
            {
                 reader = db.consultaReader("departamento_obtenLista");
                result = db.MapDataToEntityCollection<departamentoEntity>(reader).ToList();
                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch(Exception ex)
            {
                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }
           
            return result;
        }

        public departamentoEntity departamento_obten(long? id)
        {
            departamentoEntity result = new departamentoEntity();
            dbHelper db = new dbHelper();
            SqlDataReader reader = null;
            try
            {
                db.agregarParametro("@id", SqlDbType.BigInt, id);
                 reader = db.consultaReader("departamento_obten");
                result = db.MapDataToEntityCollection<departamentoEntity>(reader).ToList()[0];
                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch (Exception ex) {
                result = null;
                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }
            
           
            return result;
        }

        public long? departamento_nuevo(departamentoEntity departamento)
        {
            List<departamentoEntity> result = new List<departamentoEntity>();
            dbHelper db = new dbHelper();
            long id = 0;
            try
            {

                db.agregarParametro("@nombre", SqlDbType.VarChar, departamento.nombre);
                db.agregarParametro("@descripcion", SqlDbType.VarChar, departamento.descripcion);
                db.agregarParametro("@activo", SqlDbType.Bit, departamento.activo);
                db.agregarParametro("@id", SqlDbType.BigInt, ParameterDirection.Output);
                db.consultaOutput("departamento_nuevo");
                 id = long.Parse(db.diccionarioOutput["@id"].ToString());
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }

            return id;
        }

        public long? departamento_edita(departamentoEntity departamento)
        {

            List<departamentoEntity> result = new List<departamentoEntity>();
            dbHelper db = new dbHelper();

            try
            {
                db.agregarParametro("@nombre", SqlDbType.VarChar, departamento.nombre);
                db.agregarParametro("@descripcion", SqlDbType.VarChar, departamento.descripcion);
                db.agregarParametro("@activo", SqlDbType.Bit, departamento.activo);
                db.agregarParametro("@id", SqlDbType.BigInt, departamento.id);
                db.consultaSinRetorno("departamento_edita");
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }
          
            return departamento.id;
        }

    }
}