﻿using AppWCFService.Entity;
using AppWCFService.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace AppWCFService.Controllers
{
    public class inventarioController
    {

        public List<inventarioEntity> inventario_obtenLista()
        {

            List<inventarioEntity> result = new List<inventarioEntity>();
            SqlDataReader reader = null;
            dbHelper db = new dbHelper();
            try
            {
                 reader = db.consultaReader("inventario_obtenLista");
                result = db.MapDataToEntityCollection<inventarioEntity>(reader).ToList();
                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();
               
            }
            catch(Exception ex)
            {
                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();

            }

            return result;

        }

        public long? inventario_nuevo(inventarioEntity inventario)
        {
            List<almacenEntity> result = new List<almacenEntity>();
            dbHelper db = new dbHelper();
            long id = 0;
            try
            {
                db.agregarParametro("@articuloId", SqlDbType.BigInt, inventario.articuloId);
                db.agregarParametro("@almacenId", SqlDbType.BigInt, inventario.almacenId);
                db.agregarParametro("@cantidad", SqlDbType.Int, inventario.cantidad);
                db.agregarParametro("@estatusId", SqlDbType.BigInt, inventario.estatusId);
                db.agregarParametro("@id", SqlDbType.BigInt, ParameterDirection.Output);
                db.consultaOutput("inventario_nuevo");

                id = long.Parse(db.diccionarioOutput["@id"].ToString());
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch
            {
               
                db.conexion.Close();
                db.conexion.Dispose();
            }
           
            
            return id;
        }

        public long ? inventario_aumentaStock(inventarioEntity inventario)
        {
            //dbHelper db = new dbHelper();
            //db.agregarParametro("@articuloId", SqlDbType.BigInt, inventario.articuloId);
            //db.agregarParametro("@almacenId", SqlDbType.BigInt, inventario.almacenId);
            //db.agregarParametro("@cantidad", SqlDbType.Int, inventario.cantidad);
            //db.agregarParametro("@estatusId", SqlDbType.BigInt, inventario.estatusId);
            //db.agregarParametro("@id", SqlDbType.BigInt, inventario.id);
            //db.consultaSinRetorno("inventario_aumentaStock");        
            //db.conexion.Close();
            //db.conexion.Dispose();
            return 0;
        }

        public long? inventario_reduceStock(inventarioEntity inventario)
        {
            //dbHelper db = new dbHelper();
            //db.agregarParametro("@articuloId", SqlDbType.BigInt, inventario.articuloId);
            //db.agregarParametro("@almacenId", SqlDbType.BigInt, inventario.almacenId);
            //db.agregarParametro("@cantidad", SqlDbType.Int, inventario.cantidad);
            //db.agregarParametro("@estatusId", SqlDbType.BigInt, inventario.estatusId);
            //db.agregarParametro("@id", SqlDbType.BigInt, inventario.id);
            //db.consultaSinRetorno("inventario_reduceStock");
            //db.conexion.Close();
            //db.conexion.Dispose();
            return 0;
        }


        public decimal GetInventarioArticulo(int ? sucursalId,int ? articuloId,int ? estatusId)
        {
            dbHelper db = new dbHelper();
            long cantidad = 0;
            try
            {
                db.agregarParametro("@sucursalId", SqlDbType.BigInt, sucursalId);
                db.agregarParametro("@articuloId", SqlDbType.BigInt, articuloId);
                db.agregarParametro("@estatusId", SqlDbType.Int, estatusId);
                db.agregarParametro("@cantidad", SqlDbType.Decimal, ParameterDirection.Output);
                db.consultaOutput("inventario_obtenPorArticulo");

                cantidad = long.Parse(db.diccionarioOutput["@cantidad"].ToString());
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch {
                db.conexion.Close();
                db.conexion.Dispose();
            }
           
            
            
            return cantidad;

        }


        public bool ? GetValidaInventarioVendedor(int? vendedorId, int? articuloId, int ? tempventaId,decimal ? cantidad)
        {
            dbHelper db = new dbHelper();
            bool? valido = false;
            try
            {
                db.agregarParametro("@vendedorId", SqlDbType.BigInt, vendedorId);
                db.agregarParametro("@articuloId", SqlDbType.BigInt, articuloId);
                db.agregarParametro("@tempventaId", SqlDbType.Int, tempventaId);
                db.agregarParametro("@cantidad", SqlDbType.Decimal, cantidad);
                db.agregarParametro("@valido", SqlDbType.Bit, ParameterDirection.Output);
                db.consultaOutput("GetValidaInventarioVendedor");

                valido = bool.Parse(db.diccionarioOutput["@valido"].ToString());
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }



            return valido;

        }

    }
}