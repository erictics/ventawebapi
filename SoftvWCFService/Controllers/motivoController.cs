﻿using AppWCFService.Entity;
using AppWCFService.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace AppWCFService.Controllers
{
    public class motivoController
    {

        public List<motivoEntity> motivo_obtenLista(string tipo)
        {
            List<motivoEntity> result = new List<motivoEntity>();
            dbHelper db = new dbHelper();
            SqlDataReader reader = null;
            try
            {
                db.agregarParametro("@tipo", SqlDbType.VarChar, tipo);
                reader = db.consultaReader("motivo_obtenLista");
                result = db.MapDataToEntityCollection<motivoEntity>(reader).ToList();

                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch(Exception ex)
            {
                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }
           
            
           
            return result;
        }


        public motivoEntity motivo_obten(long? id)
        {
            motivoEntity result = new motivoEntity();
            dbHelper db = new dbHelper();
            SqlDataReader reader = null;
            try
            {
                db.agregarParametro("@id", SqlDbType.BigInt, id);
                reader = db.consultaReader("motivo_obten");
                result = db.MapDataToEntityCollection<motivoEntity>(reader).ToList()[0];
                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch (Exception ex)
            {
                result = null;
                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }            
           
            
            return result;
        }

        public long? motivo_nuevo(motivoEntity motivo)
        {
            List<almacenEntity> result = new List<almacenEntity>();
            dbHelper db = new dbHelper();
            long id = 0;
            try
            {
                db.agregarParametro("@nombre", SqlDbType.VarChar, motivo.nombre);
                db.agregarParametro("@activo", SqlDbType.Bit, motivo.activo);
                db.agregarParametro("@tipo", SqlDbType.VarChar, motivo.tipo);
                db.agregarParametro("@id", SqlDbType.BigInt, ParameterDirection.Output);
                db.consultaOutput("motivo_nuevo");
                id = long.Parse(db.diccionarioOutput["@id"].ToString());
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }

            return id;
        }

        public long? motivo_edita(motivoEntity motivo)
        {
            List<almacenEntity> result = new List<almacenEntity>();
            dbHelper db = new dbHelper();

            try
            {
                db.agregarParametro("@nombre", SqlDbType.VarChar, motivo.nombre);
                db.agregarParametro("@activo", SqlDbType.Bit, motivo.activo);
                db.agregarParametro("@tipo", SqlDbType.VarChar, motivo.tipo);
                db.agregarParametro("@id", SqlDbType.BigInt, motivo.id);
                db.consultaSinRetorno("motivo_edita");
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch{
                db.conexion.Close();
                db.conexion.Dispose();
            }
            
            
            return 1;
        }


    }
}