﻿using AppWCFService.Entity;
using AppWCFService.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace AppWCFService.Controllers
{
    public class salidaEfectivoController
    {

        usuarioController usuarioController = new usuarioController( );

        public List<salidaEfectivoEntity> salidaEfectivo_obtenLista(long ? sucursalId ,long ? cajeroId, long ? folio,string fecha,int ? userId2)
        {
            List<salidaEfectivoEntity> result = new List<salidaEfectivoEntity>();
            dbHelper db = new dbHelper();
            SqlDataReader reader = null;
            try
            {
                db.agregarParametro("@sucursalId", SqlDbType.BigInt, sucursalId);
                db.agregarParametro("@cajeroId", SqlDbType.BigInt, cajeroId);
                db.agregarParametro("@folio", SqlDbType.BigInt, folio);
                db.agregarParametro("@fecha", SqlDbType.VarChar, fecha);
                db.agregarParametro("@userId2", SqlDbType.VarChar, userId2);
                reader = db.consultaReader("salidaEfectivo_obtenLista");

                result = db.MapDataToEntityCollection<salidaEfectivoEntity>(reader).ToList();
                result.ForEach(item => {
                    item.usuario = usuarioController.usuario_obten(item.cajeroId);
                    item.fechaCorta = item.fecha.ToShortDateString();
                    item.hora = item.fecha.ToShortTimeString();
                    item.cantidadC = item.cantidad.ToString("C");
                });
                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch(Exception ex)
            {
                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }
            
           
          
            return result;
        }

        public salidaEfectivoEntity salidaEfectivo_obten(long? id)
        {
            salidaEfectivoEntity result = new salidaEfectivoEntity();
            dbHelper db = new dbHelper();
            SqlDataReader reader = null;


            try
            {
                db.agregarParametro("@id", SqlDbType.BigInt, id);
                reader = db.consultaReader("salidaEfectivo_obten");
                result = db.MapDataToEntityCollection<salidaEfectivoEntity>(reader).ToList()[0];             
                result.usuario = usuarioController.usuario_obten(result.usuarioAutorizaId);
                result.fechaCorta = result.fecha.ToShortDateString();
                result.hora = result.fecha.ToShortTimeString();
            }
            catch (Exception ex)
            {
                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();
                result = null;
            }
            reader.Close();
            db.conexion.Close();
            db.conexion.Dispose();
            return result;
        }


        public ResultEntity salidaEfectivo_nuevo(salidaEfectivoEntity salidaEfectivo)
        {
            
            dbHelper db = new dbHelper();
            long id = 0;
            string mensaje = "";
            int ErrorCode = 0;
            ResultEntity result = new ResultEntity();
            try
            {
                db.agregarParametro("@tipoSalidaId", SqlDbType.Int, salidaEfectivo.tipoSalidaId);
                db.agregarParametro("@sucursalId", SqlDbType.Int, salidaEfectivo.sucursalId);
                db.agregarParametro("@cajeroId", SqlDbType.Int, salidaEfectivo.cajeroId);
                db.agregarParametro("@cantidad", SqlDbType.Decimal, salidaEfectivo.cantidad);
                db.agregarParametro("@observaciones", SqlDbType.VarChar, salidaEfectivo.observaciones);
                db.agregarParametro("@usuarioAutorizaId", SqlDbType.BigInt, salidaEfectivo.usuarioAutorizaId);
                db.agregarParametro("@id", SqlDbType.BigInt, ParameterDirection.Output);
                db.agregarParametro("@Message", SqlDbType.VarChar, ParameterDirection.Output, 100);
                db.agregarParametro("@ErrorCode", SqlDbType.BigInt, ParameterDirection.Output);
                db.consultaOutput("salidaEfectivo_nuevo");

                 id = long.Parse(db.diccionarioOutput["@id"].ToString());
                 mensaje = db.diccionarioOutput["@Message"].ToString();
                ErrorCode = int.Parse(db.diccionarioOutput["@ErrorCode"].ToString());

                db.conexion.Close();
                db.conexion.Dispose();

               
                result.ErrorCode = ErrorCode;
                result.Message = mensaje;
            }
            catch(Exception ex)
            {
                result.ErrorCode = 1;
                result.Message = ex.Message;
            }
            return result;
        }


        public ResultEntity salidaEfectivo_edita(salidaEfectivoEntity salidaEfectivo)
        {
            
            dbHelper db = new dbHelper();
            string mensaje = "";
            int ErrorCode =0;
            ResultEntity result = new ResultEntity();
            try
            {
                db.agregarParametro("@tipoSalidaId", SqlDbType.VarChar, salidaEfectivo.tipoSalidaId);
                db.agregarParametro("@cantidad", SqlDbType.Decimal, salidaEfectivo.cantidad);
                db.agregarParametro("@observaciones", SqlDbType.VarChar, salidaEfectivo.observaciones);
                db.agregarParametro("@id", SqlDbType.BigInt, salidaEfectivo.id);
                db.agregarParametro("@Message", SqlDbType.VarChar, ParameterDirection.Output, 100);
                db.agregarParametro("@ErrorCode", SqlDbType.BigInt, ParameterDirection.Output);
                db.consultaOutput("salidaEfectivo_edita");

                 mensaje = db.diccionarioOutput["@Message"].ToString();
                ErrorCode = int.Parse(db.diccionarioOutput["@ErrorCode"].ToString());
                db.conexion.Close();
                db.conexion.Dispose();              
                result.ErrorCode = ErrorCode;
                result.Message = mensaje;
            }
            catch
            {
                result.ErrorCode = ErrorCode;
                result.Message = mensaje;
            }
            
            
            return result;
        }

        public ResultEntity salidaEfectivo_cancela(int? id, int? usuarioId)
        {
            dbHelper db = new dbHelper();
            string mensaje = "";
            int ErrorCode = 0;
            ResultEntity result = new ResultEntity();
            try
            {
                db.agregarParametro("@id", SqlDbType.BigInt, id);
                db.agregarParametro("@usuarioId", SqlDbType.BigInt, usuarioId);
                db.agregarParametro("@Message", SqlDbType.VarChar, ParameterDirection.Output, 100);
                db.agregarParametro("@ErrorCode", SqlDbType.BigInt, ParameterDirection.Output);
                db.consultaOutput("salidaEfectivo_cancela");

                 mensaje = db.diccionarioOutput["@Message"].ToString();
                 ErrorCode = int.Parse(db.diccionarioOutput["@ErrorCode"].ToString());

                db.conexion.Close();
                db.conexion.Dispose();

               
                result.ErrorCode = ErrorCode;
                result.Message = mensaje;
            }
            catch(Exception ex)
            {
                result.ErrorCode = 1;
                result.Message = ex.Message;
            }
          

            
            return result;
        }

    }
}