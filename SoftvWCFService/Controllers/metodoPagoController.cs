﻿using AppWCFService.Entity;
using AppWCFService.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace AppWCFService.Controllers
{
    public class metodoPagoController
    {
        public List<metodoPagoEntity> metodoPago_obtenLista()
        {
            List<metodoPagoEntity> result = new List<metodoPagoEntity>();
            dbHelper db = new dbHelper();
            SqlDataReader reader = db.consultaReader("metodoPago_obtenLista");
            result = db.MapDataToEntityCollection<metodoPagoEntity>(reader).ToList();
            reader.Close();
            db.conexion.Close();
            db.conexion.Dispose();
            return result;
        }

        public metodoPagoEntity metodoPago_obten(long? id)
        {
            metodoPagoEntity result = new metodoPagoEntity();
            dbHelper db = new dbHelper();
            db.agregarParametro("@id", SqlDbType.BigInt, id);
            SqlDataReader reader = db.consultaReader("metodoPago_obten");
            try
            {
                result = db.MapDataToEntityCollection<metodoPagoEntity>(reader).ToList()[0];
            }
            catch (Exception ex)
            {
                result = null;
            }
            reader.Close();
            db.conexion.Close();
            db.conexion.Dispose();
            return result;
        }


    }

}