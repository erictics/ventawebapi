﻿using AppWCFService.Entity;
using AppWCFService.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace AppWCFService.Controllers
{
    public class ventaController
    {
        cajaController cajactrl = new cajaController();
        usuarioController usuarioctrl = new usuarioController();
       // ventaController ventactrl = new ventaController();
       bancoController bancoctrl = new bancoController();
        sucursalController sucursalController = new sucursalController();
        clienteController clienteController = new clienteController();

        public ventaEntity venta_iniciar(int ? sucursalId ,int ? cajeroId, string modulo)
        {
            ventaEntity venta = new ventaEntity();
           
            dbHelper db = new dbHelper();

            try
            {
                db.agregarParametro("@sucursalId", SqlDbType.BigInt, sucursalId);
                db.agregarParametro("@cajeroId", SqlDbType.VarChar, cajeroId);
                db.agregarParametro("@modulo", SqlDbType.VarChar, modulo);
                db.agregarParametro("@id", SqlDbType.BigInt, ParameterDirection.Output);
                db.agregarParametro("@ref", SqlDbType.VarChar, ParameterDirection.Output, 15);
                db.agregarParametro("@error", SqlDbType.Int, ParameterDirection.Output);
                db.agregarParametro("@mensaje", SqlDbType.VarChar, ParameterDirection.Output, 2000);
                db.consultaOutput("venta_iniciar");
                venta.id = long.Parse(db.diccionarioOutput["@id"].ToString());
                // venta = venta_obten(venta.id);
                venta.referencia = db.diccionarioOutput["@ref"].ToString();
                venta.mensaje = db.diccionarioOutput["@mensaje"].ToString();
                venta.error = db.diccionarioOutput["@error"].ToString();
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch(Exception ex)
            {
                venta.error = ex.Message;
                db.conexion.Close();
                db.conexion.Dispose();
            }
            
            return venta;
        }

        public List<ventaEntity> venta_obtenLista(long ? cajeroId,long ? sucursalId,string fechaInicio,string fechaFin, string referencia,long ? IdVenta, string serie, int? usuarioId)
        {
            List<ventaEntity> result = new List<ventaEntity>();
         
            dbHelper db = new dbHelper();
            SqlDataReader reader = null;


            try
            {
                db.agregarParametro("@cajeroId", SqlDbType.BigInt, cajeroId);
                db.agregarParametro("@referencia", SqlDbType.VarChar, referencia);
                db.agregarParametro("@IdVenta", SqlDbType.BigInt, IdVenta);
                db.agregarParametro("@sucursalId", SqlDbType.BigInt, sucursalId);
                db.agregarParametro("@fechaInicio", SqlDbType.VarChar, fechaInicio);
                db.agregarParametro("@fechaFin", SqlDbType.VarChar, fechaFin);
                db.agregarParametro("@serie", SqlDbType.VarChar, serie);
                db.agregarParametro("@usuarioId", SqlDbType.Int, usuarioId);
                 reader = db.consultaReader("venta_obtenLista");
                result = db.MapDataToEntityCollection<ventaEntity>(reader).ToList();

                result.ForEach(y => {
                    y.sucursal = sucursalController.sucursal_obten(y.sucursalId);
                    y.usuarioCajero = usuarioctrl.usuario_obten(y.cajeroId);
                    y.fecha2 = y.fecha.ToShortDateString();
                    y.totalC = y.total.ToString("C");
                });

                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch(Exception ex) {
                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();

            }            

            
            return result;
        }

        public ventaEntity venta_obten(long id)
        {
            ventaEntity result = new ventaEntity();
            dbHelper db = new dbHelper();
            SqlDataReader reader = null;
            try
            {
                db.agregarParametro("@id", SqlDbType.BigInt, id);
                reader = db.consultaReader("venta_obten");
                result = db.MapDataToEntityCollection<ventaEntity>(reader).ToList()[0];
                result.sucursal = sucursalController.sucursal_obten(result.sucursalId);
                result.usuarioCajero = usuarioctrl.usuario_obten(result.cajeroId);
                result.fecha2 = result.fecha.ToShortDateString();
                
            }
            catch (Exception ex)
            {
                result = null;

                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }

            try
            {
                result.cliente = clienteController.cliente_obten(result.clienteId);
            }
            catch
            {

            }

            
            return result;
        }


        public decimal  venta_obtentotal(long ? ventaId)
        {
            decimal result = 0;
            dbHelper db = new dbHelper();

            SqlDataReader reader = null;
            try
            {
                db.agregarParametro("@ventaId", SqlDbType.BigInt, ventaId);
                reader = db.consultaReader("venta_obtentotal");
                while (reader.Read())
                {
                    result = decimal.Parse(reader[0].ToString());
                }

                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch (Exception ex)
            {
                result = 0;
                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }

           
            return result;
        }

        public long ? venta_terminar(ventaEntity venta)
        {
           
            dbHelper db = new dbHelper();
            long idventa = 0;
            try
            {
                db.agregarParametro("@idtemp", SqlDbType.BigInt, venta.id);
                db.agregarParametro("@bancoId", SqlDbType.Int, venta.bancoId);
                db.agregarParametro("@referencia", SqlDbType.VarChar, venta.tarjetaReferencia);
                db.agregarParametro("@clienteId", SqlDbType.Decimal, venta.clienteId);
                db.agregarParametro("@descuento", SqlDbType.Decimal, venta.descuento);
                db.agregarParametro("@cambio", SqlDbType.Decimal, venta.cambio);
                db.agregarParametro("@pagoEfectivo", SqlDbType.Decimal, venta.pagoEfectivo);
                db.agregarParametro("@pagoTarjeta", SqlDbType.Decimal, venta.pagoTarjeta);
                db.agregarParametro("@tipoTarjeta", SqlDbType.VarChar, venta.tipoTarjeta);
                db.agregarParametro("@pagoTransferencia", SqlDbType.Decimal, venta.pagoTransferencia);
                db.agregarParametro("@bancoTrans", SqlDbType.BigInt, venta.bancoTrans);
                db.agregarParametro("@AutorizacionTrans", SqlDbType.BigInt, venta.AutorizacionTrans);
                db.agregarParametro("@AutorizacionTarjeta", SqlDbType.BigInt, venta.tarjetaAutorizacion);
                db.agregarParametro("@ReferenciaTrans", SqlDbType.BigInt, venta.ReferenciaTrans);
                db.agregarParametro("@tipoVenta", SqlDbType.BigInt, venta.tipoVenta);
                db.agregarParametro("@vendedorId", SqlDbType.BigInt, venta.vendedorId);
                db.agregarParametro("@comision", SqlDbType.Decimal, venta.comision);
                db.agregarParametro("@idventa", SqlDbType.BigInt, ParameterDirection.Output);
                db.consultaOutput("venta_terminar");

                 idventa = long.Parse(db.diccionarioOutput["@idventa"].ToString());
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch(Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }
          
            return idventa;
        }

        public int ? venta_cancelar(long? ventaId,long ? usuarioId,long ? motivoId)
        {
            dbHelper db = new dbHelper();
            try
            {
                db.agregarParametro("@id", SqlDbType.BigInt, ventaId);
                db.agregarParametro("@usuarioId", SqlDbType.BigInt, usuarioId);
                db.agregarParametro("@motivoId", SqlDbType.BigInt, motivoId);
                db.consultaSinRetorno("venta_cancelar");
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch{

                db.conexion.Close();
                db.conexion.Dispose();
            }
            
            return 1;
        }

        public string GetCantidadLetra(int cantidad)
        {
            string letra = "";
            dbHelper db = new dbHelper();
            try{
                db.agregarParametro("@Numero", SqlDbType.Decimal, 100);
                db.agregarParametro("@moneda", SqlDbType.Decimal, 0);
                db.agregarParametro("@letra", SqlDbType.VarChar, ParameterDirection.Output, 2000);
                db.consultaOutput("DameCantidadALetra");
                letra = db.diccionarioOutput["@letra"].ToString();
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }
           
            return letra;
        }

        public ventaEntity GetVentaByReferencia(string referencia)
        {
            ventaEntity result = new ventaEntity();
            dbHelper db = new dbHelper();
            SqlDataReader reader = null;
            try
            {
                db.agregarParametro("@referencia", SqlDbType.VarChar, referencia);
                reader = db.consultaReader("venta_obtenByRefencia");
                result = db.MapDataToEntityCollection<ventaEntity>(reader).ToList()[0];
                result.sucursal = sucursalController.sucursal_obten(result.sucursalId);
                result.usuarioCajero = usuarioctrl.usuario_obten(result.cajeroId);
                result.fecha2 = result.fecha.ToShortDateString();
                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch (Exception ex)
            {
                result = null;
                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }

            
            return result;
        }


        public List<ventaEntity> GetVentasEfectivo(long? cajeroId, long? sucursalId, string fechaInicio, string fechaFin, string referencia, long? IdVenta, string serie)
        {
            List<ventaEntity> result = new List<ventaEntity>();

            dbHelper db = new dbHelper();
            SqlDataReader reader = null;


            try
            {
                db.agregarParametro("@cajeroId", SqlDbType.BigInt, cajeroId);
                db.agregarParametro("@referencia", SqlDbType.VarChar, referencia);
                db.agregarParametro("@IdVenta", SqlDbType.BigInt, IdVenta);
                db.agregarParametro("@sucursalId", SqlDbType.BigInt, sucursalId);
                db.agregarParametro("@fechaInicio", SqlDbType.VarChar, fechaInicio);
                db.agregarParametro("@fechaFin", SqlDbType.VarChar, fechaFin);
                db.agregarParametro("@serie", SqlDbType.VarChar, serie);
                db.agregarParametro("@usuarioId", SqlDbType.Int,0);
                reader = db.consultaReader("venta_GetVentasEfectivo");
                result = db.MapDataToEntityCollection<ventaEntity>(reader).ToList();

                result.ForEach(y => {
                    y.sucursal = sucursalController.sucursal_obten(y.sucursalId);
                    y.usuarioCajero = usuarioctrl.usuario_obten(y.cajeroId);
                    y.fecha2 = y.fecha.ToShortDateString();
                    y.totalC = y.total.ToString("C");
                });

                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch (Exception ex)
            {
                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();

            }


            return result;
        }

        public List<ventaTarjetasEntity> GetVentasPagoTarjeta(long? cajeroId, long? sucursalId, string fechaInicio, string fechaFin,string tipo)
        {
            List<ventaTarjetasEntity> result = new List<ventaTarjetasEntity>();
            dbHelper db = new dbHelper();
            SqlDataReader reader = null;
            try
            {


                db.agregarParametro("@cajeroId", SqlDbType.BigInt, cajeroId);
                db.agregarParametro("@sucursalId", SqlDbType.BigInt, sucursalId);
                db.agregarParametro("@fechaInicio", SqlDbType.VarChar, fechaInicio);
                db.agregarParametro("@fechaFin", SqlDbType.VarChar, fechaFin);
                db.agregarParametro("@tipo", SqlDbType.VarChar, tipo);
                reader = db.consultaReader("venta_obtenListaPagosTarjeta");
                result = db.MapDataToEntityCollection<ventaTarjetasEntity>(reader).ToList();

                result.ForEach(y => {
                    y.venta = venta_obten(y.ventaId);
                    y.banco = bancoctrl.banco_obten(y.bancoId);
                });

                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch {
                db.conexion.Close();
                db.conexion.Dispose();

            }

            return result;

        }


        public List<ventaTransferenciaEntity> GetVentasPagoTransferencia(long? cajeroId, long? sucursalId, string fechaInicio, string fechaFin)
        {
            List<ventaTransferenciaEntity> result = new List<ventaTransferenciaEntity>();
            dbHelper db = new dbHelper();
            SqlDataReader reader = null;
            try
            {
                db.agregarParametro("@cajeroId", SqlDbType.BigInt, cajeroId);
                db.agregarParametro("@sucursalId", SqlDbType.BigInt, sucursalId);
                db.agregarParametro("@fechaInicio", SqlDbType.VarChar, fechaInicio);
                db.agregarParametro("@fechaFin", SqlDbType.VarChar, fechaFin);
                reader = db.consultaReader("venta_obtenListaPagosTransferencia");
                result = db.MapDataToEntityCollection<ventaTransferenciaEntity>(reader).ToList();

                result.ForEach(y => {
                    y.venta = venta_obten(y.ventaId);
                    y.banco = bancoctrl.banco_obten(y.bancoId);
                });

                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch {

                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }

            return result;

        }

        public bool GetAutorizaCancelacionVenta(int ? op ,string email,string password)
        {
            bool valido = false;
            dbHelper db = new dbHelper();
            try
            {
                db.agregarParametro("@email", SqlDbType.VarChar, email);
                db.agregarParametro("@password", SqlDbType.VarChar, password);
                db.agregarParametro("@valido", SqlDbType.Bit, ParameterDirection.Output);
                db.consultaOutput("venta_autorizaCancelacion");
                valido = bool.Parse(db.diccionarioOutput["@valido"].ToString());
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }
           

            return valido;

        }


        public List<VentaCreditoEntity> GetVentacreditoLista(int ? clienteId, string fecha,string status,int ? sucursalId)
        {

            List<VentaCreditoEntity> result = new List<VentaCreditoEntity>();
            dbHelper db = new dbHelper();
            try
            {

                db.agregarParametro("@clienteId", SqlDbType.BigInt, clienteId);
                db.agregarParametro("@fechaInicio", SqlDbType.VarChar, fecha);
                db.agregarParametro("@fechaFin", SqlDbType.VarChar, fecha);
                db.agregarParametro("@status", SqlDbType.VarChar, status);
                db.agregarParametro("@sucursalId", SqlDbType.BigInt, sucursalId);
                db.agregarParametro("@detalle", SqlDbType.Bit, true);                
                SqlDataReader reader = db.consultaReader("ventas_creditoLista");
                result = db.MapDataToEntityCollection<VentaCreditoEntity>(reader).ToList();



                //result.ForEach(y=> { y.caracteristicas = articuloCaracteristica_.articulo_obtenCaracteristicas(y.id).ToList(); });
                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();

            }
            catch
            {

                db.conexion.Close();
                db.conexion.Dispose();

            }

            return result;
        }


        public int? venta_descuentoNuevo(long? ventaId, long? monto, long? usuarioId, int? porcentaje,string observaciones)
        {
            dbHelper db = new dbHelper();
            try
            {
                db.agregarParametro("@ventaId", SqlDbType.BigInt, ventaId);
                db.agregarParametro("@monto", SqlDbType.Decimal, monto);
                db.agregarParametro("@usuarioId", SqlDbType.BigInt, usuarioId);
                db.agregarParametro("@porcentaje", SqlDbType.BigInt, porcentaje);
                db.agregarParametro("@observaciones", SqlDbType.VarChar, observaciones);
                db.consultaSinRetorno("venta_descuentoNuevo");
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch
            {

                db.conexion.Close();
                db.conexion.Dispose();
            }

            return 1;
        }
        




    }
}