﻿using AppWCFService.Entity;
using AppWCFService.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace AppWCFService.Controllers
{
    public class entregaParcialController
    {

        usuarioController usuarioController = new usuarioController();

        public List<EntregaParcialEntity> entregaParcial_obtenLista(int ? sucursalId, string fechaInicio,string fechafin,int ? usuarioId, int ? id,int  ? userId2)
        {

            List<EntregaParcialEntity> result = new List<EntregaParcialEntity>();
            dbHelper db = new dbHelper();
            SqlDataReader reader = null;
            try
            {
                db.agregarParametro("@fechaInicio", SqlDbType.VarChar, fechaInicio);
                db.agregarParametro("@fechafin", SqlDbType.VarChar, fechafin);
                db.agregarParametro("@usuarioId", SqlDbType.BigInt, usuarioId);
                db.agregarParametro("@sucursalId", SqlDbType.BigInt, sucursalId);
                db.agregarParametro("@id", SqlDbType.BigInt, id);
                db.agregarParametro("@userId2", SqlDbType.BigInt, userId2);

                reader = db.consultaReader("entregaParcial_obtenLista");
                result = db.MapDataToEntityCollection<EntregaParcialEntity>(reader).ToList();
                result.ForEach(z => {

                    z.usuario = usuarioController.usuario_obten(z.usuarioId);
                    z.fecha2 = z.fecha.ToShortDateString();
                    z.cantidadC = z.cantidad.ToString("C");
                    z.Editable = (z.fecha.ToShortDateString() == DateTime.Now.ToShortDateString());
                });
                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch
            {
                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }
            
            return result;
        }

        public EntregaParcialEntity entregaParcial_obten(long? id)
        {
            EntregaParcialEntity result = new EntregaParcialEntity();
            dbHelper db = new dbHelper();
            SqlDataReader reader = null;
            try
            {
                db.agregarParametro("@id", SqlDbType.BigInt, id);
                reader = db.consultaReader("entregaParcial_obten");

                result = db.MapDataToEntityCollection<EntregaParcialEntity>(reader).ToList()[0];
                result.usuario = usuarioController.usuario_obten(result.usuarioId);
                result.fecha2 = result.fecha.ToShortDateString();
                result.cantidadC = result.cantidad.ToString("C");

                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch (Exception ex)
            {
                result = null;
                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }

           
            return result;
        }


        public ResultEntity entregaParcial_cancela(long? id ,int ? usuarioId)
        {
            ResultEntity result = new ResultEntity();
            dbHelper db = new dbHelper();
            try
            {
                
                db.agregarParametro("@id", SqlDbType.BigInt, id);
                db.agregarParametro("@usuarioId", SqlDbType.BigInt, usuarioId);
                db.agregarParametro("@Message", SqlDbType.VarChar, ParameterDirection.Output, 100);
                db.agregarParametro("@ErrorCode", SqlDbType.BigInt, ParameterDirection.Output);
                db.consultaOutput("entregaParcial_cancela");

                string mensaje = db.diccionarioOutput["@Message"].ToString();
                int ErrorCode = int.Parse(db.diccionarioOutput["@ErrorCode"].ToString());

                db.conexion.Close();
                db.conexion.Dispose();
                result.ErrorCode = ErrorCode;
                result.Message = mensaje;
            }
            catch(Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
                result.ErrorCode = 1;
                result.Message = ex.Message;
            }
            
          

           
            return result;
        }




        public ResultEntity  entregaParcial_nuevo(EntregaParcialEntity entrega)
        {
            dbHelper db = new dbHelper();
            long id = 0;
            string mensaje = "";
            int ErrorCode = 0;
            ResultEntity result = new ResultEntity();
            try
            {

                db.agregarParametro("@usuarioId", SqlDbType.BigInt, entrega.usuarioId);
                db.agregarParametro("@autorizadorId", SqlDbType.BigInt, entrega.autorizadorId);
                db.agregarParametro("@fecha", SqlDbType.VarChar, "");
                db.agregarParametro("@referencia", SqlDbType.VarChar, entrega.referencia);
                db.agregarParametro("@cantidad", SqlDbType.Money, entrega.cantidad);
                db.agregarParametro("@observaciones", SqlDbType.VarChar, entrega.observaciones);
                db.agregarParametro("@autorizado", SqlDbType.Bit, entrega.autorizado);
                db.agregarParametro("@B1000", SqlDbType.Int, entrega.b1000);
                db.agregarParametro("@B500", SqlDbType.Int, entrega.b500);
                db.agregarParametro("@B200", SqlDbType.Int, entrega.b200);
                db.agregarParametro("@B100", SqlDbType.Int, entrega.b100);
                db.agregarParametro("@B50", SqlDbType.Int, entrega.b50);
                db.agregarParametro("@B20", SqlDbType.Int, entrega.b20);
                db.agregarParametro("@M100", SqlDbType.Int, entrega.m100);
                db.agregarParametro("@M20", SqlDbType.Int, entrega.m20);
                db.agregarParametro("@M10", SqlDbType.Int, entrega.m10);
                db.agregarParametro("@M5", SqlDbType.Int, entrega.m5);
                db.agregarParametro("@M2", SqlDbType.Int, entrega.m2);
                db.agregarParametro("@M1", SqlDbType.Int, entrega.m1);
                db.agregarParametro("@M050", SqlDbType.Int, entrega.m050);
                db.agregarParametro("@Message", SqlDbType.VarChar, ParameterDirection.Output, 100);
                db.agregarParametro("@ErrorCode", SqlDbType.BigInt, ParameterDirection.Output);
                db.agregarParametro("@id", SqlDbType.BigInt, ParameterDirection.Output);
                db.consultaOutput("entregaParcial_nuevo");

                id = long.Parse(db.diccionarioOutput["@id"].ToString());
                mensaje = db.diccionarioOutput["@Message"].ToString();
                ErrorCode = int.Parse(db.diccionarioOutput["@ErrorCode"].ToString());
                db.conexion.Close();
                db.conexion.Dispose();

               
                result.ErrorCode = ErrorCode;
                result.Message = mensaje;
                
            }
            catch(Exception ex) {                
               
                db.conexion.Close();
                db.conexion.Dispose();

                result.ErrorCode = 1;
                result.Message = ex.Message;
            }

            return result;


        }


        public ResultEntity entregaParcial_editar(EntregaParcialEntity entrega)
        {
              
            dbHelper db = new dbHelper();
            ResultEntity result = new ResultEntity();
            try
            {
                db.agregarParametro("@usuarioId", SqlDbType.BigInt, entrega.usuarioId);
                db.agregarParametro("@autorizadorId", SqlDbType.BigInt, entrega.autorizadorId);
                db.agregarParametro("@fecha", SqlDbType.VarChar, entrega.fecha);
                db.agregarParametro("@referencia", SqlDbType.VarChar, entrega.referencia);
                db.agregarParametro("@cantidad", SqlDbType.Money, entrega.cantidad);
                db.agregarParametro("@observaciones", SqlDbType.VarChar, entrega.observaciones);
                db.agregarParametro("@autorizado", SqlDbType.Bit, entrega.autorizado);
                db.agregarParametro("@B1000", SqlDbType.Int, entrega.b1000);
                db.agregarParametro("@B500", SqlDbType.Int, entrega.b500);
                db.agregarParametro("@B200", SqlDbType.Int, entrega.b200);
                db.agregarParametro("@B100", SqlDbType.Int, entrega.b100);
                db.agregarParametro("@B50", SqlDbType.Int, entrega.b50);
                db.agregarParametro("@B20", SqlDbType.Int, entrega.b20);
                db.agregarParametro("@M100", SqlDbType.Int, entrega.m100);
                db.agregarParametro("@M20", SqlDbType.Int, entrega.m20);
                db.agregarParametro("@M10", SqlDbType.Int, entrega.m10);
                db.agregarParametro("@M5", SqlDbType.Int, entrega.m5);
                db.agregarParametro("@M2", SqlDbType.Int, entrega.m2);
                db.agregarParametro("@M1", SqlDbType.Int, entrega.m1);
                db.agregarParametro("@M050", SqlDbType.Int, entrega.m050);
                db.agregarParametro("@id", SqlDbType.BigInt, entrega.id);
                db.agregarParametro("@Message", SqlDbType.VarChar, ParameterDirection.Output, 100);
                db.agregarParametro("@ErrorCode", SqlDbType.BigInt, ParameterDirection.Output);
                db.consultaOutput("entregaParcial_edita");

                string mensaje = db.diccionarioOutput["@Message"].ToString();
                int ErrorCode = int.Parse(db.diccionarioOutput["@ErrorCode"].ToString());

                result.ErrorCode = ErrorCode;
                result.Message = mensaje;

                db.conexion.Close();
                db.conexion.Dispose();

            }
            catch(Exception ex){
                result.ErrorCode = 1;
                result.Message = ex.Message;
                db.conexion.Close();
                db.conexion.Dispose();
            }    
           
            return result;
        }


    }
}