﻿using AppWCFService.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
using static AppWCFService.Entity.ImagenEntity;

namespace AppWCFService.Controllers
{
    public class ArchivosController
    {


        public static byte[] ReadFully(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }

        #region Articulos


        public string saveFileArticulo(Stream archivo, int? articuloId, int? tipo, string nombre)
        {

            string archivo_str = "";
            string folder = System.AppDomain.CurrentDomain.BaseDirectory + "articulos-docs/";

            if (nombre.Contains(".jpg") || nombre.Contains(".png") || nombre.Contains(".jpeg")) // imagen
            {
                try
                {
                    archivo_str = Guid.NewGuid().ToString() + ".png";
                    System.Drawing.Image img = System.Drawing.Image.FromStream(archivo);
                    if (!Directory.Exists(folder))
                    {
                        Directory.CreateDirectory(folder);
                    }
                    img.Save(folder + "\\" + archivo_str, ImageFormat.Png);

                    dbHelper db = new dbHelper();
                    db.agregarParametro("@articuloId", SqlDbType.BigInt, articuloId);
                    db.agregarParametro("@nombre", SqlDbType.VarChar, nombre);
                    db.agregarParametro("@archivo", SqlDbType.VarChar, archivo_str);
                    db.agregarParametro("@tipo", SqlDbType.BigInt, 1);
                    db.agregarParametro("@tipoImagen", SqlDbType.BigInt, 0);
                    db.consultaSinRetorno("articuloDocs_nuevo");
                    db.conexion.Close();
                    db.conexion.Dispose();
                }
                catch (Exception ex)
                {

                }


            }
            if (nombre.Contains(".pdf")) // pdf
            {
                if (!Directory.Exists(folder))
                {
                    Directory.CreateDirectory(folder);
                }

                archivo_str = Guid.NewGuid().ToString() + ".pdf";
                Byte[] bytes = ReadFully(archivo);
                FileStream fs = new FileStream(folder + archivo_str, FileMode.OpenOrCreate);
                fs.Write(bytes, 0, bytes.Length);
                fs.Close();


                dbHelper db = new dbHelper();
                db.agregarParametro("@articuloId", SqlDbType.BigInt, articuloId);
                db.agregarParametro("@nombre", SqlDbType.VarChar, nombre);
                db.agregarParametro("@archivo", SqlDbType.VarChar, archivo_str);
                db.agregarParametro("@tipo", SqlDbType.BigInt, 2);
                db.agregarParametro("@tipoImagen", SqlDbType.BigInt, 0);
                db.consultaSinRetorno("articuloDocs_nuevo");
                db.conexion.Close();
                db.conexion.Dispose();
            }
            if (nombre.Contains(".xlsx") || nombre.Contains(".xls")) // excel
            {
                if (!Directory.Exists(folder))
                {
                    Directory.CreateDirectory(folder);
                }

                archivo_str = Guid.NewGuid().ToString() + ".xlsx";
                Byte[] bytes = ReadFully(archivo);
                FileStream fs = new FileStream(folder + archivo_str, FileMode.OpenOrCreate);
                fs.Write(bytes, 0, bytes.Length);
                fs.Close();


                dbHelper db = new dbHelper();
                db.agregarParametro("@articuloId", SqlDbType.BigInt, articuloId);
                db.agregarParametro("@nombre", SqlDbType.VarChar, nombre);
                db.agregarParametro("@archivo", SqlDbType.VarChar, archivo_str);
                db.agregarParametro("@tipo", SqlDbType.BigInt, 3);
                db.agregarParametro("@tipoImagen", SqlDbType.BigInt, 0);
                db.consultaSinRetorno("articuloDocs_nuevo");
                db.conexion.Close();
                db.conexion.Dispose();
            }


            return archivo_str;

        }



        public string DeleteFilesArticulo(int? id)
        {
            string result = "";

            try
            {
                dbHelper db = new dbHelper();
                db.agregarParametro("@id", SqlDbType.BigInt, id);
                SqlDataReader rd = db.consultaReader("articuloDocs_eliminar");
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch (Exception ex) { }

            return result;
        }




        public List<logo> GetImagenesArticulo(int? articuloId)
        {
            List<logo> lista = new List<logo>();
            try
            {
                dbHelper db = new dbHelper();
                db.agregarParametro("@articuloId", SqlDbType.BigInt, articuloId);
                SqlDataReader rd = db.consultaReader("articuloDocs_obtenlista");
                while (rd.Read())
                {
                    logo lo = new logo();
                    lo.id = int.Parse(rd[0].ToString());
                    lo.nombre = rd[1].ToString();
                    lo.tareaId = int.Parse(rd[2].ToString());
                    lo.archivo = rd[3].ToString();
                    lo.tipoImagen = int.Parse(rd[4].ToString());
                    lo.tipo = int.Parse(rd[5].ToString());
                    lista.Add(lo);


                }
                rd.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch (Exception ex) { }

            return lista;
        }


        #endregion


        #region Tareas

        public string saveFileTarea(Stream archivo, int? tareaId, int? tipo, string nombre)
        {

            string archivo_str = "";
            string folder = System.AppDomain.CurrentDomain.BaseDirectory + "tareas-docs/";

            if (nombre.Contains(".jpg") || nombre.Contains(".png") || nombre.Contains(".jpeg")) // imagen
            {
                try
                {
                    archivo_str = Guid.NewGuid().ToString() + ".png";
                    System.Drawing.Image img = System.Drawing.Image.FromStream(archivo);
                    if (!Directory.Exists(folder))
                    {
                        Directory.CreateDirectory(folder);
                    }
                    img.Save(folder + "\\" + archivo_str, ImageFormat.Png);

                    dbHelper db = new dbHelper();
                    db.agregarParametro("@tareaId", SqlDbType.BigInt, tareaId);
                    db.agregarParametro("@nombre", SqlDbType.VarChar, nombre);
                    db.agregarParametro("@archivo", SqlDbType.VarChar, archivo_str);
                    db.agregarParametro("@tipo", SqlDbType.BigInt, 1);
                    db.agregarParametro("@tipoImagen", SqlDbType.BigInt, 0);
                    db.consultaSinRetorno("tareaDocs_nuevo");
                    db.conexion.Close();
                    db.conexion.Dispose();
                }
                catch (Exception ex)
                {

                }


            }
            if (nombre.Contains(".pdf")) // pdf
            {
                if (!Directory.Exists(folder))
                {
                    Directory.CreateDirectory(folder);
                }

                archivo_str = Guid.NewGuid().ToString() + ".pdf";
                Byte[] bytes = ReadFully(archivo);
                FileStream fs = new FileStream(folder + archivo_str, FileMode.OpenOrCreate);
                fs.Write(bytes, 0, bytes.Length);
                fs.Close();


                dbHelper db = new dbHelper();
                db.agregarParametro("@tareaId", SqlDbType.BigInt, tareaId);
                db.agregarParametro("@nombre", SqlDbType.VarChar, nombre);
                db.agregarParametro("@archivo", SqlDbType.VarChar, archivo_str);
                db.agregarParametro("@tipo", SqlDbType.BigInt, 2);
                db.agregarParametro("@tipoImagen", SqlDbType.BigInt, 0);
                db.consultaSinRetorno("tareaDocs_nuevo");
                db.conexion.Close();
                db.conexion.Dispose();
            }
            if (nombre.Contains(".xlsx") || nombre.Contains(".xls")) // excel
            {
                if (!Directory.Exists(folder))
                {
                    Directory.CreateDirectory(folder);
                }

                archivo_str = Guid.NewGuid().ToString() + ".xlsx";
                Byte[] bytes = ReadFully(archivo);
                FileStream fs = new FileStream(folder + archivo_str, FileMode.OpenOrCreate);
                fs.Write(bytes, 0, bytes.Length);
                fs.Close();


                dbHelper db = new dbHelper();
                db.agregarParametro("@tareaId", SqlDbType.BigInt, tareaId);
                db.agregarParametro("@nombre", SqlDbType.VarChar, nombre);
                db.agregarParametro("@archivo", SqlDbType.VarChar, archivo_str);
                db.agregarParametro("@tipo", SqlDbType.BigInt, 3);
                db.agregarParametro("@tipoImagen", SqlDbType.BigInt, 0);
                db.consultaSinRetorno("tareaDocs_nuevo");
                db.conexion.Close();
                db.conexion.Dispose();
            }


            return archivo_str;

        }



        public string DeleteImagenesTarea(int? id)
        {
            string result = "";

            try
            {
                dbHelper db = new dbHelper();
                db.agregarParametro("@id", SqlDbType.BigInt, id);
                SqlDataReader rd = db.consultaReader("tareaDocs_eliminar");
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch (Exception ex) { }

            return result;
        }




        public List<logo> GetImagenesTarea(int? tareaId)
        {
            List<logo> lista = new List<logo>();
            try
            {
                dbHelper db = new dbHelper();
                db.agregarParametro("@tareaId", SqlDbType.BigInt, tareaId);
                SqlDataReader rd = db.consultaReader("tareaDocs_obtenlista");
                while (rd.Read())
                {
                    logo lo = new logo();
                    lo.id = int.Parse(rd[0].ToString());
                    lo.nombre = rd[1].ToString();
                    lo.tareaId = int.Parse(rd[2].ToString());
                    lo.archivo = rd[3].ToString();
                    lo.tipoImagen = int.Parse(rd[4].ToString());
                    lo.tipo = int.Parse(rd[5].ToString());
                    lista.Add(lo);


                }
                rd.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch (Exception ex) { }

            return lista;
        }

        #endregion



        #region logos

 
        public string saveLogoApp(Stream archivo, int? tipo, string nombre)
        {

            string archivo_str = "";
            string folder = System.AppDomain.CurrentDomain.BaseDirectory + "logos/";

            if (nombre.Contains(".jpg") || nombre.Contains(".png") || nombre.Contains(".jpeg")) // imagen
            {
                try
                {
                    archivo_str = Guid.NewGuid().ToString() + ".png";
                    System.Drawing.Image img = System.Drawing.Image.FromStream(archivo);
                    if (!Directory.Exists(folder))
                    {
                        Directory.CreateDirectory(folder);
                    }
                    img.Save(folder + "\\" + archivo_str, ImageFormat.Png);

                    dbHelper db = new dbHelper();                    
                    db.agregarParametro("@nombre", SqlDbType.VarChar, nombre);
                    db.agregarParametro("@archivo", SqlDbType.VarChar, archivo_str);
                    db.agregarParametro("@tipo", SqlDbType.BigInt, tipo);
                    db.agregarParametro("@tipoImagen", SqlDbType.BigInt, 0);
                    db.consultaSinRetorno("GuardaLogo");
                    db.conexion.Close();
                    db.conexion.Dispose();
                }
                catch (Exception ex)
                {

                }


            }
          


            return archivo_str;

        }



        #endregion


        #region Clientes

        public string saveFileCliente(Stream archivo, int? clienteId, int? tipo, string nombre)
        {

            string archivo_str = "";
            string folder = System.AppDomain.CurrentDomain.BaseDirectory + "clientes-docs/"+ clienteId+ "/";

            if (nombre.Contains(".jpg") || nombre.Contains(".png") || nombre.Contains(".jpeg")) // imagen
            {
                try
                {
                    archivo_str = Guid.NewGuid().ToString() + ".png";
                    System.Drawing.Image img = System.Drawing.Image.FromStream(archivo);
                    if (!Directory.Exists(folder))
                    {
                        Directory.CreateDirectory(folder);
                    }
                    img.Save(folder + "\\" + archivo_str, ImageFormat.Png);

                    dbHelper db = new dbHelper();
                    db.agregarParametro("@clienteId", SqlDbType.BigInt, clienteId);
                    db.agregarParametro("@nombre", SqlDbType.VarChar, nombre);
                    db.agregarParametro("@archivo", SqlDbType.VarChar, archivo_str);
                    db.agregarParametro("@tipo", SqlDbType.BigInt, 1);
                    db.agregarParametro("@tipoImagen", SqlDbType.BigInt, 0);
                    db.consultaSinRetorno("clienteDocs_nuevo");
                    db.conexion.Close();
                    db.conexion.Dispose();
                }
                catch (Exception ex)
                {

                }


            }
            if (nombre.Contains(".pdf")) // pdf
            {
                if (!Directory.Exists(folder))
                {
                    Directory.CreateDirectory(folder);
                }

                archivo_str = Guid.NewGuid().ToString() + ".pdf";
                Byte[] bytes = ReadFully(archivo);
                FileStream fs = new FileStream(folder + archivo_str, FileMode.OpenOrCreate);
                fs.Write(bytes, 0, bytes.Length);
                fs.Close();


                dbHelper db = new dbHelper();
                db.agregarParametro("@clienteId", SqlDbType.BigInt, clienteId);
                db.agregarParametro("@nombre", SqlDbType.VarChar, nombre);
                db.agregarParametro("@archivo", SqlDbType.VarChar, archivo_str);
                db.agregarParametro("@tipo", SqlDbType.BigInt, 2);
                db.agregarParametro("@tipoImagen", SqlDbType.BigInt, 0);
                db.consultaSinRetorno("clienteDocs_nuevo");
                db.conexion.Close();
                db.conexion.Dispose();
            }
            if (nombre.Contains(".xlsx") || nombre.Contains(".xls")) // excel
            {
                if (!Directory.Exists(folder))
                {
                    Directory.CreateDirectory(folder);
                }

                archivo_str = Guid.NewGuid().ToString() + ".xlsx";
                Byte[] bytes = ReadFully(archivo);
                FileStream fs = new FileStream(folder + archivo_str, FileMode.OpenOrCreate);
                fs.Write(bytes, 0, bytes.Length);
                fs.Close();


                dbHelper db = new dbHelper();
                db.agregarParametro("@clienteId", SqlDbType.BigInt, clienteId);
                db.agregarParametro("@nombre", SqlDbType.VarChar, nombre);
                db.agregarParametro("@archivo", SqlDbType.VarChar, archivo_str);
                db.agregarParametro("@tipo", SqlDbType.BigInt, 3);
                db.agregarParametro("@tipoImagen", SqlDbType.BigInt, 0);
                db.consultaSinRetorno("clienteDocs_nuevo");
                db.conexion.Close();
                db.conexion.Dispose();
            }


            return archivo_str;

        }



        public string DeleteFilesCliente(int? id)
        {
            string result = "";

            try
            {
                dbHelper db = new dbHelper();
                db.agregarParametro("@id", SqlDbType.BigInt, id);
                SqlDataReader rd = db.consultaReader("clienteDocs_eliminar");
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch (Exception ex) { }

            return result;
        }




        public List<logo> GetImagenesCliente(int? clienteId)
        {
            List<logo> lista = new List<logo>();
            try
            {
                dbHelper db = new dbHelper();
                db.agregarParametro("@clienteId", SqlDbType.BigInt, clienteId);
                SqlDataReader rd = db.consultaReader("clienteDocs_obtenlista");
                while (rd.Read())
                {
                    logo lo = new logo();
                    lo.id = int.Parse(rd[0].ToString());
                    lo.nombre = rd[1].ToString();
                    lo.tareaId = int.Parse(rd[2].ToString());
                    lo.archivo = rd[3].ToString();
                    lo.tipoImagen = int.Parse(rd[4].ToString());
                    lo.tipo = int.Parse(rd[5].ToString());
                    lista.Add(lo);


                }
                rd.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch (Exception ex) { }

            return lista;
        }


        #endregion

    }
}