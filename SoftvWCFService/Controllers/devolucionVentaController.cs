﻿using AppWCFService.Entity;
using AppWCFService.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace AppWCFService.Controllers
{
    public class devolucionVentaController
    {
        sucursalController suc_ctrl = new sucursalController();
        usuarioController usu_ctrl = new usuarioController();

        public List<devolucionVentaEntity> devolucionVenta_obtenLista(long ? folio,string fechaInicio, string fechaFin, long? sucursalId, long? cajeroId,long ? usuarioId,long ? ventaId)
        {
            List<devolucionVentaEntity> result = new List<devolucionVentaEntity>();
            dbHelper db = new dbHelper();
            db.agregarParametro("@folio", SqlDbType.BigInt, folio);
            db.agregarParametro("@fechaInicio", SqlDbType.VarChar, fechaInicio);
            db.agregarParametro("@fechaFin", SqlDbType.VarChar, fechaFin);
            db.agregarParametro("@sucursalId", SqlDbType.BigInt, sucursalId);
            db.agregarParametro("@cajeroId", SqlDbType.BigInt, cajeroId);
            db.agregarParametro("@usuarioId", SqlDbType.BigInt, usuarioId);
            db.agregarParametro("@ventaId", SqlDbType.BigInt, ventaId);
            SqlDataReader reader = db.consultaReader("devolucioVenta_obtenLista");
            result = db.MapDataToEntityCollection<devolucionVentaEntity>(reader).ToList();

            result.ForEach(x =>
            {
                try
                {
                    x.sucursal = suc_ctrl.sucursal_obten(x.sucursalId);
                }
                catch { }
            });

            result.ForEach(x =>
            {
                try
                {
                    x.usuario = usu_ctrl.usuario_obten(x.usuarioId);
                }
                catch { }
            });

            //result.ForEach(y=> { y.caracteristicas = articuloCaracteristica_.articulo_obtenCaracteristicas(y.id).ToList(); });
            reader.Close();
            db.conexion.Close();
            db.conexion.Dispose();
            return result;
        }


        public devolucionVentaEntity devolucionVenta_obten(long? folio)
        {
           devolucionVentaEntity result = new devolucionVentaEntity();
            dbHelper db = new dbHelper();
            db.agregarParametro("@id", SqlDbType.BigInt, folio);         
            SqlDataReader reader = db.consultaReader("devolucioVenta_obten");
            result = db.MapDataToEntityCollection<devolucionVentaEntity>(reader).ToList()[0];

            
                try
                {
                result.sucursal = suc_ctrl.sucursal_obten(result.sucursalId);
                }
                catch { }
          
                try
                {
                result.usuario = usu_ctrl.usuario_obten(result.usuarioId);
                }
                catch { }
           

            //result.ForEach(y=> { y.caracteristicas = articuloCaracteristica_.articulo_obtenCaracteristicas(y.id).ToList(); });
            reader.Close();
            db.conexion.Close();
            db.conexion.Dispose();
            return result;
        }



        public ResultEntity devolucionVenta_nueva(long? sucursalId,long ? usuarioId,decimal ? monto,long ? ventaId, string observaciones)
        {
           
            dbHelper db = new dbHelper();

            string xml = "";
       
            db.agregarParametro("@ventaId", SqlDbType.BigInt, ventaId);
            db.agregarParametro("@monto", SqlDbType.Decimal, monto);
            db.agregarParametro("@usuarioId", SqlDbType.BigInt, usuarioId);
            db.agregarParametro("@sucursalId", SqlDbType.BigInt, sucursalId);
            db.agregarParametro("@articulos", SqlDbType.Xml, xml);
            db.agregarParametro("@observaciones", SqlDbType.VarChar, observaciones);
            db.agregarParametro("@id", SqlDbType.BigInt, ParameterDirection.Output);
            db.agregarParametro("@Message", SqlDbType.VarChar, ParameterDirection.Output, 100);
            db.agregarParametro("@ErrorCode", SqlDbType.BigInt, ParameterDirection.Output);
             db.consultaOutput("devolucioVenta_obten");    


            long id = long.Parse(db.diccionarioOutput["@id"].ToString());
            string mensaje = db.diccionarioOutput["@Message"].ToString();
            int ErrorCode = int.Parse(db.diccionarioOutput["@ErrorCode"].ToString());
            db.conexion.Close();
            db.conexion.Dispose();

            ResultEntity result = new ResultEntity();
            result.ErrorCode = ErrorCode;
            result.Message = mensaje;
            return result;
        }


       

    }
}