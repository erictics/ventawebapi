﻿using AppWCFService.Entity;
using AppWCFService.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace AppWCFService.Controllers
{
    public class categoriaController
    {
        departamentoController depCtrl = new departamentoController();

        public List<categoriaEntity> categoria_obtenLista(long ? departamentoId,string nombre)
        {

            List<categoriaEntity> result = new List<categoriaEntity>();
            dbHelper db = new dbHelper();
            SqlDataReader reader = null;

            try
            {
                db.agregarParametro("@departamentoId", SqlDbType.BigInt, departamentoId);
                db.agregarParametro("@nombre", SqlDbType.VarChar, nombre);
                 reader = db.consultaReader("categoria_obtenLista");
                result = db.MapDataToEntityCollection<categoriaEntity>(reader).ToList();
                result.ForEach(y => { y.departamento = depCtrl.departamento_obten(y.departamentoId); });
                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch
            {
                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }

            
            
            return result;
        }

        public categoriaEntity categoria_obten(long? id)
        {
            categoriaEntity result = new categoriaEntity();
            dbHelper db = new dbHelper();
            SqlDataReader reader = null;
            try
            {
                db.agregarParametro("@id", SqlDbType.BigInt, id);

                reader = db.consultaReader("categoria_obten");
                try
                {
                    result = db.MapDataToEntityCollection<categoriaEntity>(reader).ToList()[0];
                    result.departamento = depCtrl.departamento_obten(result.departamentoId);
                }
                catch (Exception ex)
                {
                    result = null;
                }

                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();

            }
            catch
            {
                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }
            
            
            
            return result;
        }

        public long? categoria_nuevo(categoriaEntity categoria)
        {
            List<categoriaEntity> result = new List<categoriaEntity>();
            dbHelper db = new dbHelper();
            long id = 0;
            try
            {
                db.agregarParametro("@nombre", SqlDbType.VarChar, categoria.nombre);
                db.agregarParametro("@descripcion", SqlDbType.VarChar, categoria.descripcion);
                db.agregarParametro("@activo", SqlDbType.Bit, categoria.activo);
                db.agregarParametro("@departamentoId", SqlDbType.VarChar, categoria.departamentoId);
                db.agregarParametro("@id", SqlDbType.BigInt, ParameterDirection.Output);
                db.consultaOutput("categoria_nuevo");
                id = long.Parse(db.diccionarioOutput["@id"].ToString());
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch
            {
                db.conexion.Close();
                db.conexion.Dispose();

            }
            
            return id;
        }

        public long? categoria_edita(categoriaEntity categoria)
        {
            List<categoriaEntity> result = new List<categoriaEntity>();
            dbHelper db = new dbHelper();

            try
            {
                db.agregarParametro("@nombre", SqlDbType.VarChar, categoria.nombre);
                db.agregarParametro("@descripcion", SqlDbType.VarChar, categoria.descripcion);
                db.agregarParametro("@activo", SqlDbType.Bit, categoria.activo);
                db.agregarParametro("@departamentoId", SqlDbType.BigInt, categoria.departamentoId);
                db.agregarParametro("@id", SqlDbType.BigInt, categoria.id);
                db.consultaSinRetorno("categoria_edita");
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }
            
            return categoria.id;
        }

    }
}