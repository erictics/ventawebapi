﻿using AppWCFService.Entity;
using AppWCFService.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace AppWCFService.Controllers
{
    public class ventaDetalleController
    {
       // articuloController articuloController = new articuloController();
        unidadController unidadController = new unidadController();



        public long? venta_agregaItemLista(List<ventaDetalleEntity> articulos,int ? op)
        {
            articulos.ForEach(x =>
            {
                dbHelper db = new dbHelper();
                try
                {
                    db.agregarParametro("@ventaId", SqlDbType.BigInt, x.ventaId);
                    db.agregarParametro("@articuloId", SqlDbType.VarChar, x.articuloId);
                    db.consultaSinRetorno("venta_agregaItem");
                    db.conexion.Close();
                    db.conexion.Dispose();
                }
                catch
                {
                    db.conexion.Close();
                    db.conexion.Dispose();
                }
            });
           


            return 1;
        }

        public long ? venta_agregaItem(long? ventaId, long ? articuloId)
        {
            
            dbHelper db = new dbHelper();

            try
            {
                db.agregarParametro("@ventaId", SqlDbType.BigInt, ventaId);
                db.agregarParametro("@articuloId", SqlDbType.VarChar, articuloId);
                db.consultaSinRetorno("venta_agregaItem");
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }
            
            
            return ventaId;
        }


        public long? venta_agregaItemCredito(long? ventaId, long? articuloId,int ? meses, decimal ? precio)
        {

            dbHelper db = new dbHelper();

            try
            {
                db.agregarParametro("@ventaId", SqlDbType.BigInt, ventaId);
                db.agregarParametro("@articuloId", SqlDbType.VarChar, articuloId);
                db.agregarParametro("@mes", SqlDbType.VarChar, meses);
                db.agregarParametro("@precio", SqlDbType.VarChar, precio);
                db.consultaSinRetorno("venta_agregaItemCredito");
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }


            return ventaId;
        }


        public int  venta_eliminaItem(long? ventaId, long? articuloId,int ? op)
        {

            dbHelper db = new dbHelper();
            try
            {
                db.agregarParametro("@ventaId", SqlDbType.BigInt, ventaId);
                db.agregarParametro("@articuloId", SqlDbType.VarChar, articuloId);
                db.agregarParametro("@op", SqlDbType.Int, op);
                db.consultaSinRetorno("venta_eliminaItem");
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }
           
            return 1;
        }

        

        public List<articuloEntity> venta_obtenerItems(long ? ventaId)
        {
            dbHelper db = new dbHelper();
            List<articuloEntity> result = new List<articuloEntity>();
            SqlDataReader reader = null;
            try
            {

                db.agregarParametro("@ventaId", SqlDbType.BigInt, ventaId);
                reader = db.consultaReader("venta_obtenerItems");
                result = db.MapDataToEntityCollection<articuloEntity>(reader).ToList();
                result.ForEach(x =>
                {
                    x.precio = x.precioLista.ToString("C");
                    x.total = (x.precioLista * x.cantidad).ToString("C");
                });
                //result.ForEach(x=> { x.unidad= unidadController.unidad_obten(x.unidadId); });
                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch
            {
                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();

            }
            
            return result;
        }

        public List<articuloEntity> venta_obtenerItemsvendidos(long? ventaId)
        {
            dbHelper db = new dbHelper();
            List<articuloEntity> result = new List<articuloEntity>();
            SqlDataReader reader = null;
            try
            {
                db.agregarParametro("@ventaId", SqlDbType.BigInt, ventaId);
                reader = db.consultaReader("venta_obtenerItemsvendidos");
                result = db.MapDataToEntityCollection<articuloEntity>(reader).ToList();
                result.ForEach(x =>
                {
                    x.precio = x.precioLista.ToString("C");
                    x.total = (x.precioLista * x.cantidad).ToString("C");
                    x.unidad = unidadController.unidad_obten(x.unidadId);
                });
                //result.ForEach(x=> { ; });
                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch(Exception EX)
            {
                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }

           
            return result;
        }

    }
}