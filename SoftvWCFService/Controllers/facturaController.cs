﻿using AppWCFService.Entity;
//using AppWCFService.ServiceReference1;
using AppWCFService.Services;
using AppWCFService.Utils;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Xml;
using System.Xml.Serialization;
using Facturapi;
using System.Threading.Tasks;

namespace AppWCFService.Controllers
{


    public class facturaController
    {

        //public bool? GetActivarFacturacion(int? op)
        //{
        //    bool? activo = false;
        //    try
        //    {
        //        dbHelper db = new dbHelper();
        //        db.agregarParametro("@activo", SqlDbType.Int,op );
        //        db.consultaSinRetorno("ActivarFacturacion");              
        //    }
        //    catch (Exception ex)
        //    { }

        //    return activo;
        //}

        //public bool ? GetFacturacionActiva()
        //{
        //    bool? activo = false;
        //    try
        //    {
        //        dbHelper db = new dbHelper();
        //        db.agregarParametro("@activo", SqlDbType.Bit, ParameterDirection.Output);
        //        db.consultaOutput("facturacionActiva");

        //        activo = bool.Parse(db.diccionarioOutput["@activo"].ToString());
              
        //    }
        //    catch (Exception ex)
        //    {}

        //    return activo;
        //}

        public ResultEntity GetValidaGeneraFactura(long ? ventaId)
        {

            ResultEntity result = new ResultEntity();
            try
            {
                dbHelper db = new dbHelper();
                db.agregarParametro("@ventaId", SqlDbType.VarChar, ventaId);
                db.agregarParametro("@ErrorCode", SqlDbType.Int, ParameterDirection.Output);
                db.agregarParametro("@Message", SqlDbType.VarChar, ParameterDirection.Output);
                db.consultaOutput("venta_validaGeneraFactura");

                result.ErrorCode =int.Parse(db.diccionarioOutput["@ErrorCode"].ToString());
                result.Message=(db.diccionarioOutput["@Message"].ToString());



            }
            catch(Exception ex) {

                result.ErrorCode = 1;
                result.Message = ex.Message.ToString();

            }

            return result;

        }
      

        public List<ProductoSAT> GetCatalogoProductosSAT(string search)
        {


            List<ProductoSAT> result = GetProductosSAT(search).Result;
            return result;

        }

        public List<ProductoSAT> GetCatalogoUnidadProductosSAT(string search)
        {


            List<ProductoSAT> result = GetUnidadProductosSAT(search).Result;
            return result;

        }


        public ProductoSAT AddProductoFactura(long? id)
        {


            ProductoSAT result = GetAddProductoFactura(id).Result;
            return result;

        }

        public ProductoSAT UpdateProductoFactura(long? id)
        {


            ProductoSAT result = GetUpdateProductoFactura(id).Result;
            return result;

        }

        public ProductoSAT AddClienteFactura(long? id)
        {


            ProductoSAT result = GetAddClienteFactura(id).Result;
            return result;

        }

        public ProductoSAT AddFactura(long? id,long ? usuarioId)
        {


            ProductoSAT result = GetAddFactura(id, usuarioId).Result;
            return result;

        }

        public  ProductoSAT GetCancelaFactura(string FacturaId, long? usuarioId)
        {
            ProductoSAT result = GetCancelarFactura(FacturaId, usuarioId).Result;
            return result;

        }

        public ProductoSAT UpdateClienteFactura(long? id)
        {


            ProductoSAT result = GetUpdateClienteFactura(id).Result;
            return result;

        }

        public ProductoSAT DescargaFactura(string id)
        {


            ProductoSAT result = GetDescargaFactura(id).Result;
            return result;

        }

        public ProductoSAT EnviaFactura(long? clienteId, string facturaId)
        {

            ProductoSAT result = GetEnviaFactura(clienteId, facturaId).Result;
            return result;
        }




        public List<facturaEntity> factura_obtenLista(string rfc, string fecha, string serie, string folio,string nombre,int ? op,int ? usuarioId,int ? sucursalId)
        {
            clienteController cliente = new clienteController();
            usuarioController usuario = new usuarioController();
            ventaController venta = new ventaController();

            List<facturaEntity> result = new List<facturaEntity>();
            dbHelper db = new dbHelper();
            SqlDataReader reader = null;
            try
            {
                db.agregarParametro("@rfc", SqlDbType.VarChar, rfc);
                db.agregarParametro("@fecha", SqlDbType.VarChar, fecha);
                db.agregarParametro("@serie", SqlDbType.VarChar, serie);
                db.agregarParametro("@folio", SqlDbType.VarChar, folio);
                db.agregarParametro("@nombre", SqlDbType.VarChar, nombre);
                db.agregarParametro("@op", SqlDbType.BigInt, op);
                db.agregarParametro("@usuarioId", SqlDbType.BigInt, usuarioId);
                db.agregarParametro("@sucursalId", SqlDbType.BigInt, sucursalId);
                
                reader = db.consultaReader("factura_ObtenLista");
                result = db.MapDataToEntityCollection<facturaEntity>(reader).ToList();
                result.ForEach(x => {
                    x.totalFormato = x.total.ToString("C");
                    try
                    {
                        x.usuario = usuario.usuario_obten(x.usuarioId);
                    }
                    catch
                    {

                    }

                    try
                    {
                        x.venta = venta.venta_obten(x.ventaId);
                    }
                    catch
                    {

                    }
                    try
                    {
                        x.cliente = cliente.cliente_obtenLista("","").Where(o=> o.identificador== x.clienteId).First();
                    }
                    catch
                    {

                    }

                    x.fechaFormato = x.fecha.ToShortDateString();
                   
                });


                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch(Exception ex)
            {

                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }




            return result;
        }




        public async Task<List<ProductoSAT>> GetProductosSAT(string search)
        {
            var facturapi = new FacturapiClient("sk_test_3xy9GLX2l1dOaAYAP3RdKOm8aBDZQpgq");
            var searchResult = await facturapi.Catalog.SearchProductsAsync(
              new Dictionary<string, object>
              {
                  ["q"] = search
              }
            );

            List<ProductoSAT> lista = searchResult.Data.Select(s => new ProductoSAT { key = s.Key, description = s.Description }).ToList();
            return lista;
        }

        public async Task<List<ProductoSAT>> GetUnidadProductosSAT(string search)
        {
            var facturapi = new FacturapiClient("sk_test_3xy9GLX2l1dOaAYAP3RdKOm8aBDZQpgq");
            var searchResult = await facturapi.Catalog.SearchUnitsAsync(
              new Dictionary<string, object>
              {
                  ["q"] = search
              }
            );

            List<ProductoSAT> lista = searchResult.Data.Select(s => new ProductoSAT { key = s.Key, description = s.Description }).ToList();
            return lista;
        }

        public async Task<ProductoSAT> GetAddProductoFactura(long? id)
        {
            ProductoSAT result = new ProductoSAT();
            try
            {
                articuloController articuloController = new articuloController();
                articuloEntity articulo = articuloController.articulo_obten(id);
                var facturapi = new FacturapiClient("sk_test_3xy9GLX2l1dOaAYAP3RdKOm8aBDZQpgq");
                var product = await facturapi.Product.CreateAsync(new Dictionary<string, object>
                {
                    ["description"] = articulo.nombre,
                    ["product_key"] = articulo.sat_clvProdServ,
                    ["price"] = articulo.precioLista,
                    ["sku"] = articulo.upc,
                    ["tax_included"] = true,
                    ["unit_key"] = articulo.unidad.sat_clvUnidad

                });


                result.key = product.Id;
                articulo.identificador = product.Id;
                articuloController.articulo_edita(articulo);
            }
            catch (Exception ex)
            {
                result.key = "0";
                result.description = ex.InnerException.ToString();

            }

            return result;
        }

        public async Task<ProductoSAT> GetUpdateProductoFactura(long? id)
       {


            ProductoSAT result = new ProductoSAT();
            try
            {
                articuloController articuloController = new articuloController();
                articuloEntity articulo = articuloController.articulo_obten(id);
                var facturapi = new FacturapiClient("sk_test_3xy9GLX2l1dOaAYAP3RdKOm8aBDZQpgq");

                var articulos = await facturapi.Product.ListAsync();

                if (articulos.Data.Any(x => x.Id == articulo.identificador))
                {

                    var product = await facturapi.Product.UpdateAsync(articulo.identificador, new Dictionary<string, object>
                    {
                        ["description"] = articulo.nombre,
                        ["product_key"] = articulo.sat_clvProdServ,
                        ["price"] = articulo.precioLista,
                        ["sku"] = articulo.upc,
                        ["tax_included"] = true,
                        ["unit_key"] = articulo.unidad.sat_clvUnidad

                    });

                    result.key = product.Id;
                }
                else
                {

                    var product = await facturapi.Product.CreateAsync(new Dictionary<string, object>
                    {
                        ["description"] = articulo.nombre,
                        ["product_key"] = articulo.sat_clvProdServ,
                        ["price"] = articulo.precioLista,
                        ["sku"] = articulo.upc,
                        ["tax_included"] = true,
                        ["unit_key"] = articulo.unidad.sat_clvUnidad

                    });


                    result.key = product.Id;
                    articulo.identificador = product.Id;
                    articuloController.articulo_edita(articulo);
                }




            }
            catch (Exception ex)
            {
                result.key = "0";
                result.description = ex.Message.ToString();

            }

            return result;
        }

        public async Task<ProductoSAT> GetAddClienteFactura(long? id)
        {

            ProductoSAT result = new ProductoSAT();
            try
            {
                clienteController clienteController = new clienteController();
                clienteEntity cliente = clienteController.cliente_obten(id);

                var facturapi = new FacturapiClient("sk_test_3xy9GLX2l1dOaAYAP3RdKOm8aBDZQpgq");
                var customer = await facturapi.Customer.CreateAsync(new Dictionary<string, object>
                {
                    ["legal_name"] = cliente.razonSocial,
                    ["email"] = cliente.email,
                    ["tax_id"] = cliente.rfc,
                    ["address"] = new Dictionary<string, object>
                    {
                        ["street"] = cliente.calle.ToString(),
                        ["exterior"] = cliente.numero.ToString(),
                        ["interior"] = cliente.numeroInt.ToString(),
                        ["neighborhood"] = cliente.colonia.ToString(),
                        ["zip"] = cliente.cp.ToString(),
                        ["city"] = cliente.ciudad,
                        ["state"] = cliente.estado,
                    }
                });

                result.key = customer.Id;
                cliente.identificador = customer.Id;
                clienteController.cliente_edita(cliente);
            }
            catch (Exception ex)
            {
                result.key = "0";
                result.description = ex.Message.ToString();
            }

            return result;

        }

        public async Task<ProductoSAT> GetUpdateClienteFactura(long? id)
        {

            ProductoSAT result = new ProductoSAT();
            try
            {
                clienteController clienteController = new clienteController();
                clienteEntity cliente = clienteController.cliente_obten(id);

                var facturapi = new FacturapiClient("sk_test_3xy9GLX2l1dOaAYAP3RdKOm8aBDZQpgq");


                var searchResult = await facturapi.Customer.ListAsync();

                if (searchResult.Data.ToList().Any(x => x.Id == cliente.identificador))
                {
                    var customer = await facturapi.Customer.UpdateAsync(cliente.identificador, new Dictionary<string, object>
                    {
                        ["legal_name"] = cliente.razonSocial,
                        ["email"] = cliente.email,
                        ["tax_id"] = cliente.rfc,
                        ["address"] = new Dictionary<string, object>
                        {
                            ["street"] = cliente.calle,
                            ["exterior"] = cliente.numero.ToString(),
                            ["interior"] = cliente.numeroInt.ToString(),
                            ["neighborhood"] = cliente.colonia.ToString(),
                            ["zip"] = cliente.cp.ToString(),
                            ["city"] = cliente.ciudad,
                            ["state"] = cliente.estado,
                        }
                    });

                    result.key = customer.Id;
                }
                else
                {




                    var customer = await facturapi.Customer.CreateAsync(new Dictionary<string, object>
                    {
                        ["legal_name"] = cliente.razonSocial,
                        ["email"] = cliente.email,
                        ["tax_id"] = cliente.rfc,
                        ["address"] = new Dictionary<string, object>
                        {
                            ["street"] = cliente.calle,
                            ["exterior"] = cliente.numero.ToString(),
                            ["interior"] = cliente.numero.ToString(),
                            ["neighborhood"] = cliente.colonia,
                            ["zip"] = cliente.cp.ToString(),
                            ["city"] = cliente.ciudad,
                            ["state"] = cliente.estado,
                        }
                    });

                    result.key = customer.Id;
                    cliente.identificador = customer.Id;
                    clienteController.cliente_edita(cliente);
                }



            }
            catch (Exception ex)
            {
                result.key = "0";
                result.description = ex.Message.ToString();
            }

            return result;

        }

        public async Task<ProductoSAT> GetAddFactura(long? ventaId, long? usuarioId)
        {

            ventaController ventaController = new ventaController();
            ventaDetalleController ventaDetalleController = new ventaDetalleController();
            ventaEntity venta = ventaController.venta_obten(ventaId.Value);
            ProductoSAT result = new ProductoSAT();
            try
            {

                List<articuloEntity> articulos = ventaDetalleController.venta_obtenerItemsvendidos(ventaId);

                var items = new Dictionary<string, object>[articulos.Count];

                int index = 0;
                articulos.ForEach(x =>
                {

                    var i = new Dictionary<string, object>
                    {
                        ["quantity"] = x.cantidad,
                        ["product"] = x.identificador
                    };

                    items[index] = i;

                    index = index + 1;
                });

                var facturapi = new FacturapiClient("sk_test_3xy9GLX2l1dOaAYAP3RdKOm8aBDZQpgq");
                var invoice = await facturapi.Invoice.CreateAsync(new Dictionary<string, object>
                {
                    ["customer"] = venta.cliente.identificador,
                    ["items"] = items,
                    ["payment_form"] = venta.fomaPagoId,
                    ["folio_number"] = venta.consecutivo,
                    ["series"] = venta.sucursal.serie
                });

                result.key = invoice.Id;


                dbHelper db = new dbHelper();
                db.agregarParametro("@identificador", SqlDbType.VarChar, invoice.Id);
                db.agregarParametro("@UUID", SqlDbType.VarChar, invoice.Uuid);
                db.agregarParametro("@clienteId", SqlDbType.VarChar, invoice.Customer.Id);
                db.agregarParametro("@usuarioId", SqlDbType.BigInt, usuarioId);
                db.agregarParametro("@ventaId", SqlDbType.BigInt, ventaId);
                db.agregarParametro("@status", SqlDbType.VarChar, invoice.Status);
                db.agregarParametro("@total", SqlDbType.Decimal, invoice.Total);
                db.agregarParametro("@fecha", SqlDbType.Date, invoice.CreatedAt);
                db.agregarParametro("@serie", SqlDbType.VarChar, venta.sucursal.serie);
                db.agregarParametro("@folio", SqlDbType.VarChar, venta.consecutivo);
                db.agregarParametro("@rfc", SqlDbType.VarChar, venta.cliente.rfc);
                db.consultaSinRetorno("factura_nuevo");
                db.conexion.Close();
                db.conexion.Dispose();


            }
            catch (Exception ex)
            {
                
                result.key = "0";
                result.description = ex.Message;
            }

            return result;

        }

        public async Task<ProductoSAT> GetCancelarFactura(string FacturaId, long? usuarioId)
        {

            ProductoSAT result = new ProductoSAT();
            try
            {
               var facturapi = new FacturapiClient("sk_test_3xy9GLX2l1dOaAYAP3RdKOm8aBDZQpgq");
                var invoice = await facturapi.Invoice.CancelAsync(FacturaId);
              


                dbHelper db = new dbHelper();
                db.agregarParametro("@facturaId", SqlDbType.VarChar, invoice.Id);
                db.agregarParametro("@usuarioId", SqlDbType.BigInt, usuarioId);
                db.agregarParametro("@status", SqlDbType.VarChar, invoice.Status);
                db.consultaSinRetorno("factura_cancela");
                db.conexion.Close();
                db.conexion.Dispose();

                result.key = "0";

            }
            catch (Exception ex)
            {

                result.key = "1";
                result.description = ex.Message;

            }

            return result;

        }


        public async Task<ProductoSAT> GetDescargaFactura(string id)
        {

            var facturapi = new FacturapiClient("sk_test_3xy9GLX2l1dOaAYAP3RdKOm8aBDZQpgq");
           
            // Descargar sólo el PDF
            var pdfStream = await facturapi.Invoice.DownloadPdfAsync(id);

            string apppath = System.AppDomain.CurrentDomain.BaseDirectory;
            string name = Guid.NewGuid().ToString() + ".pdf";
            string fileName = apppath + "/Reportes/" + name;

            // Para guardar la descarga en un archivo
            var file = new System.IO.FileStream(fileName, FileMode.Create);
            pdfStream.CopyTo(file);
            file.Close();


            ProductoSAT result = new ProductoSAT();
            result.key = "0";
            result.description = name;
            return result;
        }

        public async Task<ProductoSAT> GetEnviaFactura(long ? clienteId,string facturaId )
        {
            var facturapi = new FacturapiClient("sk_test_3xy9GLX2l1dOaAYAP3RdKOm8aBDZQpgq");

            clienteController clientectrl = new clienteController();
            clienteEntity cliente = clientectrl.cliente_obten(clienteId);

            var emails = cliente.email.Split(';');
            await facturapi.Invoice.SendByEmailAsync(
            facturaId,
            new Dictionary<string, object>
            {
            ["email"] = emails
            }
            );

            ProductoSAT result = new ProductoSAT();
            return result;
        }






    }
}