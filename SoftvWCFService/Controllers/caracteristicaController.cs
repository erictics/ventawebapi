﻿using AppWCFService.Entity;
using AppWCFService.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace AppWCFService.Controllers
{
    public class caracteristicaController
    {


        

       public caracteristicaEntity caracteristica_obten(long id)
        {
            caracteristicaEntity result = new caracteristicaEntity();
            dbHelper db = new dbHelper();
            SqlDataReader reader = null;
            try
            {
                db.agregarParametro("@id", SqlDbType.BigInt, id);
                reader = db.consultaReader("caracteristica_obten");
                result = db.MapDataToEntityCollection<caracteristicaEntity>(reader).ToList()[0];
                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch {
                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();

            }

            return result;
        }


        public List<caracteristicaEntity> caracteristica_obtenLista()
        {
            List<caracteristicaEntity> result = new List<caracteristicaEntity>();
            dbHelper db = new dbHelper();
            SqlDataReader reader = null;
            try
            {
               
                 reader = db.consultaReader("caracteristica_obtenLista");
                result = db.MapDataToEntityCollection<caracteristicaEntity>(reader).ToList();
                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch {
                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();

            }           
          
            return result;
        }


        public ResultEntity caracteristica_nuevo(caracteristicaEntity caracteristica)
        {
            ResultEntity result = new ResultEntity();
            dbHelper db = new dbHelper();
            try
            {
                
                db.agregarParametro("@nombre", SqlDbType.VarChar, caracteristica.nombre);
                db.agregarParametro("@activo", SqlDbType.Bit, caracteristica.activo);
                db.consultaSinRetorno("caracteristica_nuevo");    
                db.conexion.Close();
                db.conexion.Dispose();

                result.ErrorCode = 0;
                result.Message = "";
            }
            catch (Exception ex){

                db.conexion.Close();
                db.conexion.Dispose();

                result.ErrorCode = 1;
                result.Message = ex.Message ;
            }

            return result;
        }


        public ResultEntity caracteristica_editar(caracteristicaEntity caracteristica)
        {
            ResultEntity result = new ResultEntity();
            dbHelper db = new dbHelper();
            try
            {
                
                db.agregarParametro("@id", SqlDbType.BigInt, caracteristica.id);
                db.agregarParametro("@nombre", SqlDbType.VarChar, caracteristica.nombre);
                db.agregarParametro("@activo", SqlDbType.Bit, caracteristica.activo);
                db.consultaSinRetorno("caracteristica_editar");
                db.conexion.Close();
                db.conexion.Dispose();

                result.ErrorCode = 0;
                result.Message = "";
            }
            catch (Exception ex){
                db.conexion.Close();
                db.conexion.Dispose();

                result.ErrorCode = 1;
                result.Message = ex.Message;

            }

            return result;
        }



    }
}