﻿using AppWCFService.Entity;
using AppWCFService.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace AppWCFService.Controllers
{
    public class estatusController
    {
        public List<estatusEntity> estatus_obtenLista()
        {
            List<estatusEntity> result = new List<estatusEntity>();
            dbHelper db = new dbHelper();
            SqlDataReader reader = db.consultaReader("estatus_obtenLista");
            result = db.MapDataToEntityCollection<estatusEntity>(reader).ToList();
            reader.Close();
            db.conexion.Close();
            db.conexion.Dispose();
            return result;
        }


        public estatusEntity estatus_obten(long? id)
        {
            estatusEntity result = new estatusEntity();
            dbHelper db = new dbHelper();
            db.agregarParametro("@id", SqlDbType.BigInt, id);
            SqlDataReader reader = db.consultaReader("estatus_obten");
            try
            {
                result = db.MapDataToEntityCollection<estatusEntity>(reader).ToList()[0];
            }
            catch (Exception ex){
                result = null;
            }
            
            reader.Close();
            db.conexion.Close();
            db.conexion.Dispose();
            return result;
        }
    }
}