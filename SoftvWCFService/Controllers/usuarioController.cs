﻿using AppWCFService.Entity;
using AppWCFService.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;

namespace AppWCFService.Controllers
{
    public class usuarioController
    {
        tokenService tokenService = new tokenService();
        //rolController rolController = new rolController();
      

        public List<usuarioEntity> usuario_obtenLista(long sucursalId, long tipoUsuarioId,string  login, string email,long ? usuarioId)
        {
            List<usuarioEntity> result = new List<usuarioEntity>();
            dbHelper db = new dbHelper();
            SqlDataReader reader = null;
            try
            {
                db.agregarParametro("@sucursalId", SqlDbType.BigInt, sucursalId);
                db.agregarParametro("@tipoUsuarioId", SqlDbType.BigInt, tipoUsuarioId);
                db.agregarParametro("@login", SqlDbType.VarChar, login);
                db.agregarParametro("@email", SqlDbType.VarChar, email);
                db.agregarParametro("@usuarioId", SqlDbType.BigInt, usuarioId);
                reader = db.consultaReader("usuario_obtenLista");
                result = db.MapDataToEntityCollection<usuarioEntity>(reader).ToList();
                result.ForEach(x => {
                    x.RoltipoUsuario = GetTipoUsuarioById(x.tipoUsuario);
                });


                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch
            {

                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }
            
             

          
            return result;
        }

        public usuarioEntity usuario_obten(long? id)
        {
            usuarioEntity result = new usuarioEntity();
            dbHelper db = new dbHelper();
            SqlDataReader reader = null;
            try
            {
                db.agregarParametro("@id", SqlDbType.BigInt, id);
                reader = db.consultaReader("usuario_obten");
                result = db.MapDataToEntityCollection<usuarioEntity>(reader).ToList()[0];
                result.RoltipoUsuario = GetTipoUsuarioById(result.tipoUsuario);
                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch (Exception ex)
            {

                result = null;
                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }
           
            return result;
        }

        public ResultEntity usuario_nuevo(usuarioEntity usuario, List<sucursalEntity> sucursales)
        {
            ResultEntity obj = new ResultEntity();
            List<usuarioEntity> usuarios = usuario_obtenLista(0, 0, "", "",0).ToList();
            if(usuarios.Any(x=> x.email.ToLower() == usuario.email.ToLower()))
            {
                obj.ErrorCode = 1;
                obj.Message = "El correo electrónico ya se encuentra registrado";
                return obj;
            }

            if (usuarios.Any(x => x.login.ToLower() == usuario.login.ToLower()))
            {
                obj.ErrorCode = 1;
                obj.Message = "El Login ya se encuentra registrado";
                return obj;
            }

            dbHelper db = new dbHelper();

            try
            {
                List<usuarioEntity> result = new List<usuarioEntity>();
                
                db.agregarParametro("@nombre", SqlDbType.VarChar, usuario.nombre);
                db.agregarParametro("@email", SqlDbType.VarChar, usuario.email);
                db.agregarParametro("@login", SqlDbType.VarChar, usuario.login);
                db.agregarParametro("@tipoUsuario", SqlDbType.BigInt, usuario.tipoUsuario);
                db.agregarParametro("@activo", SqlDbType.Bit, usuario.activo);
                db.agregarParametro("@password", SqlDbType.VarChar, usuario.password);
                db.agregarParametro("@message", SqlDbType.VarChar, ParameterDirection.Output);
                db.agregarParametro("@id", SqlDbType.BigInt, ParameterDirection.Output);
                db.agregarParametro("@ErrorCode", SqlDbType.BigInt, ParameterDirection.Output);
                db.consultaOutput("usuario_nuevo");
                int id = int.Parse(db.diccionarioOutput["@id"].ToString());
                obj.ErrorCode = int.Parse(db.diccionarioOutput["@ErrorCode"].ToString());
                obj.Message = db.diccionarioOutput["@message"].ToString();
                db.conexion.Close();
                db.conexion.Dispose();

                sucursales.ForEach(x => {
                    usuario_usuarioSucursal(id, x.id, 1);
                });

                

            }
            catch (Exception ex)
            {
                obj.ErrorCode = 1;
                obj.Message = ex.InnerException.Message;

                db.conexion.Close();
                db.conexion.Dispose();
            }          
            
            return obj;
        }

        public ResultEntity usuario_edita(usuarioEntity usuario,List<sucursalEntity> sucursales )
        {
            List<usuarioEntity> usuarios = usuario_obtenLista(0, 0, "", "",0).ToList();
            ResultEntity obj = new ResultEntity();
            if (usuarios.Any(x => x.email.ToLower() == usuario.email.ToLower() && x.id != usuario.id ))
            {
                obj.ErrorCode = 1;
                obj.Message = "El correo electrónico ya se encuentra registrado";
                return obj;
            }

            if (usuarios.Any(x => x.login.ToLower() == usuario.login.ToLower() && x.id != usuario.id))
            {
                obj.ErrorCode = 1;
                obj.Message = "El Login ya se encuentra registrado";
                return obj;
            }
            dbHelper db = new dbHelper();
            try
            {
                List<usuarioEntity> result = new List<usuarioEntity>();
              
                db.agregarParametro("@nombre", SqlDbType.VarChar, usuario.nombre);
                db.agregarParametro("@email", SqlDbType.VarChar, usuario.email);
                db.agregarParametro("@login", SqlDbType.VarChar, usuario.login);
                db.agregarParametro("@activo", SqlDbType.Bit, usuario.activo);
                db.agregarParametro("@tipoUsuario", SqlDbType.Int, usuario.tipoUsuario);
                db.agregarParametro("@password", SqlDbType.VarChar, usuario.password);
                db.agregarParametro("@id", SqlDbType.BigInt, usuario.id);
                db.consultaOutput("usuario_edita");
                db.conexion.Close();
                db.conexion.Dispose();

                obj.ErrorCode = 0;
                obj.Message = "Usuario Registrado correctamente";

                usuario_usuarioSucursalElimina(usuario.id);
                sucursales.ForEach(x => {
                    usuario_usuarioSucursal(usuario.id, x.id, 1);
                });
            }
            catch(Exception ex)
            {
                obj.ErrorCode = 1;
                obj.Message = ex.InnerException.Message;

                db.conexion.Close();
                db.conexion.Dispose();
            }
            

            return obj;
        }


        public long? usuario_usuarioSucursal(long usuarioId ,int sucursalId,int op)
        {
            List<usuarioEntity> result = new List<usuarioEntity>();
            dbHelper db = new dbHelper();
            try
            {
                db.agregarParametro("@sucursalId", SqlDbType.BigInt, sucursalId);
                db.agregarParametro("@usuarioId", SqlDbType.BigInt, usuarioId);
                db.agregarParametro("@op", SqlDbType.BigInt, op);
                db.consultaSinRetorno("usuarioSucursal_nuevo");
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }
            
            
            return usuarioId;
        }

        public long? usuario_usuarioSucursalElimina(long usuarioId)
        {
            List<usuarioEntity> result = new List<usuarioEntity>();
            dbHelper db = new dbHelper();
            try
            {
                db.agregarParametro("@usuarioId", SqlDbType.BigInt, usuarioId);
                db.consultaSinRetorno("usuarioSucursal_elimina");
                db.conexion.Close();
                db.conexion.Dispose();
                
            }
            catch
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }
            return usuarioId;

        }

        public List<sucursalEntity> usuarioSucursal_obtenporUsuario(long ? id)
        {
            List<sucursalEntity> result = new List<sucursalEntity>();
            dbHelper db = new dbHelper();
            SqlDataReader reader = null;
            try
            {
                db.agregarParametro("@usuarioId", SqlDbType.BigInt, id);
                reader = db.consultaReader("usuarioSucursal_obtenporUsuario");
                result = db.MapDataToEntityCollection<sucursalEntity>(reader).ToList();
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch(Exception ex )
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }
          
            
            return result;
        }


        public List<usuarioEntity> usuario_autenticacion(string email, string password)
        {
            List<usuarioEntity> result = new List<usuarioEntity>();
            dbHelper db = new dbHelper();
            SqlDataReader reader = null;
            try
            {
                db.agregarParametro("@email", SqlDbType.VarChar, email);
                db.agregarParametro("@password", SqlDbType.VarChar, password);
                 reader = db.consultaReader("usuario_autenticacion");
                result = db.MapDataToEntityCollection<usuarioEntity>(reader).ToList();
                if (result.Count > 0)
                {
                    result[0].token = tokenService.GenerateToken(email);
                    result[0].RoltipoUsuario = GetTipoUsuarioById(result[0].tipoUsuario);
                }
                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();

            }
            catch
            {
                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }
            
            return result;
        }


        public bool ? usuario_autorizacionventa(string email, string password)
        {
            List<usuarioEntity> result = new List<usuarioEntity>();
            dbHelper db = new dbHelper();
            bool valida = false;
            try
            {
                db.agregarParametro("@usuario", SqlDbType.VarChar, email);
                db.agregarParametro("@password", SqlDbType.VarChar, password);
                db.agregarParametro("@acceso", SqlDbType.Bit, ParameterDirection.Output);
                db.consultaOutput("usuario_autorizacionventa");
                valida = bool.Parse(db.diccionarioOutput["@acceso"].ToString());
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }
           
            return valida;
        }



        public List<ModuloEntity> ObtenModulos()
        {
            List<ModuloEntity> modulos = new List<ModuloEntity>();
            dbHelper db = new dbHelper();
            SqlDataReader reader = null;
            try
            {
               
                reader = db.consultaReader("GetModulosPermisos");
                modulos = db.MapDataToEntityCollection<ModuloEntity>(reader).ToList();
                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch (Exception ex)
            {
                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }

       
            modulos.ForEach(x =>
                {
                    try
                    {
                        db = new dbHelper();
                        x.SubModulos = new List<ModuloEntity>();
                        db.agregarParametro("@moduloPadre", SqlDbType.Int, x.Id);
                        reader = db.consultaReader("GetModulosHijosPermisos");
                        x.SubModulos = db.MapDataToEntityCollection<ModuloEntity>(reader).ToList();
                        reader.Close();
                        db.conexion.Close();
                        db.conexion.Dispose();
                    }
                    catch {

                        reader.Close();
                        db.conexion.Close();
                        db.conexion.Dispose();
                    }              
                });

                return modulos;

            }

        public int? GuardaPermisos(List<ModuloEntity> modulos, int? tipoUsuario)
        {
            int result = 0;
                StringBuilder sb = new StringBuilder();
                sb.Append("<permisos>");
                modulos.ForEach(sub =>
                {
                    sb.Append("<permiso>");
                    sb.Append("<tipousuario>" + tipoUsuario + "</tipousuario>");
                    sb.Append("<modulo>" + sub.Id + "</modulo>");
                    sb.Append("<add>" + 1 + "</add>");
                    sb.Append("<edit>" + 1 + "</edit>");
                    sb.Append("<delete>" + 1 + "</delete>");
                    sb.Append("</permiso>");


                    sub.SubModulos.ForEach(x => {
                        sb.Append("<permiso>");
                        sb.Append("<tipousuario>" + tipoUsuario + "</tipousuario>");
                        sb.Append("<modulo>" + x.Id + "</modulo>");
                        sb.Append("<add>" + x.Crear + "</add>");
                        sb.Append("<edit>" + x.Editar + "</edit>");
                        sb.Append("<delete>" + x.Eliminar + "</delete>");
                        sb.Append("<access>" + x.Consultar + "</access>");
                        sb.Append("</permiso>");
                    });
                });
                sb.Append("</permisos>");

            dbHelper db = new dbHelper();
            try
            {
                db.agregarParametro("@xml", SqlDbType.VarChar, sb.ToString());
                db.agregarParametro("@tipoUsuario", SqlDbType.Int, tipoUsuario);
                db.consultaSinRetorno("GuardaPermisos");
                db.conexion.Close();
                db.conexion.Dispose();
                result = 1;
            }
            catch
            {
                db.conexion.Close();
                db.conexion.Dispose();

            }   

            return result;            
        }

        public ModuloEntity GetPermisoAccesoUsuario(int ? tipoUsuario, string componente)
        {
            ModuloEntity result = new ModuloEntity();
            dbHelper db = new dbHelper();
            SqlDataReader reader = null;
            try
            {
                db.agregarParametro("@tipoUsuario", SqlDbType.Int, tipoUsuario);
                db.agregarParametro("@componente", SqlDbType.VarChar, componente);
                 reader = db.consultaReader("GetPermisoAccesoUsuario");
                result = db.MapDataToEntityCollection<ModuloEntity>(reader).ToList()[0];
                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch
            {
                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }

            
            return result;

        }

        public List<ModuloEntity> ObtenModulosUsuario(int? tipoUsuario, int? op)
        {
            List<ModuloEntity> modulos = new List<ModuloEntity>();
            dbHelper db = new dbHelper();
            SqlDataReader reader = null;
              try
                {                
                 
                db.agregarParametro("@tipoUsuario", SqlDbType.Int, tipoUsuario);
                 reader= db.consultaReader("GetModulosPrincipales");
                modulos = db.MapDataToEntityCollection<ModuloEntity>(reader).ToList();
                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }
                catch (Exception ex)
                {
                reader.Close();
                db.conexion.Close();
                    db.conexion.Dispose();
                 }

                modulos.ForEach(x =>
                {
                    try
                    {
                         db = new dbHelper();
                        x.SubModulos = new List<ModuloEntity>();                        
                        db.agregarParametro("@tipoUsuario", SqlDbType.Int, tipoUsuario);
                        db.agregarParametro("@moduloPadre", SqlDbType.Int, x.Id);
                         reader = db.consultaReader("GetModulosHijos");
                        x.SubModulos = db.MapDataToEntityCollection<ModuloEntity>(reader).ToList();
                        reader.Close();
                        db.conexion.Close();
                        db.conexion.Dispose();
                    }
                    catch (Exception ex)
                    {
                        reader.Close();
                        db.conexion.Close();
                        db.conexion.Dispose();
                    }
                });
                return modulos;
        }

        public List<tipoUsuarioEntity> GetTiposUsuario()
        {

            List<tipoUsuarioEntity> result = new List<tipoUsuarioEntity>();
            dbHelper db = new dbHelper();
            SqlDataReader reader = null;
            try
            {
                reader = db.consultaReader("tipoUsuario_ObtenLista");
                result = db.MapDataToEntityCollection<tipoUsuarioEntity>(reader).ToList();
                if (result.Count > 0)
                {
                    // result[0].token = tokenService.GenerateToken(email);
                    // result[0].rol = rolController.rol_obten(result[0].rolId);
                }
                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch
            {
                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }
           
           
            return result;
        }

        public int? AddTipoUsuario(tipoUsuarioEntity obj)
        {
            if (GetTiposUsuario().Any(x => x.Nombre.ToLower() == obj.Nombre.ToLower()))
            {
                return -1;
            }
            List<usuarioEntity> result = new List<usuarioEntity>();
            dbHelper db = new dbHelper();

            try
            {
                db.agregarParametro("@nombre", SqlDbType.VarChar, obj.Nombre);
                db.agregarParametro("@activo", SqlDbType.Bit, obj.Activo);
                db.agregarParametro("@CancelaVenta", SqlDbType.Bit, obj.CancelaVenta);
                db.agregarParametro("@ReimprimeTicket", SqlDbType.Bit, obj.ReimprimeTicket);
                db.consultaSinRetorno("tipoUsuario_nuevo");
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }
            
            return 1;

        }

        public int? UpdateTiposUsuario(tipoUsuarioEntity obj)
        {
            if (GetTiposUsuario().Any(x => x.Nombre.ToLower() == obj.Nombre.ToLower() && x.Id != obj.Id ) )
            {
                return -1;
            }


            List<usuarioEntity> result = new List<usuarioEntity>();
            dbHelper db = new dbHelper();
            try
            {
                db.agregarParametro("@nombre", SqlDbType.VarChar, obj.Nombre);
                db.agregarParametro("@activo", SqlDbType.Bit, obj.Activo);
                db.agregarParametro("@id", SqlDbType.BigInt, obj.Activo);
                db.agregarParametro("@CancelaVenta", SqlDbType.Bit, obj.CancelaVenta);
                db.agregarParametro("@ReimprimeTicket", SqlDbType.Bit, obj.ReimprimeTicket);
                db.consultaSinRetorno("tipoUsuario_edita");
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch
            {
                db.conexion.Close();
                db.conexion.Dispose();

            }
            
            return 1;

        }

        public tipoUsuarioEntity GetTipoUsuarioById(long ? Id)
        {
            tipoUsuarioEntity result = new tipoUsuarioEntity();
            dbHelper db = new dbHelper();

            try
            {
                db.agregarParametro("@id", SqlDbType.BigInt, Id);
                SqlDataReader reader = db.consultaReader("tipoUsuario_obten");
                result = db.MapDataToEntityCollection<tipoUsuarioEntity>(reader).ToList()[0];
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }
          
            
            return result;

        }

    }
}