﻿using AppWCFService.Entity;
using AppWCFService.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace AppWCFService.Controllers
{
    public class retiroEfectivoController
    {
        usuarioController usuarioController =new usuarioController();

        public List<retiroEfectivoEntity> retiroEfectivo_obtenLista(long? cajeroId, long? sucursalId, string fechaInicio, string fechaFin)
        {
            List<retiroEfectivoEntity> result = new List<retiroEfectivoEntity>();
            dbHelper db = new dbHelper();
            db.agregarParametro("@cajeroId", SqlDbType.BigInt, cajeroId);
            db.agregarParametro("@sucursalId", SqlDbType.BigInt, sucursalId);
            db.agregarParametro("@fechaInicio", SqlDbType.VarChar, fechaInicio);
            db.agregarParametro("@fechaFin", SqlDbType.VarChar, fechaFin);
            SqlDataReader reader = db.consultaReader("retiroEfectivo_obtenLista");
            result = db.MapDataToEntityCollection<retiroEfectivoEntity>(reader).ToList();
            result.ForEach(i=> {
                i.cajero = usuarioController.usuario_obten(i.cajeroId);
                i.usuario = usuarioController.usuario_obten(i.usuarioAutorizador);
                i.fechaCorta = i.fecha.ToShortDateString();
                i.hora = i.fecha.ToShortTimeString();
            });
            reader.Close();
            db.conexion.Close();
            db.conexion.Dispose();
            return result;
        }

        public retiroEfectivoEntity retiroEfectivo_obten(long? id)
        {
            retiroEfectivoEntity result = new retiroEfectivoEntity();
            dbHelper db = new dbHelper();
            db.agregarParametro("@id", SqlDbType.BigInt, id);
            SqlDataReader reader = db.consultaReader("retiroEfectivo_obten");
            try
            {
                result = db.MapDataToEntityCollection<retiroEfectivoEntity>(reader).ToList()[0];
                result.cajero = usuarioController.usuario_obten(result.cajeroId);
                result.usuario = usuarioController.usuario_obten(result.usuarioAutorizador);
                result.fechaCorta = result.fecha.ToShortDateString();
                result.hora = result.fecha.ToShortTimeString();
            }
            catch (Exception ex)
            {
                result = null;
            }
            reader.Close();
            db.conexion.Close();
            db.conexion.Dispose();
            return result;
        }


        public long? retiroEfectivo_nuevo(retiroEfectivoEntity retiroEfectivo)
        {
            List<retiroEfectivoEntity> result = new List<retiroEfectivoEntity>();
            dbHelper db = new dbHelper();
            db.agregarParametro("@usuarioAutorizador", SqlDbType.VarChar, retiroEfectivo.usuarioAutorizador);
            db.agregarParametro("@cajeroId", SqlDbType.Int, retiroEfectivo.cajeroId);
            db.agregarParametro("@cantidad", SqlDbType.Decimal, retiroEfectivo.cantidad);
            db.agregarParametro("@cajaId", SqlDbType.BigInt, retiroEfectivo.cajaId);          
            db.agregarParametro("@id", SqlDbType.BigInt, ParameterDirection.Output);
            db.consultaOutput("retiroEfectivo_nuevo");
            long id = long.Parse(db.diccionarioOutput["@id"].ToString());
            db.conexion.Close();
            db.conexion.Dispose();
            return id;
        }

        //public long? cliente_edita(retiroEfectivoEntity cliente)
        //{
        //    List<retiroEfectivoEntity> result = new List<retiroEfectivoEntity>();
        //    dbHelper db = new dbHelper();
        //    db.agregarParametro("@nombre", SqlDbType.VarChar, cliente.nombre);
        //    db.agregarParametro("@rfc", SqlDbType.VarChar, cliente.rfc);
        //    db.agregarParametro("@apellidos", SqlDbType.VarChar, cliente.apellidos);
        //    db.agregarParametro("@cp", SqlDbType.BigInt, cliente.cp);
        //    db.agregarParametro("@email", SqlDbType.VarChar, cliente.email);
        //    db.agregarParametro("@colonia", SqlDbType.VarChar, cliente.colonia);
        //    db.agregarParametro("@calle", SqlDbType.VarChar, cliente.calle);
        //    db.agregarParametro("@numero", SqlDbType.Int, cliente.numero);
        //    db.agregarParametro("@ciudad", SqlDbType.VarChar, cliente.ciudad);
        //    db.agregarParametro("@estado", SqlDbType.VarChar, cliente.estado);
        //    db.agregarParametro("@razonSocial", SqlDbType.VarChar, cliente.razonSocial);
        //    db.agregarParametro("@telefono", SqlDbType.VarChar, cliente.telefono);
        //    db.agregarParametro("@id", SqlDbType.BigInt, cliente.id);
        //    db.consultaSinRetorno("cliente_edita");
        //    db.conexion.Close();
        //    db.conexion.Dispose();
        //    return cliente.id;
        //}
    }
}