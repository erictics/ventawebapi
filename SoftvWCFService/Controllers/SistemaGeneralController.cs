﻿using AppWCFService.Entity;
using AppWCFService.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;

namespace AppWCFService.Controllers
{
    public class SistemaGeneralController
    {

        public int? sistemaGeneral_edita(sistemaGeneralEntity sistema)
        {
            dbHelper db = new dbHelper();
            try
            {
                db.agregarParametro("@nombreSistema", SqlDbType.VarChar, sistema.nombreSistema);
                db.agregarParametro("@empresa", SqlDbType.VarChar, sistema.empresa);
                db.agregarParametro("@usuariosSistema", SqlDbType.BigInt, sistema.usuariosSistema);
                db.agregarParametro("@licencia", SqlDbType.VarChar, sistema.licencia);
                db.agregarParametro("@sucursalesSistema", SqlDbType.BigInt, sistema.sucursalesSistema);
                db.agregarParametro("@emailSistema", SqlDbType.VarChar, sistema.emailSistema);
                db.agregarParametro("@facturacion", SqlDbType.Bit, sistema.facturacion);
                db.agregarParametro("@envioticket", SqlDbType.Bit, sistema.envioticket);
                db.agregarParametro("@ventaCredito", SqlDbType.Bit, sistema.ventaCredito);
                db.agregarParametro("@ticket", SqlDbType.VarChar, sistema.ticket);
                db.consultaSinRetorno("sistemaGeneral_edita");
                return 1;
            }
            catch
            {
                db.conexion.Close();
                db.conexion.Dispose();
                return 0;
            }
        }


        public int? correo_nuevo(correosEntity correo)
        {
            dbHelper db = new dbHelper();
            try
            {
                db.agregarParametro("@sucursalId", SqlDbType.BigInt, correo.sucursalId);
                db.agregarParametro("@email", SqlDbType.VarChar, correo.email);
                db.agregarParametro("@password", SqlDbType.VarChar, correo.password);
                db.agregarParametro("@smtp", SqlDbType.VarChar, correo.smtp);
                db.agregarParametro("@puerto", SqlDbType.VarChar, correo.puerto);
                db.consultaSinRetorno("correo_nuevo");
                return 1;
            }
            catch
            {
                db.conexion.Close();
                db.conexion.Dispose();
                return 0;
            }
        }

        public int? correo_elimina(long? sucursalId)
        {
            dbHelper db = new dbHelper();
            try
            {
                db.agregarParametro("@sucursalId", SqlDbType.Int, sucursalId);
                db.consultaSinRetorno("correo_elimina");
                return 1;
            }
            catch
            {
                db.conexion.Close();
                db.conexion.Dispose();
                return 0;
            }
        }






        public List<correosEntity> correo_obtenLista()
        {
            List<correosEntity> result = new List<correosEntity>();
            dbHelper db = new dbHelper();
            try
            {
                SqlDataReader reader = db.consultaReader("correo_obtenLista");
                result = db.MapDataToEntityCollection<correosEntity>(reader).ToList();

                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();


            }
            catch
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }

            return result;

        }




        public sistemaGeneralEntity sistemaGeneral_obtenLista()
        {
            sistemaGeneralEntity result = new sistemaGeneralEntity();
            dbHelper db = new dbHelper();
            try
            {
                SqlDataReader reader = db.consultaReader("sistemaGeneral_obtenLista");
                result = db.MapDataToEntityCollection<sistemaGeneralEntity>(reader).ToList()[0];

                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();


            }
            catch
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }

            return result;

        }


        public ResultEntity EnviarTicket(long? ventaId, string email_enviar)
        {

            string apppath = System.AppDomain.CurrentDomain.BaseDirectory;
            ventaController vc = new ventaController();
            ventaEntity venta = vc.venta_obten(ventaId.Value);
            ResultEntity res = new ResultEntity();

            correosEntity conf_correo = null;
            try
            {
                conf_correo = correo_obtenLista()[0];
            }
            catch (Exception ex)
            {
                res.ErrorCode = 1;
                res.Message = "No se encontro correo configurado para la sucursal";
                return res;
            }

            try
            {

                SmtpClient smtp = new SmtpClient();
                //"smtp.gmail.com"
                smtp.Host = conf_correo.smtp;
                smtp.Port = int.Parse(conf_correo.puerto);
                smtp.EnableSsl = true;
                smtp.UseDefaultCredentials = false;
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.Credentials = new NetworkCredential(conf_correo.email, conf_correo.password);

                reportes rep = new reportes();
                string archivo = rep.GetTicketVenta(ventaId, 0, false, 0);

                using (Attachment attachment = new Attachment(apppath + "/Reportes/" + archivo))
                {

                    string output = null;
                    MailMessage email = new MailMessage();

                    foreach (var address in email_enviar.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries))
                    {
                        email.To.Add(address);
                    }

                    email.From = new MailAddress(conf_correo.email);
                    email.Subject = "Ticket Cliente";
                    email.Body = "Ticket Cliente";
                    email.Attachments.Add(attachment);
                    email.IsBodyHtml = true;
                    email.Priority = MailPriority.Normal;
                    smtp.Send(email);
                    email.Dispose();

                    res.ErrorCode = 0;
                    res.Message = "Correo enviado Correctamente";
                    return res;
                }

            }
            catch (Exception ex)
            {
                res.ErrorCode = 1;
                res.Message = ex.Message;
                return res;
            }

        }



        public ResultEntity EnviaReporte(string ruta, string email_enviar)
        {

            string apppath = System.AppDomain.CurrentDomain.BaseDirectory;
            ResultEntity res = new ResultEntity();

            correosEntity conf_correo = null;
            try
            {
                conf_correo = correo_obtenLista()[0];
            }
            catch (Exception ex)
            {
                res.ErrorCode = 1;
                res.Message = "No se encontro correo configurado para la sucursal";
                return res;
            }

            try
            {

                SmtpClient smtp = new SmtpClient();
                //"smtp.gmail.com"
                smtp.Host = conf_correo.smtp;
                smtp.Port = int.Parse(conf_correo.puerto);
                smtp.EnableSsl = true;
                smtp.UseDefaultCredentials = false;
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.Credentials = new NetworkCredential(conf_correo.email, conf_correo.password);



                using (Attachment attachment = new Attachment(ruta))
                {

                    string output = null;
                    MailMessage email = new MailMessage();

                    foreach (var address in email_enviar.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries))
                    {
                        email.To.Add(address);
                    }

                    email.From = new MailAddress(conf_correo.email);
                    email.Subject = "Notificacion de Reporte";
                    email.Body = "";
                    email.Attachments.Add(attachment);
                    email.IsBodyHtml = true;
                    email.Priority = MailPriority.Normal;
                    smtp.Send(email);
                    email.Dispose();

                    res.ErrorCode = 0;
                    res.Message = "Correo enviado Correctamente";
                    return res;
                }

            }
            catch (Exception ex)
            {
                res.ErrorCode = 1;
                res.Message = ex.Message;
                return res;
            }

        }


        public ResultEntity EnvioSMS(int? op,string number_to,string number_from, string message_body)
        {

            ResultEntity res = new ResultEntity();
            try
            {

                MailMessage message = new MailMessage();
                message.To.Add("524651038908@txtlocal.co.uk");
                message.From = new MailAddress("erictics@gmail.com");
                message.Body = "mensaje de prueba";


                SmtpClient smtp = new SmtpClient("smtp.gmail.com");
                smtp.Port = 587;
                smtp.EnableSsl = true;
                smtp.UseDefaultCredentials = false;
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.Credentials = new NetworkCredential("erictics@gmail.com", "ch@vez28101993");

                smtp.Send(message);
                //var accountSid = "AC631d03999f46531fd13789bb9ece5eb6";
                //var authToken = "a3c90bb45059f9e6ccb3eb97f5f181a7";
                //TwilioClient.Init(accountSid, authToken);

                //var messageOptions = new CreateMessageOptions(
                //    new PhoneNumber("524651038908"));
                //messageOptions.MessagingServiceSid = "MGeebe56c2a114d3358f417be84308918f";

                //var message = MessageResource.Create(messageOptions);
                //Console.WriteLine(message.Body);

                //string accountSid = Environment.GetEnvironmentVariable("TWILIO_ACCOUNT_SID");
                //string authToken = Environment.GetEnvironmentVariable("TWILIO_AUTH_TOKEN");
                //ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;
                //TwilioClient.Init(accountSid, authToken);

                //var message = MessageResource.Create(
                //    body: message_body,
                //    from: new Twilio.Types.PhoneNumber("15005550006"),
                //    to: new Twilio.Types.PhoneNumber("554651038908")
                //);

            }
            catch(Exception ex)
            {
                res.ErrorCode = 1;
                res.Message = ex.Message;
            }



            return res;
        }


        public ResultEntity AddUsuarioNotificacion(List<long> cortes, List<long> inventarios, List<long> bajasexistencias)
        {
            ResultEntity res = new ResultEntity();
            dbHelper db = new dbHelper();
            try
            {
                //db.agregarParametro("@cortes", SqlDbType.BigInt, correo.sucursalId);
                //db.agregarParametro("@inventarios", SqlDbType.VarChar, correo.email);
                //db.agregarParametro("@bajasexistencias", SqlDbType.VarChar, correo.password);
                //db.consultaSinRetorno("AddUsuarioNotificacion");
                return res;
            }
            catch
            {
                db.conexion.Close();
                db.conexion.Dispose();
                return res;
            }
        }
         

    }
}