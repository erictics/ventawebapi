﻿using AppWCFService.Entity;
using AppWCFService.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace AppWCFService.Controllers
{
    

    public class cajaController
    {
        sucursalController sucursalController = new sucursalController();

        public List<cajaEntity> caja_obtenLista()
        {
            List<cajaEntity> result = new List<cajaEntity>();
            dbHelper db = new dbHelper();           
            SqlDataReader reader = db.consultaReader("caja_obtenLista");
            result = db.MapDataToEntityCollection<cajaEntity>(reader).ToList();
            result.ForEach(y=> {
                y.sucursal = sucursalController.sucursal_obten(y.id);
            });         
            reader.Close();
            db.conexion.Close();
            db.conexion.Dispose();
            return result;
        }

        public cajaEntity caja_obten(long? id)
        {
            cajaEntity result = new cajaEntity();
            dbHelper db = new dbHelper();
            db.agregarParametro("@id", SqlDbType.BigInt, id);
            SqlDataReader reader = db.consultaReader("caja_obten");
            try
            {
                result = db.MapDataToEntityCollection<cajaEntity>(reader).ToList()[0];
                result.sucursal = sucursalController.sucursal_obten(result.sucursalId);
            }
            catch (Exception ex) {
                result = null;
            }            
            reader.Close();
            db.conexion.Close();
            db.conexion.Dispose();
            return result;
        }


        public long? caja_nuevo(cajaEntity caja)
        {
            List<almacenEntity> result = new List<almacenEntity>();
            dbHelper db = new dbHelper();
            db.agregarParametro("@ipMaquina", SqlDbType.VarChar, caja.ipMaquina);
            db.agregarParametro("@nombre", SqlDbType.VarChar, caja.nombre);
            db.agregarParametro("@activo", SqlDbType.Bit, caja.activo);
            db.agregarParametro("@sucursalId", SqlDbType.Int, caja.sucursalId);                
            db.agregarParametro("@id", SqlDbType.BigInt, ParameterDirection.Output);
            db.consultaOutput("caja_nuevo");
            long id = long.Parse(db.diccionarioOutput["@id"].ToString());
            db.conexion.Close();
            db.conexion.Dispose();
            return id;
        }

        public long? caja_edita(cajaEntity caja)
        {
            List<almacenEntity> result = new List<almacenEntity>();
            dbHelper db = new dbHelper();
            db.agregarParametro("@ipMaquina", SqlDbType.VarChar, caja.ipMaquina);
            db.agregarParametro("@nombre", SqlDbType.VarChar, caja.nombre);
            db.agregarParametro("@activo", SqlDbType.Bit, caja.activo);
            db.agregarParametro("@sucursalId", SqlDbType.Int, caja.sucursalId);
            db.agregarParametro("@id", SqlDbType.BigInt, caja.id);
            db.consultaSinRetorno("caja_edita");            
            db.conexion.Close();
            db.conexion.Dispose();
            return caja.id;
        }


    }
}