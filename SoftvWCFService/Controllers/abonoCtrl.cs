﻿using AppWCFService.Entity;
using AppWCFService.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace AppWCFService.Controllers
{
    public class abonoCtrl
    {


        public long? abono_nuevo(abonosEntity abono)
        {
            long id = 0;
            dbHelper db = new dbHelper();
            try
            {

                db.agregarParametro("@fecha", SqlDbType.VarChar, abono.fecha.Replace("-",""));
                db.agregarParametro("@usuarioId", SqlDbType.VarChar, abono.usuarioId);
                db.agregarParametro("@ventaId", SqlDbType.VarChar, abono.ventaId);
                db.agregarParametro("@monto", SqlDbType.VarChar, abono.monto);
                db.agregarParametro("@observaciones", SqlDbType.VarChar, abono.observaciones);
                db.agregarParametro("@id", SqlDbType.BigInt, ParameterDirection.Output);
                db.consultaOutput("abono_nuevo");
                 id = long.Parse(db.diccionarioOutput["@id"].ToString());
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }
           
            
            return id;


        }


        public int?  abono_cancelar(int? id,int ? usuarioId)
        {

           
            dbHelper db = new dbHelper();
            try
            {
                db.agregarParametro("@id", SqlDbType.BigInt, id);                
                db.agregarParametro("@usuarioId", SqlDbType.BigInt, usuarioId);
                db.consultaSinRetorno("abono_cancelar");
                db.conexion.Close();
                db.conexion.Dispose();

            }
            catch
            {

                db.conexion.Close();
                db.conexion.Dispose();

            }

            return id;
        }

        public List<abonosEntity> abono_obtenLista(int ? id,string fecha,int ? sucursalId,string cliente, int ? usuarioId)
        {

            List<abonosEntity> result = new List<abonosEntity>();
            dbHelper db = new dbHelper();
            try
            {

                db.agregarParametro("@id", SqlDbType.BigInt, id);
                db.agregarParametro("@fecha", SqlDbType.VarChar, fecha);
                db.agregarParametro("@sucursalId", SqlDbType.BigInt, sucursalId);
                db.agregarParametro("@cliente", SqlDbType.VarChar, cliente);
                db.agregarParametro("@usuarioId", SqlDbType.BigInt, usuarioId);
                SqlDataReader reader = db.consultaReader("abono_obtenLista");
                result = db.MapDataToEntityCollection<abonosEntity>(reader).ToList();              
                db.conexion.Close();
                db.conexion.Dispose();

            }
            catch
            {

                db.conexion.Close();
                db.conexion.Dispose();

            }

            return result;
        }


        public List<abonosEntity> venta_obtenListaAbonos(string fechaInicio,string fechaFin, int? sucursalId, int? usuarioId)
        {

            List<abonosEntity> result = new List<abonosEntity>();
            dbHelper db = new dbHelper();
            try
            {
 
                
                db.agregarParametro("@fechaInicio", SqlDbType.VarChar, fechaInicio);
                db.agregarParametro("@fechaFin", SqlDbType.VarChar, fechaFin);
                db.agregarParametro("@sucursalId", SqlDbType.BigInt, sucursalId);               
                db.agregarParametro("@usuarioId", SqlDbType.BigInt, usuarioId);
                SqlDataReader reader = db.consultaReader("venta_obtenListaAbonos");
                result = db.MapDataToEntityCollection<abonosEntity>(reader).ToList();
                db.conexion.Close();
                db.conexion.Dispose();

            }
            catch
            {

                db.conexion.Close();
                db.conexion.Dispose();

            }

            return result;
        }


    }
}