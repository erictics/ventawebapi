﻿using AppWCFService.Entity;
using AppWCFService.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace AppWCFService.Controllers
{
    public class corteCajaController
    {
        cajaController cajaController = new cajaController();
        usuarioController usuarioController = new usuarioController();

        public List<corteCajaEntity> corteCaja_obtenLista(long ? cajaId,long ? sucursalId,string fecha)
        {
            List<corteCajaEntity> result = new List<corteCajaEntity>();
            dbHelper db = new dbHelper();
            
            db.agregarParametro("@cajaId", SqlDbType.BigInt, cajaId);
            db.agregarParametro("@sucursalId", SqlDbType.BigInt, sucursalId);
            db.agregarParametro("@fecha", SqlDbType.VarChar,fecha.Replace("-",""));
            SqlDataReader reader = db.consultaReader("corteCaja_obtenLista");
            result = db.MapDataToEntityCollection<corteCajaEntity>(reader).ToList();
            result.ForEach(y => {
                y.caja = cajaController.caja_obten(y.cajaId);
                y.usuario = usuarioController.usuario_obten(y.usuarioId);
                y.fechaCorte = y.fecha.ToShortDateString();
                y.horaCorte = y.fecha.ToShortTimeString();
            });
            reader.Close();
            db.conexion.Close();
            db.conexion.Dispose();
            return result;
        }


        public corteCajaEntity corteCaja_obten(long ? id)
        {
            corteCajaEntity result = new corteCajaEntity();
            dbHelper db = new dbHelper();
            db.agregarParametro("@id", SqlDbType.BigInt, id);
            SqlDataReader reader = db.consultaReader("corteCaja_obten");
            try
            {
                result = db.MapDataToEntityCollection<corteCajaEntity>(reader).ToList()[0];
                result.caja = cajaController.caja_obten(result.cajaId);
            }
            catch (Exception ex)
            {
                result = null;
            }
            reader.Close();
            db.conexion.Close();
            db.conexion.Dispose();
            return result;
        }

      public long ? corteCaja_nuevo(corteCajaEntity corte)
      {
            dbHelper db = new dbHelper();
            db.agregarParametro("@usuarioId", SqlDbType.VarChar, corte.usuarioId);
            db.agregarParametro("@efectivo", SqlDbType.Decimal, corte.efectivo);
            db.agregarParametro("@tarjeta", SqlDbType.Decimal, corte.tarjeta);
            db.agregarParametro("@vales", SqlDbType.Int, corte.vales);
            db.agregarParametro("@cajaId", SqlDbType.Int, corte.cajaId);
            db.agregarParametro("@id", SqlDbType.BigInt, ParameterDirection.Output);
            db.consultaOutput("corteCaja_nuevo");
            long id = long.Parse(db.diccionarioOutput["@id"].ToString());
            db.conexion.Close();
            db.conexion.Dispose();
            return id;

        }

    }
}