﻿using AppWCFService.Entity;
using AppWCFService.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace AppWCFService.Controllers
{
    public class bancoController
    {
        public List<BancoEntity> banco_obtenLista()
        {
            List<BancoEntity> result = new List<BancoEntity>();
            dbHelper db = new dbHelper();
            SqlDataReader reader = null;

            try
            {

                 reader = db.consultaReader("banco_obtenLista");
                result = db.MapDataToEntityCollection<BancoEntity>(reader).ToList();
                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch{
                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }

            return result;
        }

        public BancoEntity banco_obten(long? id)
        {
            BancoEntity result = new BancoEntity();
            dbHelper db = new dbHelper();
            SqlDataReader reader = null;
            try
            {
                db.agregarParametro("@id", SqlDbType.BigInt, id);
                reader = db.consultaReader("banco_obten");
                try
                {
                    result = db.MapDataToEntityCollection<BancoEntity>(reader).ToList()[0];
                }
                catch (Exception ex)
                {
                    result = null;
                }
                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();

            }
            catch
            {
                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }
            
            return result;
        }


        public long? banco_nuevo(BancoEntity banco)
        {
            List<BancoEntity> result = new List<BancoEntity>();
            dbHelper db = new dbHelper();
            long id = 0;
            try
            {
                db.agregarParametro("@nombre", SqlDbType.VarChar, banco.nombre);
                db.agregarParametro("@activo", SqlDbType.Bit, banco.activo);
                db.agregarParametro("@id", SqlDbType.BigInt, ParameterDirection.Output);
                db.consultaOutput("banco_nuevo");
                id = long.Parse(db.diccionarioOutput["@id"].ToString());
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }
           
            
            return id;
        }

        public long? banco_edita(BancoEntity banco)
        {
            
            dbHelper db = new dbHelper();
            try
            {
                db.agregarParametro("@nombre", SqlDbType.VarChar, banco.nombre);
                db.agregarParametro("@activo", SqlDbType.Bit, banco.activo);
                db.agregarParametro("@id", SqlDbType.BigInt, banco.id);
                db.consultaSinRetorno("banco_edita");
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }

            
            
            return banco.id;
        }
    }
}