﻿using AppWCFService.Entity;
using AppWCFService.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;

namespace AppWCFService.Controllers
{
    public class tareaController
    {

        sucursalController sucursalController = new sucursalController();
        usuarioController usuarioController = new usuarioController();
        articuloController articuloController = new articuloController();
        unidadController unidadController = new unidadController();
        proveedorController proveedorController = new proveedorController();
        vendedorController vendedorController = new vendedorController();


        #region FUNCIONES GENERALES

        public List<tareaEntity> tarea_obtenLista(long? id,string fechaInicio,string  fechaFin, int ? tipo, long? sucursalId, string status,int ? usuarioId)
        {
            List<tareaEntity> result = new List<tareaEntity>();
            dbHelper db = new dbHelper();
            SqlDataReader reader = null;
            try
            {
                db.agregarParametro("@id", SqlDbType.BigInt, id);
                db.agregarParametro("@fechaInicio", SqlDbType.VarChar, fechaInicio);
                db.agregarParametro("@fechaFin", SqlDbType.VarChar, fechaFin);
                db.agregarParametro("@tipo", SqlDbType.BigInt, tipo);
                db.agregarParametro("@sucursalId", SqlDbType.BigInt, sucursalId);
                db.agregarParametro("@status", SqlDbType.VarChar, status);
                db.agregarParametro("@usuarioId", SqlDbType.Int, usuarioId);
                reader = db.consultaReader("tarea_obtenLista");
                result = db.MapDataToEntityCollection<tareaEntity>(reader).ToList();
                result.ForEach(x =>
                {
                    x.fechaFormato = x.fecha.ToShortDateString();
                    x.sucursal = sucursalController.sucursal_obten(x.sucursalId);
                    x.usuario = usuarioController.usuario_obten(x.usuarioId);

                    if (x.proveedorId != null)
                    {

                        x.proveedor = proveedorController.proveedor_obten(x.proveedorId);
                    }


                    if (x.vendedorId != null)
                    {
                        x.vendedor = vendedorController.vendedor_obten(x.vendedorId);
                    }

                });
            }
            catch(Exception ex)
            {
                if (reader != null)
                {
                    reader.Close();
                }
               
                db.conexion.Close();
                db.conexion.Dispose();
            }

            db.conexion.Close();
            db.conexion.Dispose();
            if (reader != null)
            {
                reader.Close();
            }
            return result;
        }


        public tareaEntity tarea_obten(long? id)
        {
            tareaEntity result = new tareaEntity();
            dbHelper db = new dbHelper();
            db.agregarParametro("@id", SqlDbType.BigInt, id);
            SqlDataReader reader = db.consultaReader("tarea_obten");
            try
            {
                result = db.MapDataToEntityCollection<tareaEntity>(reader).ToList()[0];
                result.fechaFormato = result.fecha.ToShortDateString();
                result.usuario = usuarioController.usuario_obten(result.usuarioId);
                try
                {
                    result.usuarioCancelo = usuarioController.usuario_obten(result.usuarioIdCancelacion);
                    result.fechaCancelacionFormato = result.fechaCancelacion.Value.ToShortDateString();
                }
                catch
                {
                    reader.Close();
                    db.conexion.Close();
                    db.conexion.Dispose();
                }

            }
            catch (Exception ex)
            {
                result = null;
            }

            reader.Close();
            db.conexion.Close();
            db.conexion.Dispose();
            return result;
        }







        public List<tareaDetalleEntity> tareaDetalle_obtenLista(long? tareaId)
        {

            List<tareaDetalleEntity> result = new List<tareaDetalleEntity>();
            dbHelper db = new dbHelper();
            db.agregarParametro("@tareaId", SqlDbType.BigInt, tareaId);
            SqlDataReader reader = db.consultaReader("tareaDetalle_obtenLista");
            result = db.MapDataToEntityCollection<tareaDetalleEntity>(reader).ToList();
            result.ForEach(x => {
                articuloEntity art = articuloController.articulo_obten(x.articuloId);
                unidadEntity unidad = unidadController.unidad_obten(art.unidadId);
                x.id = art.id;
                x.upc = art.upc;
                x.descripcion = art.descripcion;
                x.unidad = unidad;
                x.nombre = art.nombre;

            });
            reader.Close();
            db.conexion.Close();
            db.conexion.Dispose();
            return result;

        }



        public int? tareaDetalle_elimina(long? tareaId)
        {
            dbHelper db = new dbHelper();
            db.agregarParametro("@tareaId", SqlDbType.BigInt, tareaId);
            db.consultaSinRetorno("tareaDetalle_elimina");
            db.conexion.Close();
            return 1;
        }



        public string detalleToXML(List<tareaDetalleEntity> detalle)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<articulos>");
            detalle.ForEach(x =>
            {
                sb.Append("<articulo>");
                sb.Append("<articuloId>" + x.articuloId + " </articuloId>");
                sb.Append("<cantidad>" + x.cantidad + " </cantidad>");
                sb.Append("<precio>" + x.precio + " </precio>");
                sb.Append("<tipoMonedaId>" + x.tipoMonedaId + " </tipoMonedaId>");
                sb.Append("</articulo>");
            });

            sb.Append("</articulos>");
            return sb.ToString();

        }

        #endregion

        #region RECEPCION DE ARTICULOS


        public long? tarea_recepcionNueva(tareaEntity tarea, List<tareaDetalleEntity> detalle)
        {

           

            dbHelper db = new dbHelper();
            long id = 0;
            try
            {
                db.agregarParametro("@tipo", SqlDbType.VarChar, tarea.tipoTarea);
                db.agregarParametro("@usuarioId", SqlDbType.BigInt, tarea.usuarioId);
                db.agregarParametro("@observaciones", SqlDbType.VarChar, tarea.observaciones);
                db.agregarParametro("@sucursalId", SqlDbType.BigInt, tarea.sucursalId);
                db.agregarParametro("@proveedorId", SqlDbType.BigInt, tarea.proveedorId);
                db.agregarParametro("@detalle", SqlDbType.Xml, detalleToXML(detalle));
                db.agregarParametro("@id", SqlDbType.BigInt, ParameterDirection.Output);
                db.consultaOutput("tarea_recepcionNueva");
                 id = long.Parse(db.diccionarioOutput["@id"].ToString());
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }
            
            return id;
        }


        public long? tarea_recepcionEdita(tareaEntity tarea, List<tareaDetalleEntity> detalle)
        {

            dbHelper db = new dbHelper();
            try
            {
                db.agregarParametro("@tipo", SqlDbType.VarChar, tarea.tipoTarea);
                db.agregarParametro("@usuarioId", SqlDbType.BigInt, tarea.usuarioId);
                db.agregarParametro("@observaciones", SqlDbType.VarChar, tarea.observaciones);
                db.agregarParametro("@sucursalId", SqlDbType.BigInt, tarea.sucursalId);
                db.agregarParametro("@proveedorId", SqlDbType.BigInt, tarea.proveedorId);
                db.agregarParametro("@detalle", SqlDbType.Xml, detalleToXML(detalle));
                db.agregarParametro("@id", SqlDbType.BigInt, tarea.id);
                db.consultaOutput("tarea_recepcionEdita");
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }
           
            
            return 1;
        }


        public ResultEntity GetCancelaRecepcion(int? id, int? usuarioId)
        {

            dbHelper db = new dbHelper();
            ResultEntity result = new ResultEntity();
            try
            {
                db.agregarParametro("@id", SqlDbType.VarChar, id);
                db.agregarParametro("@usuarioCancelaId", SqlDbType.BigInt, usuarioId);
                db.agregarParametro("@Message", SqlDbType.VarChar, ParameterDirection.Output, 100);
                db.agregarParametro("@ErrorCode", SqlDbType.BigInt, ParameterDirection.Output);
                db.consultaOutput("tarea_cancelaRecepcion");
                string mensaje = db.diccionarioOutput["@Message"].ToString();
                int ErrorCode = int.Parse(db.diccionarioOutput["@ErrorCode"].ToString());
                db.conexion.Close();
                db.conexion.Dispose();


                result.ErrorCode = ErrorCode;
                result.Message = mensaje;
            }
            catch
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }
           
            return result;
        }



        #endregion

        #region ADMINISTRACION ARTICULOS

        public long? tarea_cambioEstatusNuevo(tareaEntity tarea, List<tareaDetalleEntity> detalle,int ? estatus)
        {
            dbHelper db = new dbHelper();
            long id = 0;
            try
            {
                db.agregarParametro("@usuarioId", SqlDbType.BigInt, tarea.usuarioId);
                db.agregarParametro("@observaciones", SqlDbType.VarChar, tarea.observaciones);
                db.agregarParametro("@sucursalId", SqlDbType.BigInt, tarea.sucursalId);
                db.agregarParametro("@estatus", SqlDbType.BigInt, estatus);
                db.agregarParametro("@detalle", SqlDbType.Xml, detalleToXML(detalle));
                db.agregarParametro("@id", SqlDbType.BigInt, ParameterDirection.Output);
                db.consultaOutput("tarea_cambioEstatusNuevo");
                 id = long.Parse(db.diccionarioOutput["@id"].ToString());
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch {
                db.conexion.Close();
                db.conexion.Dispose();
            }
           
           
            return id;
        }

        public ResultEntity GetCancelaEstatus(int? id, int? usuarioId)
        {

            dbHelper db = new dbHelper();
            ResultEntity result = new ResultEntity();
            try
            {
                db.agregarParametro("@id", SqlDbType.VarChar, id);
                db.agregarParametro("@usuarioCancelaId", SqlDbType.BigInt, usuarioId);
                db.agregarParametro("@Message", SqlDbType.VarChar, ParameterDirection.Output, 100);
                db.agregarParametro("@ErrorCode", SqlDbType.BigInt, ParameterDirection.Output);
                db.consultaOutput("tarea_cancelaCambioEstatus");
                string mensaje = db.diccionarioOutput["@Message"].ToString();
                int ErrorCode = int.Parse(db.diccionarioOutput["@ErrorCode"].ToString());
                db.conexion.Close();
                db.conexion.Dispose();
                result.ErrorCode = ErrorCode;
                result.Message = mensaje;
            }
            catch
            {
                result.ErrorCode = 1;
                result.Message = "";
            }
            

            
           
            return result;
        }

        #endregion

        #region TRASPASO

        public long? tarea_AddTraspasoVendedor(tareaEntity tarea, List<tareaDetalleEntity> detalle)
        {
            dbHelper db = new dbHelper();
            long id = 0;
            try
            {
                
                db.agregarParametro("@usuarioId", SqlDbType.BigInt, tarea.usuarioId);
                db.agregarParametro("@sucursalId", SqlDbType.BigInt, tarea.sucursalId);
                db.agregarParametro("@observaciones", SqlDbType.VarChar, tarea.observaciones);               
                db.agregarParametro("@detalle", SqlDbType.Xml, detalleToXML(detalle));
                db.agregarParametro("@vendedorId", SqlDbType.BigInt, tarea.vendedorId);
                db.agregarParametro("@id", SqlDbType.BigInt, ParameterDirection.Output);
                db.consultaOutput("traspaso_vendedor");
                id = long.Parse(db.diccionarioOutput["@id"].ToString());
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch(Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }

            return id;
        }


        public long? tarea_AddTraspasoSucursal(tareaEntity tarea, List<tareaDetalleEntity> detalle)
        {
            dbHelper db = new dbHelper();
            long id = 0;
            try
            {

                db.agregarParametro("@usuarioId", SqlDbType.BigInt, tarea.usuarioId);
                db.agregarParametro("@sucursalId", SqlDbType.BigInt, tarea.sucursalId);
                db.agregarParametro("@observaciones", SqlDbType.VarChar, tarea.observaciones);
                db.agregarParametro("@detalle", SqlDbType.Xml, detalleToXML(detalle));
                db.agregarParametro("@vendedorId", SqlDbType.BigInt, tarea.vendedorId);
                db.agregarParametro("@id", SqlDbType.BigInt, ParameterDirection.Output);
                db.consultaOutput("traspaso_sucursal");
                id = long.Parse(db.diccionarioOutput["@id"].ToString());
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }

            return id;
        }


        #endregion











    }
}