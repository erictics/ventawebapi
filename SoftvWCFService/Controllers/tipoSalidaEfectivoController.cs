﻿using AppWCFService.Entity;
using AppWCFService.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace AppWCFService.Controllers
{
    public class tipoSalidaEfectivoController
    {

        usuarioController usuarioController = new usuarioController();

        public List<tipoSalidaEfectivoEntity> tipoSalidaEfectivo_obtenLista()
        {
            List<tipoSalidaEfectivoEntity> result = new List<tipoSalidaEfectivoEntity>();
            dbHelper db = new dbHelper();

            SqlDataReader reader = null;
            try
            {
                reader = db.consultaReader("tipoSalidaEfectivo_obtenLista");
                result = db.MapDataToEntityCollection<tipoSalidaEfectivoEntity>(reader).ToList();
                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch
            {
                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }
            return result;
        }

        public tipoSalidaEfectivoEntity tipoSalidaEfectivo_obten(long? id)
        {
            tipoSalidaEfectivoEntity result = new tipoSalidaEfectivoEntity();
            dbHelper db = new dbHelper();
            SqlDataReader reader = null;
            try
            {
                db.agregarParametro("@id", SqlDbType.BigInt, id);
                reader = db.consultaReader("tipoSalidaEfectivo_obten");
                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch
            {
                result = null;
                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }
            
            
           
            return result;
        }


        public long? tipoSalidaEfectivo_nuevo(tipoSalidaEfectivoEntity tipoSalidaEfectivo)
        {
           
            dbHelper db = new dbHelper();
            long id = 0;
            try
            {
                db.agregarParametro("@activo", SqlDbType.Bit, tipoSalidaEfectivo.activo);
                db.agregarParametro("@descripcion", SqlDbType.VarChar, tipoSalidaEfectivo.descripcion);
                db.agregarParametro("@autorizacion", SqlDbType.Bit, tipoSalidaEfectivo.requiereAutorizacion);
                db.agregarParametro("@id", SqlDbType.BigInt, ParameterDirection.Output);
                db.consultaOutput("tipoSalidaEfectivo_nuevo");

                id = long.Parse(db.diccionarioOutput["@id"].ToString());
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch
            {
                
                db.conexion.Close();
                db.conexion.Dispose();
            }
           
           
            return id;
        }


        public long? tipoSalidaEfectivo_edita(tipoSalidaEfectivoEntity tipoSalidaEfectivo)
        {

            dbHelper db = new dbHelper();
            try
            {
                db.agregarParametro("@activo", SqlDbType.Bit, tipoSalidaEfectivo.activo);
                db.agregarParametro("@descripcion", SqlDbType.VarChar, tipoSalidaEfectivo.descripcion);
                db.agregarParametro("@autorizacion", SqlDbType.Bit, tipoSalidaEfectivo.requiereAutorizacion);
                db.agregarParametro("@id", SqlDbType.BigInt, tipoSalidaEfectivo.id);
                db.consultaOutput("tipoSalidaEfectivo_edita");
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }
            
            return tipoSalidaEfectivo.id;
        }


    }
}