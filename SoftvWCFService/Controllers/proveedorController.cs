﻿using System;
using System.Collections.Generic;
using System.Linq;
using AppWCFService.Entity;
using System.Web;
using AppWCFService.Services;
using System.Data.SqlClient;
using System.Data;

namespace AppWCFService.Controllers
{
    public class proveedorController
    {

        public List<proveedorEntity> proveedor_obtenLista()
        {
            List<proveedorEntity> result = new List<proveedorEntity>();
            dbHelper db = new dbHelper();
            SqlDataReader reader = null;
            try
            {
                reader = db.consultaReader("proveedor_obtenLista");
                result = db.MapDataToEntityCollection<proveedorEntity>(reader).ToList();
                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch
            {
                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }
           
            return result;
        }

        public proveedorEntity proveedor_obten(long? id)
        {
            proveedorEntity result = new proveedorEntity();
            dbHelper db = new dbHelper();
            SqlDataReader reader = null;


            try
            {
                db.agregarParametro("@id", SqlDbType.BigInt, id);
                reader = db.consultaReader("proveedor_obten");
                result = db.MapDataToEntityCollection<proveedorEntity>(reader).ToList()[0];
                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch (Exception ex)
            {
                result = null;
                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }
            
            return result;
        }


        public long? proveedor_nuevo(proveedorEntity proveedor)
        {
            List<proveedorEntity> result = new List<proveedorEntity>();
            dbHelper db = new dbHelper();
            long id = 0;
            try
            {
                db.agregarParametro("@activo", SqlDbType.Bit, proveedor.activo);
                db.agregarParametro("@nombre", SqlDbType.VarChar, proveedor.nombre);
                db.agregarParametro("@telefono", SqlDbType.VarChar, proveedor.telefono);
                db.agregarParametro("@direccion", SqlDbType.VarChar, proveedor.direccion);
                db.agregarParametro("@contacto", SqlDbType.VarChar, proveedor.contacto);
                db.agregarParametro("@id", SqlDbType.BigInt, ParameterDirection.Output);
                db.consultaOutput("proveedor_nuevo");
                 id = long.Parse(db.diccionarioOutput["@id"].ToString());
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }
           
          
            return id;
        }


        public long? proveedor_edita(proveedorEntity proveedor)
        {
            List<proveedorEntity> result = new List<proveedorEntity>();
            dbHelper db = new dbHelper();

            try
            {
                db.agregarParametro("@activo", SqlDbType.Bit, proveedor.activo);
                db.agregarParametro("@id", SqlDbType.BigInt, proveedor.id);
                db.agregarParametro("@nombre", SqlDbType.VarChar, proveedor.nombre);
                db.agregarParametro("@telefono", SqlDbType.VarChar, proveedor.telefono);
                db.agregarParametro("@direccion", SqlDbType.VarChar, proveedor.direccion);
                db.agregarParametro("@contacto", SqlDbType.VarChar, proveedor.contacto);
                db.consultaSinRetorno("proveedor_edita");
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }
              
            
            return 1;
        }

    }
}