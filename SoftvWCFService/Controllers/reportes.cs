﻿using AppWCFService.Entity;
using AppWCFService.Services;
using CrystalDecisions.CrystalReports.Engine;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;

namespace AppWCFService.Controllers
{
    public class reportes
    {

        sucursalController sucursalController = new sucursalController();
        departamentoController departamentoController = new departamentoController();
        categoriaController categoriaController = new categoriaController();
        usuarioController usuarioController = new usuarioController();

        iTextSharp.text.Font _smallFont = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 8, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
        iTextSharp.text.Font _standardFont = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 10, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
        iTextSharp.text.Font _mediumFont = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 12, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
        iTextSharp.text.Font _largeFont = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA,14, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
        iTextSharp.text.Font _verylargeFont = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 16, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);

        iTextSharp.text.Font _wsmallFont = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 8, iTextSharp.text.Font.NORMAL, BaseColor.WHITE);
        iTextSharp.text.Font _wstandardFont = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 10, iTextSharp.text.Font.NORMAL, BaseColor.WHITE);
        iTextSharp.text.Font _wmediumFont = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 12, iTextSharp.text.Font.NORMAL, BaseColor.WHITE);
        iTextSharp.text.Font _wlargeFont = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 14, iTextSharp.text.Font.NORMAL, BaseColor.WHITE);
        iTextSharp.text.Font _wverylargeFont = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 16, iTextSharp.text.Font.NORMAL, BaseColor.WHITE);
        iTextSharp.text.Font _headerFont = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 18, iTextSharp.text.Font.BOLD, BaseColor.BLACK);


        iTextSharp.text.Font _headertable = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 8, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
        iTextSharp.text.Font _headertablerow = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 8, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);

        //public string GetDetalleProceso(int ? procesoId)
        //{
        //    string apppath = System.AppDomain.CurrentDomain.BaseDirectory;
        //    string name = Guid.NewGuid().ToString() + ".pdf";
        //    string fileName = apppath + "/Reportes/" + name;
        //    string logo = apppath + "/Reportes/logo.png";
        //    Document doc = new Document(PageSize.LETTER);
        //    PdfWriter writer = PdfWriter.GetInstance(doc, new FileStream(fileName, FileMode.Create));
        //    doc.Open();

        //    PdfPTable tblPrueba = new PdfPTable(2);
        //    tblPrueba.WidthPercentage = 100;
        //    procesoEntity proceso=_procesoController.proceso_obten(procesoId);
        //    if (proceso.tipo == "RSO") {
        //        doc.Add(createHeaderReporte("Recepción de mercancia #"+proceso.id, logo));
        //        tblPrueba.AddCell(CreateCell(false, "Almacén", BaseColor.WHITE, _mediumFont, 0));
        //        tblPrueba.AddCell(CreateblankCell(false));
        //        tblPrueba.AddCell(CreateCell(false, "Proveedor", BaseColor.WHITE, _mediumFont, 0));
        //        tblPrueba.AddCell(CreateblankCell(false));
        //        tblPrueba.AddCell(CreateCell(false, "Fecha:", BaseColor.WHITE, _mediumFont, 0));
        //        tblPrueba.AddCell(CreateblankCell(false));
        //        tblPrueba.AddCell(CreateCell(false, "Observaciones:", BaseColor.WHITE, _mediumFont, 0));
        //        tblPrueba.AddCell(CreateblankCell(false));
        //        tblPrueba.AddCell(CreateCell(false, "Recibió:", BaseColor.WHITE, _mediumFont, 0));
        //        tblPrueba.AddCell(CreateblankCell(false));
        //    }

        //    doc.Add(tblPrueba);


        //    PdfPTable tblContenido = new PdfPTable(5);
        //    tblContenido.WidthPercentage = 100;
        //    tblContenido.AddCell(CreateCell(true, "UPC", BaseColor.BLACK, _wstandardFont, 0));
        //    tblContenido.AddCell(CreateCell(true, "ARTICULO", BaseColor.BLACK, _wstandardFont, 0));
        //    tblContenido.AddCell(CreateCell(true, "PRECIO", BaseColor.BLACK, _wstandardFont, 0));
        //    tblContenido.AddCell(CreateCell(true, "CANTIDAD", BaseColor.BLACK, _wstandardFont, 0));
        //    tblContenido.AddCell(CreateCell(true, "UNIDAD", BaseColor.BLACK, _wstandardFont, 0));
        //    tblContenido.AddCell(CreateCell(true, "TOTAL", BaseColor.BLACK, _wstandardFont, 0));

        //    proceso.conceptos.ForEach(x=> {
        //        tblContenido.AddCell(CreateCell(false, x.articulo.upc, BaseColor.WHITE, _smallFont, 0));
        //        tblContenido.AddCell(CreateCell(false, x.articulo.nombre, BaseColor.WHITE, _smallFont, 0));
        //        tblContenido.AddCell(CreateCell(false, x.precio.ToString("C"), BaseColor.WHITE, _smallFont, 0));
        //        tblContenido.AddCell(CreateCell(false, x.cantidad.ToString(), BaseColor.WHITE, _smallFont, 0));
        //        tblContenido.AddCell(CreateCell(false, x.articulo.unidad.nombre, BaseColor.WHITE, _smallFont, 0));
        //        tblContenido.AddCell(CreateCell(false, (x.cantidad*x.precio).ToString("C"), BaseColor.WHITE, _smallFont, 0));
        //    });

        //    doc.Add(tblContenido);



        //    doc.Close();
        //    writer.Close();
        //    return name;
        //}

        //public string RecepcionMaterial(long? almacenId, long? proveedorId, long? articuloId, long? categoriaId, long? departamentoId)
        //{

        //    string apppath = System.AppDomain.CurrentDomain.BaseDirectory;
        //    string name = Guid.NewGuid().ToString() + ".pdf";
        //    string fileName = apppath + "/Reportes/" + name;
        //    string logo = apppath + "/Reportes/logo.png";
        //    Document doc = new Document(PageSize.LETTER);
        //    PdfWriter writer = PdfWriter.GetInstance(doc, new FileStream(fileName, FileMode.Create));
        //    doc.Open();

        //    almacenEntity almacen = _almacenController.almacen_obten(almacenId);
        //    proveedorEntity proveedor = _proveedorController.proveedor_obten(proveedorId);
        //    articuloEntity articulo = _articuloController.articulo_obten(articuloId);
        //    categoriaEntity categoria = _categoriaController.categoria_obten(categoriaId);
        //    departamentoEntity departamento = _departamentoController.departamento_obten(departamentoId);


        //    doc.Add(createHeaderReporte("Reporte de recepción de Mercancia", logo));


        //    PdfPTable tblPrueba = new PdfPTable(2);
        //    tblPrueba.WidthPercentage = 100;

        //    tblPrueba.AddCell(CreateCell(false, (almacen !=null) ? "Almacén: " + almacen.nombre : "Todos los almacenes", BaseColor.WHITE, _mediumFont, 0));
        //    tblPrueba.AddCell(CreateblankCell(false));
        //    tblPrueba.AddCell(CreateCell(false, (proveedor !=null) ? "Proveedor: " + proveedor.nombre : "Proveedor: Todos los proveedores", BaseColor.WHITE, _mediumFont, 0));
        //    tblPrueba.AddCell(CreateblankCell(false));
        //    tblPrueba.AddCell(CreateCell(false, (departamento !=null ) ? "Departamento: " + departamento.nombre : "Departamento: Todos los departamentos", BaseColor.WHITE, _mediumFont, 0));
        //    tblPrueba.AddCell(CreateblankCell(false));
        //    tblPrueba.AddCell(CreateCell(false, (categoria !=null) ? "Categoria:  " + categoria.nombre : "Categoria: Todos las categorias", BaseColor.WHITE, _mediumFont, 0));
        //    tblPrueba.AddCell(CreateblankCell(false));
        //    tblPrueba.AddCell(CreateCell(false, (articulo !=null) ? "Articulo:  " + articulo.nombre : "Articulo: Todos las artículos", BaseColor.WHITE, _mediumFont, 0));
        //    tblPrueba.AddCell(CreateblankCell(false));
        //    doc.Add(tblPrueba);

        //    List<ReporteRecepcion> result = new List<ReporteRecepcion>();
        //    List<ReporteRecepcion> result2 = new List<ReporteRecepcion>();
        //    dbHelper db = new dbHelper();
        //    db.agregarParametro("@almacenId", SqlDbType.BigInt, almacenId);
        //    db.agregarParametro("@departamentoId", SqlDbType.BigInt, departamentoId);
        //    db.agregarParametro("@categoriaId", SqlDbType.BigInt, categoriaId);
        //    db.agregarParametro("@proveedorId", SqlDbType.BigInt, proveedorId);
        //    db.agregarParametro("@articuloId", SqlDbType.BigInt, articuloId);
        //    SqlDataReader reader = db.consultaReader("reporte_recepcionMaterial");
        //    result = db.MapDataToEntityCollection<ReporteRecepcion>(reader).ToList();
        //    reader.NextResult();
        //    result2 = db.MapDataToEntityCollection<ReporteRecepcion>(reader).ToList();
        //    reader.Close();
        //    db.conexion.Close();
        //    db.conexion.Dispose();

        //    PdfPTable tblContenido = new PdfPTable(9);
        //    tblContenido.WidthPercentage = 100;
        //    tblContenido.SpacingBefore = 50f;
        //    tblContenido.AddCell(CreateCell(true, "#", BaseColor.BLACK, _wsmallFont, 0));
        //    tblContenido.AddCell(CreateCell(true, "FECHA", BaseColor.BLACK, _wsmallFont, 0));
        //    tblContenido.AddCell(CreateCell(true, "UPC", BaseColor.BLACK, _wsmallFont, 0));
        //    tblContenido.AddCell(CreateCell(true, "NOMBRE DE ARTICULO:", BaseColor.BLACK, _wsmallFont, 2));
        //    tblContenido.AddCell(CreateCell(true, "PRECIO:", BaseColor.BLACK, _wsmallFont, 0));
        //    tblContenido.AddCell(CreateCell(true, "CANT.:", BaseColor.BLACK, _wsmallFont, 0));
        //    tblContenido.AddCell(CreateCell(true, "UNIDAD", BaseColor.BLACK, _wsmallFont, 0));
        //    tblContenido.AddCell(CreateCell(true, "TOTAL:", BaseColor.BLACK, _wsmallFont, 0));
        //    result.ForEach(x => {
        //        tblContenido.WidthPercentage = 100;
        //        tblContenido.AddCell(CreateCell(false, x.numeroproceso.ToString(), BaseColor.WHITE, _smallFont, 0));
        //        tblContenido.AddCell(CreateCell(false, x.fecha.ToShortDateString(), BaseColor.WHITE, _smallFont, 0));
        //        tblContenido.AddCell(CreateCell(false, x.upc, BaseColor.WHITE, _smallFont, 0));

        //        tblContenido.AddCell(CreateCell(false, x.articulo, BaseColor.WHITE, _smallFont, 2));
        //        tblContenido.AddCell(CreateCell(false, x.precio.ToString("C"), BaseColor.WHITE, _smallFont, 0));
        //        tblContenido.AddCell(CreateCell(false, x.cantidad.ToString(), BaseColor.WHITE, _smallFont, 0));
        //        tblContenido.AddCell(CreateCell(false, x.unidad, BaseColor.WHITE, _smallFont, 0));
        //        tblContenido.AddCell(CreateCell(false, x.total.ToString("C"), BaseColor.WHITE, _smallFont, 0));

        //    });
        //    doc.Add(tblContenido);


        //    PdfPTable tblresumen = new PdfPTable(9);
        //    tblresumen.WidthPercentage = 100;
        //    tblresumen.SpacingBefore = 50f;
        //    tblresumen.AddCell(CreateCell(true, "RESUMEN DE REPORTE POR PROVEEDOR Y ARTICULO", BaseColor.RED, _wsmallFont, 9));
        //    tblresumen.AddCell(CreateCell(true, "PROVEEDOR", BaseColor.LIGHT_GRAY, _smallFont, 2));
        //    tblresumen.AddCell(CreateCell(true, "CATEGORIA", BaseColor.LIGHT_GRAY, _smallFont, 2));
        //    tblresumen.AddCell(CreateCell(true, "UPC", BaseColor.LIGHT_GRAY, _smallFont, 0));
        //    tblresumen.AddCell(CreateCell(true, "ARTICULO:", BaseColor.LIGHT_GRAY, _smallFont, 2));
        //    tblresumen.AddCell(CreateCell(true, "CANT.:", BaseColor.LIGHT_GRAY, _smallFont, 0));
        //    tblresumen.AddCell(CreateCell(true, "UNIDAD", BaseColor.LIGHT_GRAY, _smallFont, 0));

        //    result2.ForEach(x => {
        //        tblresumen.WidthPercentage = 100;
        //        tblresumen.AddCell(CreateCell(false, x.proveedor, BaseColor.WHITE, _smallFont, 2));
        //        tblresumen.AddCell(CreateCell(false, x.departamento, BaseColor.WHITE, _smallFont, 2));

        //        tblresumen.AddCell(CreateCell(false, x.upc, BaseColor.WHITE, _smallFont, 0));
        //        tblresumen.AddCell(CreateCell(false, x.articulo, BaseColor.WHITE, _smallFont, 2));
        //        tblresumen.AddCell(CreateCell(false, x.cantidad.ToString(), BaseColor.WHITE, _smallFont, 0));
        //        tblresumen.AddCell(CreateCell(false, x.unidad, BaseColor.WHITE, _smallFont, 0));


        //    });
        //    doc.Add(tblresumen);


        //    doc.Close();
        //    writer.Close();
        //    return name;


        //}

        //public string Inventario(int? almacenId, long? departamentoId, long? categoriaId)
        //{
        //    string apppath = System.AppDomain.CurrentDomain.BaseDirectory;        
        //    string name = Guid.NewGuid().ToString() + ".pdf";
        //    string fileName = apppath + "/Reportes/" + name;
        //    string logo = apppath + "/Reportes/logo.png";            
        //    Document doc = new Document(PageSize.LETTER);
        //    PdfWriter writer = PdfWriter.GetInstance(doc, new FileStream(fileName, FileMode.Create));          
        //    doc.Open();

        //    doc.Add(createHeaderReporte("Reporte de inventario",logo));

        //    PdfPTable tblPrueba = new PdfPTable(2);
        //    tblPrueba.WidthPercentage = 100;            
        //    tblPrueba.AddCell(CreateCell(false, "Almacén", BaseColor.WHITE, _mediumFont, 0));
        //    tblPrueba.AddCell(CreateblankCell(false));
        //    tblPrueba.AddCell(CreateCell(false, "Reporte de Inventario", BaseColor.WHITE, _mediumFont, 0));
        //    tblPrueba.AddCell(CreateblankCell(false));
        //    tblPrueba.AddCell(CreateCell(false, "Departamento", BaseColor.WHITE, _mediumFont, 0));
        //    tblPrueba.AddCell(CreateblankCell(false));
        //    tblPrueba.AddCell(CreateCell(false, "Fecha:", BaseColor.WHITE, _mediumFont, 0));
        //    doc.Add(tblPrueba);

        //    List<ReporteInventarioEntity> result = new List<ReporteInventarioEntity>();
        //    dbHelper db = new dbHelper();
        //    db.agregarParametro("@almacenId", SqlDbType.BigInt, almacenId);
        //    db.agregarParametro("@departamentoId", SqlDbType.BigInt, departamentoId);
        //    db.agregarParametro("@categoriaId", SqlDbType.BigInt, categoriaId);
        //    SqlDataReader reader = db.consultaReader("reporte_inventario");
        //    result = db.MapDataToEntityCollection<ReporteInventarioEntity>(reader).ToList();            
        //    reader.Close();
        //    db.conexion.Close();
        //    db.conexion.Dispose();

        //    PdfPTable tblContenido = new PdfPTable(10);
        //    tblContenido.WidthPercentage = 100;
        //    tblContenido.AddCell(CreateCell(true, "DEPARTAMENTO", BaseColor.BLACK, _wstandardFont, 0));
        //    tblContenido.AddCell(CreateCell(true, "CATEGORIA", BaseColor.BLACK, _wstandardFont, 0));
        //    tblContenido.AddCell(CreateCell(true, "UPC", BaseColor.BLACK, _wstandardFont, 0));
        //    tblContenido.AddCell(CreateCell(true, "ID:", BaseColor.BLACK, _wstandardFont, 0));
        //    tblContenido.AddCell(CreateCell(true, "ARTICULO:", BaseColor.BLACK, _wstandardFont, 2));
        //    tblContenido.AddCell(CreateCell(true, "PRECIO LISTA:", BaseColor.BLACK, _wstandardFont, 0));
        //    tblContenido.AddCell(CreateCell(true, "CANTIDAD:", BaseColor.BLACK, _wstandardFont, 0));
        //    tblContenido.AddCell(CreateCell(true, "UNIDAD", BaseColor.BLACK, _wstandardFont, 0));
        //    tblContenido.AddCell(CreateCell(true, "TOTAL:", BaseColor.BLACK, _wstandardFont, 0));
        //    result.ForEach(x=> {
        //        tblContenido.WidthPercentage = 100;
        //        tblContenido.AddCell(CreateCell(false, x.departamento, BaseColor.WHITE, _smallFont,0));
        //        tblContenido.AddCell(CreateCell(false, x.categoria, BaseColor.WHITE, _smallFont, 0));
        //        tblContenido.AddCell(CreateCell(false, x.upc, BaseColor.WHITE, _smallFont, 0));
        //        tblContenido.AddCell(CreateCell(false, x.articuloId.ToString(), BaseColor.WHITE, _smallFont, 0));
        //        tblContenido.AddCell(CreateCell(false, x.articulo, BaseColor.WHITE, _smallFont, 2));
        //        tblContenido.AddCell(CreateCell(false, x.precioLista.ToString("C"), BaseColor.WHITE, _smallFont, 0));
        //        tblContenido.AddCell(CreateCell(false, x.cantidad.ToString(), BaseColor.WHITE, _smallFont, 0));
        //        tblContenido.AddCell(CreateCell(false, x.unidad, BaseColor.WHITE, _smallFont, 0));
        //        tblContenido.AddCell(CreateCell(false, x.total.ToString("C"), BaseColor.WHITE, _smallFont, 0));

        //    });
        //    doc.Add(tblContenido);
        //    doc.Close();
        //    writer.Close();
        //    return name;
        //}

        iTextSharp.text.Font EncabezadoTitulo = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 10, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
        iTextSharp.text.Font EncabezadoDesc = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 10, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
        iTextSharp.text.Font Encabezadoinfo = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 8, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
        iTextSharp.text.Font MontosTotalesFont = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 12, iTextSharp.text.Font.BOLD, BaseColor.BLACK);


        public string GetReporteDetalleTarea(long? Id)
        {

            string apppath = System.AppDomain.CurrentDomain.BaseDirectory;
            string ruta = apppath + "/Rpt/";
            string name = Guid.NewGuid().ToString() + ".pdf";
            string fileName = apppath + "/Reportes/" + name;
            ReportDocument reportDocument = new ReportDocument();

            dbHelper db = new dbHelper();
            try
            {
                db.agregarParametro("@tareaId", SqlDbType.BigInt, Id);
                DataSet data = db.consultaDS("reporte_tareaDetalle");
                data.Tables[0].TableName = "SISTEMA";
                data.Tables[1].TableName = "TAREA";
                data.Tables[2].TableName = "DETALLE";

                reportDocument.Load(ruta + "reporte_tareaDetalle.rpt");
                reportDocument.SetDataSource(data);
                reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, fileName);

                db.conexion.Close();
                reportDocument.Close();
            }
            catch
            {
                db.conexion.Close();
                reportDocument.Close();
            }
           

            

            return name;

        }

        public string GetReporteCierreVenta(long? Id)
        {

            string apppath = System.AppDomain.CurrentDomain.BaseDirectory;
            string ruta = apppath + "/Rpt/";
            string name = Guid.NewGuid().ToString() + ".pdf";
            string fileName = apppath + "/Reportes/" + name;
            ReportDocument reportDocument = new ReportDocument();

            dbHelper db = new dbHelper();
            try
            {
                db.agregarParametro("@id", SqlDbType.BigInt, Id);
                DataSet data = db.consultaDS("reporte_cierreVenta");

                data.Tables[0].TableName = "titulo";
                data.Tables[1].TableName = "subtitulo";
                data.Tables[2].TableName = "cierre";

                reportDocument.Load(ruta + "reporte_cierreVenta.rpt");
                reportDocument.SetDataSource(data);
                reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, fileName);

                db.conexion.Close();
                reportDocument.Close();
            }
            catch
            {
                db.conexion.Close();
                reportDocument.Close();
            }
            

            return name;

        }



        public string GetTicketVenta(long ? ventaId,long ? usuarioId, bool ? reimpresion, int ? motivoId ) {

            string apppath = System.AppDomain.CurrentDomain.BaseDirectory;
            string ruta = apppath + "/Rpt/";
            string name = Guid.NewGuid().ToString() + ".pdf";
            string fileName = apppath + "/Reportes/" + name;
            ReportDocument reportDocument = new ReportDocument();

            dbHelper db = new dbHelper();

            try
            {
                db.agregarParametro("@ventaId", SqlDbType.BigInt, ventaId);
                db.agregarParametro("@usuarioId", SqlDbType.BigInt, usuarioId);
                db.agregarParametro("@reimpresion", SqlDbType.BigInt, reimpresion);
                db.agregarParametro("@motivoId", SqlDbType.BigInt, motivoId);
                DataSet data = db.consultaDS("reporte_ticket");

                data.Tables[0].TableName = "sistema";
                data.Tables[1].TableName = "sucursal";
                data.Tables[2].TableName = "cliente";
                data.Tables[3].TableName = "usuario";
                data.Tables[4].TableName = "venta";
                data.Tables[5].TableName = "detalle";
                data.Tables[6].TableName = "tarjetacredito";
                data.Tables[7].TableName = "tarjetadebito";
                data.Tables[8].TableName = "vales";
                data.Tables[9].TableName = "transferencia";


                //BarcodeLib.Barcode b = new BarcodeLib.Barcode();
                //b.IncludeLabel = true;
                //string imgsrc = apppath + "/Reportes/" + Guid.NewGuid() + ".jpg";
                //System.Drawing.Image img = b.Encode(BarcodeLib.TYPE.UPCA, "038000356216", Color.Black, Color.White, 290, 120);
                //img.Save(imgsrc);

                SistemaGeneralController sg = new SistemaGeneralController();
               sistemaGeneralEntity sistema= sg.sistemaGeneral_obtenLista();

                if(sistema.ticket == "58")
                {
                    reportDocument.Load(ruta + "reporte_ticket58mm.rpt");
                    reportDocument.SetDataSource(data);
                }
                else 
                {
                    reportDocument.Load(ruta + "reporte_ticket80mm.rpt");
                    reportDocument.SetDataSource(data);

                }
                

                //reportDocument.DataDefinition.FormulaFields["codigobarras"].Text = "'" + imgsrc + "'";
                reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, fileName);


                db.conexion.Close();
                reportDocument.Close();
            }
            catch
            {
                db.conexion.Close();
                reportDocument.Close();
            }
           

            return name;

        }

        //public string GetTicketPagoTarjeta(long? ventaId) {

        //    iTextSharp.text.Font Encabezadoticket = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 20, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
        //    iTextSharp.text.Font EncabezadoTableticket = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 18, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
        //    iTextSharp.text.Font contenidoTableticket = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 18, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
        //    iTextSharp.text.Font MontosTotalesFont = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 18, iTextSharp.text.Font.BOLD, BaseColor.BLACK);

        //    string apppath = System.AppDomain.CurrentDomain.BaseDirectory;
        //    string name = Guid.NewGuid().ToString() + ".pdf";
        //    string fileName = apppath + "/Reportes/" + name;
        //    string logo = apppath + "/Reportes/logo.png";
        //    Document doc = new Document(PageSize.LETTER);
        //    //Document doc = new Document(new Rectangle(288f, 144f), 0, 0, 0, 0);
        //    PdfWriter writer = PdfWriter.GetInstance(doc, new FileStream(fileName, FileMode.Create));
        //    doc.Open();

        //    ventaController ventactrl = new ventaController();
        //    ventaEntity venta = new ventaEntity();
        //    venta = ventactrl.venta_obten(ventaId.Value);

        //    PdfPTable tblDatos = new PdfPTable(1);
        //    tblDatos.WidthPercentage = 100;
        //    tblDatos.AddCell(CreateCellCenter(false, venta.caja.sucursal.nombre.ToUpper(), BaseColor.WHITE, Encabezadoticket, 0));
        //    //tblDatos.AddCell(CreateCellCenter(false, venta.caja..ToUpper(), BaseColor.WHITE, EncabezadoTitulo, 0));
        //    tblDatos.AddCell(CreateCellCenter(false, venta.caja.sucursal.calle + " #" + venta.caja.sucursal.numero + " COL." + venta.caja.sucursal.colonia, BaseColor.WHITE, Encabezadoticket, 0));
        //    tblDatos.AddCell(CreateCellCenter(false, venta.caja.sucursal.ciudad + " C.P " + venta.caja.sucursal.cp, BaseColor.WHITE, Encabezadoticket, 0));
        //    tblDatos.AddCell(CreateCellCenter(false, venta.caja.sucursal.telefono, BaseColor.WHITE, Encabezadoticket, 0));

        //    doc.Add(tblDatos);

        //    PdfPTable tblInfo = new PdfPTable(3);
        //    tblInfo.WidthPercentage = 100;
        //    tblInfo.AddCell(CreateCellCenter(false, "", BaseColor.WHITE, EncabezadoTitulo, 3));
        //    tblInfo.AddCell(CreateCellCenter(false, "CAJERO:" + venta.usuarioCajero.nombre, BaseColor.WHITE, Encabezadoticket, 3));
        //    tblInfo.AddCell(CreateCellCenter(false, "FECHA: " + DateTime.Parse(venta.fecha2).ToShortDateString(), BaseColor.WHITE, Encabezadoticket, 3));
        //    doc.Add(tblInfo);

        //    PdfPTable tblpago = new PdfPTable(3);
        //    tblpago.AddCell(CreateCellCenter(false, "REFERENCIA: "+  , BaseColor.WHITE, EncabezadoTitulo, 3));


        //}

        public class ResumenMonto
        {

            public string concepto {
                get;
                set; }

            public decimal monto { get; set; }
        }



        public string GetReporteVentasByVendedor(string fechaInicio, string fechaFin, int? usuario, int? sucursalId, int? departamentoId, int? categoriaId, int? articuloId, int? orden, int? tipoDescarga, bool? detalle, int ? vendedorId)
        {
            string apppath = System.AppDomain.CurrentDomain.BaseDirectory;
            string ruta = apppath + "/Rpt/";
            string name = Guid.NewGuid().ToString() + ".pdf";
            string fileName = apppath + "/Reportes/" + name;
            ReportDocument reportDocument = new ReportDocument();

            dbHelper db = new dbHelper();

            try
            {
                db.agregarParametro("@fechaInicio", SqlDbType.VarChar, fechaInicio);
                db.agregarParametro("@fechaFin", SqlDbType.VarChar, fechaFin);
                db.agregarParametro("@usuarioId", SqlDbType.BigInt, usuario);
                db.agregarParametro("@sucursalId", SqlDbType.BigInt, sucursalId);
                db.agregarParametro("@departamentoId", SqlDbType.BigInt, departamentoId);
                db.agregarParametro("@categoriaId", SqlDbType.BigInt, categoriaId);
                db.agregarParametro("@articuloId", SqlDbType.BigInt, articuloId);
                db.agregarParametro("@vendedorId", SqlDbType.BigInt, vendedorId);
                db.agregarParametro("@orden", SqlDbType.BigInt, orden);
                var sql = "";
                var report = "";
                
                sql = "reporte_ventasVendedor";
                report = "reporte_ventasVendedor.rpt";
                
                DataSet ds = db.consultaDS(sql);

                ds.Tables[0].TableName = "titulo";
                ds.Tables[1].TableName = "subtitulo";
                ds.Tables[2].TableName = "sistema";
                ds.Tables[3].TableName = "sucursal";
                ds.Tables[4].TableName = "ventas";

                reportDocument.Load(ruta + report);
                reportDocument.SetDataSource(ds);
                if (tipoDescarga == 1)
                {
                    reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, fileName);
                }
                else
                {
                    reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.ExcelWorkbook, fileName);
                }

                db.conexion.Close();
                reportDocument.Close();
            }
            catch
            {
                db.conexion.Close();
                reportDocument.Close();
            }


            return name;
        }


        public string GetReporteVentasByCajero(string fechaInicio, string fechaFin, int? usuario,int ? sucursalId, int? departamentoId, int? categoriaId, int? articuloId, int? orden,int ? tipoDescarga, bool? detalle)
        {
            string apppath = System.AppDomain.CurrentDomain.BaseDirectory;
            string ruta = apppath + "/Rpt/";
            string name = Guid.NewGuid().ToString() + ".pdf";
            string fileName = apppath + "/Reportes/" + name;
            ReportDocument reportDocument = new ReportDocument();

            dbHelper db = new dbHelper();

            try
            {
                db.agregarParametro("@fechaInicio", SqlDbType.VarChar, fechaInicio);
                db.agregarParametro("@fechaFin", SqlDbType.VarChar, fechaFin);
                db.agregarParametro("@usuarioId", SqlDbType.BigInt, usuario);
                db.agregarParametro("@sucursalId", SqlDbType.BigInt, sucursalId);
                db.agregarParametro("@departamentoId", SqlDbType.BigInt, departamentoId);
                db.agregarParametro("@categoriaId", SqlDbType.BigInt, categoriaId);
                db.agregarParametro("@articuloId", SqlDbType.BigInt, articuloId);
                db.agregarParametro("@orden", SqlDbType.BigInt, orden);
                var sql = "";
                var report = "";
                if (detalle == true)
                {
                    sql = "reporte_ventasCajeroDetalle";
                    report = "reporte_ventasSucursaldetalle.rpt";

                }
                else
                {
                    sql = "reporte_ventasCajero";
                    report = "reporte_ventasSucursal.rpt";
                }
                DataSet ds = db.consultaDS(sql);

                ds.Tables[0].TableName = "titulo";
                ds.Tables[1].TableName = "subtitulo";
                ds.Tables[2].TableName = "sistema";
                ds.Tables[3].TableName = "sucursal";
                ds.Tables[4].TableName = "ventas";

                reportDocument.Load(ruta + report);
                reportDocument.SetDataSource(ds);
                if (tipoDescarga == 1)
                {
                    reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, fileName);
                }
                else
                {
                    reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.ExcelWorkbook, fileName);
                }

                db.conexion.Close();
                reportDocument.Close();
            }
            catch
            {
                db.conexion.Close();
                reportDocument.Close();
            }
           

            return name;
        }

        public string GetReporteVentasBySucursal( string fechaInicio, string fechaFin, int ? sucursalId, int? departamentoId, int? categoriaId, int? articuloId, int? orden,int ? tipoDescarga, bool ? detalle)
        {
            
           

            string apppath = System.AppDomain.CurrentDomain.BaseDirectory;
            string ruta = apppath + "/Rpt/";
            string name = Guid.NewGuid().ToString() + ".pdf";
            string fileName = apppath + "/Reportes/" + name;
            ReportDocument reportDocument = new ReportDocument();

            dbHelper db = new dbHelper();

            try
            {
                db.agregarParametro("@fechaInicio", SqlDbType.VarChar, fechaInicio);
                db.agregarParametro("@fechaFin", SqlDbType.VarChar, fechaFin);
                db.agregarParametro("@sucursalId", SqlDbType.BigInt, sucursalId);
                db.agregarParametro("@departamentoId", SqlDbType.BigInt, departamentoId);
                db.agregarParametro("@categoriaId", SqlDbType.BigInt, categoriaId);
                db.agregarParametro("@articuloId", SqlDbType.BigInt, articuloId);
                db.agregarParametro("@orden", SqlDbType.BigInt, orden);
                var sql = "";
                var report = "";
                if (detalle == true)
                {
                    sql = "reporte_ventasSucursaldetalle";
                    report = "reporte_ventasSucursaldetalle.rpt";

                }
                else
                {
                    sql = "reporte_ventasSucursal";
                    report = "reporte_ventasSucursal.rpt";
                }
                DataSet ds = db.consultaDS(sql);

                ds.Tables[0].TableName = "titulo";
                ds.Tables[1].TableName = "subtitulo";
                ds.Tables[2].TableName = "sistema";
                ds.Tables[3].TableName = "sucursal";
                ds.Tables[4].TableName = "ventas";

                reportDocument.Load(ruta + report);
                reportDocument.SetDataSource(ds);
                if (tipoDescarga == 1)
                {
                    reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, fileName);
                }
                else
                {
                    reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.ExcelWorkbook, fileName);
                }

                db.conexion.Close();
                reportDocument.Close();
            }
            catch
            {
                db.conexion.Close();
                reportDocument.Close();
            }
            
           
             
            return name;
        }


        public string GetReporteRecepcion(long? sucursalId, int? departamentoId, int? categoriaId,int ? articuloId, string fechaInicio, string fechaFin,int ? proveedorId, int? tipoDescarga)
        {
            string apppath = System.AppDomain.CurrentDomain.BaseDirectory;
            string ruta = apppath + "/Rpt/";
            ReportDocument reportDocument = new ReportDocument();

            dbHelper db = new dbHelper();


            try
            {
                db.agregarParametro("@sucursalId", SqlDbType.BigInt, sucursalId);
                db.agregarParametro("@departamentoId", SqlDbType.BigInt, departamentoId);
                db.agregarParametro("@categoriaId", SqlDbType.BigInt, categoriaId);
                db.agregarParametro("@articuloId", SqlDbType.BigInt, articuloId);
                db.agregarParametro("@proveedorId", SqlDbType.BigInt, proveedorId);
                db.agregarParametro("@fechaInicio", SqlDbType.VarChar, fechaInicio);
                db.agregarParametro("@fechaFin", SqlDbType.VarChar, fechaFin);

                DataSet ds = db.consultaDS("reporte_recepcion");

                ds.Tables[0].TableName = "titulo";
                ds.Tables[1].TableName = "subtitulo";
                ds.Tables[2].TableName = "sistema";
                ds.Tables[3].TableName = "detalle";


                reportDocument.Load(ruta + "reporte_recepcion.rpt");
                reportDocument.SetDataSource(ds);

                if (tipoDescarga == 1)
                {
                    string name = Guid.NewGuid().ToString() + ".pdf";
                    string fileName = apppath + "/Reportes/" + name;
                    reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, fileName);

                    db.conexion.Close();
                    reportDocument.Close();
                    return name;
                }
                else
                {
                    string name = Guid.NewGuid().ToString() + ".xls";
                    string fileName = apppath + "/Reportes/" + name;
                    reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.Excel, fileName);

                    db.conexion.Close();
                    reportDocument.Close();
                    return name;
                }

                
            }
            catch
            {
                db.conexion.Close();
                reportDocument.Close();
                return "";
            }
            


        }

        public string GetReporteInventario(long? sucursalId,int ? departamentoId,int ? categoriaId,int ? estatus,int ? tipoDescarga)
        {
            string apppath = System.AppDomain.CurrentDomain.BaseDirectory;
            string ruta = apppath + "/Rpt/";           
            ReportDocument reportDocument = new ReportDocument();

            dbHelper db = new dbHelper();

            try
            {

                db.agregarParametro("@sucursalId", SqlDbType.BigInt, sucursalId);
                db.agregarParametro("@departamentoId", SqlDbType.BigInt, departamentoId);
                db.agregarParametro("@categoriaId", SqlDbType.BigInt, categoriaId);
                db.agregarParametro("@estatus", SqlDbType.BigInt, estatus);
                DataSet ds = db.consultaDS("reporte_inventario");

                ds.Tables[0].TableName = "titulo";
                ds.Tables[1].TableName = "subtitulo";
                ds.Tables[2].TableName = "sistema";
                ds.Tables[3].TableName = "inventario";


                reportDocument.Load(ruta + "reporte_inventario.rpt");
                reportDocument.SetDataSource(ds);

                if (tipoDescarga == 1)
                {
                    string name = Guid.NewGuid().ToString() + ".pdf";
                    string fileName = apppath + "/Reportes/" + name;
                    reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, fileName);

                    db.conexion.Close();
                    reportDocument.Close();

                    return name;
                }
                else
                {
                    string name = Guid.NewGuid().ToString() + ".xls";
                    string fileName = apppath + "/Reportes/" + name;
                    reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.Excel, fileName);

                    db.conexion.Close();
                    reportDocument.Close();

                    return name;
                }

                
            }
            catch
            {
                db.conexion.Close();
                reportDocument.Close();
                return "";
            }
            


        }


        public string GetReporteVendedor(long? vendedorId, int? departamentoId, int? categoriaId, int? estatus, int? tipoDescarga)
        {
            string apppath = System.AppDomain.CurrentDomain.BaseDirectory;
            string ruta = apppath + "/Rpt/";
            ReportDocument reportDocument = new ReportDocument();

            dbHelper db = new dbHelper();

            try
            {

                db.agregarParametro("@vendedorId", SqlDbType.BigInt, vendedorId);
                db.agregarParametro("@departamentoId", SqlDbType.BigInt, departamentoId);
                db.agregarParametro("@categoriaId", SqlDbType.BigInt, categoriaId);
                db.agregarParametro("@estatus", SqlDbType.BigInt, 1);
                DataSet ds = db.consultaDS("reporte_inventario_vendedor");

                ds.Tables[0].TableName = "titulo";
                ds.Tables[1].TableName = "subtitulo";
                ds.Tables[2].TableName = "sistema";
                ds.Tables[3].TableName = "inventario";


                reportDocument.Load(ruta + "reporte_inventario.rpt");
                reportDocument.SetDataSource(ds);

                if (tipoDescarga == 1)
                {
                    string name = Guid.NewGuid().ToString() + ".pdf";
                    string fileName = apppath + "/Reportes/" + name;
                    reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, fileName);

                    db.conexion.Close();
                    reportDocument.Close();

                    return name;
                }
                else
                {
                    string name = Guid.NewGuid().ToString() + ".xls";
                    string fileName = apppath + "/Reportes/" + name;
                    reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.Excel, fileName);

                    db.conexion.Close();
                    reportDocument.Close();

                    return name;
                }


            }
            catch(Exception ex)
            {
                db.conexion.Close();
                reportDocument.Close();
                return "";
            }



        }

        public string GetReporteResurtido(long? sucursalId, int? departamentoId, int? categoriaId,int ? articuloId, int? tipoDescarga)
        {
            string apppath = System.AppDomain.CurrentDomain.BaseDirectory;
            string ruta = apppath + "/Rpt/";
            ReportDocument reportDocument = new ReportDocument();

            dbHelper db = new dbHelper();


            try
            {
                db.agregarParametro("@sucursalId", SqlDbType.BigInt, sucursalId);
                db.agregarParametro("@departamentoId", SqlDbType.BigInt, departamentoId);
                db.agregarParametro("@categoriaId", SqlDbType.BigInt, categoriaId);
                db.agregarParametro("@articuloId", SqlDbType.BigInt, articuloId);
                DataSet ds = db.consultaDS("reporte_resurtido");

                ds.Tables[0].TableName = "titulo";
                ds.Tables[1].TableName = "subtitulo";
                ds.Tables[2].TableName = "sistema";
                ds.Tables[3].TableName = "detalle";


                reportDocument.Load(ruta + "reporte_resurtido.rpt");
                reportDocument.SetDataSource(ds);

                if (tipoDescarga == 1)
                {
                    string name = Guid.NewGuid().ToString() + ".pdf";
                    string fileName = apppath + "/Reportes/" + name;
                    reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, fileName);

                    db.conexion.Close();
                    reportDocument.Close();

                    return name;
                }
                else
                {
                    string name = Guid.NewGuid().ToString() + ".xls";
                    string fileName = apppath + "/Reportes/" + name;
                    reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.Excel, fileName);

                    db.conexion.Close();
                    reportDocument.Close();

                    return name;
                }
                
            }
            catch
            {
                db.conexion.Close();
                reportDocument.Close();
                return "";
            }
           


        }


        public class MovimientoArticulo
        {
           public long departamentoId { get; set; }
            public long categoriaId { get; set; }
            public long articuloId { get; set; }
            public long id { get; set; }
            public DateTime fecha { get; set; }
            public string hora { get; set; }
            public string movimiento { get; set; }
            public string usuario { get; set; }
            public string articulo { get; set; }
            public string cantidad { get; set; }
            public string unidad { get; set; }
            public int cantidadInicial { get; set; }
            public int cantidadRecibida { get; set; }
            public int cantidadVenta { get; set; }
            public int CabtidadDevolucionProveedor { get; set; }
            public int CantidadActual { get; set; }
        }


        public string GetReporteMovimientosArticuloResumen(int? articuloId, int? departamentoId, int? categoriaId, int? sucursalId, string fechaInicio, string fechaFin,int ? tipoDescarga)
        {


            string apppath = System.AppDomain.CurrentDomain.BaseDirectory;
            string ruta = apppath + "/Rpt/";
            ReportDocument reportDocument = new ReportDocument();

            dbHelper db = new dbHelper();


            try
            {
                db.agregarParametro("@articuloId", SqlDbType.Int, articuloId);
                db.agregarParametro("@departamentoId", SqlDbType.Int, departamentoId);
                db.agregarParametro("@categoriaId", SqlDbType.Int, categoriaId);
                db.agregarParametro("@sucursalId", SqlDbType.Int, sucursalId);
                db.agregarParametro("@fechaInicio", SqlDbType.VarChar, fechaInicio);
                db.agregarParametro("@fechaFin", SqlDbType.VarChar, fechaFin);
               
                DataSet ds = db.consultaDS("reporte_movimientosArticulosResumen");

                ds.Tables[0].TableName = "titulo";
                ds.Tables[1].TableName = "subtitulo";
                ds.Tables[2].TableName = "sistema";
                ds.Tables[3].TableName = "detalle";


                reportDocument.Load(ruta + "reporte_movimientosArticulosResumen.rpt");
                reportDocument.SetDataSource(ds);

                if (tipoDescarga == 1)
                {
                    string name = Guid.NewGuid().ToString() + ".pdf";
                    string fileName = apppath + "/Reportes/" + name;
                    reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, fileName);

                    db.conexion.Close();
                    reportDocument.Close();

                    return name;
                }
                else
                {
                    string name = Guid.NewGuid().ToString() + ".xls";
                    string fileName = apppath + "/Reportes/" + name;
                    reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.Excel, fileName);

                    db.conexion.Close();
                    reportDocument.Close();

                    return name;
                }

            }
            catch
            {
                db.conexion.Close();
                reportDocument.Close();
                return "";
            }

        }

        public string GetReporteMovimientosArticulos(int? articuloId, int? departamentoId, int? categoriaId, int? sucursalId, string fechaInicio, string fechaFin ,string fechaInicio2, string fechaFin2)
        {
            string apppath = System.AppDomain.CurrentDomain.BaseDirectory;
            string name = Guid.NewGuid().ToString() + ".pdf";
            string fileName = apppath + "/Reportes/" + name;
            Document doc = new Document(PageSize.LETTER);
            PdfWriter writer = PdfWriter.GetInstance(doc, new FileStream(fileName, FileMode.Create));
            doc.Open();

            sucursalEntity se = sucursalController.sucursal_obten(sucursalId);
           
            doc.Add(createHeaderReporteRangos("MOVIMIENTOS GENERALES ARTICULOS", "SUCURSAL" +se.nombre, "Desde " + fechaInicio2 +" Hasta " + fechaFin2 ));

            List<departamentoEntity> departamentos = new List<departamentoEntity>();
            List<categoriaEntity> categorias = new List<categoriaEntity>();
            List<MovimientoArticulo> movimientos = new List<MovimientoArticulo>();
            List<articuloEntity> articulos = new List<articuloEntity>();

            dbHelper db = new dbHelper();
            db.agregarParametro("@articuloId", SqlDbType.Int, articuloId);
            db.agregarParametro("@departamentoId", SqlDbType.Int, departamentoId);
            db.agregarParametro("@categoriaId", SqlDbType.Int, categoriaId);
            db.agregarParametro("@sucursalId", SqlDbType.Int, sucursalId);
            db.agregarParametro("@fechaInicio", SqlDbType.VarChar, fechaInicio);
            db.agregarParametro("@fechaFin", SqlDbType.VarChar, fechaFin);        
            SqlDataReader reader = db.consultaReader("reporte_movimientosArticulos");
            while (reader.HasRows)
            {
                while (reader.Read())
                {
                    departamentoEntity dep = new departamentoEntity();
                    dep.id = Int32.Parse(reader[0].ToString());
                    dep.nombre = reader[1].ToString();
                    departamentos.Add(dep);
                }
                reader.NextResult();
                while (reader.Read())
                {
                    categoriaEntity cat = new categoriaEntity();
                    cat.id = Int32.Parse(reader[0].ToString());
                    cat.nombre = reader[1].ToString();
                    cat.departamentoId = Int32.Parse(reader[2].ToString());
                    categorias.Add(cat);

                }
                reader.NextResult();
                while (reader.Read())
                {
                    articuloEntity art = new articuloEntity();
                    art.id = Int32.Parse(reader[0].ToString());
                    art.nombre = reader[1].ToString();
                    art.categoriaId = Int32.Parse(reader[2].ToString());                  
                    articulos.Add(art);
                }
                reader.NextResult();
                while (reader.Read())
                {
                    MovimientoArticulo mov = new MovimientoArticulo();
                    mov.departamentoId = long.Parse(reader[0].ToString());
                    mov.categoriaId = long.Parse(reader[1].ToString());
                    mov.articuloId = long.Parse(reader[2].ToString());
                    mov.id = long.Parse(reader[3].ToString());
                    mov.fecha = DateTime.Parse(reader[4].ToString());
                    mov.hora = reader[5].ToString();
                    mov.movimiento = reader[6].ToString();
                    mov.usuario = reader[7].ToString();
                    mov.articulo = reader[8].ToString();
                    mov.cantidad = reader[9].ToString();
                    mov.unidad = reader[10].ToString();
                    movimientos.Add(mov);
                }
            }

            departamentos.ForEach(x =>
            {

               // Paragraph p = new Paragraph("DEPARTAMENTO: " + x.nombre);
               // doc.Add(p);

                categorias.Where(y=> y.departamentoId== x.id).ToList().ForEach(y =>
                {
                    //Paragraph p2 = new Paragraph("CATEGORIA: " + y.nombre);
                   // doc.Add(p2);

                    articulos.Where(yy => yy.categoriaId == y.id ).ToList().ForEach(yy =>
                    {

                       // Paragraph p3 = new Paragraph("ARTICULO: " + yy.nombre);
                        //doc.Add(p3);

                        PdfPTable Table3 = new PdfPTable(6);
                        Table3.WidthPercentage = 100;
                        Table3.SpacingBefore = 30f;


                        Table3.AddCell(CreateCell(false, "Departamento", BaseColor.WHITE, _headertable, 0));
                        Table3.AddCell(CreateCell(false, x.nombre, BaseColor.WHITE, _headertablerow, 0));
                        Table3.AddCell(CreateCell(false, "Categoria", BaseColor.WHITE, _headertable, 0));
                        Table3.AddCell(CreateCell(false, y.nombre, BaseColor.WHITE, _headertablerow, 0));
                        Table3.AddCell(CreateCell(false, "Artículo", BaseColor.WHITE, _headertable, 0));
                        Table3.AddCell(CreateCell(false, yy.nombre, BaseColor.WHITE, _headertablerow, 0));

                        Table3.AddCell(CreateCell(false, "FECHA", BaseColor.WHITE, _headertable, 0));
                        Table3.AddCell(CreateCell(false, "HORA", BaseColor.WHITE, _headertable, 0));
                        Table3.AddCell(CreateCell(false, "MOVIMIENTO", BaseColor.WHITE, _headertable, 0));
                        Table3.AddCell(CreateCell(false, "USUARIO", BaseColor.WHITE, _headertable, 0));
                        Table3.AddCell(CreateCell(false, "CANTIDAD", BaseColor.WHITE, _headertable, 0));
                        Table3.AddCell(CreateCell(false, "UNIDAD", BaseColor.WHITE, _headertable, 0));
                        

                        movimientos.Where(zz=> zz.articuloId == yy.id && zz.categoriaId == y.id).ToList().ForEach(zz=> {

                           
                            Table3.AddCell(CreateCell(false,zz.fecha.ToShortDateString() , BaseColor.WHITE, _headertablerow, 0));
                            Table3.AddCell(CreateCell(false,zz.fecha.ToShortTimeString() , BaseColor.WHITE, _headertablerow, 0));
                            Table3.AddCell(CreateCell(false,zz.movimiento , BaseColor.WHITE, _headertablerow, 0));
                            Table3.AddCell(CreateCell(false,zz.usuario , BaseColor.WHITE, _headertablerow, 0));
                            Table3.AddCell(CreateCell(false, zz.cantidad, BaseColor.WHITE, _headertablerow, 0));
                            Table3.AddCell(CreateCell(false, zz.unidad, BaseColor.WHITE, _headertablerow, 0));

                        });

                        doc.Add(Table3);


                        PdfPTable Table4 = new PdfPTable(6);
                        db = new dbHelper();
                        db.agregarParametro("@articuloId", SqlDbType.Int, yy.id);                   
                        db.agregarParametro("@sucursalId", SqlDbType.Int, sucursalId);
                        db.agregarParametro("@fechaInicio", SqlDbType.VarChar, fechaInicio);
                        db.agregarParametro("@fechaFin", SqlDbType.VarChar, fechaFin);
                        SqlDataReader reader2 = db.consultaReader("reporte_movimientosArticulos_resumen_articulo");
                        while (reader2.Read())
                        {                           
                            Table4.WidthPercentage = 100;
                            Table4.SpacingBefore = 30f;
                            Table4.AddCell(CreateCellRight(false, "Cantidad Inicial", BaseColor.WHITE, _smallFont, 5));
                            Table4.AddCell(CreateCell(false, reader2[0].ToString(), BaseColor.WHITE, _smallFont, 1));
                            Table4.AddCell(CreateCellRight(false, "Cantidad Recibido", BaseColor.WHITE, _smallFont, 5));
                            Table4.AddCell(CreateCell(false, reader2[1].ToString(), BaseColor.WHITE, _smallFont, 1));
                            Table4.AddCell(CreateCellRight(false, "Cantidad Venta", BaseColor.WHITE, _smallFont, 5));
                            Table4.AddCell(CreateCell(false, reader2[2].ToString(), BaseColor.WHITE, _smallFont, 1));
                            Table4.AddCell(CreateCellRight(false, "Cantidad Devolución proveedor", BaseColor.WHITE, _smallFont, 5));
                            Table4.AddCell(CreateCell(false, reader2[3].ToString(), BaseColor.WHITE, _smallFont, 1));
                            Table4.AddCell(CreateCellRight(false, "Cantidad Actual", BaseColor.WHITE, _smallFont, 5));
                            Table4.AddCell(CreateCell(false, reader2[4].ToString(), BaseColor.WHITE, _smallFont, 1));
                        }                      

                        doc.Add(Table4);

                    });                    

                });

            });

            doc.Close();
            writer.Close();
            return name;
        }

        

        public string GetCortes(int ? tipo,String fechaInicio,String fechaFin,int ? sucursalId,int ? usuarioId,int ? tipoDescarga,int ? usuario)
        {
            string apppath = System.AppDomain.CurrentDomain.BaseDirectory;
            string ruta = apppath + "/Rpt/";           
            ReportDocument reportDocument = new ReportDocument();

            dbHelper db = new dbHelper();


            try
            {


                db.agregarParametro("@tipo", SqlDbType.BigInt, tipo);
                db.agregarParametro("@fechaInicio", SqlDbType.BigInt, fechaInicio);
                db.agregarParametro("@fechaFin", SqlDbType.BigInt, fechaFin);
                db.agregarParametro("@sucursalId", SqlDbType.BigInt, sucursalId);
                db.agregarParametro("@usuarioId", SqlDbType.BigInt, usuarioId);
                db.agregarParametro("@usuario", SqlDbType.BigInt, usuario);
                DataSet ds = db.consultaDS("reporte_cortes");



                ds.Tables[0].TableName = "titulo";
                ds.Tables[1].TableName = "subtitulo";
                ds.Tables[2].TableName = "entidad";
                ds.Tables[3].TableName = "ventas";
                reportDocument.Load(ruta + "reporte_cortes.rpt");
                reportDocument.SetDataSource(ds);



                if (tipoDescarga == 1)
                {
                    string name = Guid.NewGuid().ToString() + ".pdf";
                    string fileName = apppath + "/Reportes/" + name;
                    reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, fileName);

                    db.conexion.Close();
                    reportDocument.Close();

                    try
                    {
                        SistemaGeneralController sistema = new SistemaGeneralController();
                        sistema.EnviaReporte(fileName, "erictics@gmail.com");
                    }
                    catch(Exception ex)
                    {

                    }

                    return name;
                }
                else
                {
                    string name = Guid.NewGuid().ToString() + ".xls";
                    string fileName = apppath + "/Reportes/" + name;
                    reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.Excel, fileName);


                    db.conexion.Close();
                    reportDocument.Close();

                    return name;


                }




            }
            catch
            {

                db.conexion.Close();
                reportDocument.Close();
                return "";
            }
            

            
        }



        public string GetReporteArticulos(List<articuloEntity> articulos, int ? op)
        {

            string apppath = System.AppDomain.CurrentDomain.BaseDirectory;
            string name = Guid.NewGuid().ToString() + ".pdf";
            string fileName = apppath + "/Reportes/" + name;
            Document doc = new Document(PageSize.LETTER, 0f, 0f, 0f, 0f);
            
            PdfWriter writer = PdfWriter.GetInstance(doc, new FileStream(fileName, FileMode.Create));
            doc.Open();
            doc.SetMargins(0, 0, 0, 0);
            doc.Add(createHeaderReporte("", ""));
            PdfPTable TableDetalle = null;
            if (op == 1)
            {
                TableDetalle = new PdfPTable(1);
            }
            if (op == 2)
            {
                TableDetalle = new PdfPTable(2);
            }
            if (op == 3)
            {
                TableDetalle = new PdfPTable(3);
            }
            if (op == 4)
            {
                TableDetalle = new PdfPTable(4);
            }

            TableDetalle.WidthPercentage = 100;
            TableDetalle.HorizontalAlignment = Element.ALIGN_LEFT;


            articulos.ForEach(x =>
            {
               

                PdfPCell textCell = new PdfPCell(new Phrase(x.nombre + "\n " + x.precioLista.ToString("C"), _smallFont));
                textCell.BorderWidth = 0;
                //cell.BackgroundColor = color;
                //cell.Colspan = colspan.Value;
                textCell.HorizontalAlignment = Element.ALIGN_LEFT; ;
                


                BarcodeLib.Barcode b = new BarcodeLib.Barcode();
                b.IncludeLabel = true;
                string imgsrc = apppath + "/Reportes/" + Guid.NewGuid() + ".jpg";
                System.Drawing.Image img = b.Encode(BarcodeLib.TYPE.EAN13, x.upc, Color.Black, Color.White, 290, 120);
                img.Save(imgsrc);


                iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(imgsrc);
                jpg.ScaleAbsolute(100f, 70f);
                PdfPCell imageCell = new PdfPCell(jpg);
                imageCell.Colspan = 1; // either 1 if you need to insert one cell
                imageCell.Border = 0;

                TableDetalle.AddCell(textCell);
                TableDetalle.AddCell(imageCell);

            });

           


            //iTextSharp.text.Font Contenidotbl = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 7, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
            //PdfPCell points = new PdfPCell(new Phrase("and is therefore entitled to 2 points", Contenidotbl));
            //points.Colspan = 2;
            //points.Border = 0;
            //points.PaddingTop = 40f;
            //points.HorizontalAlignment = 1;//0=Left, 1=Centre, 2=Right

            //// add a image
            //iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(imgsrc);
            //jpg.ScaleAbsolute(100f, 70f);
            //PdfPCell imageCell = new PdfPCell(jpg);
            //imageCell.Colspan = 1; // either 1 if you need to insert one cell
            //imageCell.Border = 0;
            //imageCell.setHorizontalAlignment(Element.ALIGN_CENTER);


           // TableDetalle.AddCell(points);
            // add a image
           
            

            doc.Add(TableDetalle);
            doc.Close();
            writer.Close();
            return name;
        }


        public string GetReporteEntregaParcial(long? id)
        {

            entregaParcialController epc = new entregaParcialController();
            EntregaParcialEntity ep = epc.entregaParcial_obten(id);

            iTextSharp.text.Font Encabezadotbl = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 9, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
            iTextSharp.text.Font Contenidotbl = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 7, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
            iTextSharp.text.Font MontosTotalesFontCortes = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 10, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
            iTextSharp.text.Font MontosTotalesFontCortesTotal = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 13, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
            string apppath = System.AppDomain.CurrentDomain.BaseDirectory;
            string name = Guid.NewGuid().ToString() + ".pdf";
            string fileName = apppath + "/Reportes/" + name;
            Document doc = new Document(PageSize.LETTER);
            PdfWriter writer = PdfWriter.GetInstance(doc, new FileStream(fileName, FileMode.Create));
            doc.Open();            
            doc.Add(createHeaderReporte("", "Entrega Parcial No." + id));


            

            PdfPTable TableDetalle = new PdfPTable(6);
            TableDetalle.AddCell(CreateCellCenter(false,"Usuario:" + ep.usuario.nombre, BaseColor.WHITE, MontosTotalesFontCortes, 2));
            TableDetalle.AddCell(CreateCellCenter(false, "Fecha:" + ep.fecha2, BaseColor.WHITE, MontosTotalesFontCortes, 2));
            TableDetalle.AddCell(CreateCellCenter(false, "Hora:" + ep.fecha.ToShortTimeString(), BaseColor.WHITE, MontosTotalesFontCortes, 2));
            TableDetalle.AddCell(CreateCellCenter(false, "Referencia:" + ep.referencia, BaseColor.WHITE, MontosTotalesFontCortes, 6));
            TableDetalle.SpacingAfter = 30;
            TableDetalle.SpacingBefore = 30;
            TableDetalle.WidthPercentage = 100;
            doc.Add(TableDetalle);

            PdfPTable TableEntrega = new PdfPTable(2);
            TableEntrega.WidthPercentage = 100;


            PdfPTable TableEntregaBilletes = new PdfPTable(3);
            TableEntregaBilletes.WidthPercentage = 100;
            TableEntregaBilletes.AddCell(CreateCellCenter(false, "DESGLOSE DE BILLETE", BaseColor.WHITE, MontosTotalesFontCortes, 3));
            TableEntregaBilletes.AddCell(CreateCellCenter(false, "DENOMINACION" , BaseColor.WHITE, MontosTotalesFontCortes, 1));
            TableEntregaBilletes.AddCell(CreateCellCenter(false, "CANTIDAD" , BaseColor.WHITE, MontosTotalesFontCortes, 1));
            TableEntregaBilletes.AddCell(CreateCellCenter(false, "IMPORTE", BaseColor.WHITE, MontosTotalesFontCortes, 1));

            TableEntregaBilletes.AddCell(CreateCellCenter(false, "B1000", BaseColor.WHITE, MontosTotalesFontCortes, 1));
            TableEntregaBilletes.AddCell(CreateCellCenter(false, ep.b1000.ToString(), BaseColor.WHITE, MontosTotalesFontCortes, 1));
            TableEntregaBilletes.AddCell(CreateCellCenter(false, (ep.b1000 * 1000).ToString("C"), BaseColor.WHITE, MontosTotalesFontCortes, 1));

            TableEntregaBilletes.AddCell(CreateCellCenter(false, "B500", BaseColor.WHITE, MontosTotalesFontCortes, 1));
            TableEntregaBilletes.AddCell(CreateCellCenter(false, ep.b500.ToString(), BaseColor.WHITE, MontosTotalesFontCortes, 1));
            TableEntregaBilletes.AddCell(CreateCellCenter(false, (ep.b500 * 500).ToString("C"), BaseColor.WHITE, MontosTotalesFontCortes, 1));

            TableEntregaBilletes.AddCell(CreateCellCenter(false, "B200", BaseColor.WHITE, MontosTotalesFontCortes, 1));
            TableEntregaBilletes.AddCell(CreateCellCenter(false, ep.b200.ToString(), BaseColor.WHITE, MontosTotalesFontCortes, 1));
            TableEntregaBilletes.AddCell(CreateCellCenter(false, (ep.b200 * 200).ToString("C"), BaseColor.WHITE, MontosTotalesFontCortes, 1));


            TableEntregaBilletes.AddCell(CreateCellCenter(false, "B100", BaseColor.WHITE, MontosTotalesFontCortes, 1));
            TableEntregaBilletes.AddCell(CreateCellCenter(false, ep.b100.ToString(), BaseColor.WHITE, MontosTotalesFontCortes, 1));
            TableEntregaBilletes.AddCell(CreateCellCenter(false, (ep.b100 * 100).ToString("C"), BaseColor.WHITE, MontosTotalesFontCortes, 1));


            TableEntregaBilletes.AddCell(CreateCellCenter(false, "B50", BaseColor.WHITE, MontosTotalesFontCortes, 1));
            TableEntregaBilletes.AddCell(CreateCellCenter(false, ep.b50.ToString(), BaseColor.WHITE, MontosTotalesFontCortes, 1));
            TableEntregaBilletes.AddCell(CreateCellCenter(false, (ep.b50 * 50).ToString("C"), BaseColor.WHITE, MontosTotalesFontCortes, 1));


            TableEntregaBilletes.AddCell(CreateCellCenter(false, "B20", BaseColor.WHITE, MontosTotalesFontCortes, 1));
            TableEntregaBilletes.AddCell(CreateCellCenter(false, ep.b20.ToString(), BaseColor.WHITE, MontosTotalesFontCortes, 1));
            TableEntregaBilletes.AddCell(CreateCellCenter(false, (ep.b20 * 20).ToString("C"), BaseColor.WHITE, MontosTotalesFontCortes, 1));

            decimal totalBilletes = decimal.Parse((ep.b1000 * 1000).ToString()) + decimal.Parse((ep.b500 * 500).ToString()) + decimal.Parse((ep.b200 * 200).ToString()) + decimal.Parse((ep.b100 * 100).ToString()) + decimal.Parse((ep.b50 * 50).ToString()) + decimal.Parse((ep.b20 * 20).ToString()) ;
            TableEntregaBilletes.AddCell(CreateCellCenter(false, "TOTAL MONEDAS " + totalBilletes.ToString("C"), BaseColor.WHITE, MontosTotalesFontCortes, 3));


            PdfPCell cell1 = new PdfPCell(TableEntregaBilletes);
            TableEntrega.AddCell(cell1);



            PdfPTable TableEntregaMonedas = new PdfPTable(3);
            TableEntregaMonedas.WidthPercentage = 100;
            TableEntregaMonedas.AddCell(CreateCellCenter(false, "DESGLOSE DE MONEDA" , BaseColor.WHITE, MontosTotalesFontCortes, 3));
            TableEntregaMonedas.AddCell(CreateCellCenter(false, "DENOMINACION" , BaseColor.WHITE, MontosTotalesFontCortes, 1));
            TableEntregaMonedas.AddCell(CreateCellCenter(false, "CANTIDAD", BaseColor.WHITE, MontosTotalesFontCortes, 1));
            TableEntregaMonedas.AddCell(CreateCellCenter(false, "IMPORTE", BaseColor.WHITE, MontosTotalesFontCortes, 1));

            TableEntregaMonedas.AddCell(CreateCellCenter(false, "M100", BaseColor.WHITE, MontosTotalesFontCortes, 1));
            TableEntregaMonedas.AddCell(CreateCellCenter(false, ep.m100.ToString("C") , BaseColor.WHITE, MontosTotalesFontCortes, 1));
            TableEntregaMonedas.AddCell(CreateCellCenter(false, (ep.m100 * 100).ToString("C"), BaseColor.WHITE, MontosTotalesFontCortes, 1));

            TableEntregaMonedas.AddCell(CreateCellCenter(false, "M20", BaseColor.WHITE, MontosTotalesFontCortes, 1));
            TableEntregaMonedas.AddCell(CreateCellCenter(false, ep.m20.ToString("C"), BaseColor.WHITE, MontosTotalesFontCortes, 1));
            TableEntregaMonedas.AddCell(CreateCellCenter(false, (ep.m20 * 20).ToString("C"), BaseColor.WHITE, MontosTotalesFontCortes, 1));

            TableEntregaMonedas.AddCell(CreateCellCenter(false, "M10", BaseColor.WHITE, MontosTotalesFontCortes, 1));
            TableEntregaMonedas.AddCell(CreateCellCenter(false, ep.m10.ToString("C"), BaseColor.WHITE, MontosTotalesFontCortes, 1));
            TableEntregaMonedas.AddCell(CreateCellCenter(false, (ep.m10 * 10).ToString("C"), BaseColor.WHITE, MontosTotalesFontCortes, 1));

            TableEntregaMonedas.AddCell(CreateCellCenter(false, "M5", BaseColor.WHITE, MontosTotalesFontCortes, 1));
            TableEntregaMonedas.AddCell(CreateCellCenter(false, ep.m5.ToString("C"), BaseColor.WHITE, MontosTotalesFontCortes, 1));
            TableEntregaMonedas.AddCell(CreateCellCenter(false, (ep.m5 * 5).ToString("C"), BaseColor.WHITE, MontosTotalesFontCortes, 1));

            TableEntregaMonedas.AddCell(CreateCellCenter(false, "M2", BaseColor.WHITE, MontosTotalesFontCortes, 1));
            TableEntregaMonedas.AddCell(CreateCellCenter(false, ep.m2.ToString("C"), BaseColor.WHITE, MontosTotalesFontCortes, 1));
            TableEntregaMonedas.AddCell(CreateCellCenter(false, (ep.m2 * 2).ToString("C"), BaseColor.WHITE, MontosTotalesFontCortes, 1));

            TableEntregaMonedas.AddCell(CreateCellCenter(false, "M1", BaseColor.WHITE, MontosTotalesFontCortes, 1));
            TableEntregaMonedas.AddCell(CreateCellCenter(false, ep.m1.ToString("C"), BaseColor.WHITE, MontosTotalesFontCortes, 1));
            TableEntregaMonedas.AddCell(CreateCellCenter(false, (ep.m1 * 1).ToString("C"), BaseColor.WHITE, MontosTotalesFontCortes, 1));

            TableEntregaMonedas.AddCell(CreateCellCenter(false, "M050", BaseColor.WHITE, MontosTotalesFontCortes, 1));
            TableEntregaMonedas.AddCell(CreateCellCenter(false, ep.m050.ToString("C"), BaseColor.WHITE, MontosTotalesFontCortes, 1));
            TableEntregaMonedas.AddCell(CreateCellCenter(false, (ep.m050 * 0.50).ToString("C"), BaseColor.WHITE, MontosTotalesFontCortes, 1));

            

            decimal totalmonedas = decimal.Parse((ep.m100 * 100).ToString())   + decimal.Parse((ep.m20 * 20).ToString()) +  decimal.Parse((ep.m10 * 10).ToString()) +  decimal.Parse((ep.m5 * 5).ToString()) + decimal.Parse((ep.m2 * 2).ToString()) + decimal.Parse((ep.m1 * 1).ToString()) + decimal.Parse((ep.m050 * 0.50).ToString());
            TableEntregaMonedas.AddCell(CreateCellCenter(false, "TOTAL MONEDAS " + totalmonedas.ToString("C"), BaseColor.WHITE, MontosTotalesFontCortes, 3));

            PdfPCell cell2 = new PdfPCell(TableEntregaMonedas);
            TableEntrega.AddCell(cell2);
            TableEntrega.AddCell(CreateCellCenter(false, "TOTAL " + (totalBilletes + totalmonedas).ToString("C"), BaseColor.WHITE, MontosTotalesFontCortesTotal, 2));

            doc.Add(TableEntrega);

            doc.Close();
            writer.Close();
            return name;

        }


        public string GetReporteListadoEntregaParcial(int? usuarioId , string fechaInicio , string fechaFin , string rangos ,int  ? tipoDescarga)
        {

            string apppath = System.AppDomain.CurrentDomain.BaseDirectory;
            string ruta = apppath + "/Rpt/";          
            ReportDocument reportDocument = new ReportDocument();

            dbHelper db = new dbHelper();

            try
            {
                db.agregarParametro("@usuarioId", SqlDbType.BigInt, usuarioId);
                db.agregarParametro("@fechaInicio", SqlDbType.VarChar, fechaInicio);
                db.agregarParametro("@fechaFin", SqlDbType.VarChar, fechaFin);

                DataSet ds = db.consultaDS("reporte_entregaEfectivo");

                ds.Tables[0].TableName = "titulo";
                ds.Tables[1].TableName = "subtitulo";
                ds.Tables[2].TableName = "sistema";
                ds.Tables[3].TableName = "entregas";

                reportDocument.Load(ruta + "reporte_entregaEfectivo.rpt");
                reportDocument.SetDataSource(ds);
                if (tipoDescarga == 1)
                {
                    string name = Guid.NewGuid().ToString() + ".pdf";
                    string fileName = apppath + "/Reportes/" + name;
                    reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, fileName);

                    db.conexion.Close();
                    reportDocument.Close();

                    return name;
                }
                else
                {
                    string name = Guid.NewGuid().ToString() + ".xls";
                    string fileName = apppath + "/Reportes/" + name;
                    reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.Excel, fileName);
                    db.conexion.Close();
                    reportDocument.Close();
                    return name;
                }
            }
            catch
            {
                db.conexion.Close();
                reportDocument.Close();
                return "";
            }
            

            

        }



        public string GetReporteTickets(int ? sucursalId,int? usuarioId, string fechaInicio, string fechaFin, bool ? esCancelacion)
        {

            string apppath = System.AppDomain.CurrentDomain.BaseDirectory;
            string ruta = apppath + "/Rpt/";
            string name = Guid.NewGuid().ToString() + ".pdf";
            string fileName = apppath + "/Reportes/" + name;
            ReportDocument reportDocument = new ReportDocument();

            dbHelper db = new dbHelper();

            try
            {
                db.agregarParametro("@sucursalId", SqlDbType.BigInt, sucursalId);
                db.agregarParametro("@usuarioId", SqlDbType.BigInt, usuarioId);
                db.agregarParametro("@fechaInicio", SqlDbType.VarChar, fechaInicio);
                db.agregarParametro("@fechaFin", SqlDbType.VarChar, fechaFin);
                db.agregarParametro("@esCancelacion", SqlDbType.Bit, esCancelacion);

                DataSet ds = db.consultaDS("reporte_cancelacionReimpresionTicket");

                ds.Tables[0].TableName = "titulo";
                ds.Tables[1].TableName = "subtitulo";
                ds.Tables[2].TableName = "empresa";
                ds.Tables[3].TableName = "detalle";

                reportDocument.Load(ruta + "reporte_cancelacionReimpresionTicket.rpt");
                reportDocument.SetDataSource(ds);
                reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, fileName);
            }
            catch(Exception ex)
            {
                db.conexion.Close();
                reportDocument.Close();
            }
            

            return name;

        }

        public class ReporteSalidaEfectivo
        {
            public long id { get; set; }
            public string cajero { get; set; }
            public DateTime fecha { get; set; }
            public string fechaCorta { get; set; }
            public string tipoSalida { get; set; }
            public  decimal cantidad { get; set; }
            public string observaciones { get; set; }
            public string sucursal { get; set; }
        }

        public string GetReporteSalidaEfectivo(string fechaInicio, string fechaFin, int? sucursalId,int ? usuarioId,int? op, int ? tipoDescarga)
        {


            string apppath = System.AppDomain.CurrentDomain.BaseDirectory;
            string ruta = apppath + "/Rpt/";           
            ReportDocument reportDocument = new ReportDocument();

            dbHelper db = new dbHelper();

            try
            {
                db.agregarParametro("@fechaInicio", SqlDbType.VarChar, fechaInicio);
                db.agregarParametro("@fechaFin", SqlDbType.VarChar, fechaFin);
                db.agregarParametro("@sucursalId", SqlDbType.BigInt, sucursalId);
                db.agregarParametro("@usuarioId", SqlDbType.BigInt, usuarioId);
                db.agregarParametro("@op", SqlDbType.BigInt, op);
                DataSet ds = db.consultaDS("reporte_salidaEfectivo");

                ds.Tables[0].TableName = "titulo";
                ds.Tables[1].TableName = "subtitulo";
                ds.Tables[2].TableName = "sistema";
                ds.Tables[3].TableName = "salidas";

                reportDocument.Load(ruta + "reporte_salidaEfectivo.rpt");
                reportDocument.SetDataSource(ds);


                if (tipoDescarga == 1)
                {
                    string name = Guid.NewGuid().ToString() + ".pdf";
                    string fileName = apppath + "/Reportes/" + name;
                    reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, fileName);

                    db.conexion.Close();
                    reportDocument.Close();

                    return name;
                }
                else
                {
                    string name = Guid.NewGuid().ToString() + ".xls";
                    string fileName = apppath + "/Reportes/" + name;
                    reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.Excel, fileName);

                    db.conexion.Close();
                    reportDocument.Close();

                    return name;
                }

            }
            catch
            {
                db.conexion.Close();
                reportDocument.Close();
                return "";
            }
           

        }


        public string GetReporteVentasResumenSucursal(string fechaInicio, string fechaFin, int? sucursalId, int? tipoDescarga, bool? detalle)
        {



            string apppath = System.AppDomain.CurrentDomain.BaseDirectory;
            string ruta = apppath + "/Rpt/";
            
            ReportDocument reportDocument = new ReportDocument();

            dbHelper db = new dbHelper();


            try
            {
                db.agregarParametro("@sucursalId", SqlDbType.Int, sucursalId);
                db.agregarParametro("@fechaInicio", SqlDbType.VarChar, fechaInicio);
                db.agregarParametro("@fechaFin", SqlDbType.VarChar, fechaFin);
                var sql = "";
                var report = "";

                sql = "reporte_resumenventas_sucursal";
                report = "reporte_resumenventas_sucursal.rpt";



                DataSet ds = db.consultaDS(sql);

                ds.Tables[0].TableName = "titulo";
                ds.Tables[1].TableName = "subtitulo";
                ds.Tables[2].TableName = "sistema";
                ds.Tables[3].TableName = "detalle";

                reportDocument.Load(ruta + report);
                reportDocument.SetDataSource(ds);
                if (tipoDescarga == 1)
                {
                    string name = Guid.NewGuid().ToString() + ".pdf";
                    string fileName = apppath + "/Reportes/" + name;
                    reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, fileName);

                    db.conexion.Close();
                    reportDocument.Close();

                    return name;
                }
                else
                {
                    string name = Guid.NewGuid().ToString() + ".xls";
                    string fileName = apppath + "/Reportes/" + name;
                    reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.Excel, fileName);

                    db.conexion.Close();
                    reportDocument.Close();

                    return name;
                }
            }
            catch
            {
                db.conexion.Close();
                reportDocument.Close();
                return "";
            }

            


           
        }


        public string GetReporteVentasResumenUsuario(string fechaInicio, string fechaFin, int? usuarioId, int? tipoDescarga, bool? detalle)
        {



            string apppath = System.AppDomain.CurrentDomain.BaseDirectory;
            string ruta = apppath + "/Rpt/";
            
            ReportDocument reportDocument = new ReportDocument();

            dbHelper db = new dbHelper();

            try
            {

                db.agregarParametro("@usuarioId", SqlDbType.Int, usuarioId);
                db.agregarParametro("@fechaInicio", SqlDbType.VarChar, fechaInicio);
                db.agregarParametro("@fechaFin", SqlDbType.VarChar, fechaFin);
                var sql = "";
                var report = "";

                sql = "reporte_resumenventas_usuario";
                report = "reporte_resumenventas_usuario.rpt";



                DataSet ds = db.consultaDS(sql);

                ds.Tables[0].TableName = "titulo";
                ds.Tables[1].TableName = "subtitulo";
                ds.Tables[2].TableName = "sistema";
                ds.Tables[3].TableName = "detalle";

                reportDocument.Load(ruta + report);
                reportDocument.SetDataSource(ds);
                if (tipoDescarga == 1)
                {
                    string name = Guid.NewGuid().ToString() + ".pdf";
                    string fileName = apppath + "/Reportes/" + name;
                    reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, fileName);

                    db.conexion.Close();
                    reportDocument.Close();
                    return name;
                }
                else
                {
                    string name = Guid.NewGuid().ToString() + ".xls";
                    string fileName = apppath + "/Reportes/" + name;
                    reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.ExcelWorkbook, fileName);

                    db.conexion.Close();
                    reportDocument.Close();
                    return name;
                }
            }
            catch
            {
                db.conexion.Close();
                reportDocument.Close();
                return "";
            }
            


           
        }

        public string GetReporteVentasResumenDepartamento(string fechaInicio, string fechaFin, int? sucursalId,int ? departamentoId, int? tipoDescarga, bool? detalle)
        {
            
            string apppath = System.AppDomain.CurrentDomain.BaseDirectory;
            string ruta = apppath + "/Rpt/";
           
            ReportDocument reportDocument = new ReportDocument();

            dbHelper db = new dbHelper();

            try
            {
                db.agregarParametro("@sucursalId", SqlDbType.Int, sucursalId);
                db.agregarParametro("@departamentoId", SqlDbType.Int, departamentoId);
                db.agregarParametro("@fechaInicio", SqlDbType.VarChar, fechaInicio);
                db.agregarParametro("@fechaFin", SqlDbType.VarChar, fechaFin);
                var sql = "";
                var report = "";

                sql = "reporte_resumenventas_departamento";
                report = "reporte_resumenventas_departamento.rpt";



                DataSet ds = db.consultaDS(sql);

                ds.Tables[0].TableName = "titulo";
                ds.Tables[1].TableName = "subtitulo";
                ds.Tables[2].TableName = "sistema";
                ds.Tables[3].TableName = "detalle";

                reportDocument.Load(ruta + report);
                reportDocument.SetDataSource(ds);
                if (tipoDescarga == 1)
                {
                    string name = Guid.NewGuid().ToString() + ".pdf";
                    string fileName = apppath + "/Reportes/" + name;
                    reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, fileName);

                    db.conexion.Close();
                    reportDocument.Close();
                    return name;
                }
                else
                {
                    string name = Guid.NewGuid().ToString() + ".xls";
                    string fileName = apppath + "/Reportes/" + name;
                    reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.ExcelWorkbook, fileName);

                    db.conexion.Close();
                    reportDocument.Close();
                    return name;
                }
            }
            catch
            {
                db.conexion.Close();
                reportDocument.Close();
                return "";
            }
            


          
        }


        public string GetReporteVentasResumenCategoria(string fechaInicio, string fechaFin, int? sucursalId, int? categoriaId, int? tipoDescarga, bool? detalle)
        {



            string apppath = System.AppDomain.CurrentDomain.BaseDirectory;
            string ruta = apppath + "/Rpt/";
            
            ReportDocument reportDocument = new ReportDocument();

            dbHelper db = new dbHelper();


            try
            {
                db.agregarParametro("@sucursalId", SqlDbType.Int, sucursalId);
                db.agregarParametro("@categoriaId", SqlDbType.Int, categoriaId);
                db.agregarParametro("@fechaInicio", SqlDbType.VarChar, fechaInicio);
                db.agregarParametro("@fechaFin", SqlDbType.VarChar, fechaFin);




                var sql = "";
                var report = "";

                sql = "reporte_resumenventas_categoria";
                report = "reporte_resumenventas_categoria.rpt";



                DataSet ds = db.consultaDS(sql);

                ds.Tables[0].TableName = "titulo";
                ds.Tables[1].TableName = "subtitulo";
                ds.Tables[2].TableName = "sistema";
                ds.Tables[3].TableName = "detalle";

                reportDocument.Load(ruta + report);
                reportDocument.SetDataSource(ds);
                if (tipoDescarga == 1)
                {

                    string name = Guid.NewGuid().ToString() + ".pdf";
                    string fileName = apppath + "/Reportes/" + name;
                    reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, fileName);

                    db.conexion.Close();
                    reportDocument.Close();

                    return name;
                }
                else
                {
                    string name = Guid.NewGuid().ToString() + ".xls";
                    string fileName = apppath + "/Reportes/" + name;
                    reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.ExcelWorkbook, fileName);

                    db.conexion.Close();
                    reportDocument.Close();
                    return name;
                }
            }
            catch
            {
                db.conexion.Close();
                reportDocument.Close();
                return "";
            }
           


            
        }

        public string GetReporteVentasResumenArticulo(string fechaInicio, string fechaFin, int? sucursalId, int? articuloId, int? tipoDescarga, bool? detalle)
        {
            string apppath = System.AppDomain.CurrentDomain.BaseDirectory;
            string ruta = apppath + "/Rpt/";
           
            ReportDocument reportDocument = new ReportDocument();

            dbHelper db = new dbHelper();


            try
            {
                db.agregarParametro("@sucursalId", SqlDbType.Int, sucursalId);
                db.agregarParametro("@categoriaId", SqlDbType.Int, articuloId);
                db.agregarParametro("@fechaInicio", SqlDbType.VarChar, fechaInicio);
                db.agregarParametro("@fechaFin", SqlDbType.VarChar, fechaFin);
                var sql = "";
                var report = "";

                sql = "reporte_resumenventas_articulo";
                report = "reporte_resumenventas_articulo.rpt";
                DataSet ds = db.consultaDS(sql);
                ds.Tables[0].TableName = "titulo";
                ds.Tables[1].TableName = "subtitulo";
                ds.Tables[2].TableName = "sistema";
                ds.Tables[3].TableName = "detalle";

                reportDocument.Load(ruta + report);
                reportDocument.SetDataSource(ds);
                if (tipoDescarga == 1)
                {
                    string name = Guid.NewGuid().ToString() + ".pdf";
                    string fileName = apppath + "/Reportes/" + name;
                    reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, fileName);

                    db.conexion.Close();
                    reportDocument.Close();

                    return name;
                }
                else
                {
                    string name = Guid.NewGuid().ToString() + ".xls";
                    string fileName = apppath + "/Reportes/" + name;
                    reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.ExcelWorkbook, fileName);

                    db.conexion.Close();
                    reportDocument.Close();

                    return name;
                }
            }
            catch
            {
                db.conexion.Close();
                reportDocument.Close();
                return "";
            }
           



        }



        public string GetReporteFacturas(string fechaInicio, string fechaFin, int? sucursalId,int ? usuarioId,string status,  int? tipoDescarga)
        {
            string apppath = System.AppDomain.CurrentDomain.BaseDirectory;
            string ruta = apppath + "/Rpt/";

            ReportDocument reportDocument = new ReportDocument();

            dbHelper db = new dbHelper();


            try
            {

                db.agregarParametro("@fechaInicio", SqlDbType.VarChar, fechaInicio);
                db.agregarParametro("@fechaFin", SqlDbType.VarChar, fechaFin);
                db.agregarParametro("@sucursalId", SqlDbType.Int, sucursalId);
                db.agregarParametro("@usuarioId", SqlDbType.Int, usuarioId);
                db.agregarParametro("@status", SqlDbType.VarChar, usuarioId);

                var sql = "";
                var report = "";

                sql = "Reporte_facturas";
                report = "reporte_facturas.rpt";
                DataSet ds = db.consultaDS(sql);
                ds.Tables[0].TableName = "titulo";
                ds.Tables[1].TableName = "subtitulo";             
                ds.Tables[2].TableName = "detalles";

                reportDocument.Load(ruta + report);
                reportDocument.SetDataSource(ds);
                if (tipoDescarga == 1)
                {
                    string name = Guid.NewGuid().ToString() + ".pdf";
                    string fileName = apppath + "/Reportes/" + name;
                    reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, fileName);

                    db.conexion.Close();
                    reportDocument.Close();

                    return name;
                }
                else
                {
                    string name = Guid.NewGuid().ToString() + ".xls";
                    string fileName = apppath + "/Reportes/" + name;
                    reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.ExcelWorkbook, fileName);

                    db.conexion.Close();
                    reportDocument.Close();

                    return name;
                }
            }
            catch
            {
                db.conexion.Close();
                reportDocument.Close();
                return "";
            }




        }


        public string GetReporteVentaCredito(string fechaInicio, string fechaFin, int? sucursalId, int? usuarioId, string status,bool ? detalle, int? tipoDescarga)
        {
            string apppath = System.AppDomain.CurrentDomain.BaseDirectory;
            string ruta = apppath + "/Rpt/";

            ReportDocument reportDocument = new ReportDocument();

            dbHelper db = new dbHelper();


            try
            {

                db.agregarParametro("@clienteId", SqlDbType.BigInt, 0);
                db.agregarParametro("@fechaInicio", SqlDbType.VarChar, fechaInicio);
                db.agregarParametro("@fechaFin", SqlDbType.VarChar, fechaFin);
                db.agregarParametro("@sucursalId", SqlDbType.Int, sucursalId);               
                db.agregarParametro("@status", SqlDbType.VarChar, status);
                db.agregarParametro("@detalle", SqlDbType.Bit, detalle);
                var sql = "";
                var report = "";

                sql = "reporte_Ventascredito";
                report = "reporte_ventaCredito.rpt";
                DataSet ds = db.consultaDS(sql);
                ds.Tables[0].TableName = "detalle";

                reportDocument.Load(ruta + report);
                reportDocument.SetDataSource(ds);
                if (tipoDescarga == 1)
                {
                    string name = Guid.NewGuid().ToString() + ".pdf";
                    string fileName = apppath + "/Reportes/" + name;
                    reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, fileName);

                    db.conexion.Close();
                    reportDocument.Close();

                    return name;
                }
                else
                {
                    string name = Guid.NewGuid().ToString() + ".xls";
                    string fileName = apppath + "/Reportes/" + name;
                    reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.ExcelWorkbook, fileName);

                    db.conexion.Close();
                    reportDocument.Close();

                    return name;
                }
            }
            catch
            {
                db.conexion.Close();
                reportDocument.Close();
                return "";
            }




        }


        public string GetReporteTraspaso(int? vendedorId, int? sucursalId, int ? tipo, string fechaInicio, string fechaFin, int? tipoDescarga)
        {


            string apppath = System.AppDomain.CurrentDomain.BaseDirectory;
            string ruta = apppath + "/Rpt/";
            ReportDocument reportDocument = new ReportDocument();

            dbHelper db = new dbHelper();


            try
            {
                db.agregarParametro("@vendedorId", SqlDbType.Int, vendedorId);
                db.agregarParametro("@sucursalId", SqlDbType.Int, sucursalId);
                db.agregarParametro("@tipo", SqlDbType.Int, tipo);                
                db.agregarParametro("@fechaInicio", SqlDbType.VarChar, fechaInicio);
                db.agregarParametro("@fechaFin", SqlDbType.VarChar, fechaFin);

                DataSet ds = db.consultaDS("reporte_traspaso");

                ds.Tables[0].TableName = "titulo";
                ds.Tables[1].TableName = "subtitulo";
                ds.Tables[2].TableName = "sistema";
                ds.Tables[3].TableName = "detalle";

                if (tipo == 1)
                {
                    reportDocument.Load(ruta + "reporte_traspaso_vendedor.rpt");
                }
                if (tipo == 2)
                {
                    reportDocument.Load(ruta + "reporte_traspaso_sucursal.rpt");
                }
                
                reportDocument.SetDataSource(ds);

                if (tipoDescarga == 1)
                {
                    string name = Guid.NewGuid().ToString() + ".pdf";
                    string fileName = apppath + "/Reportes/" + name;
                    reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, fileName);

                    db.conexion.Close();
                    reportDocument.Close();

                    return name;
                }
                else
                {
                    string name = Guid.NewGuid().ToString() + ".xls";
                    string fileName = apppath + "/Reportes/" + name;
                    reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.Excel, fileName);

                    db.conexion.Close();
                    reportDocument.Close();

                    return name;
                }

            }
            catch
            {
                db.conexion.Close();
                reportDocument.Close();
                return "";
            }

        }


        public string GetReporteCortesVendor(int? vendedorId, int? sucursalId, int? tipoventa, string fechaInicio, string fechaFin, int ? status, int? tipoDescarga)
        {


            string apppath = System.AppDomain.CurrentDomain.BaseDirectory;
            string ruta = apppath + "/Rpt/";
            ReportDocument reportDocument = new ReportDocument();

            dbHelper db = new dbHelper();


            try
            {
                db.agregarParametro("@vendedorId", SqlDbType.Int, vendedorId);
                db.agregarParametro("@sucursalId", SqlDbType.Int, sucursalId);
                db.agregarParametro("@tipoventa", SqlDbType.Int, tipoventa);
                db.agregarParametro("@status", SqlDbType.Int, status);
                db.agregarParametro("@fechaInicio", SqlDbType.VarChar, fechaInicio);
                db.agregarParametro("@fechaFin", SqlDbType.VarChar, fechaFin);

                DataSet ds = db.consultaDS("reporte_corteVendor");

                ds.Tables[0].TableName = "titulo";
                ds.Tables[1].TableName = "subtitulo";
                ds.Tables[2].TableName = "sistema";
                ds.Tables[3].TableName = "detalle";

                
                reportDocument.Load(ruta + "reporte_corte_vendedor.rpt");
                
                

                reportDocument.SetDataSource(ds);

                if (tipoDescarga == 1)
                {
                    string name = Guid.NewGuid().ToString() + ".pdf";
                    string fileName = apppath + "/Reportes/" + name;
                    reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, fileName);

                    db.conexion.Close();
                    reportDocument.Close();

                    return name;
                }
                else
                {
                    string name = Guid.NewGuid().ToString() + ".xls";
                    string fileName = apppath + "/Reportes/" + name;
                    reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.Excel, fileName);

                    db.conexion.Close();
                    reportDocument.Close();

                    return name;
                }

            }
            catch
            {
                db.conexion.Close();
                reportDocument.Close();
                return "";
            }

        }

        public string GetReporteArqueo(string fecha, int? usuarioId)
        {
            string apppath = System.AppDomain.CurrentDomain.BaseDirectory;
            string name = Guid.NewGuid().ToString() + ".pdf";
            string fileName = apppath + "/Reportes/" + name;
            Document doc = new Document(PageSize.LETTER);
            PdfWriter writer = PdfWriter.GetInstance(doc, new FileStream(fileName, FileMode.Create));
            doc.Open();

            usuarioController uc = new usuarioController();
            usuarioEntity user= uc.usuario_obten(usuarioId);

            doc.Add(createHeaderReporte("Arqueo de Ventas", "Usuario " + user.nombre ));

            entregaParcialController entregaParcial = new entregaParcialController();
            cierreVentaController cierreVenta = new cierreVentaController();

            List<EntregaParcialEntity> entregas = entregaParcial.entregaParcial_obtenLista(0,fecha, fecha, usuarioId, 0,0).Where(x => x.estatus == "ACTIVA").ToList();
            List<cierreVentaEntity> cierreVentas = cierreVenta.cierreVenta_obtenListaCorte(fecha, 0, usuarioId,0);

            decimal Total_entregas = decimal.Parse(entregas.Sum(x => x.cantidad).ToString());
            decimal Total_efectivo = decimal.Parse(cierreVentas.Sum(x => x.totalEfectivo).ToString());
            decimal Total_tarjetadebito = decimal.Parse(cierreVentas.Sum(x => x.montoTarjetaDebito).ToString());
            decimal Total_tarjetacredito = decimal.Parse(cierreVentas.Sum(x => x.montoTarjetaCredito).ToString());
            decimal Total_tarjetavales = decimal.Parse(cierreVentas.Sum(x => x.montoTarjetaVales).ToString());
            decimal Total_transferencia = decimal.Parse(cierreVentas.Sum(x => x.montoTransferencia).ToString());

            ventaController vc = new ventaController();
            List<ventaEntity> ventas = vc.GetVentasEfectivo(usuarioId, 0, fecha, fecha, "", 0,"");
            decimal totalcobroEfectivo = ventas.Where(y => y.pagoEfectivo > 0).ToList().Sum(x => x.pagoEfectivo);

            abonoCtrl abonoCtrl = new abonoCtrl();
            decimal totalpagocredito = abonoCtrl.venta_obtenListaAbonos(fecha, fecha,0, usuarioId).Sum(x=>x.monto);


            

            List<ventaTarjetasEntity> listaDebito = vc.GetVentasPagoTarjeta(usuarioId, 0, fecha, fecha, "D");
            decimal totalcobroTarjetaDebito = listaDebito.Sum(x => x.monto);

            List<ventaTarjetasEntity> listacredito = vc.GetVentasPagoTarjeta(usuarioId, 0, fecha, fecha, "C");
            decimal totalcobroTarjetaCredito = listacredito.Sum(x => x.monto);

            List<ventaTarjetasEntity> listaVales = vc.GetVentasPagoTarjeta(usuarioId, 0, fecha, fecha, "V");
            decimal totalcobroTarjetaVales = listaVales.Sum(x => x.monto);

            salidaEfectivoController salidaefectivo = new salidaEfectivoController();
            List<salidaEfectivoEntity> salidas = salidaefectivo.salidaEfectivo_obtenLista(0,usuarioId,0,fecha,0);
            decimal totalSalidas= salidas.ToList().Sum(x=> x.cantidad);


            List<ventaTransferenciaEntity> Transferencias = new List<ventaTransferenciaEntity>();
            Transferencias  =vc.GetVentasPagoTransferencia(usuarioId,0, fecha, fecha);
            decimal totalTransferencias = vc.GetVentasPagoTransferencia(usuarioId, 0, fecha, fecha).ToList().Sum(x => x.monto);

            PdfPTable Table3 = new PdfPTable(4);
            Table3.WidthPercentage = 75;
            Table3.SpacingBefore = 30;
           // Table3.
            Table3.AddCell(CreateCellCenter(true, "ENTREGAS DE EFECTIVO", BaseColor.WHITE, Encabezadoinfo, 4));
            Table3.AddCell(CreateCell(false, "Folio Entrega", BaseColor.WHITE, Encabezadoinfo, 0));
            Table3.AddCell(CreateCell(false, "Referencia", BaseColor.WHITE, Encabezadoinfo, 0));
            Table3.AddCell(CreateCellRight(false, "Total", BaseColor.WHITE, Encabezadoinfo, 2));
            entregas.ForEach(x =>
            {
                Table3.AddCell(CreateCell(false, x.id.ToString(), BaseColor.WHITE, Encabezadoinfo, 0));
                Table3.AddCell(CreateCell(false, x.referencia, BaseColor.WHITE, Encabezadoinfo, 0));
                Table3.AddCell(CreateCellRight(false, x.cantidad.ToString("C"), BaseColor.WHITE, Encabezadoinfo, 2));

            });
            doc.Add(Table3);

            PdfPTable Table4 = new PdfPTable(4);
            Table4.WidthPercentage = 75;
            Table4.SpacingBefore = 30;
            Table4.AddCell(CreateCellRight(false, "TOTAL COBRADO EFECTIVO (CAPTURADO POR USUARIO):", BaseColor.WHITE, Encabezadoinfo, 3));
            Table4.AddCell(CreateCellRight(false, Total_efectivo.ToString("C"), BaseColor.WHITE, Encabezadoinfo, 1));
            Table4.AddCell(CreateCellRight(false, "TOTAL DE ENTREGA DE EFECTIVO", BaseColor.WHITE, Encabezadoinfo, 3));
            Table4.AddCell(CreateCellRight(false, Total_entregas.ToString("C"), BaseColor.WHITE, Encabezadoinfo, 1));
            Table4.AddCell(CreateCellRight(false, "TOTAL SALIDAS DE EFECTIVO", BaseColor.WHITE, Encabezadoinfo, 3));
            Table4.AddCell(CreateCellRight(false, totalSalidas.ToString("C"), BaseColor.WHITE, Encabezadoinfo, 1));
            Table4.AddCell(CreateCellRight(false, "TOTAL COBRO EFECTIVO", BaseColor.WHITE, Encabezadoinfo, 3));
            Table4.AddCell(CreateCellRight(false, totalcobroEfectivo.ToString("C"), BaseColor.WHITE, Encabezadoinfo, 1));
            Table4.AddCell(CreateCellRight(false, "TOTAL COBRO PAGO VENTA CREDITO", BaseColor.WHITE, Encabezadoinfo, 3));
            Table4.AddCell(CreateCellRight(false, (totalpagocredito).ToString("C"), BaseColor.WHITE, Encabezadoinfo, 1));
            Table4.AddCell(CreateCellRight(false, "DIFERENCIA", BaseColor.WHITE, Encabezadoinfo, 3));

            decimal diferencia_efectivo = (Total_entregas + Total_efectivo + totalSalidas) - (totalcobroEfectivo + totalpagocredito);
            bool positive = diferencia_efectivo >= 0;
            if (positive)
            {
                Table4.AddCell(CreateCellRight(false, diferencia_efectivo.ToString("C"), BaseColor.GREEN, Encabezadoinfo, 1));
            }
            else
            {
                Table4.AddCell(CreateCellRight(false, diferencia_efectivo.ToString("C"), BaseColor.RED, Encabezadoinfo, 1));
            }


            doc.Add(Table4);

            PdfPTable Table6 = new PdfPTable(4);
            Table6.WidthPercentage = 75;
            Table6.SpacingBefore = 30;
            Table6.AddCell(CreateCellCenter(true, "COBROS EN TARJETA DE DEBITO", BaseColor.WHITE, Encabezadoinfo, 4));
            Table6.AddCell(CreateCell(false, "Referencia", BaseColor.WHITE, Encabezadoinfo, 0));
            Table6.AddCell(CreateCell(false, "Banco", BaseColor.WHITE, Encabezadoinfo, 0));
            Table6.AddCell(CreateCell(false, "No.Cuenta", BaseColor.WHITE, Encabezadoinfo, 0));
            Table6.AddCell(CreateCellRight(false, "Total", BaseColor.WHITE, Encabezadoinfo, 0));


            listaDebito.ForEach(y => {
                Table6.AddCell(CreateCell(false, "0", BaseColor.WHITE, Encabezadoinfo, 0));
                Table6.AddCell(CreateCell(false, y.banco.nombre, BaseColor.WHITE, Encabezadoinfo, 0));
                Table6.AddCell(CreateCell(false, y.referencia, BaseColor.WHITE, Encabezadoinfo, 0));
                Table6.AddCell(CreateCellRight(false, y.monto.ToString("C"), BaseColor.WHITE, Encabezadoinfo, 0));

            });

            decimal diferenciadebito = Total_tarjetadebito - totalcobroTarjetaDebito;
            bool positive1 = diferenciadebito >= 0;



            Table6.AddCell(CreateCellRight(false, "TOTAL COBRADO EN TARJETA DE DEBITO", BaseColor.WHITE, Encabezadoinfo, 3));
            Table6.AddCell(CreateCellRight(false, Total_tarjetadebito.ToString("C"), BaseColor.WHITE, Encabezadoinfo, 1));
            Table6.AddCell(CreateCellRight(false, "TOTAL COBRO EN TARJETA DE DEBITO", BaseColor.WHITE, Encabezadoinfo, 3));
            Table6.AddCell(CreateCellRight(false, totalcobroTarjetaDebito.ToString("C"), BaseColor.WHITE, Encabezadoinfo, 1));
            Table6.AddCell(CreateCellRight(false, "DIFERENCIA", BaseColor.WHITE, Encabezadoinfo, 3));
            if (positive1)
            {
                Table6.AddCell(CreateCellRight(false, diferenciadebito.ToString("C"), BaseColor.GREEN, Encabezadoinfo, 1));
            }
            else
            {
                Table6.AddCell(CreateCellRight(false, diferenciadebito.ToString("C"), BaseColor.RED, Encabezadoinfo, 1));
            }

            doc.Add(Table6);


            PdfPTable Table7 = new PdfPTable(4);
            Table7.WidthPercentage = 75;
            Table7.SpacingBefore = 30;
            Table7.AddCell(CreateCellCenter(true, "COBROS EN TARJETA DE CREDITO", BaseColor.WHITE, Encabezadoinfo, 4));
            Table7.AddCell(CreateCell(false, "Referencia", BaseColor.WHITE, Encabezadoinfo, 0));
            Table7.AddCell(CreateCell(false, "Banco", BaseColor.WHITE, Encabezadoinfo, 0));
            Table7.AddCell(CreateCell(false, "No.Cuenta", BaseColor.WHITE, Encabezadoinfo, 0));
            Table7.AddCell(CreateCellRight(false, "Total", BaseColor.WHITE, Encabezadoinfo, 0));

            listacredito.ForEach(y => {

                Table7.AddCell(CreateCell(false, "", BaseColor.WHITE, Encabezadoinfo, 0));
                Table7.AddCell(CreateCell(false, y.banco.nombre, BaseColor.WHITE, Encabezadoinfo, 0));
                Table7.AddCell(CreateCell(false, y.referencia, BaseColor.WHITE, Encabezadoinfo, 0));
                Table7.AddCell(CreateCellRight(false, y.monto.ToString("C"), BaseColor.WHITE, Encabezadoinfo, 0));
            });

            decimal diferenciacrebito = Total_tarjetacredito - totalcobroTarjetaCredito;
            bool positive2 = diferenciacrebito >= 0;


            Table7.AddCell(CreateCellRight(false, "TOTAL COBRADO EN TARJETA DE CREDITO", BaseColor.WHITE, Encabezadoinfo, 3));
            Table7.AddCell(CreateCellRight(false, Total_tarjetacredito.ToString("C"), BaseColor.WHITE, Encabezadoinfo, 1));
            Table7.AddCell(CreateCellRight(false, "TOTAL COBR0 TARJETA DE CREDITO", BaseColor.WHITE, Encabezadoinfo, 3));
            Table7.AddCell(CreateCellRight(false, totalcobroTarjetaCredito.ToString("C"), BaseColor.WHITE, Encabezadoinfo, 1));
            Table7.AddCell(CreateCellRight(false, "DIFERENCIA", BaseColor.WHITE, Encabezadoinfo, 3));
            if (positive2)
            {
                Table7.AddCell(CreateCellRight(false, diferenciacrebito.ToString("C"), BaseColor.GREEN, Encabezadoinfo, 1));
            }
            else
            {
                Table7.AddCell(CreateCellRight(false, diferenciacrebito.ToString("C"), BaseColor.RED, Encabezadoinfo, 1));
            }
            doc.Add(Table7);



            PdfPTable Table9 = new PdfPTable(4);
            Table9.WidthPercentage = 75;
            Table9.SpacingBefore = 30;
            Table9.AddCell(CreateCellCenter(true, "COBROS EN TARJETA DE VALES ELECTRONICOS", BaseColor.WHITE, Encabezadoinfo, 4));
            Table9.AddCell(CreateCell(false, "Referencia", BaseColor.WHITE, Encabezadoinfo, 0));
            Table9.AddCell(CreateCell(false, "Banco", BaseColor.WHITE, Encabezadoinfo, 0));
            Table9.AddCell(CreateCell(false, "No.Cuenta", BaseColor.WHITE, Encabezadoinfo, 0));
            Table9.AddCell(CreateCellRight(false, "Total", BaseColor.WHITE, Encabezadoinfo, 0));

            listaVales.ForEach(y => {

                Table9.AddCell(CreateCell(false, "", BaseColor.WHITE, Encabezadoinfo, 0));
                Table9.AddCell(CreateCell(false, y.banco.nombre, BaseColor.WHITE, Encabezadoinfo, 0));
                Table9.AddCell(CreateCell(false, y.referencia, BaseColor.WHITE, Encabezadoinfo, 0));
                Table9.AddCell(CreateCellRight(false, y.monto.ToString("C"), BaseColor.WHITE, Encabezadoinfo, 0));
            });

            decimal diferenciavales = Total_tarjetavales - totalcobroTarjetaVales;
            bool positive4 = diferenciavales >= 0;


            Table9.AddCell(CreateCellRight(false, "TOTAL COBRADO EN TARJETA DE VALES", BaseColor.WHITE, Encabezadoinfo, 3));
            Table9.AddCell(CreateCellRight(false, Total_tarjetavales.ToString("C"), BaseColor.WHITE, Encabezadoinfo, 1));
            Table9.AddCell(CreateCellRight(false, "TOTAL COBR0 TARJETA DE VALES", BaseColor.WHITE, Encabezadoinfo, 3));
            Table9.AddCell(CreateCellRight(false, totalcobroTarjetaVales.ToString("C"), BaseColor.WHITE, Encabezadoinfo, 1));
            Table9.AddCell(CreateCellRight(false, "DIFERENCIA", BaseColor.WHITE, Encabezadoinfo, 3));
            if (positive4)
            {
                Table9.AddCell(CreateCellRight(false, diferenciavales.ToString("C"), BaseColor.GREEN, Encabezadoinfo, 1));
            }
            else
            {
                Table9.AddCell(CreateCellRight(false, diferenciavales.ToString("C"), BaseColor.RED, Encabezadoinfo, 1));
            }
            doc.Add(Table9);



            PdfPTable Table8 = new PdfPTable(4);
            Table8.WidthPercentage = 75;
            Table8.SpacingBefore = 30;
            Table8.AddCell(CreateCellCenter(true, "COBROS CON TRANSFERENCIA ELECTRONICA", BaseColor.WHITE, Encabezadoinfo, 4));
            Table8.AddCell(CreateCell(false, "Referencia", BaseColor.WHITE, Encabezadoinfo, 0));
            Table8.AddCell(CreateCell(false, "Banco", BaseColor.WHITE, Encabezadoinfo, 0));
            Table8.AddCell(CreateCell(false, "No.Cuenta", BaseColor.WHITE, Encabezadoinfo, 0));
            Table8.AddCell(CreateCellRight(false, "Total", BaseColor.WHITE, Encabezadoinfo, 0));

            Transferencias.ForEach(x => {

                   

                Table8.AddCell(CreateCell(false, "", BaseColor.WHITE, Encabezadoinfo, 0));
                Table8.AddCell(CreateCell(false, x.banco.nombre, BaseColor.WHITE, Encabezadoinfo, 0));
                Table8.AddCell(CreateCell(false, x.referencia, BaseColor.WHITE, Encabezadoinfo, 0));
                Table8.AddCell(CreateCellRight(false, x.monto.ToString("C"), BaseColor.WHITE, Encabezadoinfo, 0));

            });

            decimal diferenciaTrans = Total_transferencia - totalTransferencias;
            bool positive3 = diferenciaTrans >= 0;


            Table8.AddCell(CreateCellRight(false, "TOTAL COBRADO  CON TRANSFERENCIA ELECTRONICA", BaseColor.WHITE, Encabezadoinfo, 3));
            Table8.AddCell(CreateCellRight(false, totalTransferencias.ToString("C"), BaseColor.WHITE, Encabezadoinfo, 1));
            Table8.AddCell(CreateCellRight(false, "TOTAL COBRO TRANSFERENCIA ELECTRONICA", BaseColor.WHITE, Encabezadoinfo, 3));
            Table8.AddCell(CreateCellRight(false, Total_transferencia.ToString("C") , BaseColor.WHITE, Encabezadoinfo, 1));
            Table8.AddCell(CreateCellRight(false, "DIFERENCIA", BaseColor.WHITE, Encabezadoinfo, 3));
            if (positive3)
            {
                Table8.AddCell(CreateCellRight(false, diferenciaTrans.ToString("C"), BaseColor.GREEN, Encabezadoinfo, 1));
            }
            else
            {
                Table8.AddCell(CreateCellRight(false, diferenciaTrans.ToString("C"), BaseColor.RED, Encabezadoinfo, 1));
            }
            doc.Add(Table8);







            doc.Close();
            writer.Close();
            return name;

        }


        public string GetReporteClientes(int? vendedorId, int ? op,int ? op2, int ? op3,string op4,string op5, int ? tipoDescarga)
        {


            string apppath = System.AppDomain.CurrentDomain.BaseDirectory;
            string ruta = apppath + "/Rpt/";
            ReportDocument reportDocument = new ReportDocument();

            dbHelper db = new dbHelper();


            try
            {
                db.agregarParametro("@vendedorId", SqlDbType.Int, vendedorId);
                db.agregarParametro("@op", SqlDbType.Int, op);
                db.agregarParametro("@op2", SqlDbType.Int, op2);
                db.agregarParametro("@op3", SqlDbType.Int, op3);
                db.agregarParametro("@op4", SqlDbType.VarChar, op4);
                db.agregarParametro("@op5", SqlDbType.VarChar, op5);

                DataSet ds = db.consultaDS("ReporteClientes");

                ds.Tables[0].TableName = "titulo";               
                ds.Tables[1].TableName = "detalle";


                reportDocument.Load(ruta + "reporte_clientes.rpt");



                reportDocument.SetDataSource(ds);

                if (tipoDescarga == 1)
                {
                    string name = Guid.NewGuid().ToString() + ".pdf";
                    string fileName = apppath + "/Reportes/" + name;
                    reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, fileName);

                    db.conexion.Close();
                    reportDocument.Close();

                    return name;
                }
                else
                {
                    string name = Guid.NewGuid().ToString() + ".xls";
                    string fileName = apppath + "/Reportes/" + name;
                    reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.Excel, fileName);

                    db.conexion.Close();
                    reportDocument.Close();

                    return name;
                }

            }
            catch
            {
                db.conexion.Close();
                reportDocument.Close();
                return "";
            }

        }

        public string GetReporteTicketPago(int? pagoId)
        {

            string apppath = System.AppDomain.CurrentDomain.BaseDirectory;
            string ruta = apppath + "/Rpt/";
            string name = Guid.NewGuid().ToString() + ".pdf";
            string fileName = apppath + "/Reportes/" + name;
            ReportDocument reportDocument = new ReportDocument();

            dbHelper db = new dbHelper();

            try
            {
                db.agregarParametro("@pagoId", SqlDbType.BigInt, pagoId);
                DataSet ds = db.consultaDS("reporte_ticket_pago");
               
                ds.Tables[0].TableName = "sistema";
                ds.Tables[1].TableName = "sucursal";
                ds.Tables[2].TableName = "cliente";
                ds.Tables[3].TableName = "venta";
                ds.Tables[4].TableName = "pago";
                ds.Tables[5].TableName = "articulos";
                ds.Tables[6].TableName = "totalabonado";
                ds.Tables[7].TableName = "totalrestante";

                reportDocument.Load(ruta + "reporte_ticket_pago.rpt");
                reportDocument.SetDataSource(ds);
                reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, fileName);
            }
            catch (Exception ex)
            {
                db.conexion.Close();
                reportDocument.Close();
            }


            return name;

        }


        public string ReporteArticuloDescuento(int ? clasificacion,int ? categoria,int ? articulo, int? tipoDescarga)
        {


            string apppath = System.AppDomain.CurrentDomain.BaseDirectory;
            string ruta = apppath + "/Rpt/";
            ReportDocument reportDocument = new ReportDocument();

            dbHelper db = new dbHelper();


            try
            {
                db.agregarParametro("@clasificacion", SqlDbType.Int, clasificacion);
                db.agregarParametro("@tipo", SqlDbType.Int, categoria);
                db.agregarParametro("@articulo", SqlDbType.Int, articulo);               

                DataSet ds = db.consultaDS("ReporteArticuloDescuento");
                ds.Tables[0].TableName = "detalle"; 


                reportDocument.Load(ruta + "ReporteArticuloDescuento.rpt");



                reportDocument.SetDataSource(ds);

                if (tipoDescarga == 1)
                {
                    string name = Guid.NewGuid().ToString() + ".pdf";
                    string fileName = apppath + "/Reportes/" + name;
                    reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, fileName);

                    db.conexion.Close();
                    reportDocument.Close();

                    return name;
                }
                else
                {
                    string name = Guid.NewGuid().ToString() + ".xls";
                    string fileName = apppath + "/Reportes/" + name;
                    reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.Excel, fileName);

                    db.conexion.Close();
                    reportDocument.Close();

                    return name;
                }

            }
            catch
            {
                db.conexion.Close();
                reportDocument.Close();
                return "";
            }

        }



        public PdfPTable createHeaderReporteRangos(string titulo, string subtitulo, string rangos)
        {
            sistemaGeneralEntity info = ObtenDatosEmpresa();
            iTextSharp.text.Font htitulo = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 14, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
            iTextSharp.text.Font hsubtitulo = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 12, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
            iTextSharp.text.Font hdescripcion = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 10, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);

            PdfPTable tblheader = new PdfPTable(4);
            tblheader.WidthPercentage = 100;
            try
            {
                string apppath = System.AppDomain.CurrentDomain.BaseDirectory;
                string logo = apppath + "/Reportes/" + info.logoReportes;
                iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(logo);
                jpg.ScaleToFit(100f, 80f);
                PdfPCell imageCell = new PdfPCell(jpg);
                imageCell.Border = 0;
                imageCell.HorizontalAlignment = Element.ALIGN_CENTER;
                tblheader.AddCell(tblheader.AddCell(imageCell));

            }
            catch
            {
              //  tblheader.AddCell(CreateblankCell(false));
            }
            tblheader.AddCell(CreateCellCenter(false, titulo, BaseColor.WHITE, htitulo, 4));
            //tblheader.AddCell(CreateblankCell(false));
            //tblheader.AddCell(CreateblankCell(false));
            tblheader.AddCell(CreateCellCenter(false, subtitulo, BaseColor.WHITE, hsubtitulo, 4));
            ////tblheader.AddCell(CreateblankCell(false));
            ////tblheader.AddCell(CreateblankCell(false));
            tblheader.AddCell(CreateCellCenter(false,  rangos, BaseColor.WHITE, hdescripcion, 4));
            //tblheader.AddCell(CreateblankCell(false));
            //tblheader.AddCell(CreateblankCell(false));
            tblheader.AddCell(CreateCellCenter(false, "Fecha:" + DateTime.Now.ToShortDateString(), BaseColor.WHITE, hdescripcion, 4));
            //tblheader.AddCell(CreateblankCell(false));
            return tblheader;
        }


        public PdfPTable createHeaderReporte(string titulo , string subtitulo )
        {
            sistemaGeneralEntity info= ObtenDatosEmpresa();
            iTextSharp.text.Font htitulo = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 12, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
            iTextSharp.text.Font hsubtitulo = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 10, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
            iTextSharp.text.Font hdescripcion = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 8, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);

            PdfPTable tblheader = new PdfPTable(4);
            tblheader.WidthPercentage = 100;
            try
            {
                string apppath = System.AppDomain.CurrentDomain.BaseDirectory;
                string logo = apppath + "/Reportes/" + info.logoReportes;
                iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(logo);
                jpg.ScaleToFit(100f, 80f);
                PdfPCell imageCell = new PdfPCell(jpg);
                imageCell.Border = 0;
                imageCell.HorizontalAlignment = Element.ALIGN_CENTER;
                tblheader.AddCell( tblheader.AddCell(imageCell));

            }catch
            {
                tblheader.AddCell(CreateblankCell(false));
            }
            tblheader.AddCell(CreateCellCenter(false, titulo, BaseColor.WHITE, htitulo, 2));
            tblheader.AddCell(CreateblankCell(false));
            tblheader.AddCell(CreateblankCell(false));
            tblheader.AddCell(CreateCellCenter(false, subtitulo, BaseColor.WHITE, hsubtitulo, 2));
            tblheader.AddCell(CreateblankCell(false));
            tblheader.AddCell(CreateblankCell(false));
            tblheader.AddCell(CreateCellCenter(false, "Fecha:" + DateTime.Now.ToShortDateString(), BaseColor.WHITE, hdescripcion, 2));
            tblheader.AddCell(CreateblankCell(false));
            return tblheader;
        }

        public PdfPCell CreateblankCell(bool border)
        {
            PdfPCell blank = new PdfPCell(new Phrase(""));
            blank.BorderWidth = (border) ? 1 : 0;
            return blank;
        }

        public PdfPCell CreateCell(bool border,string value, iTextSharp.text.BaseColor color, iTextSharp.text.Font font,int ? colspan)
        {
            PdfPCell cell = new PdfPCell(new Phrase(value,font));
            cell.BorderWidth = (border) ? 1 : 0;
            cell.BackgroundColor = color;
            cell.Colspan = colspan.Value;
           
            return cell;
        }

        public PdfPCell CreateCellCenter(bool border, string value, iTextSharp.text.BaseColor color, iTextSharp.text.Font font, int? colspan)
        {
            PdfPCell cell = new PdfPCell(new Phrase(value, font));
            cell.BorderWidth = (border) ? 1 : 0;
            cell.BackgroundColor = color;
            cell.Colspan = colspan.Value;
            cell.HorizontalAlignment = Element.ALIGN_CENTER; ;
            return cell;
        }

        public PdfPCell CreateCellRight(bool border, string value, iTextSharp.text.BaseColor color, iTextSharp.text.Font font, int? colspan)
        {
            PdfPCell cell = new PdfPCell(new Phrase(value, font));
            cell.BorderWidth = (border) ? 1 : 0;
            cell.BackgroundColor = color;
            cell.Colspan = colspan.Value;
            cell.HorizontalAlignment = Element.ALIGN_RIGHT; ;
            return cell;
        }


        public PdfPCell CreateCellborderBottom(bool border, string value, iTextSharp.text.BaseColor color, iTextSharp.text.Font font, int? colspan)
        {
            PdfPCell cell = new PdfPCell(new Phrase(value, font));
            cell.BorderWidthBottom = 1;
            cell.BorderWidthLeft = 0;
            cell.BorderWidthRight = 0;
            cell.BorderWidthTop = 0;
            cell.BackgroundColor = color;
            cell.Colspan = colspan.Value;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            return cell;
        }

        public sistemaGeneralEntity ObtenDatosEmpresa()
        {
            List<sistemaGeneralEntity> result = new List<sistemaGeneralEntity>();
            dbHelper db = new dbHelper();          
            SqlDataReader reader = db.consultaReader("sistema_obtenInformacion");
            result = db.MapDataToEntityCollection<sistemaGeneralEntity>(reader).ToList();
            reader.Close();
            db.conexion.Close();
            db.conexion.Dispose();
            return result[0];
        }
    }
}