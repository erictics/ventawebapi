﻿using AppWCFService.Entity;
using AppWCFService.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace AppWCFService.Controllers
{
    public class formaPagoController
    {
        public List<formaPagoEntity> formaPago_obtenLista()
        {
            List<formaPagoEntity> result = new List<formaPagoEntity>();
            dbHelper db = new dbHelper();
            SqlDataReader reader = db.consultaReader("formaPago_obtenLista");
            reader.Close();
            db.conexion.Close();
            db.conexion.Dispose();
            return result;
        }

        public formaPagoEntity formaPago_obten(long? id)
        {
            formaPagoEntity result = new formaPagoEntity();
            dbHelper db = new dbHelper();
            db.agregarParametro("@id", SqlDbType.BigInt, id);
            SqlDataReader reader = db.consultaReader("formaPago_obten");
            try
            {
                result = db.MapDataToEntityCollection<formaPagoEntity>(reader).ToList()[0];
            }
            catch (Exception ex)
            {
                result = null;
            }
            reader.Close();
            db.conexion.Close();
            db.conexion.Dispose();
            return result;
        }
    }
}