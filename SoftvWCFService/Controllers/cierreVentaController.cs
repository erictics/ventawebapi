﻿using AppWCFService.Entity;
using AppWCFService.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace AppWCFService.Controllers
{
    public class cierreVentaController
    {

        usuarioController UsuarioController = new usuarioController();

        public cierreVentaEntity cierreVenta_obten(long? id)
        {
            cierreVentaEntity result = new cierreVentaEntity();
            dbHelper db = new dbHelper();
            SqlDataReader reader = null;
            try
            {
                db.agregarParametro("@id", SqlDbType.BigInt, id);
                reader = db.consultaReader("cierreVenta_obten");
                result = db.MapDataToEntityCollection<cierreVentaEntity>(reader).ToList()[0];
                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch (Exception ex)
            {
                result = null;
                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }

            
            return result;
        }


        public List<cierreVentaEntity> cierreVenta_obtenLista(string fecha, long? folio, int? usuarioId,int ? sucursalId,int ? userId2)
        {
            List<cierreVentaEntity> result = new List<cierreVentaEntity>();
            dbHelper db = new dbHelper();

            SqlDataReader reader = null;

            try
            {
                db.agregarParametro("@fecha", SqlDbType.VarChar, fecha);
                db.agregarParametro("@folio", SqlDbType.BigInt, folio);
                db.agregarParametro("@usuarioId", SqlDbType.BigInt, usuarioId);
                db.agregarParametro("@sucursalId", SqlDbType.BigInt, sucursalId);
                db.agregarParametro("@userId2", SqlDbType.BigInt, userId2);
                reader = db.consultaReader("cierreVenta_obtenLista");

                result = db.MapDataToEntityCollection<cierreVentaEntity>(reader).ToList();
                result.ForEach(x=> {
                    x.monto = x.totalEntregar.ToString("C");
                    x.fechaCorta = x.fecha.ToShortDateString();
                    x.usuario = UsuarioController.usuario_obten(x.usuarioId);
                });


                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch (Exception ex)
            {
                result = null;
                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }

            
            return result;
        }


        public List<cierreVentaEntity> cierreVenta_obtenListaCorte(string fecha, long? folio, int? usuarioId, int? sucursalId)
        {
            List<cierreVentaEntity> result = new List<cierreVentaEntity>();
            dbHelper db = new dbHelper();

            SqlDataReader reader = null;

            try
            {
                db.agregarParametro("@fecha", SqlDbType.VarChar, fecha);
                db.agregarParametro("@folio", SqlDbType.BigInt, folio);
                db.agregarParametro("@usuarioId", SqlDbType.BigInt, usuarioId);
                db.agregarParametro("@sucursalId", SqlDbType.BigInt, sucursalId);
               
                reader = db.consultaReader("cierreVenta_obtenListaCorte");

                result = db.MapDataToEntityCollection<cierreVentaEntity>(reader).ToList();
                result.ForEach(x => {
                    x.monto = x.totalEntregar.ToString("C");
                    x.fechaCorta = x.fecha.ToShortDateString();
                    x.usuario = UsuarioController.usuario_obten(x.usuarioId);
                });


                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch (Exception ex)
            {
                result = null;
                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }


            return result;
        }

        public ResultEntity cierreVenta_nuevo (cierreVentaEntity entrega)
        {
            List<cierreVentaEntity> result = new List<cierreVentaEntity>();
            dbHelper db = new dbHelper();
            ResultEntity res = new ResultEntity();
            try
            {

                db.agregarParametro("@b1000", SqlDbType.Int, entrega.b1000);
                db.agregarParametro("@b500", SqlDbType.Int, entrega.b500);
                db.agregarParametro("@b200", SqlDbType.Int, entrega.b200);
                db.agregarParametro("@b100", SqlDbType.Int, entrega.b100);
                db.agregarParametro("@b50", SqlDbType.Int, entrega.b50);
                db.agregarParametro("@b20", SqlDbType.Int, entrega.b20);
                db.agregarParametro("@m20", SqlDbType.Int, entrega.m20);
                db.agregarParametro("@m10", SqlDbType.Int, entrega.m10);
                db.agregarParametro("@m5", SqlDbType.Int, entrega.m5);
                db.agregarParametro("@m2", SqlDbType.Int, entrega.m2);
                db.agregarParametro("@m1", SqlDbType.Int, entrega.m1);
                db.agregarParametro("@m050", SqlDbType.Int, entrega.m050);
                db.agregarParametro("@montoTarjetaCredito", SqlDbType.Money, entrega.montoTarjetaCredito);
                db.agregarParametro("@montoTarjetaDebito", SqlDbType.Money, entrega.montoTarjetaDebito);
                db.agregarParametro("@montoTarjetaVales", SqlDbType.Money, entrega.montoTarjetaVales);
                db.agregarParametro("@totalEfectivo", SqlDbType.Money, entrega.totalEfectivo);
                db.agregarParametro("@totalTarjetas", SqlDbType.Money, entrega.totalTarjetas);
                db.agregarParametro("@montoTransferencia", SqlDbType.Money, entrega.montoTransferencia);
                db.agregarParametro("@totalEntregar", SqlDbType.Money, entrega.totalEntregar);
                db.agregarParametro("@usuarioId", SqlDbType.Int, entrega.usuarioId);
                db.agregarParametro("@sucursalId", SqlDbType.Int, entrega.sucursalId);
                db.agregarParametro("@id", SqlDbType.BigInt, ParameterDirection.Output);
                db.agregarParametro("@Message", SqlDbType.VarChar, ParameterDirection.Output, 100);
                db.agregarParametro("@ErrorCode", SqlDbType.BigInt, ParameterDirection.Output);
                db.consultaOutput("cierreVenta_nuevo");
                int id = int.Parse(db.diccionarioOutput["@id"].ToString());
                string mensaje = db.diccionarioOutput["@Message"].ToString();
                int ErrorCode = int.Parse(db.diccionarioOutput["@ErrorCode"].ToString());

                res.id = id;
                res.ErrorCode = ErrorCode;
                res.Message = mensaje;
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch(Exception ex)
            {
                res.id = 0;
                res.ErrorCode = 1;
                res.Message = ex.Message;
                db.conexion.Close();
                db.conexion.Dispose();

            }
         

           
            
            return res;
        }






        public long? cierreVenta_edita(cierreVentaEntity entrega)
        {
            List<cierreVentaEntity> result = new List<cierreVentaEntity>();
            dbHelper db = new dbHelper();

            try
            {
                db.agregarParametro("@b1000", SqlDbType.Int, entrega.b1000);
                db.agregarParametro("@b500", SqlDbType.Int, entrega.b500);
                db.agregarParametro("@b200", SqlDbType.Int, entrega.b200);
                db.agregarParametro("@b100", SqlDbType.Int, entrega.b100);
                db.agregarParametro("@b50", SqlDbType.Int, entrega.b50);
                db.agregarParametro("@b20", SqlDbType.Int, entrega.b20);
                db.agregarParametro("@m20", SqlDbType.Int, entrega.m20);
                db.agregarParametro("@m10", SqlDbType.Int, entrega.m10);
                db.agregarParametro("@m5", SqlDbType.Int, entrega.m5);
                db.agregarParametro("@m2", SqlDbType.Int, entrega.m2);
                db.agregarParametro("@m1", SqlDbType.Int, entrega.m1);
                db.agregarParametro("@m050", SqlDbType.Int, entrega.m050);
                db.agregarParametro("@montoTarjetaCredito", SqlDbType.Money, entrega.montoTarjetaCredito);
                db.agregarParametro("@montoTarjetaDebito", SqlDbType.Money, entrega.montoTarjetaDebito);
                db.agregarParametro("@montoTarjetaVales", SqlDbType.Money, entrega.montoTarjetaVales);
                db.agregarParametro("@montoTransferencia", SqlDbType.Money, entrega.montoTransferencia);
                db.agregarParametro("@totalEfectivo", SqlDbType.Money, entrega.totalEfectivo);
                db.agregarParametro("@totalTarjetas", SqlDbType.Money, entrega.totalTarjetas);
                db.agregarParametro("@totalEntregar", SqlDbType.Money, entrega.totalEntregar);

                db.agregarParametro("@usuarioId", SqlDbType.Int, entrega.usuarioId);
                db.agregarParametro("@sucursalId", SqlDbType.Int, entrega.sucursalId);
                db.agregarParametro("@id", SqlDbType.BigInt, entrega.id);
                db.consultaSinRetorno("cierreVenta_edita");
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch
            {

                db.conexion.Close();
                db.conexion.Dispose();
            }
          
            return 1;
        }



        public ResultEntity cierreVenta_elimina(long ? id,long ? usuarioId)
        {
            dbHelper db = new dbHelper();


            ResultEntity result = new ResultEntity();
            try
            {
                db.agregarParametro("@id", SqlDbType.BigInt, id);
                db.agregarParametro("@usuarioId", SqlDbType.BigInt, usuarioId);
                db.agregarParametro("@Message", SqlDbType.VarChar, ParameterDirection.Output, 100);
                db.agregarParametro("@ErrorCode", SqlDbType.BigInt, ParameterDirection.Output);
                db.consultaOutput("cierreVenta_elimina");
                string mensaje = db.diccionarioOutput["@Message"].ToString();
                int ErrorCode = int.Parse(db.diccionarioOutput["@ErrorCode"].ToString());
                db.conexion.Close();
                db.conexion.Dispose();
                result.ErrorCode = ErrorCode;
                result.Message = mensaje;
            }
            catch(Exception ex)
            {
                result.ErrorCode = 1;
                result.Message = ex.Message;
            }
           
            
            return result;
        }



    }
}