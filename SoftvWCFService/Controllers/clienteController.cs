﻿using AppWCFService.Entity;
using AppWCFService.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace AppWCFService.Controllers
{


    public class clienteController
    {
       

        public List<clienteEntity> cliente_obtenLista( string rfc,string nombre)
        {
            List<clienteEntity> result = new List<clienteEntity>();
            dbHelper db = new dbHelper();
            SqlDataReader reader = null;
            try
            {
                db.agregarParametro("@rfc", SqlDbType.VarChar, rfc);
                db.agregarParametro("@nombre", SqlDbType.VarChar, nombre);
                reader = db.consultaReader("cliente_obtenLista");
                result = db.MapDataToEntityCollection<clienteEntity>(reader).ToList();
                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch
            {
                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }
            
            
            return result;
        }

        public clienteEntity cliente_obten(long? id)
        {
            clienteEntity result = new clienteEntity();
            dbHelper db = new dbHelper();
            SqlDataReader reader = null;
            try
            {
                db.agregarParametro("@id", SqlDbType.BigInt, id);
                reader = db.consultaReader("cliente_obten");
                result = db.MapDataToEntityCollection<clienteEntity>(reader).ToList()[0];
                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch (Exception ex)
            {
                result = null;
                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }
            
            return result;
        }


        public long? cliente_nuevo(clienteEntity cliente)
        {

            List<clienteEntity> result = new List<clienteEntity>();
            dbHelper db = new dbHelper();
            long id = 0;
            try
            {
                db.agregarParametro("@nombre", SqlDbType.VarChar, cliente.nombre);
                db.agregarParametro("@rfc", SqlDbType.VarChar, cliente.rfc);
                db.agregarParametro("@apellidos", SqlDbType.VarChar, cliente.apellidos);
                db.agregarParametro("@cp", SqlDbType.BigInt, cliente.cp);
                db.agregarParametro("@email", SqlDbType.VarChar, cliente.email);
                db.agregarParametro("@colonia", SqlDbType.VarChar, cliente.colonia);
                db.agregarParametro("@calle", SqlDbType.VarChar, cliente.calle);
                db.agregarParametro("@numero", SqlDbType.Int, cliente.numero);
                db.agregarParametro("@ciudad", SqlDbType.VarChar, cliente.ciudad);
                db.agregarParametro("@estado", SqlDbType.VarChar, cliente.estado);
                db.agregarParametro("@razonSocial", SqlDbType.VarChar, cliente.razonSocial);
                db.agregarParametro("@telefono", SqlDbType.VarChar, cliente.telefono);
                db.agregarParametro("@numeroInt", SqlDbType.VarChar, cliente.numeroInt);
                db.agregarParametro("@entrecalles", SqlDbType.VarChar, cliente.entrecalles);
                db.agregarParametro("@referencia", SqlDbType.VarChar, cliente.referencia);
                db.agregarParametro("@latitud", SqlDbType.VarChar, cliente.latitud);
                db.agregarParametro("@longitud", SqlDbType.VarChar, cliente.longitud);
                db.agregarParametro("@vendedorId", SqlDbType.Int, cliente.vendedorId);
                db.agregarParametro("@id", SqlDbType.BigInt, ParameterDirection.Output);
                db.consultaOutput("cliente_nuevo");
                id = long.Parse(db.diccionarioOutput["@id"].ToString());
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }

            
         
            return id;
        }

        public long? cliente_edita(clienteEntity cliente)
        {
            List<almacenEntity> result = new List<almacenEntity>();
            dbHelper db = new dbHelper();

            try
            {
                db.agregarParametro("@nombre", SqlDbType.VarChar, cliente.nombre);
                db.agregarParametro("@rfc", SqlDbType.VarChar, cliente.rfc);
                db.agregarParametro("@apellidos", SqlDbType.VarChar, cliente.apellidos);
                db.agregarParametro("@cp", SqlDbType.BigInt, cliente.cp);
                db.agregarParametro("@email", SqlDbType.VarChar, cliente.email);
                db.agregarParametro("@colonia", SqlDbType.VarChar, cliente.colonia);
                db.agregarParametro("@calle", SqlDbType.VarChar, cliente.calle);
                db.agregarParametro("@numero", SqlDbType.Int, cliente.numero);
                db.agregarParametro("@ciudad", SqlDbType.VarChar, cliente.ciudad);
                db.agregarParametro("@estado", SqlDbType.VarChar, cliente.estado);
                db.agregarParametro("@razonSocial", SqlDbType.VarChar, cliente.razonSocial);
                db.agregarParametro("@telefono", SqlDbType.VarChar, cliente.telefono);
                db.agregarParametro("@identificador", SqlDbType.VarChar, cliente.identificador);
                db.agregarParametro("@numeroInt", SqlDbType.VarChar, cliente.numeroInt);
                db.agregarParametro("@entrecalles", SqlDbType.VarChar, cliente.entrecalles);
                db.agregarParametro("@referencia", SqlDbType.VarChar, cliente.referencia);
                db.agregarParametro("@latitud", SqlDbType.VarChar, cliente.latitud);
                db.agregarParametro("@longitud", SqlDbType.VarChar, cliente.longitud);
                db.agregarParametro("@vendedorId", SqlDbType.Int, cliente.vendedorId);
                db.agregarParametro("@id", SqlDbType.BigInt, cliente.id);
                db.consultaSinRetorno("cliente_edita");
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }

           
            return cliente.id;
        }

        public List<clienteEntity> cliente_filtro(clienteEntity cliente)
        {
            List<clienteEntity> result = new List<clienteEntity>();
            dbHelper db = new dbHelper();
            SqlDataReader reader = null;
            try
            {
                db.agregarParametro("@id", SqlDbType.BigInt, cliente.id);
                db.agregarParametro("@nombre", SqlDbType.VarChar, cliente.nombre);
                db.agregarParametro("@apellidos", SqlDbType.VarChar, cliente.apellidos);
                db.agregarParametro("@rfc", SqlDbType.VarChar, cliente.rfc);
                reader = db.consultaReader("cliente_filtro");
                result = db.MapDataToEntityCollection<clienteEntity>(reader).ToList();
                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch (Exception ex)
            {
                result = null;
                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }
            
            return result;
        }

    }
}