﻿using AppWCFService.Entity;
using AppWCFService.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace AppWCFService.Controllers
{
    public class unidadController
    {
        public List<unidadEntity> unidad_obtenLista()
        {
            List<unidadEntity> result = new List<unidadEntity>();
            dbHelper db = new dbHelper();
            SqlDataReader reader = null;
            try
            {
                reader = db.consultaReader("unidad_obtenLista");
                result = db.MapDataToEntityCollection<unidadEntity>(reader).ToList();
                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch {

                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }
           
            return result;
        }

        public unidadEntity unidad_obten(long? id)
        {
            unidadEntity result = new unidadEntity();
            dbHelper db = new dbHelper();
            SqlDataReader reader = null;
            try
            {
                db.agregarParametro("@id", SqlDbType.BigInt, id);
                reader = db.consultaReader("unidad_obten");
                result = db.MapDataToEntityCollection<unidadEntity>(reader).ToList()[0];
                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch (Exception ex) {
                result = null;
                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }
           
           
            return result;
        }

        public long? unidad_nuevo(unidadEntity unidad)
        {
            List<almacenEntity> result = new List<almacenEntity>();
            dbHelper db = new dbHelper();
            long id = 0;
            try
            {
                db.agregarParametro("@activo", SqlDbType.Bit, unidad.activo);
                db.agregarParametro("@nombre", SqlDbType.VarChar, unidad.nombre);
                db.agregarParametro("@sat_clvUnidad", SqlDbType.VarChar, unidad.sat_clvUnidad);

                db.agregarParametro("@id", SqlDbType.BigInt, ParameterDirection.Output);
                db.consultaOutput("unidad_nuevo");
                id = long.Parse(db.diccionarioOutput["@id"].ToString());
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }
            
            return id;
        }

        public long? unidad_edita(unidadEntity unidad)
        {          
            dbHelper db = new dbHelper();
            try
            {
                db.agregarParametro("@activo", SqlDbType.Bit, unidad.activo);
                db.agregarParametro("@nombre", SqlDbType.VarChar, unidad.nombre);
                db.agregarParametro("@sat_clvUnidad", SqlDbType.VarChar, unidad.sat_clvUnidad);
                db.agregarParametro("@id", SqlDbType.BigInt, unidad.id);
                db.consultaOutput("unidad_edita");
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }
            
            return unidad.id;
        }

    }
}