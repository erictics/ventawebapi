﻿using AppWCFService.Entity;
using AppWCFService.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace AppWCFService.Controllers
{
    public class vendedorController
    {
        public List<vendedorEntity> vendedor_obtenLista()
        {
            List<vendedorEntity> result = new List<vendedorEntity>();
            dbHelper db = new dbHelper();
            SqlDataReader reader = null;
            try
            {               
                reader = db.consultaReader("vendedor_obtenLista");
                result = db.MapDataToEntityCollection<vendedorEntity>(reader).ToList();
                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch
            {

                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }




            return result;
        }


        public vendedorEntity vendedor_obten(long? id)
        {
            vendedorEntity result = new vendedorEntity();
            dbHelper db = new dbHelper();
            SqlDataReader reader = null;
            try
            {
                db.agregarParametro("@id", SqlDbType.BigInt, id);
                reader = db.consultaReader("vendedor_obten");
                result = db.MapDataToEntityCollection<vendedorEntity>(reader).ToList()[0];
                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch (Exception ex)
            {

                result = null;
                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }

            return result;
        }


        public ResultEntity vendedor_nuevo(vendedorEntity vendedor)
        {
            dbHelper db = new dbHelper();
            ResultEntity obj = new ResultEntity();
            try
            {
                List<usuarioEntity> result = new List<usuarioEntity>();

                db.agregarParametro("@nombre", SqlDbType.VarChar, vendedor.nombre);
                db.agregarParametro("@domicilio", SqlDbType.VarChar, vendedor.domicilio);
                db.agregarParametro("@activo", SqlDbType.Bit, vendedor.activo);               
                db.agregarParametro("@message", SqlDbType.VarChar, ParameterDirection.Output);
                db.agregarParametro("@id", SqlDbType.BigInt, ParameterDirection.Output);
                db.agregarParametro("@ErrorCode", SqlDbType.BigInt, ParameterDirection.Output);
                db.consultaOutput("vendedor_nuevo");
                int id = int.Parse(db.diccionarioOutput["@id"].ToString());
                obj.ErrorCode = int.Parse(db.diccionarioOutput["@ErrorCode"].ToString());
                obj.Message = db.diccionarioOutput["@message"].ToString();
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch (Exception ex)
            {
                obj.ErrorCode = 1;
                obj.Message = ex.InnerException.Message;

                db.conexion.Close();
                db.conexion.Dispose();
            }

            return obj;
        }


        public ResultEntity vendedor_editar(vendedorEntity vendedor)
        {
            dbHelper db = new dbHelper();
            ResultEntity obj = new ResultEntity();
            try
            {
                List<usuarioEntity> result = new List<usuarioEntity>();

                db.agregarParametro("@nombre", SqlDbType.VarChar, vendedor.nombre);
                db.agregarParametro("@domicilio", SqlDbType.VarChar, vendedor.domicilio);
                db.agregarParametro("@activo", SqlDbType.Bit, vendedor.activo);
                db.agregarParametro("@message", SqlDbType.VarChar, ParameterDirection.Output);
                db.agregarParametro("@id", SqlDbType.BigInt,vendedor.id );
                db.agregarParametro("@ErrorCode", SqlDbType.BigInt, ParameterDirection.Output);
                db.consultaOutput("vendedor_editar");
              
                obj.ErrorCode = int.Parse(db.diccionarioOutput["@ErrorCode"].ToString());
                obj.Message = db.diccionarioOutput["@message"].ToString();
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch (Exception ex)
            {
                obj.ErrorCode = 1;
                obj.Message = ex.InnerException.Message;

                db.conexion.Close();
                db.conexion.Dispose();
            }

            return obj;
        }



    }
}