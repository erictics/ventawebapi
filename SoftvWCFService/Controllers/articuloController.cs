﻿using AppWCFService.Entity;
using AppWCFService.Services;
using iTextSharp.text;
using iTextSharp.text.pdf;
using PdfiumViewer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;

namespace AppWCFService.Controllers
{
    public class articuloController
    {
        inventarioController inventario_ = new inventarioController();
        unidadController unidad = new unidadController();
        categoriaController categoria = new categoriaController();
        //ubicacionController ubicacion = new ubicacionController();
        //articuloCaracteristicaController articuloCaracteristica_ = new articuloCaracteristicaController();

        public List<articuloEntity> articulo_obtenLista(string upc,string nombre,long ? departamentoId, long ? categoriaId)
        {

            List<articuloEntity> result = new List<articuloEntity>();
            dbHelper db = new dbHelper();
            try
            {
         
                db.agregarParametro("@upc", SqlDbType.VarChar, upc);
                db.agregarParametro("@nombre", SqlDbType.VarChar, nombre);
                db.agregarParametro("@departamentoId", SqlDbType.BigInt, departamentoId);
                db.agregarParametro("@categoriaId", SqlDbType.BigInt, categoriaId);
                SqlDataReader reader = db.consultaReader("articulo_obtenLista");
                result = db.MapDataToEntityCollection<articuloEntity>(reader).ToList();
                try
                {
                    result.ForEach(y => { y.unidad = unidad.unidad_obten(y.unidadId); });
                }
                catch { }

                try
                {
                    result.ForEach(y => { y.categoria = categoria.categoria_obten(y.categoriaId); });
                }
                catch { }


                result.ForEach(y=> { y.precio = y.precioLista.ToString("C"); });
                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();

            }
            catch
            {
                
                db.conexion.Close();
                db.conexion.Dispose();

            }
            
            return result;
        }



        public List<articuloEntity> GetArticuloInventario(string upc, string nombre, long? departamentoId, long? categoriaId,long ? vendedorId,long ? sucursalId)
        {

            List<articuloEntity> result = new List<articuloEntity>();
            dbHelper db = new dbHelper();
            try
            {

                db.agregarParametro("@upc", SqlDbType.VarChar, upc);
                db.agregarParametro("@nombre", SqlDbType.VarChar, nombre);
                db.agregarParametro("@departamentoId", SqlDbType.BigInt, departamentoId);
                db.agregarParametro("@categoriaId", SqlDbType.BigInt, categoriaId);
                db.agregarParametro("@vendedorId", SqlDbType.BigInt, vendedorId);
                db.agregarParametro("@sucursalId", SqlDbType.BigInt, sucursalId);
                SqlDataReader reader = db.consultaReader("GetArticuloInventario");
                result = db.MapDataToEntityCollection<articuloEntity>(reader).ToList();
                try
                {
                    result.ForEach(y => { y.unidad = unidad.unidad_obten(y.unidadId); });
                }
                catch { }

                try
                {
                    result.ForEach(y => { y.categoria = categoria.categoria_obten(y.categoriaId); });
                }
                catch { }


                result.ForEach(y => { y.precio = y.precioLista.ToString("C"); });
                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();

            }
            catch
            {

                db.conexion.Close();
                db.conexion.Dispose();

            }

            return result;
        }

        public articuloEntity GetArticuloUPC(string upc)
        {

            articuloEntity result = new articuloEntity();
            dbHelper db = new dbHelper();
            SqlDataReader reader = null;
            try
            {


                db.agregarParametro("@upc", SqlDbType.VarChar, upc);
                 reader = db.consultaReader("articulo_ObtenUPC");
                try
                {
                    result = db.MapDataToEntityCollection<articuloEntity>(reader).ToList()[0];
                    result.unidad = unidad.unidad_obten(result.unidadId);
                    result.categoria = categoria.categoria_obten(result.categoriaId);
                    // result.caracteristicas = articuloCaracteristica_.articulo_obtenCaracteristicas(result.id);
                }
                catch (Exception ex)
                {
                    result = null;
                }

                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();

            }
            catch
            {
                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }
           
            

           
            return result;
        }


        public articuloEntity articulo_obten(long ? id)
        {

            articuloEntity result = new articuloEntity();
            dbHelper db = new dbHelper();
            SqlDataReader reader = null;

            try
            {
               
                db.agregarParametro("@id", SqlDbType.BigInt, id);
                reader = db.consultaReader("articulo_obten");
                try
                {
                    result = db.MapDataToEntityCollection<articuloEntity>(reader).ToList()[0];
                    result.unidad = unidad.unidad_obten(result.unidadId);
                    result.categoria = categoria.categoria_obten(result.categoriaId);
                   
                    //result.caracteristicas = articuloCaracteristica_.articulo_obtenCaracteristicas(result.id);
                }
                catch (Exception ex)
                {
                    result = null;
                }
                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();

            }
            catch{
                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }
            
            
            
            
            return result;
        }

        public long? articulo_nuevo(articuloEntity articulo)
        {
            List<almacenEntity> result = new List<almacenEntity>();
            dbHelper db = new dbHelper();
            db.agregarParametro("@nombre", SqlDbType.VarChar, articulo.nombre);
            db.agregarParametro("@precioUnitario", SqlDbType.Decimal, articulo.precioLista);
           // db.agregarParametro("@precioCredito", SqlDbType.Decimal, articulo.precioCredito);
            db.agregarParametro("@categoriaId", SqlDbType.Int, articulo.categoriaId);
            db.agregarParametro("@activo", SqlDbType.Bit, articulo.activo);
            db.agregarParametro("@descripcion", SqlDbType.VarChar, articulo.descripcion);
           // db.agregarParametro("@credito", SqlDbType.Bit, articulo.credito);
            db.agregarParametro("@upc", SqlDbType.VarChar, articulo.upc);
            db.agregarParametro("@unidadId", SqlDbType.Int, articulo.unidadId);
           // db.agregarParametro("@precioMayoreo", SqlDbType.Decimal, articulo.precioMayoreo);
            db.agregarParametro("@minInventario", SqlDbType.Int, articulo.minInventario);
           // db.agregarParametro("@mayoreo", SqlDbType.Bit, articulo.mayoreo);
            db.agregarParametro("@venta", SqlDbType.Bit, articulo.credito);
            db.agregarParametro("@stockSeguro", SqlDbType.Int, articulo.stockSeguro);
            db.agregarParametro("@sat_clvProdServ", SqlDbType.VarChar, articulo.sat_clvProdServ);
            db.agregarParametro("@id", SqlDbType.BigInt, ParameterDirection.Output);
            db.consultaOutput("articulo_nuevo");
            long id = long.Parse(db.diccionarioOutput["@id"].ToString());
            db.conexion.Close();
            db.conexion.Dispose();
            return id;


        }



        public long? articulo_edita(articuloEntity articulo)
        {
           
            dbHelper db = new dbHelper();
            db.agregarParametro("@nombre", SqlDbType.VarChar, articulo.nombre);
            db.agregarParametro("@precioUnitario", SqlDbType.Decimal, articulo.precioLista);           
            db.agregarParametro("@categoriaId", SqlDbType.Int, articulo.categoriaId);
            db.agregarParametro("@activo", SqlDbType.Bit, articulo.activo);
            db.agregarParametro("@descripcion", SqlDbType.VarChar, articulo.descripcion);          
            db.agregarParametro("@upc", SqlDbType.VarChar, articulo.upc);
            db.agregarParametro("@unidadId", SqlDbType.Int, articulo.unidadId);         
            db.agregarParametro("@minInventario", SqlDbType.Int, articulo.minInventario);
            db.agregarParametro("@venta", SqlDbType.Bit, articulo.credito);
            db.agregarParametro("@stockSeguro", SqlDbType.Int, articulo.stockSeguro);
            db.agregarParametro("@sat_clvProdServ", SqlDbType.VarChar, articulo.sat_clvProdServ);
            db.agregarParametro("@identificador", SqlDbType.VarChar, articulo.identificador);
            db.agregarParametro("@id", SqlDbType.BigInt, articulo.id);
            db.consultaSinRetorno("articulo_edita");           
            db.conexion.Close();
            db.conexion.Dispose();

           

            return articulo.id;
        }

         
        






        


        public long? articulo_eliminaUbicacion(long? id)
        {
            dbHelper db = new dbHelper();
            db.agregarParametro("@id", SqlDbType.BigInt, id);
            db.consultaSinRetorno("articulo_eliminaUbicacion");
            db.conexion.Close();
            db.conexion.Dispose();
            return id;
        }

        public List<articuloUbicacionEntity> articulo_obtenUbicacion(long ? id)
        {

            List<articuloUbicacionEntity> result = new List<articuloUbicacionEntity>();
            dbHelper db = new dbHelper();
            SqlDataReader reader = null;
            try
            {
                db.agregarParametro("@articuloId", SqlDbType.BigInt, id);
                reader = db.consultaReader("articulo_obtenUbicacion");
                result = db.MapDataToEntityCollection<articuloUbicacionEntity>(reader).ToList();
                result.ForEach(y => { y.articulo = articulo_obten(y.articuloId); });
                //result.ForEach(y => { y.ubicacion = ubicacion.ubicacion_obten(y.ubicacionId); });
                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch
            {
                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }
            
            
            return result;
        }

        public List<articuloEntity> GetBusquedaArticuloVenta(int ? op,string  buscar)
        {
            List<articuloEntity> result = new List<articuloEntity>();
            dbHelper db = new dbHelper();
            SqlDataReader reader = null;
            try
            {
                db.agregarParametro("@buscar", SqlDbType.VarChar, buscar);
                reader = db.consultaReader("BusquedaArticuloVenta");
                result = db.MapDataToEntityCollection<articuloEntity>(reader).ToList();
                result.ForEach(y=>{
                    y.unidad = unidad.unidad_obten(y.unidadId);
                    y.categoria = categoria.categoria_obten(y.categoriaId);
                });
                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();
                // result.caracteristicas = articuloCaracteristica_.articulo_obtenCaracteristicas(result.id);
            }
            catch (Exception ex)
            {
                result = null;
                reader.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }

           
            return result;
        }


        public List<caracteristicaEntity> GetCaracteristicaArticulo(long? articuloId)
        {
            List<caracteristicaEntity> result = new List<caracteristicaEntity>();
            dbHelper db = new dbHelper();
            SqlDataReader reader = null;



            try
            {
                db.agregarParametro("@articuloId", SqlDbType.BigInt, articuloId);
                reader = db.consultaReader("articuloCaracteristica_obtenLista");
                result = db.MapDataToEntityCollection<caracteristicaEntity>(reader).ToList();
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }

            return result;
        }


        public ResultEntity AddCaracteristicaArticulo(long? articuloId,long ? caracteristicaId, string valor)
        {
            ResultEntity result = new ResultEntity();
            dbHelper db = new dbHelper();

            try{
                db.agregarParametro("@articuloId", SqlDbType.BigInt, articuloId);
                db.agregarParametro("@caracteristicaId", SqlDbType.BigInt, caracteristicaId);
                db.agregarParametro("@valor", SqlDbType.VarChar, valor);
                db.consultaSinRetorno("articuloCaracteristica_nuevo");

                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }

                                
           
            return result;
        }

        public ResultEntity DeleteCaracteristicaArticulo(long? id)
        {
            ResultEntity result = new ResultEntity();
            dbHelper db = new dbHelper();
            try
            {
                db.agregarParametro("@id", SqlDbType.BigInt, id);
                db.consultaSinRetorno("articuloCaracteristica_eliminar");
                db.conexion.Close();
                db.conexion.Dispose();

            }
            catch
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }

            
            
            return result;
        }

        public bool ? GetexisteUPC(long ? id,string upc)
        {
            bool? existe = false;
            dbHelper db = new dbHelper();
            try
            {
                db.agregarParametro("@id", SqlDbType.BigInt, id);
                db.agregarParametro("@upc", SqlDbType.VarChar, upc);
                db.agregarParametro("@existe", SqlDbType.Bit, ParameterDirection.Output);
                db.consultaOutput("existeUPC");
                existe = bool.Parse(db.diccionarioOutput["@existe"].ToString());
                db.conexion.Close();
                db.conexion.Dispose();

            }
            catch
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }

            return existe;

        }


        public string GetDescargaCodigoBarrasDescripcion( int? id)
        {
            string apppath = System.AppDomain.CurrentDomain.BaseDirectory;
            string name = Guid.NewGuid().ToString() + ".pdf";
            string fileName = apppath + "/Reportes/" + name;

            articuloController artCtrl = new articuloController();
            articuloEntity articulo = artCtrl.articulo_obten(id);

            var pgSize = new iTextSharp.text.Rectangle(400, 100);
         

            Document doc = new Document(pgSize, 0f, 0f, 0f, 0f);
            iTextSharp.text.Font Fontr = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 18, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
            PdfWriter writer = PdfWriter.GetInstance(doc, new FileStream(fileName, FileMode.Create));
            doc.Open();
            doc.SetMargins(0, 0, 0, 0);
            
            PdfPTable TableDetalle = null;            
            TableDetalle = new PdfPTable(2);
            TableDetalle.WidthPercentage = 100;
            TableDetalle.HorizontalAlignment = Element.ALIGN_CENTER;
           
            PdfPCell textCell = new PdfPCell(new Phrase(""+ articulo.nombre + "" + "\n " +  "" + articulo.precioLista.ToString("C") + "\n " + articulo.categoria.nombre, Fontr));
            textCell.BorderWidth = 0;
                //cell.BackgroundColor = color;
                //cell.Colspan = colspan.Value;
                textCell.HorizontalAlignment = Element.ALIGN_LEFT; ;



                BarcodeLib.Barcode b = new BarcodeLib.Barcode();
                b.IncludeLabel = true;
                string imgsrc = apppath + "/Reportes/" + Guid.NewGuid() + ".jpg";
                System.Drawing.Image img = b.Encode(BarcodeLib.TYPE.EAN13, articulo.upc, Color.Black, Color.White, 290, 120);
                img.Save(imgsrc);


                iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(imgsrc);
                jpg.ScaleAbsolute(150f, 70f);
                PdfPCell imageCell = new PdfPCell(jpg);
                imageCell.Colspan = 1; // either 1 if you need to insert one cell
                imageCell.Border = 0;

                TableDetalle.AddCell(textCell);
                TableDetalle.AddCell(imageCell);

           
            doc.Add(TableDetalle);
            doc.Close();
            writer.Close();
            Size size = new Size(100,100);

            string imgsrc2 = apppath + "/Reportes/" + Guid.NewGuid() + ".png";
            FileStream stream = new FileStream(fileName, FileMode.Open);
            byte[] png = Freeware.Pdf2Png.Convert(stream, 1);
            string base64String = Convert.ToBase64String(png);
            return base64String;


            //using (FileStream file = File.OpenRead(apppath)) // in file
            //{
            //    var bytes = new byte[file.Length];
            //    file.Read(bytes, 0, bytes.Length);
            //    using (var pdf = new LibPdf(bytes))
            //    {
            //        byte[] pngBytes = pdf.GetImage(0, ImageType.PNG); // image type
            //        using (var outFile = File.Create(imgsrc2)) // out file
            //        {
            //            outFile.Write(pngBytes, 0, pngBytes.Length);
            //        }
            //    }
            //}

            return name;
        }

        public string GetDescargaCodigoBarras(string upc,int ? op)
        {
            BarcodeLib.Barcode b = new BarcodeLib.Barcode();

           

            string apppath = System.AppDomain.CurrentDomain.BaseDirectory;
            b.IncludeLabel = true;
            string imgsrc = apppath + "/Reportes/" + Guid.NewGuid() + ".jpg";
            System.Drawing.Image img = b.Encode(BarcodeLib.TYPE.EAN13, upc, Color.Black, Color.White, 290, 120);
            img.Save(imgsrc);


            using (System.Drawing.Image image = System.Drawing.Image.FromFile(imgsrc))
            {
                using (MemoryStream m = new MemoryStream())
                {
                    image.Save(m, image.RawFormat);
                    byte[] imageBytes = m.ToArray();

                    // Convert byte[] to Base64 String
                    string base64String = Convert.ToBase64String(imageBytes);
                    return base64String;
                }
            }
        }


      


        public string GenerateRandomCode()
        {
          string code=  CalculateEan13("750", "0002", GenerateRandomNo().ToString());

          return code;
        }
        
        public int GenerateRandomNo()
        {
            int _min = 10000;
            int _max = 99999;
            Random _rdm = new Random();
            return _rdm.Next(_min, _max);
        }

        public string CalculateEan13(string country, string manufacturer, string product)
        {
            string temp = $"{country}{manufacturer}{product}";
            int sum = 0;
            int digit = 0;

            // Calculate the checksum digit here.
            for (int i = temp.Length; i >= 1; i--)
            {
                digit = Convert.ToInt32(temp.Substring(i - 1, 1));
                // This appears to be backwards but the 
                // EAN-13 checksum must be calculated
                // this way to be compatible with UPC-A.
                if (i % 2 == 0)
                { // odd  
                    sum += digit * 3;
                }
                else
                { // even
                    sum += digit * 1;
                }
            }
            int checkSum = (10 - (sum % 10)) % 10;
            return $"{temp}{checkSum}";
        }



        public ResultEntity AddArticuloPrecioCredito(long? articuloId, long ? mes,decimal ? precio)
        {
            ResultEntity result = new ResultEntity();
            dbHelper db = new dbHelper();

            try
            {
                db.agregarParametro("@articuloId", SqlDbType.BigInt, articuloId);
                db.agregarParametro("@mes", SqlDbType.BigInt, mes);
                db.agregarParametro("@precio", SqlDbType.Decimal, precio);
                db.consultaSinRetorno("articuloPrecioCredito_nuevo");

                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch(Exception ex)
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }



            return result;
        }


        public List<articuloPrecioCredito> GetArticuloPrecioCredito(long? articuloId)
        {
            List<articuloPrecioCredito> result = new List<articuloPrecioCredito>();
            dbHelper db = new dbHelper();
            SqlDataReader reader = null;



            try
            {
                db.agregarParametro("@articuloId", SqlDbType.BigInt, articuloId);
                reader = db.consultaReader("articuloPrecioCredito_obtenLista");
                result = db.MapDataToEntityCollection<articuloPrecioCredito>(reader).ToList();
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch
            {
                db.conexion.Close();
                db.conexion.Dispose();
            }

            return result;
        }


        

    }
}