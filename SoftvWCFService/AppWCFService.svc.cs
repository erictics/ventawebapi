﻿
using Microsoft.IdentityModel.Tokens;
using AppWCFService.Contracts;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Security.Claims;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Xml.Linq;
using AppWCFService.Controllers;
using AppWCFService.Entity;
using static AppWCFService.Entity.ImagenEntity;

namespace AppWCFService
{
    [ScriptService]
    public partial class AppWCFService : IUsuario, 
        IEstatus,IMotivo, ISucursal, ICaja, IFormaPago,
        IUnidad,IMetodoPago, IVenta,ICliente, IArticulo,IReportes,
        ICategoria, IDepartamento,IBanco,ICorteCaja,ItipoSalidaEfectivo,ISalidaEfectivo,IRetiroEfectivo,IProveedor,ITarea,IEntregaParcial,ICierreVenta,ISistemaGeneral,ICaracteristica, IDevolucionVenta,IArchivo,IFactura,IVendedor

    {
        
        estatusController estatusController = new estatusController();
       
        usuarioController usuarioController = new usuarioController();       
        motivoController motivoController = new motivoController();
        cajaController cajaController = new cajaController();
        sucursalController sucursalController = new sucursalController();
        formaPagoController formaPagoController = new formaPagoController();
        metodoPagoController metodoPagoController = new metodoPagoController();
        ventaController ventaController = new ventaController();
        ventaDetalleController ventaDetalleController = new ventaDetalleController();
        clienteController clienteController = new clienteController();
        bancoController bancoController = new bancoController();
        departamentoController departamentoController = new departamentoController();
        articuloController articuloController = new articuloController();
        categoriaController categoriaController = new categoriaController();
        unidadController unidadController = new unidadController();
        reportes reportes = new reportes();
        corteCajaController corteCajaController = new corteCajaController();
        tipoSalidaEfectivoController tipoSalidaEfectivoController = new tipoSalidaEfectivoController();
        salidaEfectivoController salidaEfectivoController = new salidaEfectivoController();
        retiroEfectivoController retiroEfectivoController = new retiroEfectivoController();
        proveedorController proveedorController = new proveedorController();
        tareaController tareaController = new tareaController();
        entregaParcialController entregaParcialController = new entregaParcialController();
        cierreVentaController cierreVentaController = new cierreVentaController();
        SistemaGeneralController SistemaGeneralController = new SistemaGeneralController();
        inventarioController inventarioController = new inventarioController();
        devolucionVentaController devolucionVentaController = new devolucionVentaController();
        ArchivosController ArchivosController = new ArchivosController();
        vendedorController vendedorController = new vendedorController();

        #region sistemaGeneral

        public int? AddCorreo(correosEntity correo)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return SistemaGeneralController.correo_nuevo(correo);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }

        }

        public int? DeleteCorreo(long? sucursalId)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return SistemaGeneralController.correo_elimina(sucursalId);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }

        }

        public List<correosEntity> GetCorreo()
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return SistemaGeneralController.correo_obtenLista();
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }

        }




        public sistemaGeneralEntity GetSistemaGeneral()
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return SistemaGeneralController.sistemaGeneral_obtenLista();
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        public int ? UpdateSistemaGeneral(sistemaGeneralEntity sistema)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return SistemaGeneralController.sistemaGeneral_edita(sistema);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }


        public ResultEntity EnviarTicket(long? ventaId, string email_enviar)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return SistemaGeneralController.EnviarTicket(ventaId, email_enviar);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        #endregion

        #region Usuario

        #endregion

        #region Estatus

        public List<estatusEntity> GetEstatus()
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return estatusController.estatus_obtenLista();
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        public estatusEntity GetEstatusById(long? id)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return estatusController.estatus_obten(id);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        #endregion
    
        #region Motivo

        public List<motivoEntity> GetMotivo()
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                var request = HttpContext.Current.Request;
                string tipo=request["tipo"].ToString();

                return motivoController.motivo_obtenLista(tipo);
            
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }

        }

        public motivoEntity GetMotivoById()
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                var request = HttpContext.Current.Request;
                long id = long.Parse(request["id"].ToString());
                return motivoController.motivo_obten(id);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }

        }

        public long? AddMotivo(motivoEntity motivo)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
              
                return motivoController.motivo_nuevo(motivo);

            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }

        }

        public long? UpdateMotivo(motivoEntity motivo)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return motivoController.motivo_edita(motivo);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }

        }

        #endregion

        #region Banco


        public List<BancoEntity> GetBanco()
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                var request = HttpContext.Current.Request;
                return bancoController.banco_obtenLista();

            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }

        }

        public BancoEntity GetBancoById()
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                var request = HttpContext.Current.Request;
                long id = long.Parse(request["id"].ToString());
                return bancoController.banco_obten(id);

            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }

        }

        public long? AddBanco(BancoEntity banco)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {

                return bancoController.banco_nuevo(banco);

            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }

        }

        public long? UpdateBanco(BancoEntity banco)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return bancoController.banco_edita(banco);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }

        }


        #endregion

        #region Caja
        public List<cajaEntity> GetCaja()
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return cajaController.caja_obtenLista();

            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }

        }

        public cajaEntity GetCajaById()
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                var request = HttpContext.Current.Request;
                long id = long.Parse(request["id"].ToString());
                return cajaController.caja_obten(id);

            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }

        }

        public long? AddCaja(cajaEntity caja)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return cajaController.caja_nuevo(caja);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }

        }

        public long? UpdateCaja(cajaEntity caja)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return cajaController.caja_edita(caja);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }

        }


        #endregion
        
        #region sucursal
        public List<sucursalEntity> GetSucursal()
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                var request = HttpContext.Current.Request;
                long usuarioId = 0;
                try
                {
                    usuarioId=long.Parse(request["usuarioId"].ToString());
                }
                catch
                {

                }
                   
                return sucursalController.sucursal_obtenLista(usuarioId);

            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }

        }

        public sucursalEntity GetSucursalById()
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                var request = HttpContext.Current.Request;
                long id = long.Parse(request["id"].ToString());
                return sucursalController.sucursal_obten(id);

            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }

        }

        public long? AddSucursal(sucursalEntity sucursal)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return sucursalController.sucursal_nuevo(sucursal);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }

        }

        public long? UpdateSucursal(sucursalEntity sucursal)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return sucursalController.sucursal_edita(sucursal);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }

        }


        #endregion

        #region formaPago
        public List<formaPagoEntity> GetFormaPago()
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return formaPagoController.formaPago_obtenLista();

            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }

        }

        public formaPagoEntity GetFormaPagoById()
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                var request = HttpContext.Current.Request;
                long id = long.Parse(request["id"].ToString());
                return formaPagoController.formaPago_obten(id);

            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }

        }


        #endregion

        #region MetodoPago

        public List<metodoPagoEntity> GetMetodoPago()
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return metodoPagoController.metodoPago_obtenLista();

            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }

        }

        public metodoPagoEntity GetMetodoPagoById()
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                var request = HttpContext.Current.Request;
                long id = long.Parse(request["id"].ToString());
                return metodoPagoController.metodoPago_obten(id);

            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }

        }
        #endregion

        #region Usuario

        public List<ModuloEntity> GetModulos()
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return usuarioController.ObtenModulos();
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }

        }

        public  ModuloEntity GetPermisoAccesoUsuario(int? tipoUsuario, string componente)
        {
            if(tipoUsuario== null)
            {
                tipoUsuario = 0;
            }

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return usuarioController.GetPermisoAccesoUsuario(tipoUsuario, componente);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }

        }

        public int? GetGuardaPermisos(List<ModuloEntity> modulos, int? tipoUsuario)
        {

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return usuarioController.GuardaPermisos(modulos, tipoUsuario);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        public List<ModuloEntity> GetObtenModulosUsuario(int? tipoUsuario, int? op)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return usuarioController.ObtenModulosUsuario(tipoUsuario, op);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        public List<tipoUsuarioEntity> GetTiposUsuario()
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return usuarioController.GetTiposUsuario();
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        public int? AddTiposUsuario(tipoUsuarioEntity obj)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return usuarioController.AddTipoUsuario(obj);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }


        public int? UpdateTiposUsuario(tipoUsuarioEntity obj)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return usuarioController.UpdateTiposUsuario(obj);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        public tipoUsuarioEntity GetTipoUsuarioById()
        {

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                var request = HttpContext.Current.Request;
                long id = long.Parse(request["id"].ToString());
                return usuarioController.GetTipoUsuarioById(id);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }




        public List<usuarioEntity> GetAutentificaUsuario(long ? op,string email, string password)
        {

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return usuarioController.usuario_autenticacion(email, password);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }

        }

        public bool? GetUsuarioAutorizacionVenta(string email, string password)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return usuarioController.usuario_autorizacionventa(email, password);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        public List<usuarioEntity> GetUsuario()
        {

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                var request = HttpContext.Current.Request;
                long sucursalId = long.Parse(request["sucursalId"].ToString());
                long tipoUsuarioId = long.Parse(request["tipoUsuarioId"].ToString());
                string login = request["login"].ToString();
                string email = request["email"].ToString();
                long usuarioId = long.Parse(request["usuarioId"].ToString());
                return usuarioController.usuario_obtenLista(sucursalId, tipoUsuarioId, login, email, usuarioId);
            }
            catch (Exception ex)
            {
                List<usuarioEntity> l = new List<usuarioEntity>();
                return l;
               // throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        public usuarioEntity GetUsuarioById()
        {

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                var request = HttpContext.Current.Request;
                long id = long.Parse(request["id"].ToString());
                return usuarioController.usuario_obten(id);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        public ResultEntity AddUsuario(usuarioEntity usuario, List<sucursalEntity> sucursales)
        {

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                
                return usuarioController.usuario_nuevo(usuario, sucursales);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        public ResultEntity UpdateUsuario(usuarioEntity usuario, List<sucursalEntity> sucursales)
        {

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {

                return usuarioController.usuario_edita(usuario, sucursales);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        public List<sucursalEntity> GetSucursalUsuario()
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                var request = HttpContext.Current.Request;
                long id = long.Parse(request["id"].ToString());
                return usuarioController.usuarioSucursal_obtenporUsuario(id);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }

        }


        public articuloEntity GetArticuloUPC()
        {

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                var request = HttpContext.Current.Request;
                string upc = request["upc"].ToString();
                return articuloController.GetArticuloUPC(upc);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }


        public bool? GetexisteUPC()
        {

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                var request = HttpContext.Current.Request;
                long id = long.Parse(request["id"].ToString());
                string upc = request["upc"].ToString();
                return articuloController.GetexisteUPC(id,upc);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }

        }
        #endregion

        #region  venta

       public  bool? GetValidaInventarioVendedor(int? vendedorId, int? articuloId, int? tempventaId, decimal? cantidad)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {

                return inventarioController.GetValidaInventarioVendedor(vendedorId, articuloId, tempventaId, cantidad);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        public int ? AddDescuentoVenta(long? ventaId, long? monto, long? usuarioId, int? porcentaje, string observaciones)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {

                return ventaController.venta_descuentoNuevo(ventaId,  monto,  usuarioId,  porcentaje,  observaciones);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        public List<VentaCreditoEntity> GetVentacreditoLista(int? clienteId, string fecha, string status, int? sucursalId)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {

                return ventaController.GetVentacreditoLista(clienteId, fecha, status, sucursalId);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }

        }

        public bool ? GetAutorizaCancelacionVenta(int? op, string email, string password)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {

                return ventaController.GetAutorizaCancelacionVenta(0,email, password);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }

        }


       public List<ventaEntity> GetVenta(long? cajeroId, long? sucursalId, string fechaInicio, string fechaFin, string referencia, long? IdVenta,string serie,int ? usuarioId)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {

                return ventaController.venta_obtenLista( cajeroId, sucursalId,  fechaInicio,  fechaFin, referencia, IdVenta, serie, usuarioId);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }

        }

        public  ventaEntity  GetIniciaVenta(int? sucursalId, int? cajeroId, string modulo)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
               
                return ventaController.venta_iniciar(sucursalId, cajeroId, modulo);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }

        }

        public decimal ? GetVentaTotal(long ? ventaId)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {

                return ventaController.venta_obtentotal(ventaId);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        public string GetVentaTicket(long? ventaId, long? usuarioId, bool? reimpresion, int? motivoId)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {

                return reportes.GetTicketVenta(ventaId, usuarioId, reimpresion, motivoId);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        public string GetReporteTickets(int? sucursalId, int? usuarioId, string fechaInicio, string fechaFin, bool? esCancelacion)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {

                return reportes.GetReporteTickets(sucursalId, usuarioId, fechaInicio, fechaFin, esCancelacion);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }

        }

        public long? GetVentaTerminar(ventaEntity venta)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {

                return ventaController.venta_terminar(venta);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        public long? GetCancelaVenta(long? ventaId, long? usuarioId,long ? motivoId)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {

                return ventaController.venta_cancelar(ventaId, usuarioId, motivoId);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        public ventaEntity GetVentaByReferencia()
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                var request = HttpContext.Current.Request;
                string id = request["id"].ToString();
                return ventaController.GetVentaByReferencia(id);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }
        #endregion

        #region ventaDetalle

        public long? AgregaItemVentaCredito(long? ventaId, long? articuloId,int ? meses, decimal? precio)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {

                return ventaDetalleController.venta_agregaItemCredito(ventaId, articuloId, meses, precio);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }

        }

        public long? AgregaItemVenta(long? ventaId, long? articuloId)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {

                return ventaDetalleController.venta_agregaItem(ventaId, articuloId);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }

        }

         public long? AgregaItemVentaLista(List<ventaDetalleEntity> articulos, int? op)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {

                return ventaDetalleController.venta_agregaItemLista(articulos, op);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }

        }

        public int ? DeleteItemVenta(long? ventaId, long? articuloId, int ? op)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {

                return ventaDetalleController.venta_eliminaItem(ventaId, articuloId,op);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }

        }

        public List<articuloEntity> GetItemsVenta(long? ventaId)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {

                return ventaDetalleController.venta_obtenerItems(ventaId);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }

        }
        #endregion

        #region cliente


        public List<clienteEntity> GetCliente(string rfc,int ? op,string nombre)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return clienteController.cliente_obtenLista(rfc, nombre);

            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }

        }

        public clienteEntity GetClienteById()
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                var request = HttpContext.Current.Request;
                long id = long.Parse(request["id"].ToString());
                return clienteController.cliente_obten(id);

            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }

        }

        public long? AddCliente(clienteEntity cliente)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return clienteController.cliente_nuevo(cliente);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }

        }

        public long? UpdateCliente(clienteEntity cliente)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return clienteController.cliente_edita(cliente);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }

        }

        public List<clienteEntity> cliente_filtro(clienteEntity cliente)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return clienteController.cliente_filtro(cliente);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        #endregion

        #region Articulo

       public string GenerateRandomCode()
        {

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return articuloController.GenerateRandomCode();
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }


        public string GetDescargaCodigoBarras(string upc, int? op)
        {

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return articuloController.GetDescargaCodigoBarras(upc, op);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }

        }
        public string GetDescargaCodigoBarrasDescripcion(int? id)
        {

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return articuloController.GetDescargaCodigoBarrasDescripcion(id);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }

        }


        

        public ResultEntity AddCaracteristicaArticulo(long? articuloId, long? caracteristicaId, string valor)
        {

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return articuloController.AddCaracteristicaArticulo(articuloId, caracteristicaId, valor);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }

        }

       public ResultEntity DeleteCaracteristicaArticulo(long? id)
        {

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return articuloController.DeleteCaracteristicaArticulo(id);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }

        }

        public List<caracteristicaEntity> GetCaracteristicaArticulo(long? articuloId)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return articuloController.GetCaracteristicaArticulo(articuloId);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }

        }



        public List<articuloEntity> GetBusquedaArticuloVenta(int? op, string buscar) {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return articuloController.GetBusquedaArticuloVenta(0,buscar);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        public ResultEntity AddArticuloPrecioCredito(long? articuloId, long? mes, decimal? precio)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return articuloController.AddArticuloPrecioCredito(articuloId, mes, precio);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }

        }

        public List<articuloPrecioCredito> GetArticuloPrecioCredito(long? articuloId)
        {

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return articuloController.GetArticuloPrecioCredito(articuloId);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }


          public   List<articuloEntity> GetArticulo()
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                var request = HttpContext.Current.Request;
                string upc = request["upc"].ToString();
                string nombre= request["nombre"].ToString();
                long ? departamentoId =long.Parse(request["departamentoId"].ToString());
                long? categoriaId = long.Parse(request["categoriaId"].ToString());                 
                return articuloController.articulo_obtenLista(upc,nombre,departamentoId, categoriaId);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }



        public List<articuloEntity> GetArticuloInventario()
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                var request = HttpContext.Current.Request;
                string upc = request["upc"].ToString();
                string nombre = request["nombre"].ToString();
                long? departamentoId = long.Parse(request["departamentoId"].ToString());
                long? categoriaId = long.Parse(request["categoriaId"].ToString());
                long? vendedorId = long.Parse(request["vendedorId"].ToString());
                long? sucursalId = long.Parse(request["sucursalId"].ToString());
                return articuloController.GetArticuloInventario(upc, nombre, departamentoId, categoriaId, vendedorId, sucursalId);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        public articuloEntity GetArticuloById()
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                var request = HttpContext.Current.Request;
                long id = long.Parse(request["id"].ToString());
                return articuloController.articulo_obten(id);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        public long ? AddArticulo(articuloEntity articulo)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return articuloController.articulo_nuevo(articulo);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        public long? UpdateArticulo(articuloEntity articulo)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return articuloController.articulo_edita(articulo);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }   

       


        public long? DeleteArticuloUbicacion()
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                var request = HttpContext.Current.Request;
                long id = long.Parse(request["id"].ToString());
                return articuloController.articulo_eliminaUbicacion(id);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        public List<articuloUbicacionEntity> GetArticuloUbicacion()
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                var request = HttpContext.Current.Request;
                long id = long.Parse(request["id"].ToString());
                return articuloController.articulo_obtenUbicacion(id);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }

        }


      

        #endregion

        #region Departamento
        public List<departamentoEntity> GetDepartamento()
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return departamentoController.departamento_obtenLista();
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        public departamentoEntity GetDepartamentoById()
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                var request = HttpContext.Current.Request;
                long id = long.Parse(request["id"].ToString());
                return departamentoController.departamento_obten(id);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        public long? AddDepartamento(departamentoEntity departamento)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return departamentoController.departamento_nuevo(departamento);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        public long? UpdateDepartamento(departamentoEntity departamento)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return departamentoController.departamento_edita(departamento);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        #endregion

        #region Categoria
        public List<categoriaEntity> GetCategoria()
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                var request = HttpContext.Current.Request;
                long departamentoId = long.Parse(request["departamentoId"].ToString());
                string nombre = request["nombre"].ToString();
                return categoriaController.categoria_obtenLista(departamentoId,nombre);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        public categoriaEntity GetCategoriaById()
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                var request = HttpContext.Current.Request;
                long id = long.Parse(request["id"].ToString());
                return categoriaController.categoria_obten(id);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        public long ? AddCategoria(categoriaEntity categoria)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return categoriaController.categoria_nuevo(categoria);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        public long? UpdateCategoria(categoriaEntity categoria)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return categoriaController.categoria_edita(categoria);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }
        #endregion

        #region Unidad
        public long? AddUnidad(unidadEntity unidad)
        {

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return unidadController.unidad_nuevo(unidad);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        public long? UpdateUnidad(unidadEntity unidad)
        {

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return unidadController.unidad_edita(unidad);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        public List<unidadEntity> GetUnidad()
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return unidadController.unidad_obtenLista();
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        public unidadEntity GetUnidadById()
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                var request = HttpContext.Current.Request;
                long id = long.Parse(request["id"].ToString());
                return unidadController.unidad_obten(id);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        #endregion

        #region corteCaja

        public List<corteCajaEntity> GetCorteCaja(long? cajaId, long? sucursalId, string fecha)
        {
            

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return corteCajaController.corteCaja_obtenLista(cajaId, sucursalId, fecha);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }


        public long? AddCorteCaja(corteCajaEntity corte){

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return corteCajaController.corteCaja_nuevo(corte);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        public corteCajaEntity GetCorteCajaById()
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                var request = HttpContext.Current.Request;
                long id = long.Parse(request["id"].ToString());
                return corteCajaController.corteCaja_obten(id);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }

        }
        #endregion

        #region tipoSalidaEfectivo

        public List<tipoSalidaEfectivoEntity> GettipoSalidaEfectivo()
        {


            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return tipoSalidaEfectivoController.tipoSalidaEfectivo_obtenLista();
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }


        public long? AddtipoSalidaEfectivo(tipoSalidaEfectivoEntity tipoSalidaEfectivo)
        {

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return tipoSalidaEfectivoController.tipoSalidaEfectivo_nuevo(tipoSalidaEfectivo);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        public long? UpdatetipoSalidaEfectivo(tipoSalidaEfectivoEntity tipoSalidaEfectivo)
        {

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return tipoSalidaEfectivoController.tipoSalidaEfectivo_edita(tipoSalidaEfectivo);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        public tipoSalidaEfectivoEntity GettipoSalidaEfectivoById()
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                var request = HttpContext.Current.Request;
                long id = long.Parse(request["id"].ToString());
                return tipoSalidaEfectivoController.tipoSalidaEfectivo_obten(id);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        #endregion

        #region salidaEfectivo

        public List<salidaEfectivoEntity> GetSalidaEfectivo(long? sucursalId, long? cajeroId, long? folio, string fecha,int ? userId2)
        {


            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return salidaEfectivoController.salidaEfectivo_obtenLista(sucursalId,cajeroId,  folio, fecha, userId2);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        public salidaEfectivoEntity GetSalidaEfectivoById()
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                var request = HttpContext.Current.Request;
                long id = long.Parse(request["id"].ToString());
                return salidaEfectivoController.salidaEfectivo_obten(id);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }


        public ResultEntity AddSalidaEfectivo(salidaEfectivoEntity salidaEfectivo)
        {

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return salidaEfectivoController.salidaEfectivo_nuevo(salidaEfectivo);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }


        public ResultEntity UpdateSalidaEfectivo(salidaEfectivoEntity salidaEfectivo)
        {

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return salidaEfectivoController.salidaEfectivo_edita(salidaEfectivo);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        public ResultEntity GetSalidaEfectivoCancela(int ? id, int? usuarioId)
        {

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return salidaEfectivoController.salidaEfectivo_cancela(id, usuarioId);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }


        #endregion

        #region retiroEfectivo

        public List<retiroEfectivoEntity> GetRetiroEfectivo(long? cajeroId, long? sucursalId, string fechaInicio, string fechaFin)
        {


            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return retiroEfectivoController.retiroEfectivo_obtenLista( cajeroId,   sucursalId,  fechaInicio,  fechaFin);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        public retiroEfectivoEntity GetRetiroEfectivoById()
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                var request = HttpContext.Current.Request;
                long id = long.Parse(request["id"].ToString());
                return retiroEfectivoController.retiroEfectivo_obten(id);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }


        public long? AddRetiroEfectivo(retiroEfectivoEntity RetiroEfectivo)
        {

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return retiroEfectivoController.retiroEfectivo_nuevo(RetiroEfectivo);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }


        //public long? UpdateRetiroEfectivo(retiroEfectivoEntity RetiroEfectivo)
        //{

        //    if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
        //    try
        //    {
        //        return retiroEfectivoController.(salidaEfectivo);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
        //    }
        //}



        #endregion

        #region caracteristica

        caracteristicaController caracteristica_ = new caracteristicaController();
        
        public List<caracteristicaEntity> GetCaracteristica()
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return caracteristica_.caracteristica_obtenLista();
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }

        }

        public ResultEntity AddCaracteristica(caracteristicaEntity caracteristica)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return caracteristica_.caracteristica_nuevo(caracteristica);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }

        }


        public ResultEntity UpdateCaracteristica(caracteristicaEntity caracteristica)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return caracteristica_.caracteristica_editar(caracteristica);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }

        }

        public caracteristicaEntity GetCaracteristicaById()
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                var request = HttpContext.Current.Request;
                long id = long.Parse(request["id"].ToString());
                return caracteristica_.caracteristica_obten(id);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }


        #endregion

        #region Reportes


        public string GetReporteFacturas(string fechaInicio, string fechaFin, int? sucursalId, int? usuarioId, string status, int? tipoDescarga)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return reportes.GetReporteFacturas( fechaInicio,  fechaFin, sucursalId,  usuarioId, status,  tipoDescarga);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }

        }

        public string GetReporteDetalleTarea(long? Id)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return reportes.GetReporteDetalleTarea(Id);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }

        }

       public string GetReporteResurtido(long? sucursalId, int? departamentoId, int? categoriaId, int? articuloId, int? tipoDescarga)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return reportes.GetReporteResurtido(sucursalId, departamentoId, categoriaId, articuloId, tipoDescarga);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }

        }

       public string GetReporteRecepcion(long? sucursalId, int? departamentoId, int? categoriaId, int? articuloId, string fechaInicio, string fechaFin, int? proveedorId, int? tipoDescarga)
        {

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return reportes.GetReporteRecepcion(sucursalId, departamentoId, categoriaId, articuloId,fechaInicio,fechaFin,proveedorId ,tipoDescarga);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        public string GetReporteMovimientosArticuloResumen(int? articuloId, int? departamentoId, int? categoriaId, int? sucursalId, string fechaInicio, string fechaFin, int ? tipoDescarga)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return reportes.GetReporteMovimientosArticuloResumen(articuloId, departamentoId, categoriaId, sucursalId, fechaInicio, fechaFin, tipoDescarga);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }

        }


        public string GetReporteArqueo(string fecha, int? usuarioId)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return reportes.GetReporteArqueo(fecha, usuarioId);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }


        public string GetReporteEntregaParcial(long? id)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return reportes.GetReporteEntregaParcial(id);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }


       public string GetReporteListadoEntregaParcial(int? usuarioId, string fechaInicio, string fechaFin, string rangos, int? tipoDescarga)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return reportes.GetReporteListadoEntregaParcial(usuarioId, fechaInicio, fechaFin, rangos, tipoDescarga);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }

        }


        public string GetReporteMovimientosArticulos(int? articuloId, int? departamentoId, int? categoriaId, int? sucursalId, string fechaInicio, string fechaFin, string fechaInicio2, string fechaFin2)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return reportes.GetReporteMovimientosArticulos(articuloId, departamentoId, categoriaId, sucursalId, fechaInicio, fechaFin, fechaInicio2, fechaFin2);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }


        }

      

        public string GetReporteInventario(long? sucursalId, int? departamentoId, int? categoriaId, int? estatus, int? tipoDescarga)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return reportes.GetReporteInventario(sucursalId, departamentoId, categoriaId, estatus, tipoDescarga);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        public string GetReporteInventarioVendedor(long? vendedorId, int? departamentoId, int? categoriaId, int? estatus, int? tipoDescarga)
        {

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return reportes.GetReporteVendedor(vendedorId, departamentoId, categoriaId, estatus, tipoDescarga);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }

        }


        public string GetReporteTraspaso(int? vendedorId, int? sucursalId, int? tipo, string fechaInicio, string fechaFin, int? tipoDescarga)
        {

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return reportes.GetReporteTraspaso( vendedorId,  sucursalId,  tipo,  fechaInicio,  fechaFin,  tipoDescarga);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        public string GetCortes(int? tipo, String fechaInicio, String fechaFin, int? sucursalId, int? usuarioId, int? tipoDescarga, int? usuario)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return reportes.GetCortes(tipo, fechaInicio, fechaFin, sucursalId, usuarioId, tipoDescarga, usuario);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }


        public string GetReporteCortesVendor(int? vendedorId, int? sucursalId, int? tipoventa, string fechaInicio, string fechaFin, int? status, int? tipoDescarga)
        {

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return reportes.GetReporteCortesVendor( vendedorId,  sucursalId,  tipoventa,  fechaInicio,  fechaFin,  status,  tipoDescarga);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }

        }







        public string GetReporteVentasByCajero(string fechaInicio, string fechaFin, int? usuario, int? sucursalId, int? departamentoId, int? categoriaId, int? articuloId, int? orden, int? tipoDescarga, bool? detalle)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return reportes.GetReporteVentasByCajero(fechaInicio, fechaFin, usuario, sucursalId, departamentoId, categoriaId, articuloId, orden, tipoDescarga, detalle);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        //public string GetReporteCorteCajero(string fechaInicio, string fechaFin, int? cajeroId)
        //{
        //    if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
        //    try
        //    {
        //        return reportes.GetReporteCorteCajero(fechaInicio, fechaFin, cajeroId);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
        //    }
        //}

        //public string GetReporteCorteSucursal(string fechaInicio, string fechaFin, int? sucursalId)
        //{
        //    if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
        //    try
        //    {
        //        return reportes.GetReporteCorteSucursal(fechaInicio, fechaFin, sucursalId);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
        //    }
        //}

        public string GetReporteSalidaEfectivo(string fechaInicio, string fechaFin, int? sucursalId, int? usuarioId, int? op, int? tipoDescarga)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return reportes.GetReporteSalidaEfectivo(fechaInicio, fechaFin, sucursalId, usuarioId, op, tipoDescarga);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }


        public string GetReporteCierreVenta(int ? id)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return reportes.GetReporteCierreVenta(id);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }


        public string GetReporteVentasResumenSucursal(string fechaInicio, string fechaFin, int? sucursalId, int? tipoDescarga, bool? detalle)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return reportes.GetReporteVentasResumenSucursal(fechaInicio, fechaFin, sucursalId, tipoDescarga, detalle);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }


        }


       public string GetReporteVentasResumenUsuario(string fechaInicio, string fechaFin, int? usuarioId, int? tipoDescarga, bool? detalle)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return reportes.GetReporteVentasResumenUsuario(fechaInicio, fechaFin, usuarioId, tipoDescarga, detalle);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }

        }


        public string GetReporteVentasResumenDepartamento(string fechaInicio, string fechaFin, int? sucursalId, int? departamentoId, int? tipoDescarga, bool? detalle)
        {
            


           if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return reportes.GetReporteVentasResumenDepartamento(fechaInicio, fechaFin, sucursalId, departamentoId, tipoDescarga, detalle);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }

        }


        public string GetReporteVentasResumenCategoria(string fechaInicio, string fechaFin, int? sucursalId, int? categoriaId, int? tipoDescarga, bool? detalle)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return reportes.GetReporteVentasResumenCategoria(fechaInicio, fechaFin, sucursalId, categoriaId, tipoDescarga, detalle);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }

        }



        public string GetReporteVentasResumenArticulo(string fechaInicio, string fechaFin, int? sucursalId, int? articuloId, int? tipoDescarga, bool? detalle)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return reportes.GetReporteVentasResumenArticulo(fechaInicio, fechaFin, sucursalId, articuloId, tipoDescarga, detalle);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }


        }



        #endregion

        #region Proveedor 




        public List<proveedorEntity> GetProveedor()
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return proveedorController.proveedor_obtenLista();
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        public proveedorEntity GetProveedorById()
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                var request = HttpContext.Current.Request;
                long id = long.Parse(request["id"].ToString());
                return proveedorController.proveedor_obten(id);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }


        public long? AddProveedor(proveedorEntity proveedor)
        {

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return proveedorController.proveedor_nuevo(proveedor);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        public long? UpdateProveedor(proveedorEntity proveedor)
        {

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return proveedorController.proveedor_edita(proveedor);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }



        #endregion

        #region Tarea

        public List<tareaEntity> GetTarea(long ? id, string fechaInicio,string fechaFin, int ? tipo, long ? sucursalId, string status, int? usuarioId)
       {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return tareaController.tarea_obtenLista(id, fechaInicio, fechaFin, tipo, sucursalId, status, usuarioId);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }


        public tareaEntity GetTareaById()
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                var request = HttpContext.Current.Request;
                long id = long.Parse(request["id"].ToString());
                return tareaController.tarea_obten(id);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        public long? AddRecepcionTarea(tareaEntity tarea, List<tareaDetalleEntity> detalle)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return tareaController.tarea_recepcionNueva(tarea, detalle);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }
        public long? AddTraspasoVendedor(tareaEntity tarea, List<tareaDetalleEntity> detalle)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return tareaController.tarea_AddTraspasoVendedor(tarea, detalle);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }


        public long? AddTraspasoSucursal(tareaEntity tarea, List<tareaDetalleEntity> detalle)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return tareaController.tarea_AddTraspasoSucursal(tarea, detalle);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }



        public long? UpdateRecepcionTarea(tareaEntity tarea, List<tareaDetalleEntity> detalle)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return tareaController.tarea_recepcionEdita(tarea,detalle);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        public ResultEntity GetCancelaRecepcion(int ? id, int ? usuarioId) {

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return tareaController.GetCancelaRecepcion(id, usuarioId);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }

        }

        public ResultEntity GetCancelaEstatus(int? id, int? usuarioId)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return tareaController.GetCancelaEstatus(id, usuarioId);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }


        public List<tareaDetalleEntity> GetTareaDetalle()
        {

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                var request = HttpContext.Current.Request;
                long id = long.Parse(request["id"].ToString());
                return tareaController.tareaDetalle_obtenLista(id);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }

        }



        public long? GetCambioEstatusNuevo(tareaEntity tarea, List<tareaDetalleEntity> detalle, int? estatus)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
               
                return tareaController.tarea_cambioEstatusNuevo(tarea,   detalle,  estatus);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }


        }





        #endregion


        #region Inventario 

        public decimal ? GetInventarioArticulo(int? sucursalId, int? articuloId, int? estatusId)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return inventarioController.GetInventarioArticulo(sucursalId, articuloId, estatusId);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        #endregion

        #region 


        public cierreVentaEntity GetCierreVentaById()
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                var request = HttpContext.Current.Request;
                long id = long.Parse(request["id"].ToString());
                return cierreVentaController.cierreVenta_obten(id);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }


        public List<cierreVentaEntity> GetCierreVenta(string fecha,long ? folio, int ? usuarioId,int ? sucursalId,int ? userId2)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
              
                return cierreVentaController.cierreVenta_obtenLista(fecha, folio, usuarioId, sucursalId, userId2);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }


        public ResultEntity AddCierreVenta(cierreVentaEntity entrega)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {

                return cierreVentaController.cierreVenta_nuevo(entrega);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }


        public long? UpdateCierreVenta(cierreVentaEntity entrega)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {

                return cierreVentaController.cierreVenta_edita(entrega);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        public ResultEntity DeleteCierreVenta(long? id, long? usuarioId)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {

                return cierreVentaController.cierreVenta_elimina(id, usuarioId);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }

        }

        #endregion

        #region 

        public ResultEntity UpdateEntregaParcial(EntregaParcialEntity entrega)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return entregaParcialController.entregaParcial_editar(entrega);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }


        public ResultEntity DeleteEntregaParcial(long? id, int? usuarioId)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return entregaParcialController.entregaParcial_cancela(id, usuarioId);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }

        }

        public List<EntregaParcialEntity> GetEntregaParcial()
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                
                    var request = HttpContext.Current.Request;
                string fechaInicio =request["fechaInicio"].ToString();
                int sucursalId = int.Parse(request["sucursalId"].ToString());
                string fechafin = request["fechafin"].ToString();
                int usuarioId = int.Parse(request["usuarioId"].ToString());
                int id = int.Parse(request["id"].ToString());
                int userId2 = int.Parse(request["userId2"].ToString());

                
                return entregaParcialController.entregaParcial_obtenLista(sucursalId ,fechaInicio,  fechafin, usuarioId,  id, userId2);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }


        public EntregaParcialEntity GetEntregaParcialById()
        {

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                var request = HttpContext.Current.Request;
                long id = long.Parse(request["id"].ToString());
                return entregaParcialController.entregaParcial_obten(id);
                
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }


        public ResultEntity AddEntregaParcial(EntregaParcialEntity entrega)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                return entregaParcialController.entregaParcial_nuevo(entrega);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        #endregion

        //facturaController factura_Controller = new facturaController();

        // public  List<SATEntity> GetUnidadSAT()
        //{

        //    if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
        //    try
        //    {
        //        var request = HttpContext.Current.Request;
        //        string descripcion = request["descripcion"].ToString();
        //        return factura_Controller.GetUnidadSAT(descripcion);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
        //    }
        //}


        //public List<SATEntity> GetProdSerSAT()
        //{

        //    if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
        //    try
        //    {
        //        var request = HttpContext.Current.Request;
        //        string descripcion = request["descripcion"].ToString();
        //        return factura_Controller.GetProdSerSAT(descripcion);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
        //    }
        //}


        //public ResultEntity GetFacturar(long? ventaId)
        //{
        //    if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
        //    try
        //    {
              
        //        return factura_Controller.Facturar(ventaId);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
        //    }

        //}

        #region Factura




        #endregion



        #region devolucionVenta 



        public List<devolucionVentaEntity> GetDevolucionVenta(long? folio, string fechaInicio, string fechaFin, long? sucursalId, long? cajeroId, long? usuarioId, long? ventaId)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {

                return devolucionVentaController.devolucionVenta_obtenLista(folio, fechaInicio, fechaFin, sucursalId, cajeroId, usuarioId, ventaId);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }

        }

        #endregion


        #region archivos






        public string saveFileTarea()
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            else
            {
                try
                {
                    var request = HttpContext.Current.Request;
                    //int clv_departamento =Int32.Parse(request["tipo"].ToString());                   

                    // string URL = request["URL"].ToString();
                    if (request.Files.Count > 0)
                    {
                        int? tipo = Int32.Parse(request.Params["tipo"].ToString());
                        int? tareaId = Int32.Parse(request.Params["TareaId"].ToString());
                        Stream streamed = request.Files[0].InputStream;
                        string nombre = request.Files[0].FileName;
                        return ArchivosController.saveFileTarea(streamed, tareaId, tipo, nombre);
                    }
                    else
                    {
                        throw new WebFaultException<string>("Archivo no encontrado", HttpStatusCode.ExpectationFailed);
                    }

                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }

        public List<logo> GetFileTarea(int? tareaId)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            else
            {
                try
                {
                    return ArchivosController.GetImagenesTarea(tareaId);
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }

        public string DeleteImagenesTarea(int? id)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            else
            {
                try
                {
                    return ArchivosController.DeleteImagenesTarea(id);
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }



        public string saveLogoApp()
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            else
            {
                try
                {
                    var request = HttpContext.Current.Request;
                    //int clv_departamento =Int32.Parse(request["tipo"].ToString());                   

                    // string URL = request["URL"].ToString();
                    if (request.Files.Count > 0)
                    {
                        int? tipo = Int32.Parse(request.Params["tipo"].ToString());
                        Stream streamed = request.Files[0].InputStream;
                        string nombre = request.Files[0].FileName;
                        return ArchivosController.saveLogoApp(streamed, tipo, nombre);
                    }
                    else
                    {
                        throw new WebFaultException<string>("Archivo no encontrado", HttpStatusCode.ExpectationFailed);
                    }

                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }

        public string saveFileArticulo()
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            else
            {
                try
                {
                    var request = HttpContext.Current.Request;
                    //int clv_departamento =Int32.Parse(request["tipo"].ToString());                   

                    // string URL = request["URL"].ToString();
                    if (request.Files.Count > 0)
                    {
                        int? tipo = Int32.Parse(request.Params["tipo"].ToString());
                        int? articuloId = Int32.Parse(request.Params["articuloId"].ToString());
                        Stream streamed = request.Files[0].InputStream;
                        string nombre = request.Files[0].FileName;
                        return ArchivosController.saveFileArticulo(streamed, articuloId, tipo, nombre);
                    }
                    else
                    {
                        throw new WebFaultException<string>("Archivo no encontrado", HttpStatusCode.ExpectationFailed);
                    }

                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }

        public List<logo> GetFileArticulo(int? articuloId)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            else
            {
                try
                {
                    return ArchivosController.GetImagenesArticulo(articuloId);
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }

        public string DeleteFilesArticulo(int? id)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            else
            {
                try
                {
                    return ArchivosController.DeleteFilesArticulo(id);
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }





        public string saveFileCliente()
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            else
            {
                try
                {
                    var request = HttpContext.Current.Request;
                    //int clv_departamento =Int32.Parse(request["tipo"].ToString());                   

                    // string URL = request["URL"].ToString();
                    if (request.Files.Count > 0)
                    {
                        int? tipo = Int32.Parse(request.Params["tipo"].ToString());
                        int? clienteId = Int32.Parse(request.Params["clienteId"].ToString());
                        Stream streamed = request.Files[0].InputStream;
                        string nombre = request.Files[0].FileName;
                        return ArchivosController.saveFileCliente(streamed, clienteId, tipo, nombre);
                    }
                    else
                    {
                        throw new WebFaultException<string>("Archivo no encontrado", HttpStatusCode.ExpectationFailed);
                    }

                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }

        public List<logo> GetFileCliente(int? clienteId)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            else
            {
                try
                {
                    return ArchivosController.GetImagenesCliente(clienteId);
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }

        public string DeleteFilesCliente(int? id)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            else
            {
                try
                {
                    return ArchivosController.DeleteFilesCliente(id);
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }



        #endregion



        #region factura



        public ProductoSAT GetEnviaFactura(long? clienteId, string facturaId)
        {

            facturaController facturaController = new facturaController();
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            else
            {
                try
                {
                    return facturaController.EnviaFactura(clienteId, facturaId);
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
        }

        public ProductoSAT GetDescargaFactura(int ? op,string id)
        {

            facturaController facturaController = new facturaController();
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            else
            {
                try
                {
                    return facturaController.DescargaFactura(id);
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }

        public List<facturaEntity> GetFacturas(string rfc, string fecha, string serie, string folio, string nombre, int? op, int? usuarioId, int? sucursalId)
        {
            facturaController facturaController = new facturaController();
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            else
            {
                try
                {
                    return facturaController.factura_obtenLista(rfc, fecha, serie, folio, nombre, op, usuarioId, sucursalId);
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
        }


        public ProductoSAT AddFactura(long? id,long ? usuarioId)

        {
            facturaController facturaController = new facturaController();
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            else
            {
                try
                {
                    return facturaController.AddFactura(id, usuarioId);
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }


        public ProductoSAT GetCancelaFactura(string FacturaId, long? usuarioId)

        {
            facturaController facturaController = new facturaController();
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            else
            {
                try
                {
                    return facturaController.GetCancelaFactura(FacturaId, usuarioId);
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }

        public List<ProductoSAT> GetCatalogoProductosSAT()
        {
            facturaController facturaController = new facturaController();
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            else
            {
                try
                {
                    var request = HttpContext.Current.Request;
                    string search = request.Params["search"].ToString();
                    return facturaController.GetCatalogoProductosSAT(search);
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
        }

        public List<ProductoSAT> GetCatalogoUnidadProductosSAT()
        {
            facturaController facturaController = new facturaController();
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            else
            {
                try
                {
                    var request = HttpContext.Current.Request;
                    string search = request.Params["search"].ToString();
                    return facturaController.GetCatalogoUnidadProductosSAT(search);
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
        }


        public ProductoSAT AddProductoFactura(long? id)
        {
            facturaController facturaController = new facturaController();
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            else
            {
                try
                {
                                  
                    return facturaController.AddProductoFactura(id);
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }


       public  ProductoSAT UpdateProductoFactura(long? id)
        {
            facturaController facturaController = new facturaController();
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            else
            {
                try
                {

                    return facturaController.UpdateProductoFactura(id);
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }
        }


      public  ProductoSAT AddClienteFactura(long? id)
        {
            facturaController facturaController = new facturaController();
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            else
            {
                try
                {

                    return facturaController.AddClienteFactura(id);
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }


        public ProductoSAT UpdateClienteFactura(long? id)
        {
            facturaController facturaController = new facturaController();
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            else
            {
                try
                {

                    return facturaController.UpdateClienteFactura(id);
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }

        //public bool? GetActivarFacturacion(int? op)
        //{
        //    facturaController facturaController = new facturaController();
        //    if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
        //    else
        //    {
        //        try
        //        {

        //            return facturaController.GetActivarFacturacion(op);
        //        }
        //        catch (Exception ex)
        //        {
        //            throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
        //        }
        //    }
        //}

        //public bool? GetFacturacionActiva()
        //{
        //    facturaController facturaController = new facturaController();
        //    if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
        //    else
        //    {
        //        try
        //        {

        //            return facturaController.GetFacturacionActiva();
        //        }
        //        catch (Exception ex)
        //        {
        //            throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
        //        }
        //    }
        //}

        #endregion



        #region Abonos
        abonoCtrl abonoCtrl = new abonoCtrl();
        public long? AddAbono(abonosEntity abono)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                
                return abonoCtrl.abono_nuevo(abono);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }


        public List<abonosEntity> GetAbonos(int? id, string fecha, int? sucursalId, string cliente, int? usuarioId)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {

                return abonoCtrl.abono_obtenLista(id, fecha,  sucursalId, cliente, usuarioId);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        public  int? DeleteAbono(int? id, int? usuarioId)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {

                return abonoCtrl.abono_cancelar(id,  usuarioId);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        public string GetReporteVentaCredito(string fechaInicio, string fechaFin, int? sucursalId, int? usuarioId, string status, bool? detalle, int? tipoDescarga)
        {

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {

                return reportes.GetReporteVentaCredito(fechaInicio, fechaFin, sucursalId, usuarioId, status, detalle, tipoDescarga);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }


        #endregion


        #region Vendedor

        
        public ResultEntity AddVendedor(vendedorEntity vendedor)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {

                return vendedorController.vendedor_nuevo(vendedor);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }


        public ResultEntity UpdateVendedor(vendedorEntity vendedor)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {

                return vendedorController.vendedor_editar(vendedor);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        public vendedorEntity GetVendedorById()
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                var request = HttpContext.Current.Request;
                long id = long.Parse(request["id"].ToString());
                return vendedorController.vendedor_obten(id);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }


        public List<vendedorEntity> GetVendedor(int ? op)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {                
                return vendedorController.vendedor_obtenLista();
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }


        #endregion


        public ResultEntity EnvioSMS(int? op, string number_to, string number_from, string message_body)
        {

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {

                return SistemaGeneralController.EnvioSMS(op,  number_to,  number_from,  message_body);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }

        }


        public string GetReporteArticulos(List<articuloEntity> articulos, int? op)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                
                return reportes.GetReporteArticulos(articulos,op);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }


        public string GetReporteClientes(int? vendedorId, int? op, int? op2, int? op3, string op4, string op5, int? tipoDescarga)
        {

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {

                return reportes.GetReporteClientes( vendedorId,  op,  op2,  op3,  op4,  op5,  tipoDescarga);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        public string GetReporteTicketPago(int? pagoId)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {

                return reportes.GetReporteTicketPago(pagoId);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }

        }

        public string ReporteArticuloDescuento(int? clasificacion, int? categoria, int? articulo, int? tipoDescarga)
        {

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {

                return reportes.ReporteArticuloDescuento(clasificacion, categoria, articulo, tipoDescarga);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }



    }
}
