﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AppWCFService.ServiceReference {
    using System.Runtime.Serialization;
    using System;
    
    

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="RespuestaCFDi", Namespace="http://schemas.datacontract.org/2004/07/FacturaService")]
    [System.SerializableAttribute()]
    public partial class RespuestaCFDi : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private byte[] DocumentoField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string MensajeField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public byte[] Documento {
            get {
                return this.DocumentoField;
            }
            set {
                if ((object.ReferenceEquals(this.DocumentoField, value) != true)) {
                    this.DocumentoField = value;
                    this.RaisePropertyChanged("Documento");
                
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Mensaje {
            get {
                return this.MensajeField;
            }
            set {
                if ((object.ReferenceEquals(this.MensajeField, value) != true)) {
                    this.MensajeField = value;
                    this.RaisePropertyChanged("Mensaje");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="ServiceReference.ITimbrado")]
    public interface ITimbrado {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ITimbrado/Timbrar", ReplyAction="http://tempuri.org/ITimbrado/TimbrarResponse")]
        ServiceReference.RespuestaCFDi Timbrar(string Usuario, string Password, byte[] ArchivoXML);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ITimbrado/Timbrar", ReplyAction="http://tempuri.org/ITimbrado/TimbrarResponse")]
        System.Threading.Tasks.Task<ServiceReference.RespuestaCFDi> TimbrarAsync(string Usuario, string Password, byte[] ArchivoXML);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ITimbrado/TimbrarTest", ReplyAction="http://tempuri.org/ITimbrado/TimbrarTestResponse")]
        ServiceReference.RespuestaCFDi TimbrarTest(string Usuario, string Password, byte[] ArchivoXML);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ITimbrado/TimbrarTest", ReplyAction="http://tempuri.org/ITimbrado/TimbrarTestResponse")]
        System.Threading.Tasks.Task<ServiceReference.RespuestaCFDi> TimbrarTestAsync(string Usuario, string Password, byte[] ArchivoXML);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ITimbrado/PDF", ReplyAction="http://tempuri.org/ITimbrado/PDFResponse")]
        ServiceReference.RespuestaCFDi PDF(string Usuario, string Password, byte[] ArchivoXML, byte[] ArchivoACK);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ITimbrado/PDF", ReplyAction="http://tempuri.org/ITimbrado/PDFResponse")]
        System.Threading.Tasks.Task<ServiceReference.RespuestaCFDi> PDFAsync(string Usuario, string Password, byte[] ArchivoXML, byte[] ArchivoACK);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ITimbrado/Cancelar", ReplyAction="http://tempuri.org/ITimbrado/CancelarResponse")]
       ServiceReference.RespuestaCFDi Cancelar(string Usuario, string Password, byte[] PFX, string[] UUID, string ContraseñaPFX);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ITimbrado/Cancelar", ReplyAction="http://tempuri.org/ITimbrado/CancelarResponse")]
        System.Threading.Tasks.Task<ServiceReference.RespuestaCFDi> CancelarAsync(string Usuario, string Password, byte[] PFX, string[] UUID, string ContraseñaPFX);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ITimbrado/CambiarContrasena", ReplyAction="http://tempuri.org/ITimbrado/CambiarContrasenaResponse")]
        ServiceReference.RespuestaCFDi CambiarContrasena(string Usuario, string Password, string NuevoPassword);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ITimbrado/CambiarContrasena", ReplyAction="http://tempuri.org/ITimbrado/CambiarContrasenaResponse")]
        System.Threading.Tasks.Task<ServiceReference.RespuestaCFDi> CambiarContrasenaAsync(string Usuario, string Password, string NuevoPassword);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ITimbrado/Login", ReplyAction="http://tempuri.org/ITimbrado/LoginResponse")]
        ServiceReference.RespuestaCFDi Login(string Usuario, string Password);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ITimbrado/Login", ReplyAction="http://tempuri.org/ITimbrado/LoginResponse")]
        System.Threading.Tasks.Task<ServiceReference.RespuestaCFDi> LoginAsync(string Usuario, string Password);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ITimbrado/PFX", ReplyAction="http://tempuri.org/ITimbrado/PFXResponse")]
        ServiceReference.RespuestaCFDi PFX(string Usuario, string PAssword, byte[] ArchivoCER, byte[] ArchivoKey, string ClavePrivada);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ITimbrado/PFX", ReplyAction="http://tempuri.org/ITimbrado/PFXResponse")]
        System.Threading.Tasks.Task<ServiceReference.RespuestaCFDi> PFXAsync(string Usuario, string PAssword, byte[] ArchivoCER, byte[] ArchivoKey, string ClavePrivada);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ITimbrado/CancelarAsincrono", ReplyAction="http://tempuri.org/ITimbrado/CancelarAsincronoResponse")]
        ServiceReference.RespuestaCFDi CancelarAsincrono(string Usuario, string Password, byte[] PFX, string UUID, string ContraseñaPFX, double Total, string RFCEmior, string RFCReceptor);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ITimbrado/CancelarAsincrono", ReplyAction="http://tempuri.org/ITimbrado/CancelarAsincronoResponse")]
        System.Threading.Tasks.Task<ServiceReference.RespuestaCFDi> CancelarAsincronoAsync(string Usuario, string Password, byte[] PFX, string UUID, string ContraseñaPFX, double Total, string RFCEmior, string RFCReceptor);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ITimbrado/VerStatus", ReplyAction="http://tempuri.org/ITimbrado/VerStatusResponse")]
        ServiceReference.RespuestaCFDi VerStatus(string Usuario, string Password, string UUID, double Total, string RFCEmisor, string RFCReceptor);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ITimbrado/VerStatus", ReplyAction="http://tempuri.org/ITimbrado/VerStatusResponse")]
        System.Threading.Tasks.Task<ServiceReference.RespuestaCFDi> VerStatusAsync(string Usuario, string Password, string UUID, double Total, string RFCEmisor, string RFCReceptor);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface ITimbradoChannel : ServiceReference.ITimbrado, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class TimbradoClient : System.ServiceModel.ClientBase<ServiceReference.ITimbrado>, ServiceReference.ITimbrado {
        
        public TimbradoClient() {
        }
        
        public TimbradoClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public TimbradoClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public TimbradoClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public TimbradoClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public ServiceReference.RespuestaCFDi Timbrar(string Usuario, string Password, byte[] ArchivoXML) {
            return base.Channel.Timbrar(Usuario, Password, ArchivoXML);
        }
        
        public System.Threading.Tasks.Task<ServiceReference.RespuestaCFDi> TimbrarAsync(string Usuario, string Password, byte[] ArchivoXML) {
            return base.Channel.TimbrarAsync(Usuario, Password, ArchivoXML);
        }
        
        public ServiceReference.RespuestaCFDi TimbrarTest(string Usuario, string Password, byte[] ArchivoXML) {
            return base.Channel.TimbrarTest(Usuario, Password, ArchivoXML);
        }
        
        public System.Threading.Tasks.Task<ServiceReference.RespuestaCFDi> TimbrarTestAsync(string Usuario, string Password, byte[] ArchivoXML) {
            return base.Channel.TimbrarTestAsync(Usuario, Password, ArchivoXML);
        }
        
        public ServiceReference.RespuestaCFDi PDF(string Usuario, string Password, byte[] ArchivoXML, byte[] ArchivoACK) {
            return base.Channel.PDF(Usuario, Password, ArchivoXML, ArchivoACK);
        }
        
        public System.Threading.Tasks.Task<ServiceReference.RespuestaCFDi> PDFAsync(string Usuario, string Password, byte[] ArchivoXML, byte[] ArchivoACK) {
            return base.Channel.PDFAsync(Usuario, Password, ArchivoXML, ArchivoACK);
        }
        
        public ServiceReference.RespuestaCFDi Cancelar(string Usuario, string Password, byte[] PFX, string[] UUID, string ContraseñaPFX) {
            return base.Channel.Cancelar(Usuario, Password, PFX, UUID, ContraseñaPFX);
        }
        
        public System.Threading.Tasks.Task<ServiceReference.RespuestaCFDi> CancelarAsync(string Usuario, string Password, byte[] PFX, string[] UUID, string ContraseñaPFX) {
            return base.Channel.CancelarAsync(Usuario, Password, PFX, UUID, ContraseñaPFX);
        }
        
        public ServiceReference.RespuestaCFDi CambiarContrasena(string Usuario, string Password, string NuevoPassword) {
            return base.Channel.CambiarContrasena(Usuario, Password, NuevoPassword);
        }
        
        public System.Threading.Tasks.Task<ServiceReference.RespuestaCFDi> CambiarContrasenaAsync(string Usuario, string Password, string NuevoPassword) {
            return base.Channel.CambiarContrasenaAsync(Usuario, Password, NuevoPassword);
        }
        
        public ServiceReference.RespuestaCFDi Login(string Usuario, string Password) {
            return base.Channel.Login(Usuario, Password);
        }
        
        public System.Threading.Tasks.Task<ServiceReference.RespuestaCFDi> LoginAsync(string Usuario, string Password) {
            return base.Channel.LoginAsync(Usuario, Password);
        }
        
        public ServiceReference.RespuestaCFDi PFX(string Usuario, string PAssword, byte[] ArchivoCER, byte[] ArchivoKey, string ClavePrivada) {
            return base.Channel.PFX(Usuario, PAssword, ArchivoCER, ArchivoKey, ClavePrivada);
        }
        
        public System.Threading.Tasks.Task<ServiceReference.RespuestaCFDi> PFXAsync(string Usuario, string PAssword, byte[] ArchivoCER, byte[] ArchivoKey, string ClavePrivada) {
            return base.Channel.PFXAsync(Usuario, PAssword, ArchivoCER, ArchivoKey, ClavePrivada);
        }
        
        public ServiceReference.RespuestaCFDi CancelarAsincrono(string Usuario, string Password, byte[] PFX, string UUID, string ContraseñaPFX, double Total, string RFCEmior, string RFCReceptor) {
            return base.Channel.CancelarAsincrono(Usuario, Password, PFX, UUID, ContraseñaPFX, Total, RFCEmior, RFCReceptor);
        }
        
        public System.Threading.Tasks.Task<ServiceReference.RespuestaCFDi> CancelarAsincronoAsync(string Usuario, string Password, byte[] PFX, string UUID, string ContraseñaPFX, double Total, string RFCEmior, string RFCReceptor) {
            return base.Channel.CancelarAsincronoAsync(Usuario, Password, PFX, UUID, ContraseñaPFX, Total, RFCEmior, RFCReceptor);
        }
        
        public ServiceReference.RespuestaCFDi VerStatus(string Usuario, string Password, string UUID, double Total, string RFCEmisor, string RFCReceptor) {
            return base.Channel.VerStatus(Usuario, Password, UUID, Total, RFCEmisor, RFCReceptor);
        }
        
        public System.Threading.Tasks.Task<ServiceReference.RespuestaCFDi> VerStatusAsync(string Usuario, string Password, string UUID, double Total, string RFCEmisor, string RFCReceptor) {
            return base.Channel.VerStatusAsync(Usuario, Password, UUID, Total, RFCEmisor, RFCReceptor);
        }
    }
}
