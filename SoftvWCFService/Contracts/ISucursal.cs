﻿using AppWCFService.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Web;

namespace AppWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface ISucursal
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddSucursal", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        long? AddSucursal(sucursalEntity sucursal);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSucursal", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<sucursalEntity> GetSucursal();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSucursalById", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        sucursalEntity GetSucursalById();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateSucursal", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        long? UpdateSucursal(sucursalEntity sucursal);


    }
}