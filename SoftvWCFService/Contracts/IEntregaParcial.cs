﻿using AppWCFService.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Web;

namespace AppWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IEntregaParcial
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddEntregaParcial", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ResultEntity AddEntregaParcial(EntregaParcialEntity entrega);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetEntregaParcialById", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        EntregaParcialEntity GetEntregaParcialById();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetEntregaParcial", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<EntregaParcialEntity> GetEntregaParcial();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateEntregaParcial", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ResultEntity UpdateEntregaParcial(EntregaParcialEntity entrega);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteEntregaParcial", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ResultEntity DeleteEntregaParcial(long? id, int? usuarioId);


    }
}