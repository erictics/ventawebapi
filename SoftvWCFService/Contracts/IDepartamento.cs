﻿using AppWCFService.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Web;

namespace AppWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IDepartamento
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddDepartamento", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
         long? AddDepartamento(departamentoEntity departamento);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDepartamento", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<departamentoEntity> GetDepartamento();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDepartamentoById", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        departamentoEntity GetDepartamentoById();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateDepartamento", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
         long? UpdateDepartamento(departamentoEntity departamento);


    }
}