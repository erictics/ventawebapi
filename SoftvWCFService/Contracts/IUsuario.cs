﻿
using AppWCFService.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace AppWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IUsuario
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetAutentificaUsuario", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<usuarioEntity> GetAutentificaUsuario(long? op, string email, string password);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetUsuario", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<usuarioEntity> GetUsuario();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetUsuarioById", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        usuarioEntity GetUsuarioById();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddUsuario", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ResultEntity AddUsuario(usuarioEntity usuario, List<sucursalEntity> sucursales);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateUsuario", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ResultEntity UpdateUsuario(usuarioEntity usuario, List<sucursalEntity> sucursales);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSucursalUsuario", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<sucursalEntity> GetSucursalUsuario();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetUsuarioAutorizacionVenta", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        bool? GetUsuarioAutorizacionVenta(string email, string password);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetModulos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<ModuloEntity> GetModulos();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetGuardaPermisos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetGuardaPermisos(List<ModuloEntity> modulos, int? tipoUsuario);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetObtenModulosUsuario", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<ModuloEntity> GetObtenModulosUsuario(int? tipoUsuario, int? op);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetTiposUsuario", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<tipoUsuarioEntity> GetTiposUsuario();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddTiposUsuario", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddTiposUsuario(tipoUsuarioEntity obj);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateTiposUsuario", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateTiposUsuario(tipoUsuarioEntity obj);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetTipoUsuarioById", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        tipoUsuarioEntity GetTipoUsuarioById();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetPermisoAccesoUsuario", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ModuloEntity GetPermisoAccesoUsuario(int? tipoUsuario, string componente);

    }
}

