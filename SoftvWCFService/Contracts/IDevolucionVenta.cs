﻿using AppWCFService.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Web;

namespace AppWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IDevolucionVenta
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDevolucionVenta", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<devolucionVentaEntity> GetDevolucionVenta(long? folio, string fechaInicio, string fechaFin, long? sucursalId, long? cajeroId, long? usuarioId, long? ventaId);

    }

}