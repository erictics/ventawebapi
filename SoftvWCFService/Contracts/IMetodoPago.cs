﻿using AppWCFService.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Web;

namespace AppWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IMetodoPago
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMetodoPago", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<metodoPagoEntity> GetMetodoPago();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMetodoPagoById", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        metodoPagoEntity GetMetodoPagoById();
    }
}