﻿using AppWCFService.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Web;

namespace AppWCFService.Contracts
{

    [AuthenticatingHeader]
    [ServiceContract]
    public interface ICaracteristica
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetCaracteristica", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<caracteristicaEntity> GetCaracteristica();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddCaracteristica", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ResultEntity AddCaracteristica(caracteristicaEntity caracteristica);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateCaracteristica", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ResultEntity UpdateCaracteristica(caracteristicaEntity caracteristica);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetCaracteristicaById", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        caracteristicaEntity GetCaracteristicaById();

    }
}