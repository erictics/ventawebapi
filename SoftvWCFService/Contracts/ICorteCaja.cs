﻿using AppWCFService.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Web;

namespace AppWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface ICorteCaja
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddCorteCaja", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        long? AddCorteCaja(corteCajaEntity corte);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetCorteCaja", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<corteCajaEntity> GetCorteCaja(long? cajaId, long? sucursalId, string fecha);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetCorteCajaById", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        corteCajaEntity GetCorteCajaById();

        //[OperationContract]
        //[WebInvoke(Method = "*", UriTemplate = "UpdateCaja", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        //long? UpdateCaja(cajaEntity caja);


    }
}