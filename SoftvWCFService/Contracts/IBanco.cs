﻿using AppWCFService.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Web;

namespace AppWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IBanco
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddBanco", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        long? AddBanco(BancoEntity banco);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetBanco", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<BancoEntity> GetBanco();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetBancoById", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        BancoEntity GetBancoById();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateBanco", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        long? UpdateBanco(BancoEntity banco);


    }
}