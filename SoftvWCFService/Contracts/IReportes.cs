﻿using AppWCFService.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Web;

namespace AppWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IReportes
    {
    
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteVentasByCajero", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetReporteVentasByCajero(string fechaInicio, string fechaFin, int? usuario, int? sucursalId, int? departamentoId, int? categoriaId, int? articuloId, int? orden, int? tipoDescarga, bool? detalle);
        

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteSalidaEfectivo", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetReporteSalidaEfectivo(string fechaInicio, string fechaFin, int? sucursalId, int? usuarioId, int? op,int ? tipoDescarga);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteInventario", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
         string GetReporteInventario(long? sucursalId, int? departamentoId, int? categoriaId, int? estatus, int? tipoDescarga);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteInventarioVendedor", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetReporteInventarioVendedor(long? vendedorId, int? departamentoId, int? categoriaId, int? estatus, int? tipoDescarga);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteMovimientosArticulos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetReporteMovimientosArticulos(int? articuloId, int? departamentoId, int? categoriaId, int? sucursalId, string fechaInicio, string fechaFin, string fechaInicio2, string fechaFin2);
        
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteEntregaParcial", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetReporteEntregaParcial(long? id);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteListadoEntregaParcial", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetReporteListadoEntregaParcial(int? usuarioId, string fechaInicio, string fechaFin, string rangos, int? tipoDescarga);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteArqueo", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetReporteArqueo(string fecha, int? usuarioId);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteMovimientosArticuloResumen", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetReporteMovimientosArticuloResumen(int? articuloId, int? departamentoId, int? categoriaId, int? sucursalId, string fechaInicio, string fechaFin, int ? tipoDescarga);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetCortes", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetCortes(int? tipo, String fechaInicio, String fechaFin, int? sucursalId, int? usuarioId, int? tipoDescarga, int? usuario);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteCierreVenta", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetReporteCierreVenta(int? id);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteDetalleTarea", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetReporteDetalleTarea(long? Id);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteTickets", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetReporteTickets(int? sucursalId, int? usuarioId, string fechaInicio, string fechaFin, bool? esCancelacion);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteVentasResumenSucursal", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetReporteVentasResumenSucursal(string fechaInicio, string fechaFin, int? sucursalId, int? tipoDescarga, bool? detalle);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteVentasResumenUsuario", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetReporteVentasResumenUsuario(string fechaInicio, string fechaFin, int? usuarioId, int? tipoDescarga, bool? detalle);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteVentasResumenDepartamento", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetReporteVentasResumenDepartamento(string fechaInicio, string fechaFin, int? sucursalId, int? departamentoId, int? tipoDescarga, bool? detalle);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteVentasResumenCategoria", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetReporteVentasResumenCategoria(string fechaInicio, string fechaFin, int? sucursalId, int? categoriaId, int? tipoDescarga, bool? detalle);



        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteVentasResumenArticulo", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetReporteVentasResumenArticulo(string fechaInicio, string fechaFin, int? sucursalId, int? articuloId, int? tipoDescarga, bool? detalle);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteResurtido", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetReporteResurtido(long? sucursalId, int? departamentoId, int? categoriaId, int? articuloId, int? tipoDescarga);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteRecepcion", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetReporteRecepcion(long? sucursalId, int? departamentoId, int? categoriaId, int? articuloId, string fechaInicio, string fechaFin, int? proveedorId, int? tipoDescarga);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteFacturas", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetReporteFacturas(string fechaInicio, string fechaFin, int? sucursalId, int? usuarioId, string status, int? tipoDescarga);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteVentaCredito", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
         string GetReporteVentaCredito(string fechaInicio, string fechaFin, int? sucursalId, int? usuarioId, string status, bool? detalle, int? tipoDescarga);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteArticulos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetReporteArticulos(List<articuloEntity> articulos, int? op);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteTraspaso", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetReporteTraspaso(int? vendedorId, int? sucursalId, int? tipo, string fechaInicio, string fechaFin, int? tipoDescarga);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteCortesVendor", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetReporteCortesVendor(int? vendedorId, int? sucursalId, int? tipoventa, string fechaInicio, string fechaFin, int? status, int? tipoDescarga);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteClientes", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetReporteClientes(int? vendedorId, int? op, int? op2, int? op3, string op4, string op5, int? tipoDescarga);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetReporteTicketPago", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetReporteTicketPago(int? pagoId);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "ReporteArticuloDescuento", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string ReporteArticuloDescuento(int? clasificacion, int? categoria, int? articulo, int? tipoDescarga);
    }
}