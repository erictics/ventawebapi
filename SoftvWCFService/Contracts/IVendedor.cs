﻿using AppWCFService.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Web;

namespace AppWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface  IVendedor
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetVendedor", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<vendedorEntity> GetVendedor(int? op);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetVendedorById", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        vendedorEntity GetVendedorById();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddVendedor", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ResultEntity AddVendedor(vendedorEntity vendedor);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateVendedor", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ResultEntity UpdateVendedor(vendedorEntity vendedor);




    }
}