﻿using AppWCFService.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Web;

namespace AppWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface ICaja
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddCaja", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        long? AddCaja(cajaEntity caja);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetCaja", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<cajaEntity> GetCaja();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetCajaById", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        cajaEntity GetCajaById();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateCaja", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        long? UpdateCaja(cajaEntity caja);


    }
}