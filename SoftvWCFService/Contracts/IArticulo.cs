﻿using AppWCFService.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Web;

namespace AppWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IArticulo
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetArticulo", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<articuloEntity> GetArticulo();


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetArticuloInventario", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<articuloEntity> GetArticuloInventario();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetArticuloById", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        articuloEntity GetArticuloById();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddArticulo", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        long? AddArticulo(articuloEntity articulo);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateArticulo", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        long? UpdateArticulo(articuloEntity articulo);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetArticuloUPC", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        articuloEntity GetArticuloUPC();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetBusquedaArticuloVenta", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<articuloEntity> GetBusquedaArticuloVenta(int? op, string buscar);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetInventarioArticulo", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        decimal? GetInventarioArticulo(int? sucursalId, int? articuloId, int? estatusId);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddCaracteristicaArticulo", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ResultEntity AddCaracteristicaArticulo(long? articuloId, long? caracteristicaId, string valor);

         [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteCaracteristicaArticulo", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ResultEntity DeleteCaracteristicaArticulo(long? id);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetCaracteristicaArticulo", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<caracteristicaEntity> GetCaracteristicaArticulo(long? articuloId);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetexisteUPC", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        bool? GetexisteUPC();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GenerateRandomCode", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GenerateRandomCode();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddArticuloPrecioCredito", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ResultEntity AddArticuloPrecioCredito(long? articuloId, long? mes, decimal? precio);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetArticuloPrecioCredito", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<articuloPrecioCredito> GetArticuloPrecioCredito(long? articuloId);

       

    }
}