﻿using AppWCFService.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Web;

namespace AppWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface ISalidaEfectivo
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSalidaEfectivo", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<salidaEfectivoEntity> GetSalidaEfectivo(long? sucursalId,long? cajeroId, long? folio, string fecha, int ?  userId2);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSalidaEfectivoById", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        salidaEfectivoEntity GetSalidaEfectivoById();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddSalidaEfectivo", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ResultEntity AddSalidaEfectivo(salidaEfectivoEntity salidaEfectivo);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateSalidaEfectivo", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ResultEntity UpdateSalidaEfectivo(salidaEfectivoEntity salidaEfectivo);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSalidaEfectivoCancela", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ResultEntity GetSalidaEfectivoCancela(int? id, int? usuarioId);
        

    }
}