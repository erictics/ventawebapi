﻿using AppWCFService.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Web;

namespace AppWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface ItipoSalidaEfectivo
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GettipoSalidaEfectivo", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<tipoSalidaEfectivoEntity> GettipoSalidaEfectivo();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddtipoSalidaEfectivo", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        long? AddtipoSalidaEfectivo(tipoSalidaEfectivoEntity tipoSalidaEfectivo);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdatetipoSalidaEfectivo", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        long? UpdatetipoSalidaEfectivo(tipoSalidaEfectivoEntity tipoSalidaEfectivo);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GettipoSalidaEfectivoById", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        tipoSalidaEfectivoEntity GettipoSalidaEfectivoById();

      

    }
}