﻿using AppWCFService.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Web;

namespace AppWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IFormaPago
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetFormaPago", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<formaPagoEntity> GetFormaPago();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetFormaPagoById", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        formaPagoEntity GetFormaPagoById();
    }
}