﻿using AppWCFService.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Web;

namespace AppWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IEstatus
    {   

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "GetEstatus", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<estatusEntity> GetEstatus();

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "GetEstatusById", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        estatusEntity GetEstatusById(long ? id);

     


    }
}