﻿using AppWCFService.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Web;

namespace AppWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface ISistemaGeneral
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetSistemaGeneral", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        sistemaGeneralEntity GetSistemaGeneral();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateSistemaGeneral", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateSistemaGeneral(sistemaGeneralEntity sistema);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetCorreo", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<correosEntity> GetCorreo();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteCorreo", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeleteCorreo(long? sucursalId);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddCorreo", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddCorreo(correosEntity correo);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "EnviarTicket", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ResultEntity EnviarTicket(long? ventaId, string email_enviar);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "EnvioSMS", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ResultEntity EnvioSMS(int? op, string number_to, string number_from, string message_body);
    }
}