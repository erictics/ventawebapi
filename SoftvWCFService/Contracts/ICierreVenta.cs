﻿using AppWCFService.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Web;

namespace AppWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface ICierreVenta
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetCierreVentaById", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
         cierreVentaEntity GetCierreVentaById();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetCierreVenta", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<cierreVentaEntity> GetCierreVenta(string fecha, long? folio, int? usuarioId,int ? sucursalId ,int ? userId2 );

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddCierreVenta", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ResultEntity AddCierreVenta(cierreVentaEntity entrega);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateCierreVenta", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        long? UpdateCierreVenta(cierreVentaEntity entrega);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteCierreVenta", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ResultEntity DeleteCierreVenta(long? id, long? usuarioId);


       


    }
}