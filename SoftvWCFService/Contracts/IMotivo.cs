﻿using AppWCFService.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Web;

namespace AppWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IMotivo
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMotivo", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<motivoEntity> GetMotivo();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetMotivoById", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        motivoEntity GetMotivoById();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddMotivo", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
         long ?  AddMotivo(motivoEntity motivo);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateMotivo", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        long? UpdateMotivo(motivoEntity motivo);



    }
}