﻿using AppWCFService.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Web;

namespace AppWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface ICliente
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetCliente", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<clienteEntity> GetCliente(string rfc, int? op,string nombre);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetClienteById", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        clienteEntity GetClienteById();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddCliente", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        long? AddCliente(clienteEntity cliente);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateCliente", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        long? UpdateCliente(clienteEntity cliente);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "cliente_filtro", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<clienteEntity> cliente_filtro(clienteEntity cliente);

    }
}