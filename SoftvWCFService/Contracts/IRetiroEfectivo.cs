﻿using AppWCFService.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Web;

namespace AppWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IRetiroEfectivo
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRetiroEfectivo", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<retiroEfectivoEntity> GetRetiroEfectivo(long? cajeroId, long? sucursalId, string fechaInicio, string fechaFin);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetRetiroEfectivoById", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        retiroEfectivoEntity GetRetiroEfectivoById();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddRetiroEfectivo", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        long? AddRetiroEfectivo(retiroEfectivoEntity RetiroEfectivo);




    }
}