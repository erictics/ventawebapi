﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Web;
using static AppWCFService.Entity.ImagenEntity;

namespace AppWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IArchivo
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "saveFileTarea", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        string saveFileTarea();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetFileTarea", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<logo> GetFileTarea(int? tareaId);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteImagenesTarea", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string DeleteImagenesTarea(int? id);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "saveFileArticulo", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        string saveFileArticulo();


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "saveLogoApp", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        string saveLogoApp();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetFileArticulo", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<logo> GetFileArticulo(int? articuloId);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteFilesArticulo", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string DeleteFilesArticulo(int? id);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDescargaCodigoBarras", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetDescargaCodigoBarras(string upc, int? op);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDescargaCodigoBarrasDescripcion", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetDescargaCodigoBarrasDescripcion(int? id);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "saveFileCliente", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        string saveFileCliente();



        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteFilesCliente", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string DeleteFilesCliente(int? id);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetFileCliente", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<logo> GetFileCliente(int? clienteId);

    }
}