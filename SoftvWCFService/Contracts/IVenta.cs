﻿using AppWCFService.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Web;

namespace AppWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IVenta
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetIniciaVenta", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ventaEntity GetIniciaVenta(int? sucursalId, int? cajeroId,string modulo);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AgregaItemVenta", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        long? AgregaItemVenta(long? ventaId, long? articuloId);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AgregaItemVentaCredito", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        long? AgregaItemVentaCredito(long? ventaId, long? articuloId, int? meses, decimal? precio);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AgregaItemVentaLista", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        long? AgregaItemVentaLista(List<ventaDetalleEntity> articulos, int? op);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteItemVenta", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeleteItemVenta(long? ventaId, long? articuloId,int ? op);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetVentaTerminar", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        long? GetVentaTerminar(ventaEntity venta);



        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetItemsVenta", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<articuloEntity> GetItemsVenta(long? ventaId);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetCancelaVenta", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        long? GetCancelaVenta(long? ventaId, long? usuarioId,long ? motivoId);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetVentaTotal", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        decimal ? GetVentaTotal(long ? ventaId);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetVentaTicket", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetVentaTicket(long? ventaId, long? usuarioId, bool? reimpresion, int? motivoId);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetVenta", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<ventaEntity> GetVenta(long? cajeroId, long? sucursalId, string fechaInicio, string fechaFin, string referencia, long? IdVenta, string serie, int? usuarioId);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetVentaByReferencia", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ventaEntity GetVentaByReferencia();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetAutorizaCancelacionVenta", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        bool ? GetAutorizaCancelacionVenta(int? op, string email, string password);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetVentacreditoLista", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<VentaCreditoEntity> GetVentacreditoLista(int? clienteId, string fecha, string status, int? sucursalId);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddAbono", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        long? AddAbono(abonosEntity abono);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetAbonos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<abonosEntity> GetAbonos(int? id, string fecha, int? sucursalId, string cliente, int? usuarioId);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DeleteAbono", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? DeleteAbono(int? id, int? usuarioId);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddDescuentoVenta", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
         int? AddDescuentoVenta(long? ventaId, long? monto, long? usuarioId, int? porcentaje, string observaciones);



        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetValidaInventarioVendedor", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        bool? GetValidaInventarioVendedor(int? vendedorId, int? articuloId, int? tempventaId, decimal? cantidad);
    }
}