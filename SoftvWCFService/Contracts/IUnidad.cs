﻿using AppWCFService.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Web;

namespace AppWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IUnidad
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetUnidad", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<unidadEntity> GetUnidad();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetUnidadById", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        unidadEntity GetUnidadById();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddUnidad", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        long? AddUnidad(unidadEntity unidad);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateUnidad", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        long? UpdateUnidad(unidadEntity unidad);


    }
}