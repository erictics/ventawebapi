﻿using AppWCFService.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Web;

namespace AppWCFService.Contracts
{
    
        [AuthenticatingHeader]
        [ServiceContract]
        public interface ITarea
        {

            


            [OperationContract]
            [WebInvoke(Method = "*", UriTemplate = "GetTarea", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
            List<tareaEntity> GetTarea(long? id, string fechaInicio, string fechaFin, int ? tipo, long? sucursalId, string status, int? usuarioId);

            [OperationContract]
            [WebInvoke(Method = "*", UriTemplate = "GetTareaById", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
            tareaEntity GetTareaById();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddRecepcionTarea", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
         long? AddRecepcionTarea(tareaEntity tarea, List<tareaDetalleEntity> detalle);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateRecepcionTarea", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        long? UpdateRecepcionTarea(tareaEntity tarea, List<tareaDetalleEntity> detalle);

            [OperationContract]
            [WebInvoke(Method = "*", UriTemplate = "GetCancelaRecepcion", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
            ResultEntity GetCancelaRecepcion(int? id, int? usuarioId);

            [OperationContract]
            [WebInvoke(Method = "*", UriTemplate = "GetTareaDetalle", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
            List<tareaDetalleEntity> GetTareaDetalle();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetCancelaCambioEstatus", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ResultEntity GetCancelaEstatus(int? id, int? usuarioId);

        //[OperationContract]
        //[WebInvoke(Method = "*", UriTemplate = "AddTareaDetalle", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        //int? AddTareaDetalle(List<tareaDetalleEntity> detalle, long? tareaId);

        //[OperationContract]
        //[WebInvoke(Method = "*", UriTemplate = "UpdateTareaDetalle", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        //int? UpdateTareaDetalle(List<tareaDetalleEntity> detalle, long? tareaId);


        //[OperationContract]
        //[WebInvoke(Method = "*", UriTemplate = "DeleteTareaDetalleConcepto", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        //int? DeleteTareaDetalleConcepto(long? detalleId);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetCambioEstatusNuevo", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        long? GetCambioEstatusNuevo(tareaEntity tarea, List<tareaDetalleEntity> detalle, int? estatus);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddTraspasoVendedor", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        long? AddTraspasoVendedor(tareaEntity tarea, List<tareaDetalleEntity> detalle);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddTraspasoSucursal", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        long? AddTraspasoSucursal(tareaEntity tarea, List<tareaDetalleEntity> detalle);
    }
    
}