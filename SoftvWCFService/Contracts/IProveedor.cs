﻿using System;
using AppWCFService.Entity;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Web;

namespace AppWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IProveedor
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetProveedor", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<proveedorEntity> GetProveedor();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetProveedorById", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        proveedorEntity GetProveedorById();


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddProveedor", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        long? AddProveedor(proveedorEntity proveedor);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateProveedor", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        long? UpdateProveedor(proveedorEntity proveedor);

    }
}