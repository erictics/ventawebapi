﻿using AppWCFService.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Web;

namespace AppWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface ICategoria
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetCategoria", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<categoriaEntity> GetCategoria();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetCategoriaById", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        categoriaEntity GetCategoriaById();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddCategoria", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        long? AddCategoria(categoriaEntity categoria);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateCategoria", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        long? UpdateCategoria(categoriaEntity categoria);


    }
}