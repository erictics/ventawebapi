﻿using AppWCFService.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Web;

namespace AppWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IFactura
    {

        //[OperationContract]
        //[WebInvoke(Method = "*", UriTemplate = "GetUnidadSAT", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        //List<SATEntity> GetUnidadSAT();

        //[OperationContract]
        //[WebInvoke(Method = "*", UriTemplate = "GetProdSerSAT", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        //List<SATEntity> GetProdSerSAT();


        //[OperationContract]
        //[WebInvoke(Method = "*", UriTemplate = "GeneraFactura", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        //string GeneraFactura();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetCatalogoProductosSAT", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<ProductoSAT> GetCatalogoProductosSAT();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetCatalogoUnidadProductosSAT", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<ProductoSAT> GetCatalogoUnidadProductosSAT();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddProductoFactura", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ProductoSAT AddProductoFactura(long? id);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateProductoFactura", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ProductoSAT UpdateProductoFactura(long? id);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddClienteFactura", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ProductoSAT AddClienteFactura(long? id);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateClienteFactura", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ProductoSAT UpdateClienteFactura(long? id);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddFactura", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ProductoSAT AddFactura(long? id,long ?  usuarioId);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetFacturas", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<facturaEntity> GetFacturas(string rfc, string fecha, string serie, string folio, string nombre, int? op, int? usuarioId, int? sucursalId);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDescargaFactura", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ProductoSAT GetDescargaFactura(int? op, string id);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetEnviaFactura", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ProductoSAT GetEnviaFactura(long? clienteId, string facturaId);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetCancelaFactura", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ProductoSAT GetCancelaFactura(string FacturaId, long? usuarioId);


        //[OperationContract]
        //[WebInvoke(Method = "*", UriTemplate = "GetFacturacionActiva", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        //bool? GetFacturacionActiva();

        //[OperationContract]
        //[WebInvoke(Method = "*", UriTemplate = "GetActivarFacturacion", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        //bool? GetActivarFacturacion(int? op);
    }
}