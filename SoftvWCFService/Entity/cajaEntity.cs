﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace AppWCFService.Entity
{
    [DataContract]
    [Serializable]
    public class cajaEntity
    {
        [DataMember]
        public int id { get; set; }

        [DataMember]
        public string ipMaquina { get; set; }
        [DataMember]
        public string nombre { get; set; }
        [DataMember]
        public DateTime fechaRegistro { get; set; }
        [DataMember]
        public bool activo { get; set; }
        [DataMember]
        public int sucursalId { get; set; }
        [DataMember]
        public DateTime fechaBaja { get; set; }
        [DataMember]
        public sucursalEntity sucursal { get; set; }

    }
}