﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace AppWCFService.Entity
{
    [DataContract]
    [Serializable]
    public class articuloUbicacionEntity
    {
        [DataMember]
        public long id { get; set; }
        [DataMember]
        public articuloEntity articulo { get; set; }
        [DataMember]
        public ubicacionEntity ubicacion { get; set; }
        [DataMember]
        public long articuloId { get; set; }
        [DataMember]
        public long ubicacionId { get; set; }
        
    }
}