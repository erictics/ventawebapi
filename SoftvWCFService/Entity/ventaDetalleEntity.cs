﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace AppWCFService.Entity
{
    [DataContract]
    [Serializable]
    public class ventaDetalleEntity
    {
        [DataMember]
        public long ventaId { get; set; }
        [DataMember]
        public long articuloId { get; set; }
        [DataMember]
        public decimal precio { get; set; }
        [DataMember]
        public int cantidad { get; set; }
        [DataMember]
        public int unidadId { get; set; }
        [DataMember]
        public articuloEntity articulo { get; set; }
    }
}