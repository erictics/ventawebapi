﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace AppWCFService.Entity
{


    [DataContract]
    [Serializable]
    public class articuloEntity
    {
        [DataMember]
        public long id { get; set; }
        [DataMember]
        public string nombre { get; set; }
        [DataMember]
        public decimal precioLista { get; set; }
        //[DataMember]
        //public decimal precioCredito { get; set; }
        [DataMember]
        public int categoriaId { get; set; }
        [DataMember]
        public bool activo { get; set; }
        [DataMember]
        public string descripcion { get; set; }
        //[DataMember]
        //public bool credito { get; set; }
        [DataMember]
        public string upc { get; set; }
        [DataMember]
        public int unidadId { get; set; }
        //[DataMember]
        //public decimal precioMayoreo { get; set; }
        [DataMember]
        public int minInventario { get; set; }
        //[DataMember]
        //public bool mayoreo { get; set; }
        [DataMember]
        public bool credito { get; set; }
        [DataMember]
        public int stockSeguro { get; set; }
        //[DataMember]
        //public DateTime? fechaRegistro { get; set; }
        //[DataMember]
        //public DateTime? fechaBaja { get; set; }
        //[DataMember]
        //public int cantidadMayoreo { get; set; }
        [DataMember]
        public int cantidad { get; set; }
        [DataMember]
        public categoriaEntity categoria { get; set; }
        [DataMember]
        public unidadEntity unidad { get; set; }
        [DataMember]
        public List<articuloCaracteristicaEntity> caracteristicas { get; set; }

        [DataMember]
        public string precio{ get; set; }

        [DataMember]
        public string total { get; set; }

        [DataMember]
        public decimal totalVenta { get; set; }

        [DataMember]
        public string sat_clvProdServ { get; set; }

        [DataMember]
        public string identificador { get; set; }

        [DataMember]
        public string imagenPrincipal { get; set; }
    }
}