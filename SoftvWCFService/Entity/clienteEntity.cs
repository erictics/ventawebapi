﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWCFService.Entity
{
    public class clienteEntity
    {
        public int id { get; set; }

        public string nombre { get; set; }

        public string apellidos { get; set; }

        public string rfc { get; set; }

        public long cp { get; set; }

        public string colonia { get; set; }

        public string calle { get; set; }

        public string numero { get; set; }

        public string ciudad { get; set; }

        public string estado { get; set; }

        public string email { get; set; }

        public string razonSocial { get; set; }

        public string telefono { get; set; }

        public string usoCFDI { get; set; }

        public string identificador { get; set; }

       public string numeroInt { get; set; }

        public int vendedorId { get; set; }

        public string entrecalles { get; set; }
        public string referencia { get; set; }
        public string latitud { get; set; }
        public string longitud { get; set; }

    }
}