﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Web;

namespace AppWCFService.Entity
{
    [DataContract]
    [Serializable]
    public class tipoUsuarioEntity
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Nombre { get; set; }
        [DataMember]
        public bool Activo { get; set; }
        [DataMember]
        public bool CancelaVenta { get; set; }
        [DataMember]
        public bool ReimprimeTicket { get; set; }

    }
}