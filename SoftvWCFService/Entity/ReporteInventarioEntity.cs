﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWCFService.Entity
{
    public class ReporteInventarioEntity
    {
        public int departamentoId { get; set; }
        public string departamento { get; set; }
        public int categoriaId { get; set; }
        public string categoria { get; set; }
        public int articuloId { get; set; }
        public string articulo { get; set; }
        public int cantidad { get; set; }
        public string unidad { get; set; }
        public decimal precioLista { get; set; }
        public decimal total { get; set; }
        public string upc { get; set; } 
      }

    public class ReporteRecepcion
    {
        public int numeroproceso { get; set; }
        public DateTime fecha { get; set; }
        public string proveedor { get; set; }
        public string categoria { get; set; }
        public string departamento { get; set; }
        public string upc { get; set; }
        public string articulo { get; set; }
        public int cantidad { get; set; }
        public string unidad { get; set; }
       public decimal precio { get; set;  }
        public decimal total { get; set; }
      
    }


}