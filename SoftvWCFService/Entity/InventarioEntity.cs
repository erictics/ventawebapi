﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWCFService.Entity
{
    public class inventarioEntity
    {
        public int id { get; set; }

        public long articuloId { get; set; }

        public long almacenId { get; set; }

        public int cantidad { get; set; }

        public long estatusId { get; set; }
    }
}