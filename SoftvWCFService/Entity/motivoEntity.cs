﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWCFService.Entity
{
    public class motivoEntity
    {
        public int id { get; set; }

        public string nombre { get; set; }

        public string tipo { get; set; }

        public bool activo { get; set; }
    }
}