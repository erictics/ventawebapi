﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWCFService.Entity
{
    public class categoriaEntity
    {
        public long id { get; set; }

        public string nombre { get; set; }

        public string descripcion { get; set; }

        public bool activo { get; set; }

        public int departamentoId { get; set; }

        public departamentoEntity departamento { get; set; }
    }
}