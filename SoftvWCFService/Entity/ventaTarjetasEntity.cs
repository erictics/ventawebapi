﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace AppWCFService.Entity
{
    [DataContract]
    [Serializable]
    public class ventaTarjetasEntity
    {
        [DataMember]
        public long ventaId { get; set; }
        [DataMember]
        public int bancoId { get; set; }
        [DataMember]
        public string referencia { get; set; }
        [DataMember]
        public string serie { get; set; }
        [DataMember]
        public decimal monto { get; set; }
        [DataMember]
        public string tipoTarjeta { get; set; }
        [DataMember]
        public int id { get; set; }
        [DataMember]
        public ventaEntity venta { get; set; }
        [DataMember]
        public BancoEntity  banco { get; set; }

    }
}