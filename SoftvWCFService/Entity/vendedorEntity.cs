﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWCFService.Entity
{
    public class vendedorEntity
    {
        public int  id { get; set; }
        public string nombre { get; set; }
        public string domicilio { get; set; }
        public DateTime fechaCaptura { get; set; }
        public bool activo { get; set; }
    }
}