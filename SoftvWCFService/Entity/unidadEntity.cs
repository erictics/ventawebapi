﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWCFService.Entity
{
    public class unidadEntity
    {
        public long id { get; set; }

        public string nombre { get; set; }

        public bool activo { get; set; }

        public string sat_clvUnidad { get; set; }
    }
}