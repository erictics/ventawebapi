﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWCFService.Entity
{
    public class ResultEntity
    {
        public int? id { get; set; }

        public int ? ErrorCode { get; set; }

        public string Message { get; set; }

    }
}