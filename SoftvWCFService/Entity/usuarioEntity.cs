﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace AppWCFService.Entity
{
    [DataContract]
    [Serializable]
    public class usuarioEntity
    {
        [DataMember]
        public long id { get; set; }
        [DataMember]
        public string login { get; set; }
        [DataMember]
        public string nombre { get; set; }
        [DataMember]
        public string email { get; set; }
        [DataMember]
        public bool activo { get; set; }
        [DataMember]
        public DateTime? fechaRegistro { get; set; }
        [DataMember]
        public int tipoUsuario { get; set; }   
        [DataMember]
        public string password { get; set; }
        [DataMember]
        public string token { get; set; }

        [DataMember]
        public tipoUsuarioEntity RoltipoUsuario { get; set; }
        
    }


    [DataContract]
    [Serializable]
    public class usuarioSucursalEntity
    {
        [DataMember]
        public long id { get; set; }

        [DataMember]
        public long usuarioId { get; set; }

        [DataMember]
        public long sucursalId { get; set; }

    }
}