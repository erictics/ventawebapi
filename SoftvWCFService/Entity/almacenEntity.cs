﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace AppWCFService.Entity
{
    [DataContract]
    [Serializable]
    public class almacenEntity    {
        [DataMember]
        public string nombre { get; set; }
        [DataMember]
        public bool activo { get; set; }
        [DataMember]
        public string calle { get; set; }
        [DataMember]
        public string colonia { get; set; }
        [DataMember]
        public int? numero { get; set; }
        [DataMember]
        public string ciudad { get; set; }
        [DataMember]
        public bool principal { get; set; }
        [DataMember]
        public int almacenRelId { get; set; }
        [DataMember]
        public long id { get; set; }
        
    }
}