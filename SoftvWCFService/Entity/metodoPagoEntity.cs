﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace AppWCFService.Entity
{
    [DataContract]
    [Serializable]
    public class metodoPagoEntity
    {
        [DataMember]
        public int id { get; set; }
        [DataMember]
        public string nombre { get; set; }
    }
}