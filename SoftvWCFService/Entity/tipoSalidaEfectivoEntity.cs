﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace AppWCFService.Entity
{
    [DataContract]
    [Serializable]
    public class tipoSalidaEfectivoEntity
    {
        [DataMember]
        public long id { get; set; }

        [DataMember]
        public string descripcion { get; set; }

        [DataMember]
        public bool activo { get; set; }

        [DataMember]
        public bool requiereAutorizacion { get; set; }
    }
}