﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWCFService.Entity
{
    public class sistemaGeneralEntity
    {
        public string nombreSistema { get; set; }

        public string  empresa { get; set; }
 
        public string logoSistema { get; set; }

        public string logoReportes { get; set; }

        public int usuariosSistema { get; set; }

        public int sucursalesSistema { get; set; }

        public string licencia { get; set; }

        public string emailSistema { get; set; }

        public bool facturacion { get; set; }

        public bool envioticket { get; set; }

        public bool ventaCredito { get; set; }

        public string ticket { get; set; }

    }

    public class correosEntity
    {
        public int sucursalId { get; set; }

        public string email { get; set; }

        public string password { get; set; }

        public string smtp { get; set; }

        public string puerto { get; set; }

    }
}