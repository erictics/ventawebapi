﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWCFService.Entity
{
    public class procesoEntity
    {
        public long id { get; set; }      

        public int estatusId { get; set; }

        public long usuarioId { get; set; }

        public long almacenId { get; set; }

        public long proveedorId { get; set; }

        public DateTime fecha { get; set; }

        public DateTime? fechaCancelacion { get; set; }

        public string observaciones { get; set; }

        public long procesoRelId { get; set; }

        public string tipo { get; set; }

        public long motivoId { get; set; }

        public string fecha2 { get; set; }

        public decimal total { get; set; }

        public estatusEntity estatus { get; set; }

        public motivoEntity motivo { get; set; }

        public usuarioEntity usuario { get; set; }

        public almacenEntity almacen { get; set; }

        public proveedorEntity proveedor { get; set; }   
        
        public List<procesoDetalleEntity> conceptos { get; set; }    

    }

}