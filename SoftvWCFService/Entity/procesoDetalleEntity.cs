﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWCFService.Entity
{
    public class procesoDetalleEntity
    {
        public long id { get; set; }

        public long procesoId { get; set; }

        public long articuloId { get; set; }

        public int cantidad { get; set; }

        public decimal precio { get; set; }

        public long unidadId { get; set; }

        public int opcion { get; set;}

        public articuloEntity articulo { get; set; }

        public unidadEntity unidad { get; set; }
    }
}