﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWCFService.Entity
{
    
        public class tareaEntity
        {
            public long id { get; set; }

            public DateTime fecha { get; set; }

            public string fechaFormato { get; set; }

            public int tipoTarea { get; set; }

            public long usuarioId { get; set; }

            public string observaciones { get; set; }

            public string status { get; set; }

            public long sucursalId { get; set; }

            public DateTime? fechaCancelacion { get; set; }

            public string fechaCancelacionFormato { get; set; }

            public long? usuarioIdCancelacion { get; set; }

           // public string fechaFormatter { get; set; }

            public int proveedorId { get; set; }

        public int vendedorId { get; set; }

        public sucursalEntity sucursal { get; set; }

        public proveedorEntity proveedor { get; set; }

        public usuarioEntity  usuario { get; set; }

        public vendedorEntity vendedor { get; set; }

        public usuarioEntity usuarioCancelo { get; set; }


    }
    
}