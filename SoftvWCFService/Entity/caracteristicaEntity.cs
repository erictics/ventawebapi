﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWCFService.Entity
{
    public class caracteristicaEntity
    {
        public int id { get; set; }

        public string nombre { get; set; }

        public bool activo { get; set; }

        public long articuloId { get; set; }

        public articuloEntity articulo { get; set; }

        public string valor { get; set; }

    }

    public class articuloPrecioCredito
    {
      public int   articuloId { get; set; }

        public int mes { get; set; }

        public decimal  precio { get; set; }

        public string descripcion { get; set; }
    }
}