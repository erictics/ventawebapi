﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace AppWCFService.Entity
{
    public class SATEntity
    {
        public string clave_sat { get; set; }

        public string descripcion { get; set; }
    }


    public class ProductoSAT
    {
        [DataMember]
        public string key { get; set; }
        [DataMember]
        public string description { get; set; }
    }
}