﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWCFService.Entity
{
    public class servicioEntity
    {

        public int id { get; set; }

        public string nombre { get; set; }

        public bool activo { get; set; }

        public decimal precio { get; set; }

        public string clave { get; set; }

        public DateTime? fechaRegistro { get; set; }

        public string descripcion { get; set; }

        public DateTime? fechaBaja { get; set; }

        public List<servicioArticuloEntity> articulos { get; set; }
    }
}