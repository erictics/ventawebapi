﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace AppWCFService.Entity
{
    [DataContract]
    [Serializable]
    public class EntregaParcialEntity
    {
        [DataMember]
        public long id { get; set; }
        [DataMember]
        public long usuarioId { get; set; }       
        [DataMember]
        public long? autorizadorId { get; set; }
        [DataMember]
        public DateTime  fecha { get; set; }
        [DataMember]
        public usuarioEntity usuario { get; set; }
        [DataMember]
        public string fecha2 { get; set; }
        [DataMember]
        public string referencia { get; set; }
        [DataMember]
        public decimal cantidad { get; set; }
        [DataMember]
        public string observaciones { get; set; }
        [DataMember]
        public bool? autorizado { get; set; }

        [DataMember]
        public int b1000 { get; set; }

        [DataMember]
        public int b500 { get; set; }

        [DataMember]
        public int b200 { get; set; }

        [DataMember]
        public int b100 { get; set; }

        [DataMember]
        public int b50 { get; set; }

        [DataMember]
        public int b20 { get; set; }

        [DataMember]
        public int m100 { get; set; }

        [DataMember]
        public int m20 { get; set; }

        [DataMember]
        public int m10 { get; set; }

        [DataMember]
        public int m5 { get; set; }

        [DataMember]
        public int m2 { get; set; }

        [DataMember]
        public int m1 { get; set; }

        [DataMember]
        public int m050 { get; set; }

        [DataMember]
        public string estatus { get; set; }

        [DataMember]
        public string cantidadC { get; set; }
        [DataMember]
        public bool  Editable { get; set; }



    }
}