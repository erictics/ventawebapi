﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace AppWCFService.Entity
{
    [DataContract]
    [Serializable]
    public class ubicacionEntity
    {
        [DataMember]
        public int id { get; set; }
        [DataMember]
        public string clave { get; set; }
        [DataMember]
        public string nombre { get; set; }
        [DataMember]
        public long almacenId { get; set; }
        [DataMember]
        public bool activo { get; set; }
        [DataMember]
        public almacenEntity almacen { get; set; }

        
    }
}