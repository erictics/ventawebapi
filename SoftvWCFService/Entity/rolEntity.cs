﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWCFService.Entity
{
    public class rolEntity
    {
        public long id { get; set; }

        public string nombre { get; set; }

        public string descripcion { get; set; }

        public bool activo { get; set; }

        public string clave { get; set; }

    }
}