﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWCFService.Entity
{
    public class corteCajaEntity
    {
        public long id { get; set; }

        public DateTime fecha { get; set; }

        public int usuarioId { get; set; }

        public decimal efectivo { get; set; }

        public decimal tarjeta { get; set; }

        public decimal vales { get; set; }

        public int cajaId { get; set; }

        public decimal retiros { get; set; }

        public decimal fondoFijo { get; set; }

        public decimal montoCalculado { get; set; }

        public cajaEntity caja { get; set; }

        public usuarioEntity usuario { get; set; }

        public string fechaCorte { get; set; }

        public string horaCorte { get; set; }

    }

}