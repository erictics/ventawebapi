﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace AppWCFService.Entity
{
    [DataContract]
    [Serializable]
    public class ventaEntity
    {
        [DataMember]
        public long id { get; set; }
        [DataMember]
        public DateTime fecha { get; set; }
        [DataMember]
        public long clienteId { get; set; }
        [DataMember]
        public long cajeroId { get; set; }
        [DataMember]
        public String fomaPagoId { get; set; }
        [DataMember]
        public string estatus { get; set; }
        [DataMember]
        public string tipoVenta { get; set; }
        [DataMember]
        public decimal subtotal { get; set; }
        [DataMember]
        public decimal total { get; set; }
        [DataMember]
        public string totalC { get; set; }
        [DataMember]
        public decimal descuento { get; set; }
        [DataMember]
        public decimal cambio { get; set; }
        [DataMember]
        public decimal pagoEfectivo { get; set; }
        [DataMember]
        public decimal pagoTarjeta { get; set; }
        [DataMember]
        public string referencia { get; set; }
        [DataMember]
        public string ReferenciaTrans { get; set; }        
        [DataMember]
        public decimal iva { get; set; }
        [DataMember]
        public string tipoTarjeta { get; set; }
        [DataMember]
        public DateTime? fechaCancelacion { get; set; }
        [DataMember]
        public String metodoPagoId { get; set; }
        [DataMember]
        public int cajaId { get; set; }
        [DataMember]
        public int bancoId { get; set; }
        [DataMember]
        public int consecutivo { get; set; }
        [DataMember]
        public string tarjetaReferencia { get; set; }
        [DataMember]
        public string error { get; set; }
        [DataMember]
        public string serie { get; set; }
        [DataMember]
        public string mensaje { get; set; }
        [DataMember]
        public string fecha2 { get; set; }

        [DataMember]
        public long sucursalId { get; set; }
        [DataMember]
        public sucursalEntity sucursal { get; set; }

        [DataMember]
        public usuarioEntity usuarioCajero { get; set; }

        [DataMember]
        public clienteEntity cliente { get; set; }

        [DataMember]
        public decimal pagoTransferencia { get; set; }

        [DataMember]
        public decimal comision { get; set; }

        [DataMember]
        public int bancoTrans { get; set; }
        [DataMember]
       public long AutorizacionTrans { get; set; }


        [DataMember]
        public long tarjetaAutorizacion { get; set; }


        [DataMember]
        public long vendedorId { get; set; }

        [DataMember]
        public string facturaId { get; set; }


    }


       public class VentaCreditoEntity
       {
        public long ventaId { get; set; }
        public long clienteId { get; set; }
        public long sucursalId { get; set; }
        public long usuarioId { get; set; }
        public  string estatus  { get; set; }
        public decimal totalVenta  { get; set; }
        public decimal totalAbonado  { get; set; }
        public decimal totalRestante  { get; set; }
        public decimal descuento { get; set; }
        public string Fecha  { get; set; }
        public  string sucursal  { get; set; }
        public string usuario  { get; set; }
        public string cliente  { get; set; }
        public string folio { get; set; }
        public bool Pago  { get; set; }
        public int idPago { get; set; }
        public string observaciones { get; set; }

    }


    public class abonosEntity
    {
        public int id { get; set; }

        public long ventaId { get; set; }

        public long clienteId { get; set; }

        public decimal  monto { get; set; }

        public string  fecha { get; set; }

        public long usuarioId { get; set; }

        public string status { get; set; }

        public DateTime? fechaCancelacion { get; set; }

        public long? usuarioCancelo { get; set; }

        public string observaciones { get; set; }

    }

}