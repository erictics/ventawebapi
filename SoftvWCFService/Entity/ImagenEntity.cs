﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace AppWCFService.Entity
{
    public class ImagenEntity
    {
        public class Archivo
        {
            public Stream filestream { get; set; }

            public int? Tipo { get; set; }

        }

        public class logo
        {
            public int id { get; set; }
            public string nombre { get; set; }
            public int tareaId { get; set; }
            public string archivo { get; set; }
            public int tipoImagen { get; set; }
            public int tipo { get; set; }

        }

    }
}