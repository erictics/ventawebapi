﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWCFService.Entity
{
    public class comboEntity
    {
        public long id { get; set; }

        public string nombre { get; set; }

        public string descripcion { get; set; }

        public bool activo { get; set; }

        public DateTime? fechaRegistro { get; set; }

        public DateTime? FechaBaja { get; set; }

        public decimal precio { get; set; }
    }
}