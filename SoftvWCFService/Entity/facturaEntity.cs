﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWCFService.Entity
{
    public class facturaEntity
    {
        public long id { get; set; }

        public string identificador { get; set; }

        public string UUID { get; set; }

        public string clienteId { get; set; }

        public long usuarioId { get; set; }

        public long ventaId { get; set; }

        public string status { get; set; }

        public decimal total { get; set; }

        public string totalFormato { get; set; }

        public string serie { get; set; }

        public string folio { get; set; }

        public string razonSocial { get; set; }

        public string rfc { get; set; }

        public DateTime fecha { get; set; }

        public string fechaFormato { get; set; }

        

        public clienteEntity cliente { get; set; }

        public usuarioEntity usuario { get; set; }

        public ventaEntity venta { get; set; }

    }
}