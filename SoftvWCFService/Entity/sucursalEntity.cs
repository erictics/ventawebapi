﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace AppWCFService.Entity
{
    [DataContract]
    [Serializable]
    public class sucursalEntity
    {
        [DataMember]
        public int id { get; set; }
        [DataMember]
        public string nombre { get; set; }
        [DataMember]
        public string calle { get; set; }
        [DataMember]
        public int numero { get; set; }
        [DataMember]
        public string colonia { get; set; }
        [DataMember]
        public long cp { get; set; }
        [DataMember]
        public string ciudad { get; set; }
        [DataMember]
        public string estado { get; set; }
        [DataMember]
        public string telefono { get; set; }
        [DataMember]
        public string email { get; set; }
        [DataMember]
        public string serie { get; set; }
        [DataMember]
        public int  op { get; set; }
        [DataMember]
        public string rfc { get; set; }
        [DataMember]
        public string razonSocial { get; set; }
        [DataMember]
        public string regimenFiscal { get; set; }
        [DataMember]
        public string cer { get; set; }
        [DataMember]
        public string key { get; set; }
        [DataMember]
        public string keyPassword { get; set; }
    }
}