﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWCFService.Entity
{
    public class devolucionVentadetalleEntity
    {
        public long id { get; set; }

        public long devolucionId { get; set; }

        public long articuloId { get; set; }

        public long cantidad { get; set; }

        public decimal precio { get; set; }
    }
}