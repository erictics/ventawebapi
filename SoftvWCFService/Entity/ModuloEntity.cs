﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace AppWCFService.Entity
{
    [DataContract]
    [Serializable]
    public class ModuloEntity
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string nombre { get; set; }
        [DataMember]
        public string componente { get; set; }
        [DataMember]
        public bool activo { get; set; }

        [DataMember]
        public string Icono { get; set; }
        [DataMember]
        public bool Principal { get; set; }
        [DataMember]
        public int? ModuloPadre { get; set; }
        [DataMember]
        public List<ModuloEntity> SubModulos { get; set; }

        [DataMember]
        public bool Consultar { get; set; }

        [DataMember]
        public bool Editar { get; set; }

        [DataMember]
        public bool Crear { get; set; }

        [DataMember]
        public bool Eliminar { get; set; }


    }
}