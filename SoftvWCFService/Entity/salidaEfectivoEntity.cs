﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWCFService.Entity
{
    public class salidaEfectivoEntity
    {
        public long id { get; set; }

        public int tipoSalidaId { get; set; }

        public int sucursalId { get; set; }

        public string observaciones { get; set; }

        public DateTime fecha { get; set; }        

        public int cajeroId { get; set; }

        public bool activo { get; set; }

        public decimal cantidad { get; set; }

        public string cantidadC { get; set; }

        public int usuarioAutorizaId { get; set; }         

        public usuarioEntity usuario { get; set; }

        public tipoSalidaEfectivoEntity tipoSalidaEfectivo { get; set; }

        public string fechaCorta { get; set; }

        public string hora { get; set; }

        public string estatus { get; set; }

        
    }

}