﻿using AppWCFService.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace AppWCFService.Entity
{
    [DataContract]
    [Serializable]
    public class ventaTransferenciaEntity
    {

        [DataMember]
        public long id { get; set; }

        [DataMember]
        public long ventaId { get; set; }
        [DataMember]
        public string serie { get; set; }

        [DataMember]
        public long bancoId { get; set; }

        [DataMember]
        public string referencia { get; set; }

        [DataMember]
        public decimal monto { get; set; }

        [DataMember]
        public decimal autorizacion { get; set; }

        [DataMember]
        public BancoEntity banco { get; set; }

        [DataMember]
        public ventaEntity venta { get; set; }




    }
}