﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWCFService.Entity
{
    public class tareaDetalleEntity
    {
        public long id { get; set; }

        public long tareaId { get; set; }

        public long articuloId { get; set; }

        public int cantidad { get; set; }

        public decimal precio { get; set; }

        public int tipoMonedaId { get; set; }

        public string upc { get; set; }

        public string descripcion { get; set; }

        public string nombre { get; set; }

        public unidadEntity unidad { get; set; }

        
    }
}