﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWCFService.Entity
{
    public class devolucionVentaEntity
    {
        public int id { get; set; }

        public DateTime fecha { get; set; }

        public string estatus { get; set; }

        public long sucursalId { get; set; }

        public long usuarioId { get; set; }

        public decimal monto { get; set; }

        public long ventaId { get; set; }

        public string observaciones { get; set; }

        public sucursalEntity sucursal { get; set; }

        public usuarioEntity usuario { get; set; }

        public string fechaFormato { get; set; }
    }
}