﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace AppWCFService.Entity
{
    [DataContract]
    [Serializable]
    public class retiroEfectivoEntity
    {
        [DataMember]
        public long id { get; set; }
        [DataMember]
        public DateTime fecha { get; set; }
        [DataMember]
        public int usuarioAutorizador { get; set; }
        [DataMember]
        public int cajeroId { get; set; }
        [DataMember]
        public decimal cantidad { get; set; }
        [DataMember]
        public int cajaId { get; set; }
        [DataMember]
        public usuarioEntity cajero { get; set; }
        [DataMember]
        public usuarioEntity usuario { get; set; }
        [DataMember]
        public string  fechaCorta { get; set; }
        [DataMember]
        public string hora { get; set; }
    }
}