﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWCFService.Entity
{
    public class servicioArticuloEntity
    {
        public long id { get; set; }

        public long articuloId { get; set; }

        public long servicioId { get; set; }

        public int cantidad { get; set; }

        public articuloEntity articulo { get; set; }

        public servicioEntity servicio { get; set; }

    }
}