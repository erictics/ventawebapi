﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace AppWCFService.Entity
{
    [DataContract]
    [Serializable]
    public class articuloCaracteristicaEntity
    {
        [DataMember]
        public long id { get; set; }
        [DataMember]
        public long articuloId { get; set; }
        [DataMember]
        public long caracteristicaId { get; set; }
        [DataMember]
        public string nombre { get; set; }
        [DataMember]
        public string valor { get; set; }
        [DataMember]
        public int opcion { get; set; }
    }
}