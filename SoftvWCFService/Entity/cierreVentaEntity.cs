﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppWCFService.Entity
{
    public class cierreVentaEntity
    {
        public int id { get; set; }

        public int b1000 { get; set; }

        public int b500 { get; set; }

        public int b200 { get; set; }

        public int b100 { get; set; }

        public int b50 { get; set; }

        public int b20 { get; set; }

        public int m20 { get; set; }

        public int m10 { get; set; }

        public int m5 { get; set; }

        public int m2 { get; set; }

        public int m1 { get; set; }

        public int m050 { get; set; }

        public decimal montoTarjetaCredito { get; set; }

        public decimal montoTarjetaDebito { get; set; }

        public decimal montoTarjetaVales { get; set; }

        public decimal montoTransferencia { get; set; }

        public decimal montosPagoProveedor { get; set; }

        public decimal montoEntregaParcial { get; set; }
        public string monto { get; set; }

        public decimal montoGastos { get; set; }

        public decimal totalEfectivo { get; set; }

        public decimal totalTarjetas { get; set; }

        public decimal totalEntregaParcial { get; set; }

        public decimal totalSalidaEfectivo { get; set; }

        public decimal totalEntregar { get; set; }

        public int usuarioId { get; set; }

        public int sucursalId { get; set; }

        public DateTime fecha { get; set; }

        public DateTime? fechaCancelacion { get; set; }

        public int? usuarioCapturaId { get; set; }

        public usuarioEntity usuario { get; set; }

        public string fechaCorta { get; set; }

        public string estatus { get; set; }
    }
}